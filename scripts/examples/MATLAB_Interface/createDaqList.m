%Copyright (c) 2020 [HAN University of Applied Sciences]

%Permission is hereby granted, free of charge, to any person obtaining a copy
%of this software and associated documentation files (the "Software"), to deal
%in the Software without restriction, including without limitation the rights
%to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
%copies of the Software, and to permit persons to whom the Software is
%furnished to do so, subject to the following conditions:

%The above copyright notice and this permission notice shall be included in
%all copies or substantial portions of the Software.

%THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
%BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
%IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
%WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
%OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

function [daqItemNames, daqList] = createDaqList(modelName, filter)
    eval(modelName + "([], [], [], 'compile')");
    blocks = find_system(modelName, 'FindAll', 'on', 'FollowLinks', 'on', 'Virtual', 'off', 'type', 'block');
    daqList = [];
    daqItemNames = [];
    daqItemIndex = 1;
    for blockHandle = blocks.'
        block = get(blockHandle);
        for portHandle = block.PortHandles.Outport
            port = get(portHandle);
            portSize = getPortSize(port);
            for i = 1 : portSize

                signalName = '';
                if(strcmp(filter, 'signals') && ~strcmp(port.UserSpecifiedLogName, ''))
                    signalName = port.UserSpecifiedLogName;
                elseif (strcmp(filter, 'blocks'))
                    signalName = getFullBlockName(modelName, blockHandle);
                end

                if (block.BlockType == "SubSystem")
                    signalName = signalName + ":" + getInternalPortName(block, port);
                end

                if (portSize > 1)
                    signalName = signalName + ":" + i;
                end

                if (~strcmp(signalName, ''))
                    daqItemNames{daqItemIndex} = signalName;
                    daqList(daqItemIndex,:) = [blockHandle, port.PortNumber, i];
                    daqItemIndex = daqItemIndex + 1;
                end
            end
        end
    end
    eval(modelName + "([], [], [], 'term')");
end

function name = getInternalPortName(block, port)
    outportHandle = find_system(block.Path + "/" + block.Name, 'FindAll', 'on', 'SearchDepth', 1, 'BlockType', 'Outport', 'Port', int2str(port.PortNumber));
    outport = get(outportHandle);
    name = outport.Name;
end

function name = getFullBlockName(modelName, block)
    blockName = getfullname(block);
    name = erase(blockName, modelName + "/");
end

function portSize = getPortSize(port)
    portSize = 0;
    if (~isempty(port.CompiledPortDimensions))
        dimensions = port.CompiledPortDimensions;
        index = 1;

        while index < length(dimensions)
            valueSize = 1;
            for j = 1 : dimensions(index)
                index = index + 1;
                valueSize = valueSize * dimensions(index);
            end
            portSize = portSize + valueSize;
            index = index + 1;
        end
    end
end