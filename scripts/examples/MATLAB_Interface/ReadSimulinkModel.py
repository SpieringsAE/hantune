#Copyright (c) 2020 [HAN University of Applied Sciences]

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
#BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
#IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
#OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#For more information see: http://openmbd.com/wiki/HANtune/MATLAB_Interface

#Change these to the name and location of your model
modelName = 'MyModel'
modelPath = 'C:\Users\Michiel Klifman\Desktop'

#Add folders to MATLAB path
from java.lang import System
scriptPath = System.getProperty("user.dir") + "\scripts\examples\MATLAB_Interface"
engine.eval("addpath('" + scriptPath + "')")
engine.eval("addpath('" + modelPath + "')")

#Create a daqlist from signals in the model
result = engine.feval(2, 'createDaqList', modelName, 'signals')
daqItemNames = result[0]
daqList = result[1]
daqSize = len(daqList)

#Create a signal for each item in the daq list
signals = []
removeAllSignals()
for name in daqItemNames:
	signal = createSignal(name)
	signals.append(signal)
updateLayout()
print 'DAQ list created with ' + str(daqSize) + ' items'

#Start the simulation
engine.eval("set_param('" + modelName +"','SimulationCommand','start');")
print 'Simulation started...'

#Read data from the model in Simulink and add it to the signals in HANtune
while (True):
	data = engine.feval('getDaqListData', modelName, daqList, daqSize)
	if not data:
		break

	i = 0
	for value in data:
		signals[i].setValueAndTime(value, 0)
		i += 1

print 'Simulation completed!'