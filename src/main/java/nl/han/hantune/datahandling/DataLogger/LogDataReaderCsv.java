/*
    Copyright (c) 2020 [HAN University of Applied Sciences]

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
    OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    */
package nl.han.hantune.datahandling.DataLogger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ErrorLogger.AppendToLogfile;

public class LogDataReaderCsv implements LogDataReader<CsvLogInfo, Double, LogDataReaderCsv.ReaderResult> {
    // When parsing HANtune CSV files, they can contain ',' or '.' as decimal separator, therefore
    // replace by '.' to by sure.
    // It is assumed that HANtune CSV files do NOT contain a thousands separator
    static final char DECIMAL_SEPARATOR_COMMA = ',';
    static final char DECIMAL_SEPARATOR_DOT = '.';

    private static final int MAX_HEADER_LINES = 30; // don't look beyond this number of lines for header.


    public class ReaderResult {
        double timeStamp;
        List<ResultItem> resultItems;

        ReaderResult(Double time, List<ResultItem> items) {
            timeStamp = time;
            resultItems = items;
        }

        public double getTimeStampSecs() {
            return timeStamp;
        }

        public List<ResultItem> getResultItems() {
            return resultItems;
        }
    }

    public class ResultItem {
        String protocolName;
        String itemName;
        double itemValue;

        ResultItem(String prot, String name, double val) {
            protocolName = prot;
            itemName = name;
            itemValue = val;
        }

        public String getProtocolName() {
            return protocolName;
        }

        public String getItemName() {
            return itemName;
        }

        public double getItemValue() {
            return itemValue;
        }
    }

    private static final LogDataReaderCsv instance = new LogDataReaderCsv();

    private LogDataReaderCsv() {
    }

    public static LogDataReaderCsv getInstance() {
        return instance;
    }


    private LogReadWriteDataLines rwDataLines;
    private String errString = "";


    public boolean initRead(File file) {
        try {
            rwDataLines = new LogReadWriteDataLines();
            rwDataLines.init(file, false);

            rwDataLines.setLogInfo(readHeader());

            LogTimes logTimes = new LogTimes();
            if (!logTimes.readTimes(rwDataLines)) {
                return false;
            }
            rwDataLines.setLogTimes(logTimes);
        } catch (IOException | NumberFormatException ex) {
            handleException(ex);
            return false;
        }
        return true;
    }


    public boolean initReadWhileWrite() {
        // When in read-while-write mode, use the SAME instance of LogReadWriteDataLines object
        rwDataLines = LogDataWriterCsv.getInstance().getReadWriteDataLines();
        return rwDataLines != null;
    }

    public File getFile() {
        return rwDataLines.getFile();
    }

    /**
     * Returns reader error message, if available.
     *
     * @return error message
     */
    public String getError() {
        return errString;
    }


    public boolean isReading() {
        return rwDataLines != null;
    }

    public double getCurrentLogTimeSec() {
        return rwDataLines.getLogTimes().getCurrentTimeValue();
    }

    public double getMinimumLogTimeSec() {
        return rwDataLines.getLogTimes().getMinTimeValue();
    }


    public double getMaximumLogTimeSec() {
        return rwDataLines.getLogTimes().getMaxTimeValue();
    }

    public int getCurrentDataLineNum() {
        return rwDataLines.getLogTimes().getCurrentTimePosIdx();
    }


    public int getTotalDataLines() {
        return rwDataLines.getLogTimes().getTimeposSize();
    }

    public double getDelayToAdjacentStep(boolean direction) {
        return rwDataLines.getLogTimes().getDurationCurrentToAdjacentElement(direction);
    }

    private CsvLogInfo readHeader() throws IOException {
        CsvLogInfo logHdrInfo = new CsvLogInfo();
        String line;
        int lineCount = 0;
        rwDataLines.seekStartPos();
        int protocolCount = 0;
        int signalCount = 0;

        while ((line = rwDataLines.readLine()) != null && lineCount < MAX_HEADER_LINES) {
            lineCount++;

            if (parseHeaderDescriptionLine(logHdrInfo, line)) {
                continue;
            }

            if ((protocolCount == 0) && ((protocolCount = parseProtocols(line, logHdrInfo)) > 0)) {
                continue;
            }
            if (signalCount == 0 && ((signalCount = parseSignalNames(line, logHdrInfo)) > 0) &&
                logHdrInfo.isHeaderComplete()) {
                if (protocolCount != signalCount) {
                    String err =
                        String.format("Number of protocolnames and signalnames in header don't match: %d vs %d",
                            protocolCount, signalCount);
                    throw new IOException(err);
                }

                return logHdrInfo;
            }
        }
        throw new IOException("Error parsing file header. Header incomplete at line " + lineCount);
    }


    private boolean parseHeaderDescriptionLine(CsvLogInfo logHdrInfo, String line) {
        if (line.length() <= 1) { // skip almost empty lines
            return true;
        }

        if (logHdrInfo.getProjectName().isEmpty() && parseProjectName(line, logHdrInfo)) {
            return true;
        }

        if (logHdrInfo.getEcuId().isEmpty() && parseEcuId(line, logHdrInfo)) {
            return true;
        }

        if (parseDbcId(line, logHdrInfo)) {
            return true;
        }

        if (parseDaqListId(line, logHdrInfo)) {
            return true;
        }

        return logHdrInfo.getStartDate().isEmpty() && parseDate(line, logHdrInfo);
    }


    private boolean parseProjectName(String line, CsvLogInfo logHdr) {
        Pattern p = Pattern.compile("project: (.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        if (m.find()) {
            logHdr.setProjectName(m.group(1));
            return true;
        }
        return false;
    }

    private boolean parseEcuId(String line, CsvLogInfo logHdr) {
        Pattern p = Pattern.compile("ecu: (.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        if (m.find()) {
            logHdr.setEcuId(m.group(1));
            return true;
        }
        return false;
    }

    private boolean parseDaqListId(String line, CsvLogInfo logHdr) {
        Pattern p = Pattern.compile("DAQ List D\\s*(.*):\\s*(.*),\\s*(.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        if (m.find() && (m.groupCount() == 3)) {
            logHdr.addDaqListInfo(m.group(1), m.group(2), m.group(3));
            return true;
        }
        return false;
    }

    private boolean parseDbcId(String line, CsvLogInfo logHdr) {
        Pattern p = Pattern.compile("^Active DBC:\\s*(.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        if (m.find() && (m.groupCount() == 1)) {
            logHdr.addDbcInfo(m.group(1));
            return true;
        }
        return false;
    }


    private boolean parseDate(String line, CsvLogInfo logHdr) {
        Pattern p = Pattern.compile("^Date:\\s*(.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        if (m.find() && (m.groupCount() == 1)) {
            logHdr.setStartDate(m.group(1));
            return true;
        }
        return false;
    }


    private int parseProtocols(String line, CsvLogInfo logHdr) {
        Pattern p = Pattern.compile("^(;)\\s*(.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        if (m.find() && (m.groupCount() == 2)) {
            String[] names = m.group(2).split(";");
            logHdr.addProtocolNames(names);
            return names.length;
        }
        return 0;
    }


    private int parseSignalNames(String line, CsvLogInfo logHdr) {
        // why the ':*' in pattern? Some old logfiles still have a leftover ':' behind 'Time'
        Pattern p = Pattern.compile("^(Time:*;)\\s*(.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        if (m.find() && (m.groupCount() == 2)) {
            String[] names = m.group(2).split(";");
            logHdr.addSignalNames(names);
            return names.length;
        }
        return 0;
    }


    @Override
    public CsvLogInfo getLogHeaderInfo() {
        return rwDataLines == null ? null : (CsvLogInfo) rwDataLines.getLogInfo();
    }


    @Override
    public ReaderResult getFirstElement() throws IOException {
        if (rwDataLines == null) {
            return null;
        }

        try {
            return convertResultLine(rwDataLines.readLineAtPos(rwDataLines.getLogTimes().getFirstFilePosition()));
        } catch (IOException ex) {
            handleException(ex);
            throw ex;
        }
    }


    @Override
    public ReaderResult getLastElement() throws IOException {
        if (rwDataLines == null) {
            return null;
        }

        try {
            return convertResultLine(rwDataLines.readLineAtPos(rwDataLines.getLogTimes().getLastFilePosition()));
        } catch (IOException ex) {
            handleException(ex);
            throw ex;
        }
    }

    @Override
    public ReaderResult getNextElement() throws IOException {
        if (rwDataLines == null) {
            return null;
        }
        synchronized (rwDataLines) {
            long pos = rwDataLines.getLogTimes().getNextFilePosition();
            if (pos == LogTimes.OUT_OF_RANGE) {
                return null;
            } else {
                try {
                    return convertResultLine(rwDataLines.readLineAtPos(pos));

                } catch (IOException ex) {
                    handleException(ex);
                    throw ex;
                }
            }
        }
    }


    @Override
    public List<ReaderResult> getNextElements(int count) throws IOException {
        if (rwDataLines == null) {
            return Collections.emptyList();
        }
        long pos = rwDataLines.getLogTimes().getCurrentFilePosition();
        if (pos == LogTimes.OUT_OF_RANGE) {
            return Collections.emptyList();
        } else {
            try {
                return convertResultLines(rwDataLines.readLinesAtPos(pos, count));
            } catch (IOException ex) {
                handleException(ex);
                throw ex;
            }
        }
    }


    @Override
    public ReaderResult getPrevElement() throws IOException {
        if (rwDataLines == null) {
            return null;
        }


        long pos = rwDataLines.getLogTimes().getPrevFilePosition();
        if (pos == LogTimes.OUT_OF_RANGE) {
            return null;
        } else {
            try {
                return convertResultLine(rwDataLines.readLineAtPos(pos));
            } catch (IOException ex) {
                handleException(ex);
                throw ex;
            }
        }
    }


    @Override
    public ReaderResult getRefElement(Double ref) throws IOException {
        if (rwDataLines == null) {
            return null;
        }

        try {
            return convertResultLine(rwDataLines.readLineAtPos(rwDataLines.getLogTimes().
                getFilePositionAtGivenTime(ref)));
        } catch (IOException ex) {
            handleException(ex);
            throw new IOException(ex);
        }
    }


    /**
     * Set current reading location at refTime with given offset and direction
     *
     * @param refTime        holds number of sec as a decimal value: xxx.yyy
     * @param offset         number of csv-lines from refTime
     * @param isBeyondRefPos true: offset beyond refTime, false: before refTime
     * @return number of csv-lines available from refTime (mostly same as offset), or OUT_OF_RANGE on error
     */
    public int gotoRefElementWithOffset(double refTime, int offset, boolean isBeyondRefPos) {
        if (rwDataLines == null) {
            return LogTimes.OUT_OF_RANGE;
        }

        return rwDataLines.getLogTimes().moveFilePositionToGivenTimeAndOffset(refTime, isBeyondRefPos ? offset : -offset);
    }


    /**
     * Set current reading location at given relative value with given offset and direction
     *
     * @param relVal         Decimal value between 0.0 and 1.0 representing beginning - end
     * @param offset         number of csv-lines from refPos
     * @param isBeyondRefPos true: offset beyond relVal, false: before refPos
     * @return number of csv-lines available from relVal (mostly same as offset), or OUT_OF_RANGE on error
     */
    public int gotoRelativeElementWithOffset(double relVal, int offset, boolean isBeyondRefPos) {
        if (rwDataLines == null) {
            return LogTimes.OUT_OF_RANGE;
        }

        rwDataLines.getLogTimes().getFilePositionRelative(relVal);
        return rwDataLines.getLogTimes().moveFilePositionToGivenTimeAndOffset(getCurrentLogTimeSec(),
            isBeyondRefPos ? offset : -offset);

    }


    /**
     * Move current reading location with given number of lines
     *
     * @param offset           number of csv-lines from current reading location
     * @param beyondCurrentPos true: offset beyond current pos, false: before current pos
     * @return number of csv-lines actually moved.
     */
    public int moveCurrentReadingLocation(int offset, boolean beyondCurrentPos) {
        if (rwDataLines == null) {
            return LogTimes.OUT_OF_RANGE;
        }

        return rwDataLines.getLogTimes().moveCurrentTimePosIdx(offset, beyondCurrentPos);
    }


    /**
     * Get element at relative position
     *
     * @param relativeVal decimal value between 0 and 1 representing beginning - end
     * @return value at given relative position
     * @throws IOException
     */
    @Override
    public ReaderResult getRelativeElement(double relativeVal) throws IOException {
        if (rwDataLines == null) {
            return null;
        }

        try {
            return convertResultLine(rwDataLines.readLineAtPos(rwDataLines.getLogTimes()
                .getFilePositionRelative(relativeVal)));
        } catch (IOException ex) {
            handleException(ex);
            throw ex;
        }
    }


    private ReaderResult convertResultLine(String line) throws IOException {
        if (rwDataLines == null) {
            return null;
        }

        List<ResultItem> items = new ArrayList<>();
        CsvLogInfo logInfo = (CsvLogInfo) rwDataLines.getLogInfo();

        try {
            if (line == null) {
                throw new IOException("line: null.  ");
            }
            // some HANtune .CSV files have a comma as decimal separator, therefore replace here
            String cLine = line.replace(DECIMAL_SEPARATOR_COMMA, DECIMAL_SEPARATOR_DOT);
            int startOfData = cLine.indexOf(';');
            Double timeVal = Double.parseDouble(cLine.substring(0, startOfData));
            String[] dataArr = cLine.substring(startOfData + 1).split(";");

            int idx;
            for (idx = 0; idx < dataArr.length; idx++) {
                if (!dataArr[idx].isEmpty()) {
                    CsvLogInfo.ProtocolSignalName id = logInfo.getProtocolSignalNames().get(idx);
                    items.add(new ResultItem(id.getProtocol(), id.getSignal(), Double.parseDouble(dataArr[idx])));
                }
            }
            return new ReaderResult(timeVal, items);
        } catch (NumberFormatException ex) {
            throw new IOException("Error parsing value.  " + line + " " + ex.getMessage(), ex);
        }
    }


    private List<ReaderResult> convertResultLines(List<String> lines) throws IOException {
        List<ReaderResult> rslt = new ArrayList<>();
        for (String ln : lines) {
            rslt.add(convertResultLine(ln));
        }
        return rslt;
    }


    private void handleException(Exception ex) {
        errString = String.format("Filename: %s\n%s", rwDataLines.getFile().getName(), ex.getMessage());
        AppendToLogfile.appendMessage(errString);
        AppendToLogfile.appendError(Thread.currentThread(), ex);
    }

    public void stopReader(boolean doClose) {
        if (doClose && rwDataLines != null) {
            rwDataLines.close();
        }
        rwDataLines = null;
    }
}
