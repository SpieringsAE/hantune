/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.datahandling.DataLogger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ErrorLogger.AppendToLogfile;
import util.OptimizedRandomAccessFile;

/**
 * shared class between LogDataWriterCsv and LogDataReaderCsv during read-while-write
 */
public class LogReadWriteDataLines {
    private static final String NEW_LINE = System.getProperty("line.separator");

    private String errString;

    private File file;
    private boolean isReadAndWrite;
    private OptimizedRandomAccessFile raf = null;
    private LogSizeLimiter logSizeLimiter = new LogSizeLimiter();
    private LogInfo logInfo = null;
    private LogTimes logTimes = null;


    void init(File file, boolean isReadAndWrite) {
        this.file = file;
        errString = null;
        this.isReadAndWrite = isReadAndWrite;
        try {
            if (this.isReadAndWrite) {
                if (!file.getParentFile().exists()) {
                    file.getParentFile().mkdirs();
                }
                raf = new OptimizedRandomAccessFile(file, "rw");

            } else {
                raf = new OptimizedRandomAccessFile(file, "r");
            }
        } catch (FileNotFoundException | IllegalArgumentException e) {
            handleException(e);
        }
    }


    void close() {
        if (raf == null) {
            return;
        }

        try {
            raf.close();
        } catch (IOException ex) {
            raf = null; // prevent re-closing of raf when calling handleException() here
            handleException(ex);
        } finally {
            raf = null;
        }
    }


    private void handleException(Exception ex) {
        errString = String.format("Filename: %s\n%s", getFileName(), ex.getMessage());
        AppendToLogfile.appendMessage(errString);
        AppendToLogfile.appendError(Thread.currentThread(), ex);

        close();
    }


    long writeln(String str) {
        try {
            synchronized (this) { // make sure that file pos doesn't get changed by a READ at another filepos
                long rtn;
                rtn = raf.length();
                raf.seek(rtn); // force writing at end of file
                raf.writeBytes(str + NEW_LINE);
                return rtn;
            }
        } catch (IOException e) {
            handleException(e);
        }
        return 0L;
    }


    long writeDataLnTime(double timeSec, String str) {
        try {
            synchronized (this) { // make sure that file pos doesn't get changed by a READ at another filepos
                long rtn;
                rtn = raf.length();
                raf.seek(rtn); // force writing at end of file
                raf.writeBytes(str + NEW_LINE);
                logTimes.appendTimeItem(new LogTimes.TimeFilePos(timeSec, rtn));
                return rtn;
            }
        } catch (IOException e) {
            handleException(e);
        }
        return 0L;
    }


    void seekStartPos() {
        try {
            raf.seek(0L);
        } catch (IOException e) {
            handleException(e);
        }
    }


    long getCurrentPos() {
        try {
            return raf.getFilePointer();
        } catch (IOException e) {
            handleException(e);
        }
        return 0L;
    }


    String readLine() {
        try {
            return raf.readLine();
        } catch (IOException e) {
            handleException(e);
        }
        return null;
    }


    String readLineAtPos(long pos) {
        try {
            // synchronize: make sure that file pos doesn't get changed by a WRITE at another filepos
            synchronized (this) {
                raf.seek(pos);
                return raf.readLine();
            }
        } catch (IOException e) {
            handleException(e);
        }
        return null;
    }


    List<String> readLinesAtPos(long pos, int numLines) {
        try {
            // synchronize: make sure that file pos doesn't get changed by a WRITE at another filepos
            synchronized (this) {
                raf.seek(pos);
                List<String> rslt = new ArrayList<>();
                for (int cnt = 0; cnt < numLines; cnt++) {
                    rslt.add(raf.readLine());
                }
                return rslt;
            }
        } catch (IOException e) {
            handleException(e);
        }
        return new ArrayList<>();
    }


    boolean isLogfileSizeLimit() {
        return logSizeLimiter.checkLogSize(raf);
    }

    String getFileName() {
        return file.getName();
    }

    String getFullFileName() {
        return file.getPath();
    }

    String getError() {
        return errString;
    }

    boolean isReadWriteActive() {
        return (raf != null && isReadAndWrite);
    }

    public File getFile() {
        return file;
    }

    public LogInfo getLogInfo() {
        return logInfo;
    }

    public void setLogInfo(LogInfo logInfo) {
        this.logInfo = logInfo;
    }

    public LogTimes getLogTimes() {
        return logTimes;
    }

    public void setLogTimes(LogTimes logTimes) {
        this.logTimes = logTimes;
    }


}
