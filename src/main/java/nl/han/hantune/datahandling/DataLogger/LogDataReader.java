/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.datahandling.DataLogger;

import java.io.IOException;
import java.util.List;

public interface LogDataReader<T, K, V> {

    /**
     * get properties of current log data type T
     * @return properties
     */
    public T getLogHeaderInfo();

    /**
     * get first element
     * @return dataElement V
     */
    public V getFirstElement() throws IOException;


    /**
     * get first element
     * @return dataElement V
     */
    public V getLastElement() throws IOException;

    /**
     * get next element at current reading location
     * @return dataElement V
     */
    public V getNextElement() throws IOException;

    /**
     * get next count elements at current reading location
     * @param count number of elements to get
     * @return List of dataElement V
     */
    public List<V> getNextElements(int count) throws IOException;

    /**
     * get previous element at current reading location
     * @return dataElement V
     */
    public V getPrevElement() throws IOException;

    /* get first element with reference K containing log data element type V
     * @return reference K, dataElement V
     */
    public V getRefElement(K elementReference) throws IOException;

    /**
     * Get element type V at relative position (0.0 .. 1.0)
     * @param relativePos
     * @return dataElement V at given position
     * @throws IOException
     */
    public V getRelativeElement(double relativePos) throws IOException;
}
