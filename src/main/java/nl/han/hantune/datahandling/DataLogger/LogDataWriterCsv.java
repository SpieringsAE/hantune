/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.datahandling.DataLogger;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;

import HANtune.CustomFileChooser.FileType;
import HANtune.HANtune;
import HANtune.actions.LogReaderAction;
import datahandling.CurrentConfig;
import util.MessagePane;
import util.Util;


/**
 * Performs the tasks related to the logging of incoming data
 *
 * @author Aart-Jan, Michiel Klifman
 */
public class LogDataWriterCsv implements LogDataWriter<CsvLogInfo, String> {

    private static final LogDataWriterCsv instance = new LogDataWriterCsv();
    private final CurrentConfig currentConfig = CurrentConfig.getInstance();
    private final String fileExt = FileType.LOG_FILE.getExtension();
    private String prefix = "";
    private String error = "";
    private long startTime = 0;
    private boolean writingActive = false; // True: write incoming communication data to logfile
    private boolean writingStandBy = false; // holds state of writer when not connected.
    private boolean readWhileWriteStandby = false; // holds state of readWhileWrite when not connected.
    private final Map<String, Double> valuesBySignal = new LinkedHashMap<>();
    private static final String TIME_FORMAT = "0.00000";
    private static final String VALUE_FORMAT = "0.####################";

    private LogReadWriteDataLines dataLines = null;

    private LogDataWriterCsv() {
    }

    public static LogDataWriterCsv getInstance() {
        return instance;
    }

    /**
     * Opens a new datalog file in the 'log' directory of HANtune. The filename is based on project name
     * plus the current date and time.
     *
     * @return true if OK
     */
    public boolean initDatalog() {
        writingActive = false;
        boolean restartLogReaderAction = false;
        if (LogReaderAction.getInstance().isReadWhileWriteMode()) {
            // When read-while-log is active, dataLines object is shared between this object and
            // LogDataReaderCsv and therefore should be stopped
            LogReaderAction.getInstance().disableRead();
            restartLogReaderAction = true;
        }
        dataLines = new LogReadWriteDataLines();
        dataLines.init(new File(currentConfig.getLogFile_Path(), constructLogfileName()), true);
        if (!dataLines.isReadWriteActive()) {
            setError(dataLines.getError());
            closeDatalog();
            writingStandBy = false;
            return false;
        }
        // add header information
        CsvLogInfo hdr = new CsvLogInfo();
        hdr.initCsvLogInfo(currentConfig);
        dataLines.setLogInfo(hdr);

        dataLines.setLogTimes(new LogTimes());

        writeLogHeaderInfo(hdr);
        writeLogSignalProtocolNames(hdr);

        if (restartLogReaderAction && !LogReaderAction.getInstance().reEnableReadWrite()) {
            return false;
        }

        startTime = 0;
        writingStandBy = true;
        writingActive = true;
        return true;
    }

    private void startLogging(boolean doReadWhileWriteEnable) {
        LogDataWriterCsv datalogWriter = LogDataWriterCsv.getInstance();
        if (currentConfig.getXcp() != null && currentConfig.getXcp().isConnected()
            && currentConfig.getASAP2Data().getASAP2Measurements().size() > 0) {
            if (datalogWriter.initDatalog()) {
                if (doReadWhileWriteEnable) {
                    LogReaderAction.getInstance().openReadWhileWriteActionDialog();
                }
            } else {
                MessagePane.showError("Could not start logfile:\r\n" + datalogWriter.getError());
                datalogWriter.closeDatalog();
            }
        }
    }

    private void stopLogging(boolean doReadWhileWriteEnable) {
        LogReaderAction logReaderAction = LogReaderAction.getInstance();
        if (!doReadWhileWriteEnable && logReaderAction.isActive() && logReaderAction.isReadWhileWriteMode()) {
            logReaderAction.dispose();
        }
        LogDataWriterCsv.getInstance().closeDatalog();
    }


    public void enableDatalogging(boolean doLogEnable, boolean doReadWhileWriteEnable, boolean restartLog) {
        LogDataWriterCsv datalogWriter = LogDataWriterCsv.getInstance();

        boolean currentLogActive = datalogWriter.isWritingActive();
        boolean currentReadWhileWriteActive = LogReaderAction.getInstance().isReadWhileWriteMode();

        if (doLogEnable && !currentLogActive) {
            startLogging(doReadWhileWriteEnable);
        } else if (!doLogEnable && currentLogActive) {
            stopLogging(doReadWhileWriteEnable);
        } else if (doLogEnable) {
            if (restartLog) {
                datalogWriter.closeDatalog();

                startLogging(doReadWhileWriteEnable);
            } else {
                if (doReadWhileWriteEnable && !currentReadWhileWriteActive) {
                    LogReaderAction.getInstance().openReadWhileWriteActionDialog();
                } else if (!doReadWhileWriteEnable && currentReadWhileWriteActive) {
                    LogReaderAction.getInstance().dispose();
                }
            }
        }
    }


    @Override
    public void writeLogHeaderInfo(CsvLogInfo hdr) {
        dataLines.writeln(createHeaderInfoText(hdr, false));
        dataLines.writeln("");
    }


    public String createHeaderInfoTextHtml(CsvLogInfo hdr) {
        return createHeaderInfoText(hdr, true);
    }


    private String createHeaderInfoText(CsvLogInfo hdr, boolean useHtmlNewlines) {
        StringBuilder txt = new StringBuilder();
        String newLine;
        if (useHtmlNewlines) {
            newLine = "<br>";
        } else {
            newLine =  System.getProperty("line.separator");
        }
        txt.append("Project: ").append(hdr.getProjectName()).append(newLine);

        if (!hdr.getEcuId().isEmpty()) {
            txt.append("ECU: ").append(hdr.getEcuId()).append(newLine);
        }

        if (!hdr.getDbcInfos().isEmpty()) {
            hdr.getDbcInfos().forEach(nm -> txt.append("Active DBC: ").append(nm).append(newLine));
        }


        if (!hdr.getDaqListInfos().isEmpty()) {
            hdr.getDaqListInfos().forEach(dl -> txt.append(String.format("DAQ List %s: %s, %sHz",
                dl.getId(), dl.getName(), dl.getFreq())).append(newLine));
        }

        txt.append("Date: ").append(hdr.getStartDate());
        return txt.toString();
    }


    @Override
    public void writeNextElement(String dataElement) throws IOException {
        // type doesn't fulfill need, therefore empty
    }


    private void writeLogSignalProtocolNames(CsvLogInfo hdr) {
        valuesBySignal.clear();

        // Build csv line containing protocol names: ;XCP;XCP;CAN;...
        StringBuilder protNames = new StringBuilder(";");
        // Build csv line containing signal names: Time;Signal_1;Signal_2;LedValue;...
        StringBuilder sigNames = new StringBuilder("Time;");

        hdr.getProtocolSignalNames().forEach(protSig -> {
            valuesBySignal.put(protSig.getSignal(), null);
            protNames.append(protSig.getProtocol()).append(";");
            sigNames.append(protSig.getSignal()).append(";");
        });
        dataLines.writeln(protNames.toString());
        dataLines.writeln(sigNames.toString());
    }


    /**
     *
     */
    private String constructLogfileName() {
        LocalDateTime date = LocalDateTime.now();
        String strDate = date.format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss"));
        if (prefix.equals("")) {
            prefix = Util.getFilename(currentConfig.getProjectFile().getName()).replace(".hml", "");
        }
        return prefix + strDate + "." + fileExt;
    }


    private double calculateTimeSinceStartSeconds(long timeStamp) {
        if (startTime == 0)
            startTime = timeStamp;

        return ((double) (timeStamp - startTime)) / 1_000_000;
    }


    public void addValue(String name, double value) {
        valuesBySignal.replace(name, value);
    }


    public synchronized void writeValues(long timeStamp) {
        if (writingActive) {
            double timeInSec = calculateTimeSinceStartSeconds(timeStamp);

            StringBuilder wrStr = new StringBuilder(Util.getStringValue(timeInSec, TIME_FORMAT) + ";");
            valuesBySignal.forEach((k, v) -> {
                wrStr.append(v != null ? Util.getStringValue(v, VALUE_FORMAT) : "").append(";");
                valuesBySignal.replace(k, null);
            });

            dataLines.writeDataLnTime(timeInSec, wrStr.toString());

            LogReaderAction.getInstance().updateDialogReadWhileWriteComponents();


            checkLogSize();
        }
    }


    private void checkLogSize() {
        if (dataLines != null && dataLines.isLogfileSizeLimit()) {
            if (!this.restart()) {
                MessagePane.showError("Size limit reached. Could not restart logfile:\r\n" + getError());
            }
            HANtune.getInstance().updateDatalogGuiComponents();
        }
    }


    /**
     * Closes the active datalog.
     */
    public void closeDatalog() {
        if (dataLines != null && !LogReaderAction.getInstance().isReadWhileWriteMode()) {
            dataLines.close();
        }
        LogReaderAction.getInstance().disableReadWhileWrite();
        writingActive = false;
        valuesBySignal.clear();
        dataLines = null;
    }


    /**
     * Restarts datalog with new file, if possible
     */
    public synchronized boolean restart() {
        if (isWritingActive()) {
            if (LogReaderAction.getInstance().isReadWhileWriteMode()) {
                LogReaderAction.getInstance().disableRead();
            }
            if (dataLines != null) {
                dataLines.close();
            }

            writingActive = false;
            dataLines = null;
        }
        if ((currentConfig.getASAP2Data().getASAP2Measurements().size() > 0) && !initDatalog()) {
            closeDatalog();
            return false;
        }
        return true;
    }


    LogReadWriteDataLines getReadWriteDataLines() {
        return dataLines;
    }


    public String getError() {
        return error;
    }


    private void setError(String error) {
        this.error = error;
    }


    public String getPrefix() {
        return prefix;
    }


    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }


    public boolean isWritingActive() {
        return writingActive;
    }


    /**
     * @return the fileName, without path
     */
    public String getFileName() {
        return dataLines.getFileName();
    }


    /**
     * @return the full fileName, with path
     */
    public String getFullFileName() {
        return dataLines.getFullFileName();
    }


    /**
     * @return the standby state
     */
    public boolean isWritingStandby() {
        return writingStandBy;
    }

    public void setWritingStandBy(boolean writingStandBy) {
        this.writingStandBy = writingStandBy;
    }


    public boolean isReadWhileWriteStandby() {
        return readWhileWriteStandby;
    }

    public void setReadWhileWriteStandby(boolean readWhileWriteStandby) {
        this.readWhileWriteStandby = readWhileWriteStandby;
    }
}
