/*
SSHManager is based on original work produced by Charity Leschinski.
Original work Copyright (c) 2009 [Charity Leschinski]
Modified work Copyright (c) 2020 [HAN University of applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

More information on the original work of Charity Leschinski can be found at: https://stackoverflow.com/questions/2405885/run-a-command-over-ssh-with-jsch/47025597

*/

package nl.han.hantune.net.ssh;

import ErrorLogger.AppendToLogfile;
import com.jcraft.jsch.*;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import nl.han.hantune.scripting.Console;

/**
 * Provides basic ssh functionality.
 *
 * @author Michiel Klifman
 */
public class SSHManager {

    private static final String NO_KNOWN_HOSTS = "";
    private static final int DEFAULT_PORT = 22;
    private static final int DEFAULT_TIMEOUT = 60000;
    private JSch jsch;
    private String username;
    private String password;
    private String ip;
    private int port;
    private String knownHostsFile;
    private final int timeout;
    private Session session;

    public SSHManager(String username, String password, String ip) {
        this(username, password, ip, DEFAULT_PORT, NO_KNOWN_HOSTS, DEFAULT_TIMEOUT);
    }

    public SSHManager(String username, String password, String ip, int port) {
        this(username, password, ip, port, NO_KNOWN_HOSTS, DEFAULT_TIMEOUT);
    }

    public SSHManager(String userName, String password, String ip, String knownHostsFile) {
        this(userName, password, ip, DEFAULT_PORT, knownHostsFile, DEFAULT_TIMEOUT);
    }

    public SSHManager(String username, String password, String ip, String knownHostsFile, int port) {
        this(username, password, ip, port, knownHostsFile, DEFAULT_TIMEOUT);
    }

    public SSHManager(String username, String password, String ip, int port, String knownHostsFile, int timeOutMilliseconds) {

        this.username = username;
        this.password = password;
        this.ip = ip;
        this.port = port;
        this.timeout = timeOutMilliseconds;
        this.knownHostsFile = knownHostsFile;
    }

    /**
     * Connect to the sshd server
     * @return "Connected" on success and an error message otherwise
     */
    public String connect() {
        jsch = new JSch();

        try {
            jsch.setKnownHosts(knownHostsFile);
        } catch (JSchException exception) {
            Console.print(exception.getMessage());
        }

        String result;

        try {
            session = jsch.getSession(username, ip, port);
            session.setPassword(password);
            if (knownHostsFile.equals(NO_KNOWN_HOSTS)) {
                // UNCOMMENT THIS FOR TESTING PURPOSES, BUT DO NOT USE IN PRODUCTION
                session.setConfig("StrictHostKeyChecking", "no");
            }
            session.connect(timeout);
            result = "Connected";
        } catch (JSchException jschX) {
            result = jschX.getMessage();
        }

        return result;
    }

    /**
     * Send a command to the sshd server
     * @param command - the command to send
     * @return the result
     */
    public String sendCommand(String command) {
        StringBuilder outputBuffer = new StringBuilder();

        try {
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);
            InputStream commandOutput = channel.getInputStream();
            channel.connect();
            int readByte = commandOutput.read();

            while (readByte != 0xffffffff) {
                outputBuffer.append((char) readByte);
                readByte = commandOutput.read();
            }

            channel.disconnect();
        } catch (IOException | JSchException ex) {
            AppendToLogfile.appendError(Thread.currentThread(), ex);
            return null;
        }

        return outputBuffer.toString();
    }

    /**
     * Changes directory on the sshd server
     * @param path - the path to change the directory to.
     */
    public void changeDirectory(String path) {
        ChannelSftp channel = null;
        try {
            channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();
            channel.cd(path);
        } catch (JSchException | SftpException ex) {
            Console.print(ex);
        } finally {
            if (channel != null) {
                channel.exit();
                channel.disconnect();
            }
        }
    }

    /**
     * Downloads a file from the sshd server
     * @param remoteFile - the file to download
     * @param localFile - the location of the new file
     */
    public void downloadFile(String remoteFile, String localFile) {
        BufferedInputStream input = null;
        BufferedOutputStream output = null;
        try {

            ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();
            byte[] buffer = new byte[1024];
            input = new BufferedInputStream(channel.get(remoteFile));
            output = new BufferedOutputStream(new FileOutputStream(new File(localFile)));
            int readCount;
            while ((readCount = input.read(buffer)) > 0) {
                output.write(buffer, 0, readCount);
            }
        } catch (JSchException | SftpException | IOException ex) {
            Console.print(ex);
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
                if (output != null) {
                    output.close();
                }
            } catch (IOException ex) {
                Console.print(ex);
            }
        }
    }

    /**
     * Send a file to the sshd server
     * @param localFile - the file to send
     * @param remoteFile - the location on the sshd server for the new file
     * @throws Exception
     */
    public void sendFile(String localFile, String remoteFile) throws Exception {
        ChannelSftp channel = null;
        try {
            channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();
            File f = new File(localFile);
            channel.put(new FileInputStream(f), remoteFile);
        } catch (JSchException | FileNotFoundException | SftpException ex) {
            Console.print(ex);
        } finally {
            if (channel != null) {
                channel.exit();
                channel.disconnect();
            }
        }
    }

    /**
     * Removes a file on the sshd server
     * @param remoteFile the location of the file on the sshd server
     */
    public void removeFile(String remoteFile) {
        ChannelSftp channel = null;
        try {
            channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();
            channel.rm(remoteFile);
        } catch (JSchException | SftpException ex) {
            Console.print(ex);
        } finally {
            if (channel != null) {
                channel.exit();
                channel.disconnect();
            }
        }
    }

    /**
     * Close the connection to the sshd server
     */
    public void close() {
        session.disconnect();
    }

}
