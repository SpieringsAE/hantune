/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.net.ftp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.io.CopyStreamAdapter;

/**
 * Adapter class for the FTPClient and the IDataSource It enables the methods of
 * the IDataSource to be used on the FTPClient.
 *
 * @author Bailey Bosma
 */
public class HANtuneApacheFTP {

    public static final int REPLY_CODE_DOES_NOT_EXIST = 550;
    public static final String BASE_DIR = "/";

    private FTPClient ftpClient;
    private final FileInterpreter fileInterpreter;
    private CopyStreamAdapter apacheCopyStreamAdapter;

    public HANtuneApacheFTP() {
        ftpClient = new FTPClient();
        fileInterpreter = new FileInterpreter();
        apacheCopyStreamAdapter = new CopyStreamAdapter();
    }

    public void setFTPClient(FTPClient ftpClient) {
        this.ftpClient = ftpClient;
    }

    /*
    * This fucntion is used to create a connection with the ftp server on the target
     */
    public boolean createConnection(String ip, int port, String username, String password) {
        try {
            ftpClient.connect(ip, port);
            boolean success = ftpClient.login(username, password);
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            ftpClient.setCopyStreamListener(apacheCopyStreamAdapter);
            return success;
        } catch (IOException ex) {
            Logger.getLogger(HANtuneApacheFTP.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /*
    * This function is used to show the list of files on the remote server
     */
    public FTPFile[] showFileList(String path) {
        try {
            FTPFile[] files = ftpClient.listFiles(path);
            /* Get the FTPFiles in the main directory. */
            return fileInterpreter.getValidFiles(files);
        } catch (IOException ex) {
            Logger.getLogger(HANtuneApacheFTP.class.getName()).log(Level.FINE, null, ex);
        }
        return new FTPFile[0];
    }

    /*
    * Downloads a file based on two paths contained in two Strings
     */
    public boolean downloadFile(String remoteFilePath, String localPath) {
        /* Creates a reference to the output file. */
        File downloadFile = new File(localPath);
        OutputStream outputStream;
        try {
            /* Create a (custom) outputstream (for testing purposes) to the (local) target file */
            outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile));
            /* Execute the download operation and store its succes in a boolean*/
            boolean success = ftpClient.retrieveFile(remoteFilePath, outputStream);
            /* Downloads the file and stores the succes of the download in a variable. */
            outputStream.close();
            return success;
        } catch (FileNotFoundException ex) {
            System.out.println("file not found");
        } catch (IOException ex) {
             System.out.println("io exception");
            Logger.getLogger(HANtuneApacheFTP.class.getName()).log(Level.SEVERE, ex.toString(), ex);
        }
        return false;
    }

    /**
     * This method uploads a file based on a remote path(where the file be
     * uploaded to) and a localPath(where the file comes from)
     *
     * @param remoteFilePath
     * @param localFilePath
     * @return
     */
    public boolean uploadFile(String remoteFilePath, String localFilePath) {
        /* Creates a reference to the output file. */
        File uploadFile = new File(localFilePath);
        InputStream inputStream;
        try {
            /* Create a outputstream to the (remote) target file */
            inputStream = new BufferedInputStream(new FileInputStream(uploadFile));
            /* Execute the upload operation and store its succes in a boolean*/
            boolean success = ftpClient.storeFile(remoteFilePath, inputStream);
            /* uploads the file and stores the succes of the download in a variable. */
            inputStream.close();
            return success;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(HANtuneApacheFTP.class.getName()).log(Level.SEVERE, ex.toString(), ex);
        } catch (IOException ex) {
            Logger.getLogger(HANtuneApacheFTP.class.getName()).log(Level.SEVERE, ex.toString(), ex);
        }
        return false;
    }

    /**
     * This method is used to delete a file based on a remotepath based of the
     * location of the file on the remote server.
     *
     * @param remoteFilePath
     */
    public void deleteFile(String remoteFilePath) {
        try {
            ftpClient.deleteFile(remoteFilePath);
        } catch (IOException ex) {
            Logger.getLogger(HANtuneApacheFTP.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
    * Closes the connection of the FTPClient.
     */
    public boolean closeConnection() {
        try {
            ftpClient.disconnect();
            return true;
        } catch (IOException ex) {
            Logger.getLogger(HANtuneApacheFTP.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /*
    * Returns true if the FTP client is connected
     */
    public boolean isConnected() {
        return ftpClient.isConnected();
    }

    /*
    * Returns true if the folder with the given path is a directory.
    * The file does not exist if the reply code is equal to REPLY_CODE_DOES_NOT_EXIST
    * or a IOException has been thrown. Finally, the current directory will be reset
    * to the base directory.
     */
    public boolean isDirectory(String path) {
        try {
            /* Change the current working directory */
            ftpClient.changeWorkingDirectory(path);
            /* Evaluates the reply code */
            return ftpClient.getReplyCode() == REPLY_CODE_DOES_NOT_EXIST;
        } catch (IOException ex) {
            Logger.getLogger(HANtuneApacheFTP.class.getName()).log(Level.SEVERE, ex.toString(), ex);
            return false;
        } finally {
            returnToBaseDirectory();
        }
    }

    /*
    * Return to the base directory.
     */
    public void returnToBaseDirectory() {
        try {
            ftpClient.changeWorkingDirectory(BASE_DIR);
        } catch (IOException ex) {
            Logger.getLogger(HANtuneApacheFTP.class.getName()).log(Level.SEVERE, ex.toString(), ex);
        }
    }
}
