/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.net.ftp;

import java.util.Arrays;
import org.apache.commons.net.ftp.FTPFile;

/**
 * The FileInterpreter is used to return a set of valid logfiles based on their
 * names. The validity of a file is based on the implementation of this class
 *
 * @author Bailey Bosma
 */
public class FileInterpreter {

    public static final String DOT_PATTERN = ".";
    public static final String PATH_SEPERATOR = "/";

    /*
    * Splits a path in String format to an array of String, divided by a '/' sign.
    * The last String in the array will represent the filename and is returned.
     */
    public String getFileName(String filePath) {
        /* Convert path to individual Strings in a String array. */
        String[] paths = filePath.split(PATH_SEPERATOR);
        return paths[paths.length - 1];
    }

    /* This function is used to filter the logfiles from al the other files present on the target.
    *  The FTPFile array is given as parameter in this function is filtered on the name LogFile.
    *  This means that every file that begins with LogFile is inserted into a new array.
    *  This new array is returned at the end of this function.
     */
    public FTPFile[] getValidFiles(FTPFile[] files) {
        return Arrays.stream(files)
                .filter(f -> !DOT_PATTERN.equals(f.getName()) && !(DOT_PATTERN + DOT_PATTERN).equals(f.getName()))
                .toArray(FTPFile[]::new);
    }

}
