/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.config;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import ErrorLogger.AppendToLogfile;


/**
 * class holding all properties of application.
 */
public final class ApplicationProperties {
    // File must reside in HANtune.exe dir. If not present, then default property values are used
    static final String APPLIC_PROPERTIES_NAME = "application.properties";
    private static Properties props;

    static {
        initApplicationProperties(APPLIC_PROPERTIES_NAME);
    }


    private static void initApplicationProperties(String name) {
        props = new Properties(ConfigProperties.getAllProperties());
        try (FileInputStream fileProps = new FileInputStream(name)){
            props.load(fileProps);
        } catch (IOException e) {
            AppendToLogfile.appendMessage("Property file " + name + " not found. Using default properties.");
        }
    }

    public static String[] getAllPropertyStringArray() {
        List<String> lst = new ArrayList<>();
        Set<Object> keys = props.keySet();
        for (Object k : keys) {
            lst.add((String) k + "=" + props.getProperty((String) k));
        }

        return lst.toArray(new String[0]);
    }

    // prevent instantiation of 'static' class
    private ApplicationProperties() {
        throw new AssertionError(); // prevent local instantiation.
    }


    public static boolean getBoolean(ConfigProperties param) {
        return Boolean.parseBoolean(props.getProperty(param.getKey()));
    }


    public static int getInt(ConfigProperties param) {
        String value = props.getProperty(param.getKey());
        try {
            return Integer.decode(value);
        } catch (NumberFormatException e) {
            logMessage(param, value);
            return Integer.parseUnsignedInt(param.getValue());
        }
    }


    public static String getString(ConfigProperties param) {
        return props.getProperty(param.getKey());
    }

    public static ConfigProperties.TopBottomLabelPos getTopBottomLabelPos(ConfigProperties param) {
        return ConfigProperties.TopBottomLabelPos.valueOf(props.getProperty(param.getKey()));
    }

    public static Color getColor(ConfigProperties param) {
        String value = props.getProperty(param.getKey());
        try {
            Color clr = getColorFromString(value);
            if (clr != null) {
                return clr;
            } else {
                return getColorFromString(param.getValue());
            }
        } catch (IllegalArgumentException e) {
            logMessage(param, value);

            return getColorFromString(param.getValue());
        }
    }


    private static Color getColorFromString(String hlp) {
        String[] parts = hlp.trim().split("[,\\s]+");
        if (parts.length == 3) {
            return new Color(Integer.decode(parts[0]), Integer.decode(parts[1]),
                Integer.decode(parts[2]));
        }
        return null;
    }


    private static void logMessage(ConfigProperties param, String value) {
        AppendToLogfile.appendMessage(
            "Property " + param.getKey() + ", " + value + " from " + APPLIC_PROPERTIES_NAME +
                " could not be parsed. Using default property value '" + param.getValue() + "' instead.");
    }
}
