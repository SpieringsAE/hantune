/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.scripting;

import HANtune.HANtune;
import datahandling.BasicDescriptionFile;
import datahandling.Reference;
import datahandling.ReferenceStateListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import util.CompositeList;

/**
 * A singleton to manage all Scripts and ScriptedSignals in HANtune.
 *
 * @author Michiel Klifman
 */
public class ScriptingManager extends BasicDescriptionFile implements ReferenceStateListener {

    private static ScriptingManager manager;

    private final List<Script> scripts = new ArrayList<>();
    private final List<ScriptedSignal> scriptedSignals = new ArrayList<>();
    private final List<ScriptingManagerListener> listeners = new ArrayList<>();

    private ScriptingManager() {
        super(Script.SOURCE, Script.SOURCE);
    }

    public static ScriptingManager getInstance() {
        if (manager == null) {
            manager = new ScriptingManager();
        }
        return manager;
    }

    /**
     * Will add a Script to the ScriptingManager and subscribe to it, if it does
     * not already contain an instance of the Script.
     * @param script - the Script to add
     */
    public void addScript(Script script) {
        if (!scripts.contains(script)) {
            scripts.add(script);
            script.addStateListener(this);
        }
    }

    /**
     * Removes a Script from the ScriptingManager and unsubscribes to it.
     * @param script
     */
    public void removeScript(Script script) {
        scripts.remove(script);
        script.removeStateListener(this);
    }

    /**
     * @return all Scripts the manager has.
     */
    public List<Script> getScripts() {
        return scripts;
    }

    /**
     * Get a Script from the manager by its name. If it cannot be found, will
     * return null.
     * @param name - the name of the Script
     * @return the Script or null if it does not have a Script by that name
     */
    public Script getScriptByName(String name) {
        return scripts.stream()
                .filter(s -> s.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    /**
     * Removes all Scripts from the manager and unsubscribes to them.
     */
    public void removeAllScripts() {
        scripts.forEach(s -> s.removeStateListener(this));
        scripts.clear();
    }

    /**
     * Called whenever a Script changes state (e.g. is running)
     */
    @Override
    public void stateUpdate() {
        HANtune.getInstance().updateProjectTreeUI();
    }

    /**
     * Adds a ScriptedSignal to the manager
     * @param signal the signl to be added
     */
    public void addScriptedSignal(ScriptedSignal signal) {
        scriptedSignals.add(signal);
        updateListeners();
    }

    public void removeScriptedSignal(ScriptedSignal signal) {
        scriptedSignals.remove(signal);
        updateListeners();
    }

    public void removeScriptedSignal(String name) {
        scriptedSignals.removeIf(s -> s.getName().equals(name));
        updateListeners();
    }

    public void removeAllScriptedSignals() {
        scriptedSignals.clear();
        updateListeners();
    }

    /**
     * @return all ScriptedSignals this manager has
     */
    public List<ScriptedSignal> getScriptedSignals() {
        return scriptedSignals;
    }

    /**
     * Subscribes a listener to be notified in case of ScriptedSignal updates.
     * @param listener the listener to be notified
     */
    public void addListener(ScriptingManagerListener listener) {
        listeners.add(listener);
    }

    /**
     * Notifies all listeners that this manager's ScriptedSignals have been
     * updated (i.e. added or removed)
     */
    public void updateListeners() {
        listeners.forEach(l -> l.scriptedSignalUpdate());
    }

    /**
     * @return a single list of all Scripts and ScriptedSignals
     */
    @Override
    public List<? extends Reference> getReferences() {
        return new CompositeList<>(scripts, scriptedSignals);
    }

    public void addReferenceToInterpreters(Reference reference) {
        Console.getInteractiveInterpreter().set(reference.getName(), reference);
        scripts.forEach(s -> s.getInterpreter().set(reference.getName(), reference));
    }

    public void addReferencesToInterpreters(List<? extends Reference> references) {
        references.forEach(r -> addReferenceToInterpreters(r));
    }

    Map<String, Object> globals = new HashMap<>();

    public void setGlobal(String name, Object value) {
        globals.put(name, value);
        scripts.forEach(s -> s.getInterpreter().set(name, value));
    }

    public Map<String, Object> getGlobals() {
        return globals;
    }

}
