/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.scripting.jython;

import HANtune.HANtune;
import nl.han.hantune.scripting.ScriptingManager;
import datahandling.CurrentConfig;
import java.io.InputStream;

import org.python.core.PyObject;
import org.python.core.imp;
import org.python.util.PythonInterpreter;

/**
 * Helper class to initialize a Jython interpreter and connect it to HANtune so
 * it can utilize its API functions.
 * @author Michiel Klifman
 */
public class JythonInterpreterInitializer {

    /**
     * Dynamically loads HANtuneJythonAPI.py from its package and imports the
     * "file" from an input stream into Jython.
     * @param interpreter
     */
    public static void initInterpreter(PythonInterpreter interpreter) {
        InputStream in = interpreter.getClass().getResourceAsStream("/scripts/jython/HANtuneJythonAPI.py");
        imp.createFromSource("HANtuneJythonAPI", in, "scripts/HANtuneJythonAPI.py");
        interpreter.exec("from HANtuneJythonAPI import *");

        PyObject entryPointClass = interpreter.get("JythonEntryPoint");
        PyObject entryPointObject = entryPointClass.__call__();

        JythonEntryPoint entryPoint = (JythonEntryPoint) entryPointObject.__tojava__(JythonEntryPoint.class);
        entryPoint.setHANtune(HANtune.getInstance());
        CurrentConfig.getInstance().getAllReferences().forEach(r -> interpreter.set(r.getName(), r));
        ScriptingManager.getInstance().getGlobals().forEach((k, v) -> interpreter.set(k, v));
    }

}
