/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.scripting.jython;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.Reader;
import javax.swing.SwingUtilities;

import nl.han.hantune.scripting.Console;
import org.python.core.PyException;
import org.python.util.InteractiveConsole;

/**
 * Wrapper around Jython's InteractiveConsole (interpreter). Reads lines from a
 * reader object and pushes them to the interpreter.
 * @author Michiel Klifman
 */
public class InteractiveJythonInterpreter implements Runnable {

    private final InteractiveConsole interpreter;
    private final BufferedReader bufferedReader;
    private Executor executor;
    private Printer printer;

    /**
     * Initializes the interpreter and creates a BufferedReader so we can read
     * lines from the supplied Reader object.
     * @param reader the Reader object to read lines from
     */
    public InteractiveJythonInterpreter(Reader reader) {
        interpreter = new InteractiveConsole();
        JythonInterpreterInitializer.initInterpreter(interpreter);
        bufferedReader = new BufferedReader(reader) {
            @Override
            public String readLine() throws IOException {
                String line = super.readLine();
                //JConsole puts a ";" into empty lines by default. The Jython
                //interpreter cannot handle that, so we nee to remove it.
                return ";".equals(line) ? "" : line;
            }
        };

        interpreter.setIn(bufferedReader);
    }

    /**
     * Reads lines from the BufferedReader, pushes them to the interpreter and
     * prints a prompt when it returns. Will be called when an object of this
     * class is supplied to a Thread.
     */
    @Override
    @SuppressWarnings("CallToThreadYield")
    public void run() {
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                boolean moreInputRequired = push(line);
                Console.printPrompt(moreInputRequired);
                Thread.yield();
            }
        } catch (IOException ex) {
            Console.print(ex);
        }
    }

    /**
     * Creates input and output stream and pushes a line to the interpreter in a
     * new Thread. The output of the interpreter is also read in a new thread to
     * reduce the burden on the console GUI.
     * @param line the line to push
     * @return whether or not the interpreter requires more input.
     */
    private boolean push(String line) {
        try {
            PipedInputStream inputStream = new PipedInputStream();
            OutputStream outputStream = new PipedOutputStream(inputStream);
            executor = new Executor(line, outputStream);
            printer = new Printer(inputStream);
            executor.start();
            printer.start();
            executor.join();
            printer.join();
        } catch (InterruptedException | IOException ex) {
            Console.print(ex);
        }
        return executor.getResult();
    }

    /**
     * Interrupts execution of the last pushed line by interrupting the Thread
     * it is being executed on and the Thread it uses to print its output. Will
     * reset the buffer of the interpreter.
     */
    public void interrupt() {
        executor.interrupt();
        printer.interrupt();
        interpreter.resetbuffer();
    }

    /**
     * Sets a variable in the interpreter.
     * @param name - the name of the variable
     * @param value - the value of the variable
     */
    public void set(String name, Object value) {
        interpreter.set(name, value);
    }

    /**
     * Inner class that extends Thread and executes a line when run. Because the
     * line is executed in a separate Thread, we can stop the interpreter by
     * interrupting the thread it is running on.
     */
    private class Executor extends Thread {

        private final String line;
        private final OutputStream output;
        private boolean moreInputRequired;

        private Executor(String line, OutputStream output) {
            this.line = line;
            this.output = output;
            interpreter.setOut(output);
            interpreter.setErr(output);
        }

        @Override
        public void run() {
            try {
                moreInputRequired = interpreter.push(line);
            } catch (PyException ex) {
                SwingUtilities.invokeLater(() -> Console.print(ex));
            } finally {
                try {
                    output.close();
                } catch (IOException ex) {
                    Console.print(ex);
                }
            }
        }

        public boolean getResult() {
            return moreInputRequired;
        }
    }

    /**
     * Inner class that extends Thread and prints all output of the interactive
     * interpreter to the console. This Printer differs slightly from the
     * JythonInterpreter.Printer in that a BufferedReader cannot be used to read
     * lines, but instead has to be done manually. The reason for this is the
     * Jython help function (called by entering help() in the console). A
     * BufferedReader will only read a line if it ends with a line feed (i.e.
     * \n). The help function's prompt (help> ) however does not end with a line
     * feed and will therefor not be read until the user has pressed enter
     * again. To prevent this it reads all characters, append them to a
     * StringBuilder and prints them when it either detects a line feed (\n) or
     * the help prompt (help> ).
     */
    private class Printer extends Thread {

        private final InputStreamReader reader;

        private Printer(InputStream input) {
            reader = new InputStreamReader(input);
        }

        @Override
        @SuppressWarnings("CallToThreadYield")
        public void run() {
            try {
                StringBuilder builder = new StringBuilder();
                int r;
                while ((r = reader.read()) != -1) {
                    char ch = (char) r;
                    builder.append(ch);
                    if (ch == '\n' || builder.toString().equals("help> ")) {
                        Console.print(builder.toString());
                        builder.setLength(0);
                    }
                    Thread.yield();
                }
            } catch (IOException ex) {
            }
        }

        @Override
        public void interrupt() {
            try {
                reader.close();
            } catch (IOException ex) {
                    Console.print(ex);
            }
        }
    }
}
