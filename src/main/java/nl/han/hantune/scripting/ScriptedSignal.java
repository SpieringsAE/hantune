/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.scripting;

import datahandling.ReferenceStateListener;
import datahandling.Signal;
import datahandling.SignalListener;
import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of the Signal interface to allow user to create their own
 * signals in a (Python) script.
 *
 * @author Michiel Klifman
 */
public class ScriptedSignal implements Signal {

    private final String name;
    private double value;
    private long timestamp;
    private final List<SignalListener> signalListeners;
    private final List<ReferenceStateListener> stateListeners;

    private double minimum = 0;
    private double maximum = 1;
    private double factor = 1;
    private String unit = "";
    private boolean active = true;

    public ScriptedSignal(String name) {
        this.name = name;
        signalListeners = new ArrayList<>();
        stateListeners = new ArrayList<>();
    }

    /**
     * @return the name of this script
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Because this signal was created in a script, the source of this signal is
     * "Scripts".
     * @return the source of this script.
     */
    @Override
    public String getSource() {
        return Script.SOURCE;
    }

    /**
     * Overridden to show this signal's name when used as node in a JTree.
     * @return the name of this signal
     */
    @Override
    public String toString() {
        return this.getName();
    }

    /**
     * @return the current value of this signal
     */
    @Override
    public double getValue() {
        return value;
    }

    /**
     * @param value - the value this signal should have
     */
    public void setValue(double value) {
        this.value = value;
        updateSignalListeners();
    }

    /**
     * As this signal was created in a script and not received through a
     * specific protocol, it does not have a raw value.
     * @return
     */
    @Override
    public double getRawValue() {
        return value;
    }

    /**
     * @return the current timestamp of this signal
     */
    @Override
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * @param value - the value this signal should have
     * @param timestamp - the timestamp this signal should have
     */
    @Override
    public void setValueAndTime(double value, long timestamp) {
        this.value = value;
        this.timestamp = timestamp;
        updateSignalListeners();
    }

    /**
     * @return whether or not this signal is currently active
     */
    @Override
    public boolean isActive() {
        return active;
    }

    /**
     * @param active - whether or not this signal should be active
     */
    @Override
    public void setActive(boolean active) {
        this.active = active;
        updateStateListeners();
    }

    /**
     * @param value the value to be converted to a binary string.
     * @return the binary string representation of the provided value.
     */
    @Override
    public String getBitString(double value) {
        if (hasDecimals()) {
            return Long.toBinaryString(Double.doubleToRawLongBits(value));
        } else {
            return Long.toBinaryString((long) value);
        }
    }

    /**
     * @param value the value to be converted to a hexadecimal string.
     * @return the hexadecimal string representation of the provided value.
     */
    @Override
    public String getHexString(double value) {
        if (hasDecimals()) {
            return Long.toHexString(Double.doubleToRawLongBits(value));
        } else {
            return Long.toHexString((long) value);
        }
    }

    /**
     * Because this signal was created in a script, it does not belong to a
     * specific protocol and will therefor return "Scripts".
     * @return the protocol name of this script
     */
    @Override
    public String getProtocolName() {
        return Script.SOURCE;
    }

    /**
     * @return the minimum value this signal can have
     */
    @Override
    public double getMinimum() {
        return minimum;
    }

    /**
     * @param minimum the minimum value this signal should be able to have
     */
    public void setMinimum(double minimum) {
        this.minimum = minimum;
    }

    /**
     * @return the maximum value this signal can have
     */
    @Override
    public double getMaximum() {
        return maximum;
    }

    /**
     * @param maximum the maximum value this signal should be able to have
     */
    public void setMaximum(double maximum) {
        this.maximum = maximum;
    }

    /**
     * The unit of this signal, used when displaying the value in the GUI.
     * @return the unit of this signal (empty String unless specifically set)
     */
    @Override
    public String getUnit() {
        return unit;
    }

    /**
     * @param unit the unit this signal should have
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * @return the factor this signal uses to calculate its value
     */
    @Override
    public double getFactor() {
        return factor;
    }

    /**
     * @param factor the factor this signal should use to calculate its value
     */
    public void setFactor(double factor) {
        this.factor = factor;
    }

    /**
     * @return the amount of decimals this signal uses
     */
    @Override
    public int getDecimalCount() {
        return 0;
    }

    /**
     * @return whether or not the value of this signal has decimals
     */
    @Override
    public boolean hasDecimals() {
        return false;
    }

    /**
     * @param listener the listener that should be notified in case of updates
     */
    @Override
    public void addListener(SignalListener listener) {
        signalListeners.add(listener);
    }

    /**
     * @param listener the listener that should be removed
     */
    @Override
    public void removeListener(SignalListener listener) {
        signalListeners.remove(listener);
    }

    /**
     * Notifies all listeners that have subscribed to this signal.
     */
    private void updateSignalListeners() {
        signalListeners.forEach(l -> l.valueUpdate(value, timestamp));
    }

    /**
     * @param listener the listener that should be notified when this signal
     * changes state (i.e. whether it is active or not).
     */
    @Override
    public void addStateListener(ReferenceStateListener listener) {
        stateListeners.add(listener);
    }

    /**
     * @param listener the state listener that should be removed
     */
    @Override
    public void removeStateListener(ReferenceStateListener listener) {
        stateListeners.remove(listener);
    }

    /**
     * Notifies all listeners in case this signal changes state (i.e. when it
     * becomes active or is deactivated).
     */
    private void updateStateListeners() {
        stateListeners.forEach(l -> l.stateUpdate());
    }

}
