/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.windows.im;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

/**
 *
 * @author Michiel Klifman
 */
public class RotateManipulation extends Manipulation {

    public static final String NAME = "Rotate";

    private double radians;

    @Override
    public void convertManipulation(double[] settings) {
        radians = Math.toRadians(settings[0]);
    }

    /**
     * Updates the settings for the manipulation
     *
     * @param g2d The graphics object of the image that is to be manipulated.
     */
    @Override
    public void applyManipulation(Graphics2D g2d, BufferedImage image) {
        int x = image.getWidth()/2;
        int y = image.getHeight()/2;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.rotate(radians, x, y);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String[] getPropertyNames() {
        return new String[]{"Angle"};
    }

    @Override
    public String[] getUnits() {
        return new String[]{"°"};
    }

    @Override
    public double[] getMinimumDefaults() {
        return new double[]{0.0};
    }

    @Override
    public double[] getMaximumDefaults() {
        return new double[]{360.0};
    }

    private Dimension getAreaRequired(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        int diagonal = (int) Math.ceil(Math.sqrt((width * width) + (height * height)));
        return new Dimension(diagonal, diagonal);
    }
}
