/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.sidepanel;

import nl.han.hantune.scripting.ScriptedSignal;
import nl.han.hantune.scripting.ScriptingManager;
import nl.han.hantune.scripting.ScriptingManagerListener;
import components.sidepanel.AbstractTree;
import components.sidepanel.SidePanel;
import components.sidepanel.TreeTransferHandler;
import java.awt.Component;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import nl.han.hantune.gui.sidepanel.tree.TransferableTreeNode;

/**
 * GUI component that holds the tree with scripted signals
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class ScriptedElementsPanel extends SidePanel implements ScriptingManagerListener {

    private final AbstractTree tree;

    /**
     * Create the tree and add it to the panel
     *
     * @param title the title the panel should display
     */
    public ScriptedElementsPanel(String title) {
        super(title);

        tree = new AbstractTree();
        tree.setDragEnabled(true);
        tree.setTransferHandler(new TreeTransferHandler(tree));
        tree.setModel(new DefaultTreeModel(null));
        tree.setRootVisible(false);
        tree.setShowsRootHandles(true);
        tree.setCellRenderer(new DefaultTreeCellRenderer() {
            ImageIcon scriptedSignalIcon = tree.createImageIcon("/images/tree/SCRIPTv4.png");

            @Override
            public Component getTreeCellRendererComponent(JTree tree,
                    Object value, boolean selected, boolean expanded,
                    boolean isLeaf, int row, boolean focused) {
                Component c = super.getTreeCellRendererComponent(tree, value,
                        selected, expanded, isLeaf, row, focused);
                if (!(value instanceof TransferableTreeNode)) {
                    return c;
                }
                TransferableTreeNode node = (TransferableTreeNode) value;

                if (node.isLeaf()) {
                    setIcon(scriptedSignalIcon);
                }
                return c;
            }
        });
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(tree);
        scrollPane.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
        this.add(scrollPane);
    }

    /**
     * Called when a ScriptedSignal is added or removed from the ScriptManager
     */
    @Override
    public void scriptedSignalUpdate() {
        TransferableTreeNode root = new TransferableTreeNode();
        List<ScriptedSignal> scriptedSigals = ScriptingManager.getInstance().getScriptedSignals();
        if (!scriptedSigals.isEmpty()) {
            TransferableTreeNode scriptedSignalsNode = buildScriptedSignalsNode(scriptedSigals);
            root.add(scriptedSignalsNode);
        }
        tree.setModel(new DefaultTreeModel(root));
        tree.expandTree();
    }

    /**
     * Rebuilds the tree after an update
     * @param signals - the list of signals to display in the tree
     * @return node holding all the scripted signal nodes.
     */
    private TransferableTreeNode buildScriptedSignalsNode(List<ScriptedSignal> signals) {
        TransferableTreeNode scriptedSignalsNode = new TransferableTreeNode();
        for (ScriptedSignal signal : signals) {
            TransferableTreeNode signalNode = new TransferableTreeNode(signal);
            scriptedSignalsNode.add(signalNode);
        }
        scriptedSignalsNode.setUserObject("Scripted Signals " + tree.getNodeSuffix(scriptedSignalsNode));
        return scriptedSignalsNode;
    }

}
