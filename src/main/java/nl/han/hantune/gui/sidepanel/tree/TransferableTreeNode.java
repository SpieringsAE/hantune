/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.sidepanel.tree;

import java.awt.datatransfer.DataFlavor;
import java.util.HashSet;
import java.util.Set;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class TransferableTreeNode extends DefaultMutableTreeNode {

    private final Set<DataFlavor> flavors = new HashSet<>();
    private boolean dragEnabled = true;

    public TransferableTreeNode() {
        super();
    }

    public TransferableTreeNode(Object userObject) {
        super(userObject);
        addDataFlavors(userObject);
    }

    @Override
    public void setUserObject(Object userObject) {
        super.setUserObject(userObject);
        addDataFlavors(userObject);
    }

    /**
     * Creates data flavors for the type of the given userObject and all
     * interfaces it implements.
     *
     * @param userObject
     */
    private void addDataFlavors(Object userObject) {
        if (userObject != null) {
            Class<?> type = userObject.getClass();
            DataFlavor flavor = new DataFlavor(type, type.getName());
            flavors.add(flavor);
            for (Class<?> interfaceType : type.getInterfaces()) {
                flavor = new DataFlavor(interfaceType, interfaceType.getName());
                flavors.add(flavor);
            }
        }
    }

    /**
     * @return an array of data flavors based on this node's user object and all
     * the interfaces the user object implements.
     */
    public DataFlavor[] getDataFlavors() {
        return flavors.toArray(new DataFlavor[flavors.size()]);
    }

    /**
     * @param flavorsToFind - the data flavors that need to be checked.
     * @return - whether or not this node has any of the given flavors.
     */
    public boolean hasFlavor(DataFlavor... flavorsToFind) {
        for (DataFlavor flavor : flavorsToFind) {
            if (flavors.contains(flavor)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return - whether or not this node can be dragged.
     */
    public boolean isDragEnabled() {
        return dragEnabled;
    }

    /**
     * @param dragEnabled - whether or not this node should be able to be
     * dragged.
     */
    public void setDragEnable(boolean dragEnabled) {
        this.dragEnabled = dragEnabled;
    }

}
