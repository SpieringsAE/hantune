/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.statusBar;

import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JLabel;

import ErrorMonitoring.ErrorObject;
import ErrorMonitoring.IErrorObserver;
import HANtune.HANtune;
import HANtune.HANtuneWindowFactory;
import util.MessagePane;

@SuppressWarnings("serial")
public class ErrorLabel extends JLabel implements IErrorObserver {
    private HANtune hanTune;

    ErrorLabel(HANtune hanTune) {
        super();
        this.hanTune = hanTune;

        setForeground(Color.WHITE);
        setBorder(
            javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
                javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        setOpaque(true);
        setPreferredSize(new java.awt.Dimension(95, 20));
        addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                labelErrorMouseClicked(evt);
            }
        });

    }


    void reset(boolean doResetText) {
        setBackground(HANtuneMainStatusBar.STATUSCOLOR_OFF);
        setForeground(Color.WHITE);
        setToolTipText("Click to open an error viewer");
        if (doResetText) {
            setText("Errors: OFF");
        }
    }


    private void labelErrorMouseClicked(java.awt.event.MouseEvent evt) {
        if (hanTune.isConnected() && hanTune.currentConfig.getXcpsettings().isErrorMonitoringEnabled()) {
            if (hanTune.currentConfig.getXcpsettings().isErrorMonitoringSupported()) {
                hanTune.currentConfig.getHANtuneManager().addWindowToTab(hanTune.getTabbedPane().getSelectedIndex(),
                    HANtuneWindowFactory.HANtuneWindowType.ErrorViewer);
            } else {
                MessagePane.showInfo("Controller does not support error monitoring.");
            }
        } else {
            MessagePane.showInfo("Please reconnect with error monitoring checked");
        }
    }

    /**
     * Callback method of the IErrorObserver interface. Gets called when new
     * error info is available that should be shown on the error label in the
     * status bar.
     *
     * @param active List with active (RAM) errors.
     * @param stored List with stored (EEPROM) errors.
     */
    @Override
    public void update(ArrayList<ErrorObject> active, ArrayList<ErrorObject> stored) {
        // update label on the status bar.
        int iActive = active.size();
        int iStored = stored.size();

        setText("Errors: A" + iActive + " / S" + iStored);
        if (iActive > 0) {
            setBackground(HANtuneMainStatusBar.STATUSCOLOR_ALERT);
        } else if (iActive == 0 && iStored > 0) {
            setBackground(HANtuneMainStatusBar.STATUSCOLOR_WARNING);
        } else {
            setBackground(HANtuneMainStatusBar.STATUSCOLOR_ON);
        }
    }


}
