/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.statusBar;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;

import HANtune.HANtune;

import static nl.han.hantune.gui.statusBar.HANtuneMainStatusBar.STATUSCOLOR_OFF;
import static nl.han.hantune.gui.statusBar.HANtuneMainStatusBar.STATUSCOLOR_ON;


@SuppressWarnings("serial")
public class DaqDxLabel extends JLabel {
    private HANtune hanTune;
    private int lblIdx;


    DaqDxLabel(HANtune hantune, int idx) {
        super();
        this.hanTune = hantune;
        this.lblIdx = idx;

        setName("" + idx);
        setText("D" + (idx + 1));
        setToolTipText("DAQ list " + (idx + 1));

        setBackground(STATUSCOLOR_OFF);
        setForeground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
            javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        setOpaque(true);
        setPreferredSize(new java.awt.Dimension(100, 20));

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                HANtuneMainStatusBar.handleFrequencyLabelEvent(evt);
            }
        }); // start dialog on right mouse click
    }

    void enablePrescalers() {
        if (hanTune.getDaqListId(lblIdx) != -1) {
            setText(hanTune.daqLists.get(hanTune.getDaqListId(lblIdx)).getScreenId());
            setName(String.valueOf(hanTune.getDaqListId(lblIdx)));
            setBackground(hanTune.isConnected() ? STATUSCOLOR_ON : STATUSCOLOR_OFF);
            setToolTipText("DAQ list: " + hanTune.daqLists.get(hanTune.getDaqListId(lblIdx)).getName());
        } else {
            setText("");
            setName("");
            setBackground(STATUSCOLOR_OFF);
            setToolTipText("DAQ list: " + (lblIdx + 1));
        }
    }

}
