/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.statusBar;

import java.awt.Color;
import java.awt.event.MouseEvent;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import HANtune.HANtune;
import HANtune.actions.LogReaderAction;
import nl.han.hantune.datahandling.DataLogger.LogDataWriterCsv;

@SuppressWarnings("serial")
public class DataLogLabel extends JLabel {
    private HANtune hanTune;
    private JPopupMenu dataLogPopupMenu;
    private JCheckBoxMenuItem mDataLogStartLog;
    private JCheckBoxMenuItem mDataLogReadWhileWrite;

    DataLogLabel(HANtune hantune) {
        super();
        this.hanTune = hantune;
        setForeground(Color.WHITE);
        setBorder(
            javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
                javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        setOpaque(true);
        setPreferredSize(new java.awt.Dimension(295, 20));
        addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                labelDataLogMouseClicked(evt);
            }
        });

        dataLogPopupMenu = new JPopupMenu();

        mDataLogStartLog = new JCheckBoxMenuItem();
        mDataLogStartLog.setText("Enable Datalogging");
        mDataLogStartLog.addActionListener(this::mDataLogStartLogPerformed);
        dataLogPopupMenu.add(mDataLogStartLog);

        mDataLogReadWhileWrite = new JCheckBoxMenuItem();
        mDataLogReadWhileWrite.setText("Enable Read while write log");
        mDataLogReadWhileWrite.addActionListener(this::mDataLogReadWhileWritePerformed);
        dataLogPopupMenu.add(mDataLogReadWhileWrite);

        dataLogPopupMenu.addPopupMenuListener(popupMenuListener);
        this.add(dataLogPopupMenu);
    }

     void init() {
        boolean enableLogger = hanTune.getUserPrefs().getBoolean("rememberLoggerState", false)
            && hanTune.getUserPrefs().getBoolean("loggerEnabled", false);
        LogDataWriterCsv.getInstance().setWritingStandBy(enableLogger);

        boolean enableReadWhileWriteLogger = hanTune.getUserPrefs().getBoolean("rememberLoggerState", false) &&
            hanTune.getUserPrefs().getBoolean("readWhileWriteLoggerEnabled", false);
        LogDataWriterCsv.getInstance().setReadWhileWriteStandby(enableReadWhileWriteLogger);

         updateComponents();
    }



    void updateComponents() {
        LogDataWriterCsv log = LogDataWriterCsv.getInstance();
        LogReaderAction dlg = LogReaderAction.getInstance();
        if (log.isWritingActive()) {
            if (dlg.isReadWhileWriteMode()) {
                setText("Read-Write Log: " + log.getFileName());
                setToolTipText(
                    "<html>Currently Read while write into logfile: <br>" + log.getFullFileName() + "<br> Click to stop logging.</html>");
            } else {
                setText("Logfile: " + log.getFileName());
                setToolTipText("<html>Logfile:<br>" + log.getFullFileName() + "<br> Click to stop datalogging.</html>");
            }
            setBackground(HANtuneMainStatusBar.STATUSCOLOR_ON);
        } else {
            setBackground(HANtuneMainStatusBar.STATUSCOLOR_OFF);
            if (log.isWritingStandby()) {
                if (log.isReadWhileWriteStandby()) {
                    setText("Logfile: STANDBY. Read while write: STANDBY");
                    setToolTipText("Read while write datalogging starts when connected. Click to disable");
                } else {
                    setText("Logfile: STANDBY");
                    setToolTipText("Datalogging starts when connected. Click to disable");
                }
            } else if (log.isReadWhileWriteStandby()) {
                setText("Logfile: OFF. Read while write: STANDBY");
                setToolTipText("Click to enable read while write datalogging");
            } else {
                setText("Log: OFF");
                setToolTipText("Click to enable datalogging");
            }
        }
    }


    private PopupMenuListener popupMenuListener = new PopupMenuListener() {
        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
            LogDataWriterCsv writer = LogDataWriterCsv.getInstance();

            mDataLogStartLog.setSelected(writer.isWritingActive() || writer.isWritingStandby());
            mDataLogReadWhileWrite.setSelected(LogDataWriterCsv.getInstance().isReadWhileWriteStandby());
        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            // not used
        }

        @Override
        public void popupMenuCanceled(PopupMenuEvent e) {
            // not used
        }
    };


    private void labelDataLogMouseClicked(java.awt.event.MouseEvent evt) {
        if (evt.getButton() == MouseEvent.BUTTON1) {
            LogDataWriterCsv writer = LogDataWriterCsv.getInstance();
            boolean isReadWhileWriteStandby = writer.isReadWhileWriteStandby();

            writer.setWritingStandBy(!writer.isWritingActive() && !writer.isWritingStandby());

            writer.enableDatalogging(writer.isWritingStandby(),
                writer.isWritingStandby() && isReadWhileWriteStandby, false);

            // ReadwhileWriteStandby is cleared during dispose(), so reset it here
            writer.setReadWhileWriteStandby(isReadWhileWriteStandby);
            hanTune.updateDatalogGuiComponents();
        } else if (evt.getButton() == MouseEvent.BUTTON3) {
            dataLogPopupMenu.show(this, evt.getX(), evt.getY());
        }
    }


    private void mDataLogStartLogPerformed(java.awt.event.ActionEvent evt) {
        LogDataWriterCsv writer = LogDataWriterCsv.getInstance();

        writer.setWritingStandBy(mDataLogStartLog.isSelected());
        writer.enableDatalogging(writer.isWritingStandby(), writer.isReadWhileWriteStandby(), false);

        hanTune.updateDatalogGuiComponents();
    }


    private void mDataLogReadWhileWritePerformed(java.awt.event.ActionEvent evt) {
        LogDataWriterCsv writer = LogDataWriterCsv.getInstance();
        writer.setReadWhileWriteStandby(mDataLogReadWhileWrite.isSelected());
        writer.enableDatalogging(writer.isWritingStandby(), writer.isReadWhileWriteStandby(), false);
        hanTune.updateDatalogGuiComponents();
    }
}
