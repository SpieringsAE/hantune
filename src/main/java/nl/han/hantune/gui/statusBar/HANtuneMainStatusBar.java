/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.statusBar;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;

import HANtune.HANtune;

public class HANtuneMainStatusBar {
    public static final Color STATUSCOLOR_ON = new Color(0, 180, 0);
    static final Color STATUSCOLOR_ALERT = new Color(220, 0, 0);
    static final Color STATUSCOLOR_OFF = new Color(150, 150, 150);
    static final Color STATUSCOLOR_WARNING = new Color(255, 153, 51);

    private static final int MAX_DAQ_LABELS = 3;

    private final JPanel statusPanel;
    private final ConnectedLabel connectedLabel;
    private final ErrorLabel errorLabel;
    private final BusloadProgressBar busloadBarProgress;
    private final DaqMaxLabel daqMaxLabel;
    private final List<DaqDxLabel> daqDxLabels;
    private final List<DaqDxFreqLabel> daqDxFreqLabels;
    private final RotatorLabel rotatorLabel;
    private final LagIndicatorBar lagIndicatorBar;
    private final DataLogLabel dataLogLabel;

    public HANtuneMainStatusBar(HANtune hantune) {
        statusPanel = new JPanel();
        connectedLabel = new ConnectedLabel(hantune);
        errorLabel = new ErrorLabel(hantune);
        busloadBarProgress = new BusloadProgressBar(hantune);
        daqMaxLabel = new DaqMaxLabel(hantune);

        daqDxLabels = new ArrayList<>();
        daqDxFreqLabels = new ArrayList<>();
        for (int idx = 0; idx < MAX_DAQ_LABELS; idx++) {
            daqDxLabels.add(new DaqDxLabel(hantune, idx));
            daqDxFreqLabels.add(new DaqDxFreqLabel(hantune, idx));
        }
        rotatorLabel = new RotatorLabel();
        lagIndicatorBar = new LagIndicatorBar(hantune);
        dataLogLabel = new DataLogLabel(hantune);

        initStatusPanel();
    }

    /**
     * Initializes the statusbar components
     */
    public void init() {
        errorLabel.reset(true);
        dataLogLabel.init();
    }

    public void setVisible(boolean visible) {
        statusPanel.setVisible(visible);
    }

    public void setConnected(boolean isConnect, String connectTxt) {
        connectedLabel.setConnected(isConnect, connectTxt);
        errorLabel.reset(isConnect);

        if (!isConnect) {
            rotatorLabel.resetRotator();
            lagIndicatorBar.clearLagValue();
        }
        busloadBarProgress.updateBusloadIndicator();
        daqMaxLabel.updateMaxFrequency();
        daqDxLabels.forEach(DaqDxLabel::enablePrescalers);
        lagIndicatorBar.resetMovingAvrg();
        dataLogLabel.updateComponents();
    }

    public void initErrorLabel() {
        errorLabel.reset(true);
    }

    public void updateBusloadIndicator() {
        busloadBarProgress.updateBusloadIndicator();
    }

    public void updateMaxDaqFrequency() {
        daqMaxLabel.updateMaxFrequency();
    }

    public void enableDaqPrescalers() {
        daqDxLabels.forEach(DaqDxLabel::enablePrescalers);
        daqDxFreqLabels.forEach(DaqDxFreqLabel::enablePrescalers);
    }

    public void updateDaqFrequency() {
        daqDxFreqLabels.forEach(DaqDxFreqLabel::updateFrequency);
    }

    public void updateDataLogComponents() {
        dataLogLabel.updateComponents();
    }

    public void resetRotator() {
        rotatorLabel.resetRotator();
    }

    public void rotatorWarning() {
        rotatorLabel.setWarning();
    }

    public void clearLag() {
        lagIndicatorBar.clearLagValue();
    }

    public void updateDaqListReceived() {
        rotatorLabel.stepRotator();
        lagIndicatorBar.updateLagIndicator();

    }

    public void resetLagMovingAvrg() {
        lagIndicatorBar.resetMovingAvrg();
    }


    static void handleFrequencyLabelEvent(MouseEvent event) {
        if (HANtune.getInstance().currentConfig.isServiceToolMode()) {
            return;
        }
        String name = ((JLabel) event.getSource()).getName();
        if (name != null) {
            HANtune.getInstance().currentConfig.getHANtuneManager().modifyDaqlist(Integer.parseInt(name));
        }
    }

    private void initStatusPanel() {
        statusPanel.setBackground(new java.awt.Color(204, 204, 204));
        statusPanel.setPreferredSize(new java.awt.Dimension(1105, 30));
        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(statusPanelLayout.createSequentialGroup().addGap(25, 25, 25)
                    .addComponent(connectedLabel, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(errorLabel, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(busloadBarProgress, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(daqMaxLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 73,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(4, 4, 4)
                    .addComponent(daqDxLabels.get(0), javax.swing.GroupLayout.PREFERRED_SIZE, 25,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(daqDxFreqLabels.get(0), javax.swing.GroupLayout.PREFERRED_SIZE, 50,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(daqDxLabels.get(1), javax.swing.GroupLayout.PREFERRED_SIZE, 25,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(daqDxFreqLabels.get(1), javax.swing.GroupLayout.PREFERRED_SIZE, 50,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(daqDxLabels.get(2), javax.swing.GroupLayout.PREFERRED_SIZE, 25,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(daqDxFreqLabels.get(2), javax.swing.GroupLayout.PREFERRED_SIZE, 50,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(rotatorLabel, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(lagIndicatorBar, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(dataLogLabel, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(142, Short.MAX_VALUE)));

        statusPanelLayout.setVerticalGroup(statusPanelLayout
            .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup().addGap(5, 5, 5)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(daqDxFreqLabels.get(0), javax.swing.GroupLayout.PREFERRED_SIZE, 20,
                        javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(daqDxLabels.get(0), javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(daqDxLabels.get(1), javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(daqDxFreqLabels.get(1), javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(daqDxLabels.get(2), javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(daqDxFreqLabels.get(2), javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(connectedLabel, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(errorLabel, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(busloadBarProgress, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(daqMaxLabel, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rotatorLabel, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lagIndicatorBar, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dataLogLabel, javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))));
    }

    public JPanel getStatusPanel() {
        return statusPanel;
    }

    public ErrorLabel getErrorObserver() {
        return errorLabel;
    }
}
