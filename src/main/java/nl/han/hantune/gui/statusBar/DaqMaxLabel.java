/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.statusBar;

import javax.swing.JLabel;

import HANtune.HANtune;

@SuppressWarnings("serial")
public class DaqMaxLabel extends JLabel {
    HANtune hantune;

    DaqMaxLabel(HANtune hantune) {
        super();
        this.hantune = hantune;

        setToolTipText("Maximum frequency for DAQ lists");
        setBorder(
            javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(),
                javax.swing.BorderFactory.createEmptyBorder(0, 3, 0, 3)));
        setEnabled(false);
        setPreferredSize(new java.awt.Dimension(90, 20));
    }


    void updateMaxFrequency() {
        int maxFrequency = Math.round(hantune.currentConfig.getXcpsettings().getSlaveFrequency() * 100) / 100;
        setText("Max: " + maxFrequency + "Hz");
        setToolTipText("Maximum frequency for DAQ lists: " + maxFrequency + "Hz");
        setEnabled(true);
    }

}
