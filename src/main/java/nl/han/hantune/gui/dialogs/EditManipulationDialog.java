/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.dialogs;

import HANtune.HanTuneDialog;
import datahandling.Signal;
import java.awt.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.*;
import nl.han.hantune.gui.windows.im.Manipulation;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public final class EditManipulationDialog extends HanTuneDialog {

    private final List<BoundsAndSettingsRow> settingRows = new ArrayList<>();
    private Box rowBox;
    private final Manipulation manipulation;
    private boolean editted;
    private JComboBox<String> boundsComboBox;

    public EditManipulationDialog(Manipulation manipulation, Signal signal, Window component) {
        super(true);
        this.manipulation = manipulation;
        setTitle("Edit " + manipulation.getName() + " Manipulation");
        setResizable(false);
        setLocationRelativeTo(component);
        setLocation(component.getX() + component.getWidth(), component.getY());

        JPanel panel = (JPanel) getContentPane();
        panel.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        createHeader();
        createBody();
        createFooter();
        pack();

        Map<Double, double[]> settings = manipulation.getBoundsAndProperties();
        for (Map.Entry<Double, double[]> entry : settings.entrySet()) {
            Double key = entry.getKey();
            double[] value = entry.getValue();
            createRow(key, value);
        }
    }

    public boolean editManipulation() {
        setVisible(true);
        return editted;
    }

    protected void createHeader() {
        Box header = Box.createVerticalBox();

        Box infoBox = Box.createHorizontalBox();

        JTextArea infoArea = new JTextArea("Edit the manipulation by setting signal values and corresponding values for the properties of the image. Values within bounds will be linearly interpolated. Values out of bounds can be saturated or extrapolated.");
        infoArea.setLineWrap(true);
        infoArea.setWrapStyleWord(true);
        infoArea.setEditable(false);
        infoArea.setFont(new Font("Tahoma", 0, 11));
        infoArea.setRows(3);
        infoArea.setBackground(this.getBackground());
        infoArea.setPreferredSize(new Dimension(100, 85));
        infoBox.add(infoArea);
        header.add(infoBox);

        Box buttonsBox = Box.createHorizontalBox();

        JLabel boundsLabel = new JLabel("Out of bounds: ");
        buttonsBox.add(boundsLabel);
        boundsComboBox = new JComboBox<>(new String[]{Manipulation.SATURATE, Manipulation.EXTRAPOLATE});
        boundsComboBox.setSelectedItem(manipulation.getMode());
        boundsComboBox.setMaximumSize(new Dimension(50, 21));
        buttonsBox.add(boundsComboBox);

        buttonsBox.add(Box.createHorizontalGlue());

        JButton addRowButton = new JButton("   Add   ");
        addRowButton.addActionListener(e -> createRow(0, new double[manipulation.getPropertyNames().length]));
        buttonsBox.add(addRowButton);

        header.add(buttonsBox);
        header.add(Box.createVerticalStrut(5));

        Box labelsBox = Box.createHorizontalBox();

        JLabel signalLabel = new JLabel(" Value");
        signalLabel.setPreferredSize(new Dimension(75, 20));
        labelsBox.add(signalLabel);

        labelsBox.add(Box.createHorizontalStrut(20));

        for (String settingName : manipulation.getPropertyNames()) {
            JLabel settingLabel = new JLabel(settingName);
            settingLabel.setPreferredSize(new Dimension(75, 20));
            labelsBox.add(settingLabel);
            labelsBox.add(Box.createHorizontalStrut(20));
        }

        labelsBox.add(Box.createHorizontalGlue());
        header.add(labelsBox);
        header.setBorder(BorderFactory.createEmptyBorder(3, 3, 0, 3));

        add(header, BorderLayout.NORTH);
    }

    protected void createBody() {
        rowBox = Box.createVerticalBox();
        rowBox.setBorder(BorderFactory.createEmptyBorder(0, 3, 3, 3));
        add(rowBox, BorderLayout.CENTER);
        pack();
    }

    private void createFooter() {
        Box footerBox = Box.createHorizontalBox();
        footerBox.add(Box.createHorizontalGlue());

        JButton okButton = new JButton("OK");
        okButton.setAlignmentX(Component.RIGHT_ALIGNMENT);
        okButton.addActionListener(e -> editAndClose());
        footerBox.add(okButton);

        footerBox.add(Box.createHorizontalStrut(5));

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> dispose());
        footerBox.add(cancelButton);
        footerBox.setBorder(BorderFactory.createEmptyBorder(20, 3, 3, 3));

        add(footerBox, BorderLayout.SOUTH);
    }

    private BoundsAndSettingsRow createRow(double bound, double[] settings) {
        BoundsAndSettingsRow row = new BoundsAndSettingsRow(bound, settings);
        row.setBorder(BorderFactory.createEmptyBorder(0, 0, 3, 0));
        settingRows.add(row);
        rowBox.add(row);
        pack();
        return row;
    }

    private void removeRow(BoundsAndSettingsRow row) {
        if (settingRows.size() > 2) {
            settingRows.remove(row);
            rowBox.remove(row);
            pack();
        }
    }

    private void editAndClose() {
        Map<Double, double[]> bounds = manipulation.getBoundsAndProperties();
        bounds.clear();
        for (BoundsAndSettingsRow row : settingRows) {
            Double bound = Double.parseDouble(row.boundField.getText());
            double[] settings = new double[row.settingFields.length];
            for (int i = 0; i < row.settingFields.length; i++) {
                settings[i] = Double.parseDouble(row.settingFields[i].getText());
                bounds.put(bound, settings);
            }
        }
        String selectedMode = (String) boundsComboBox.getSelectedItem();
        manipulation.setMode(selectedMode);
        editted = true;
        dispose();
    }

    private final class BoundsAndSettingsRow extends Box {

        private final JTextField boundField;
        private final JTextField settingFields[];

        public BoundsAndSettingsRow(double bound, double[] settings) {
            super(BoxLayout.X_AXIS);

            boundField = createTextField(String.valueOf(bound));
            add(boundField);
            add(new JLabel("     "));

            settingFields = new JTextField[settings.length];
            for (int i = 0; i < settings.length; i++) {
                settingFields[i] = createTextField(String.valueOf(settings[i]));
                add(settingFields[i]);
                add(new JLabel(" " + manipulation.getUnits()[i] + "  "));
            }

            JButton removeButton = new JButton("Remove");
            removeButton.addActionListener(e -> removeRow(this));
            add(removeButton);
        }

        private JTextField createTextField(String text) {
            JTextField textField = new JTextField(text);
            textField.setPreferredSize(new Dimension(75, 21));
            textField.setMaximumSize(textField.getPreferredSize());
            textField.setHorizontalAlignment(SwingConstants.RIGHT);
            return textField;
        }
    }

}
