/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.dialogs;

import datahandling.CurrentConfig;
import datahandling.DescriptionFile;
import datahandling.Reference;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 *
 * @author Michiel Klifman
 * @param <T> the type of Reference (e.g. Signal, Parameter, Table)
 */
public class GetReferenceDialog<T extends Reference> {

    private Class<T> type;

    public GetReferenceDialog(Class<T> type) {
        this.type = type;
    }

    public T showDialog() {
        return showDialog(null);
    }

    public T showDialog(List<T> references) {
        JPanel contentPanel = new JPanel(new BorderLayout(5, 5));

        JPanel labels = new JPanel(new GridLayout(0, 1, 2, 2));
        labels.add(new JLabel("Source", SwingConstants.RIGHT));
        labels.add(new JLabel(type.getSimpleName(), SwingConstants.RIGHT));
        contentPanel.add(labels, BorderLayout.WEST);

        JPanel controls = new JPanel(new GridLayout(0, 1, 2, 2));
        DescriptionFile[] descriptionFiles = CurrentConfig.getInstance().getDescriptionFiles().stream()
                .filter(d -> !d.getReferencesOfType(type).isEmpty())
                .toArray(DescriptionFile[]::new);
        JComboBox<DescriptionFile> fileComboBox = new JComboBox<>(descriptionFiles);
        JComboBox<Reference> referenceComboBox = new JComboBox<>();
        fileComboBox.addActionListener(e -> {
            DescriptionFile file = (DescriptionFile) fileComboBox.getSelectedItem();
            List<T> referenceList = file.getReferencesOfType(type).stream()
                    .filter(reference -> references == null || !references.contains(reference))
                    .collect(Collectors.toList());
            Reference[] referenceArray = referenceList.toArray(new Reference[referenceList.size()]);
            referenceComboBox.setModel(new DefaultComboBoxModel<>(referenceArray));
        });
        fileComboBox.setSelectedIndex(0);
        controls.add(fileComboBox);
        controls.add(referenceComboBox);
        contentPanel.add(controls, BorderLayout.CENTER);

        int option = JOptionPane.showConfirmDialog(null, contentPanel, "Add " + type.getSimpleName(), JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            return type.cast(referenceComboBox.getSelectedItem());
        } else {
            return null;
        }
    }
}
