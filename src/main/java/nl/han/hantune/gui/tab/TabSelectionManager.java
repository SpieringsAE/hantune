/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.tab;

import HANtune.HANtune;
import HANtune.HANtuneTab;
import HANtune.HANtuneWindow;
import datahandling.CurrentConfig;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.stream.Collectors;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

/**
 * Handles (multi) selection of HANtuneWindows in a HANtuneTab and enables them
 * to be moved as a group.
 *
 * @author Michiel Klifman
 */
public class TabSelectionManager extends MouseSelectionAdapter {

    private final HANtuneTab tab;

    public TabSelectionManager(HANtuneTab tab) {
        this.tab = tab;
    }

    @Override
    public void mousePressed(MouseEvent event) {
        if (tab.isResizeAndMoveModeEnabled()) {
            super.mousePressed(event);
        }
    }

    @Override
    public void mouseDragged(MouseEvent event) {
        if (tab.isResizeAndMoveModeEnabled()) {
            super.mouseDragged(event);
        }
    }

    @Override
    public void mouseReleased(MouseEvent event) {
        if (SwingUtilities.isLeftMouseButton(event) && tab.isResizeAndMoveModeEnabled()) {
            super.mouseReleased(event);
            Rectangle selection = getSelection();
            tab.getWindows().forEach(window -> window.setSelectedInTab(
                    selection.contains(window.getBounds())
                            ? !(event.isControlDown() && window.isSelectedInTab())
                            : (event.isControlDown() || event.isShiftDown()) && window.isSelectedInTab()
            ));
            updateMoverInsets();
        }
    }

    /**
     * @return A list of all selected HANtuneWindows in this manager's tab
     */
    public List<HANtuneWindow> getSelectedWindows() {
        return tab.getWindows().stream()
                .filter(w -> w.isSelectedInTab())
                .collect(Collectors.toList());
    }

    /**
     * Close all windows that are currently selected.
     */
    public void closeSelectedWindows() {
        int windowCount = getSelectedWindows().size();

        for (HANtuneWindow window : getSelectedWindows()) {
            window.setMarkedForDeletion(true);
        }
        if (windowCount > 0 && HANtune.showConfirm("Are you sure you want to close all " + windowCount + " selected windows?") == 0) {
            for (HANtuneWindow window : getSelectedWindows()) {
                window.forceCloseAction();
            }
        } else {

            for (HANtuneWindow window : getSelectedWindows()) {
                window.setMarkedForDeletion(false);
            }
        }
    }

    /**
     * Adds the required mouse and component listeners to the supplied window so
     * it can be selected and moved as a group.
     *
     * @param window the window to add the listeners to
     */
    public void subscribeWindow(HANtuneWindow window) {
        Point point = new Point();
        window.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent event) {
                point.x = window.getX();
                point.y = window.getY();
            }
        });


        window.getGlassPane().addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                if (!window.isSelectedInTab() && !event.isControlDown()) {
                    tab.getWindows().forEach(w -> w.setSelectedInTab(w.equals(window)));
                    updateMoverInsets();
                }
                point.x = window.getX();
                point.y = window.getY();
            }

            @Override
            public void mouseClicked(MouseEvent event) {
                if (SwingUtilities.isLeftMouseButton(event)) {
                    if (event.isControlDown()) {
                        window.setSelectedInTab(!window.isSelectedInTab());
                    } else if (window.isSelectedInTab()) {
                        tab.getWindows().forEach(w -> w.setSelectedInTab(w.equals(window)));
                    }
                    updateMoverInsets();
                } else if (!CurrentConfig.getInstance().isServiceToolMode()) {
                    JPopupMenu menu = new JPopupMenu();
                    JMenuItem menuItem = new JMenuItem("Delete selected windows");
                    menuItem.addActionListener(e -> closeSelectedWindows());
                    menu.add(menuItem);
                    menu.show(event.getComponent(), event.getX(), event.getY());
                }
            }
        });

        window.getGlassPane().addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent event) {
                if (!window.isSelectedInTab()) {
                    window.setSelectedInTab(true);
                }
            }
        });

        window.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentMoved(ComponentEvent event) {
                if (window.hasFocus()) {
                    for (HANtuneWindow selectedWindow : getSelectedWindows()) {
                        if (window != selectedWindow) {
                            int newX = selectedWindow.getX() + (window.getX() - point.x);
                            int newY = selectedWindow.getY() + (window.getY() - point.y);
                            if (newX >= 0 && newY >= 0) {
                                selectedWindow.setLocation(newX, newY);
                            }
                        }
                    }
                    point.x = window.getX();
                    point.y = window.getY();
                }
            }
        });
    }

    /**
     * Update the edge insets of all selected HANtuneWindow's ComponentMovers.
     * The insets determine how far a group of windows can move.
     */
    private void updateMoverInsets() {
        Rectangle bounds = getSelectionBounds();
        for (HANtuneWindow window : getSelectedWindows()) {
            Insets insets = new Insets(window.getY() - bounds.y, window.getX() - bounds.x, -500, -500);
            window.getMover().setEdgeInsets(insets);
        }
    }

    /**
     * @return the bounds of all selected windows as a group
     */
    private Rectangle getSelectionBounds() {
        List<HANtuneWindow> selectedWindows = getSelectedWindows();
        Rectangle bounds = selectedWindows.isEmpty() ? new Rectangle() : selectedWindows.get(0).getBounds();
        for (HANtuneWindow window : selectedWindows) {
            if (window.getX() < bounds.x) {
                bounds.x = window.getX();
            }
            if (window.getY() < bounds.y) {
                bounds.y = window.getY();
            }
            if (window.getX() + window.getWidth() > bounds.width) {
                bounds.width = window.getX() + window.getWidth();
            }
            if (window.getY() + window.getHeight() > bounds.height) {
                bounds.height = window.getY() + window.getHeight();
            }
        }
        bounds.width -= bounds.x;
        bounds.height -= bounds.y;
        return bounds;
    }
}
