/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.tab;

import java.awt.BasicStroke;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingUtilities;

/**
 * An extension of MouseAdapter that can draw a "selection rectangle". Add the
 * adapter as both a Mouse- and MouseMotionListener to the component on which to
 * draw the selection on. Override the component's paint method and call
 * drawSelectionRectangle(g) to draw the selection.
 *
 * @author Michiel Klifman
 */
public class MouseSelectionAdapter extends MouseAdapter {

    private int pressedX, pressedY;
    private Rectangle selection;
    private boolean dragging;
    private final BasicStroke selectionStroke = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 1.0f, new float[]{5.0f}, 0.0f);

    @Override
    public void mousePressed(MouseEvent event) {
        if (SwingUtilities.isLeftMouseButton(event)) {
            pressedX = event.getX();
            pressedY = event.getY();
            selection = new Rectangle();
        }
    }

    @Override
    public void mouseReleased(MouseEvent event) {
        if (SwingUtilities.isLeftMouseButton(event)) {
            dragging = false;
            event.getComponent().repaint();
        }
    }

    @Override
    public void mouseDragged(MouseEvent event) {
        if (SwingUtilities.isLeftMouseButton(event)) {
            Component component = event.getComponent();
            dragging = true;
            int draggedX = event.getX();
            int draggedY = event.getY();

            if (draggedX < component.getX()) {
                selection.x = component.getX();
                selection.width = pressedX;
            } else if (draggedX > component.getWidth()) {
                selection.x = pressedX;
                selection.width = component.getWidth() - pressedX;
            } else if (pressedX < draggedX) {
                selection.x = pressedX;
                selection.width = draggedX - pressedX;
            } else if (pressedX > draggedX) {
                selection.x = draggedX;
                selection.width = pressedX - draggedX;
            }

            if (draggedY < component.getY()) {
                selection.y = component.getY();
                selection.height = pressedY;
            } else if (draggedY > component.getHeight()) {
                selection.y = pressedY;
                selection.height = component.getHeight() - pressedY;
            } else if (pressedY < draggedY) {
                selection.y = pressedY;
                selection.height = draggedY - pressedY;
            } else if (pressedY > draggedY) {
                selection.y = draggedY;
                selection.height = pressedY - draggedY;
            }
            component.repaint();
        }
    }

    /**
     * @param g the graphics on which to draw the "selection rectangle on"
     */
    public void drawSelectionRectangle(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(selectionStroke);
        g2.drawRect(selection.x, selection.y, selection.width, selection.height);
    }

    /**
     * @return whether or not the MouseSelectionAdapter is currently dragging
     */
    public boolean isDragging() {
        return dragging;
    }

    /**
     * @return the selection
     */
    public Rectangle getSelection() {
        return selection;
    }
}
