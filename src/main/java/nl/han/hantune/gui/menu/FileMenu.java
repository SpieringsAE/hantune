/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JPopupMenu.Separator;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import HANtune.HANtune;
import HANtune.HANtuneProject;
import HANtune.UserPreferences;
import HANtune.actions.CreateCalibration;
import HANtune.actions.FlashCalibration;
import HANtune.actions.LogReaderAction;
import datahandling.CurrentConfig;
import nl.han.hantune.datahandling.DataLogger.LogDataWriterCsv;
import util.MessagePane;

@SuppressWarnings({"serial"})
public class FileMenu extends JMenu {
    private HANtune hanTune;
    private HANtuneProject hanTuneProject;

    private MenuItem newProjectMenuItem;
    private MenuItem closeProjectMenuItem;
    private MenuItem saveMenuItem;
    private MenuItem saveAsMenuItem;
    private JMenu newSubMenu;
    private JMenu recentProjectsSubMenu;
    private JMenu importSubMenu;
    private JMenu exportSubMenu;
    private MenuItem exportServiceProjectMenuItem;
    private Separator itemSeparator1;
    private Separator itemSeparator2;
    private Separator itemSeparator3;
    private Separator itemSeparator4;
    private MenuItem flashTargetMenuItem;
    private MenuItem playLogFileMenuItem;
    private JCheckBoxMenuItem readWhileWriteCheckboxMenuItem;
    private Preferences userPrefs = Preferences.userNodeForPackage(UserPreferences.class);


    FileMenu(HANtune hantune) {
        this.hanTune = hantune;
        this.hanTuneProject = hantune.getHanTuneProject();
        setText("File");
        setMnemonic('F');

        newProjectMenuItem =
            new MenuItem.Builder("New Project")
                .acceleratorKey(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK)
                .icon("/images/tree/PROJECT.png")
                .actionListener(this::newProjectMenuItemActionPerformed)
                .build();
        add(newProjectMenuItem);

        itemSeparator1 = (Separator) add(new Separator());

        MenuItem openProjectMenuItem = new MenuItem.Builder("Open Project...")
            .acceleratorKey(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK)
            .icon("/images/open.png")
            .actionListener(this::openMenuItemActionPerformed)
            .build();
        add(openProjectMenuItem);


        recentProjectsSubMenu = new JMenu("Recent Projects");
        add(recentProjectsSubMenu);
        recentProjectsSubMenu.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent me) {
                rebuildRecentProjectsMenu(userPrefs);
            }
        });
        add(recentProjectsSubMenu);

        closeProjectMenuItem = new MenuItem.Builder("Close Project")
            .actionListener(this::closeMenuItemActionPerformed)
            .build();
        add(closeProjectMenuItem);

        saveMenuItem =
            new MenuItem.Builder("Save Project")
                .acceleratorKey(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK)
                .icon("/images/save.png")
                .actionListener(this::saveMenuItemActionPerformed)
                .build();
        add(saveMenuItem);


        saveAsMenuItem = new MenuItem.Builder("Save Project As...")
            .actionListener(this::saveAsMenuItemActionPerformed)
            .build();
        add(saveAsMenuItem);


        itemSeparator2 = new Separator();
        add(itemSeparator2);

        // Submenu: New
        newSubMenu = new JMenu();
        newSubMenu.setText("New");

        MenuItem newDaqlistMenuItem = new MenuItem.Builder("New DAQ list")
            .icon("/images/tree/DAQLIST.png")
            .actionListener(this::newDaqlistMenuItemActionPerformed)
            .build();
        newSubMenu.add(newDaqlistMenuItem);

        MenuItem newLayoutMenuItem = new MenuItem.Builder("New Layout")
            .icon("/images/tree/LAYOUT.png")
            .actionListener(this::newLayoutMenuItemActionPerformed)
            .build();
        newSubMenu.add(newLayoutMenuItem);


        MenuItem newCalibrationMenuItem = new MenuItem.Builder("New Calibration")
            .icon("/images/tree/CALIBRATION.png")
            .actionListener(this::newCalibrationMenuItemActionPerformed)
            .build();
        newSubMenu.add(newCalibrationMenuItem);
        add(newSubMenu); // add New submenu to file menu

        // Submenu: Import
        importSubMenu = new JMenu();
        importSubMenu.setText("Import");


        MenuItem importASAP2MenuItem = new MenuItem.Builder("ASAP2 File")
            .icon("/images/tree/ASAP2.png")
            .actionListener(this::importASAP2MenuItemActionPerformed)
            .build();
        importSubMenu.add(importASAP2MenuItem);

        MenuItem importDbcMenuItem = new MenuItem.Builder("DBC")
            .icon("/images/sidepanel/dbc.png")
            .actionListener(this::importDbcActionPerformed)
            .build();
        importSubMenu.add(importDbcMenuItem);

        MenuItem importDataMenuItem = new MenuItem.Builder("Import Data")
            .actionListener(this::importDataMenuItemActionPerformed)
            .build();
        importSubMenu.add(importDataMenuItem);
        add(importSubMenu); // add Import submenu to file menu

        // Submenu: Export
        exportSubMenu = new JMenu();
        exportSubMenu.setText("Export");

        exportServiceProjectMenuItem = new MenuItem.Builder("Service tool Project...")
            .toolTiptext("Export current project as Service Tool project file")
            .actionListener(this::exportServiceProjectMenuItemActionPerformed)
            .build();
        exportSubMenu.add(exportServiceProjectMenuItem);
        add(exportSubMenu); // add Export submenu to file menu

        itemSeparator3 = new Separator();
        add(itemSeparator3);

        flashTargetMenuItem = new MenuItem.Builder("Flash Target...")
            .toolTiptext("Select a file to flash directly into target")
            .actionListener(this::flashTargetActionPerformed)
            .build();
        add(flashTargetMenuItem);

        playLogFileMenuItem = new MenuItem.Builder("Read log file...")
            .toolTiptext("Select a logfile to examine")
            .actionListener(this::readLogActionPerformed)
            .build();
        add(playLogFileMenuItem);

        readWhileWriteCheckboxMenuItem = new JCheckBoxMenuItem("Enable Read while write log");
        readWhileWriteCheckboxMenuItem.setToolTipText("Write log while playback elsewhere in same logfile. Activated when writing to log");
        readWhileWriteCheckboxMenuItem.addActionListener(evt -> readWhileWriteLogActionPerformed());
        add(readWhileWriteCheckboxMenuItem);

        itemSeparator4 = new Separator();
        add(itemSeparator4);

        MenuItem exitMenuItem = new MenuItem.Builder("Exit")
            .acceleratorKey(KeyEvent.VK_F4, InputEvent.ALT_DOWN_MASK)
            .actionListener(this::exitMenuItemActionPerformed)
            .build();
        add(exitMenuItem);

        addMenuListener(fileMenuListener);

    }

    /*
     * Reconstructs the Recent Projects menu under the File Menus,
     * based on the "lastProjectPathList" string saved in userPreferences
     */
    public JMenu rebuildRecentProjectsMenu(Preferences userPrefs) {
        recentProjectsSubMenu.removeAll();
        MenuItem menuItem = null;
        List<String> projectPaths = CurrentConfig.getInstance().getHANtuneManager().getLastProjectPathList(userPrefs);

        for (String path : projectPaths) {                                      // add an actionListener to each menuItem with a reference to the project to be loaded
            menuItem = new MenuItem.Builder(path)
                .actionListener(evt -> menuItemRecentActionPerformed(path))
                .build();
            recentProjectsSubMenu.add(menuItem, 0);                              // add new menuItem to the top of the list
        }

        if (menuItem == null) {      // give a sensible hint towards the user that there is no recent project yet
            menuItem = new MenuItem.Builder("empty...").build();
            menuItem.setEnabled(false);
            recentProjectsSubMenu.add(menuItem);
        }
        return recentProjectsSubMenu;
    }

    private void menuItemRecentActionPerformed(String project) {
        File projectFile = new File(project);
        if (hanTuneProject.isProceedOKCheckModifiedProjectSave()) {
            hanTuneProject.openProjectFile(projectFile);
        }
    }

    void updateItemsServiceModeVisibility() {
        boolean isNormalMode = !CurrentConfig.getInstance().isServiceToolMode();

        // Set only service tool related menuItems
        newProjectMenuItem.setVisible(isNormalMode);
        itemSeparator1.setVisible(isNormalMode);

        closeProjectMenuItem.setVisible(isNormalMode);
        saveMenuItem.setVisible(isNormalMode);
        saveAsMenuItem.setVisible(isNormalMode);
        exportServiceProjectMenuItem.setVisible(isNormalMode);
        itemSeparator2.setVisible(isNormalMode);

        newSubMenu.setVisible(isNormalMode);
        importSubMenu.setVisible(isNormalMode);
        exportSubMenu.setVisible(isNormalMode);
        itemSeparator3.setVisible(isNormalMode);

        itemSeparator4.setVisible(isNormalMode);

        flashTargetMenuItem.setVisible(isNormalMode);
        playLogFileMenuItem.setVisible(isNormalMode);
    }

    private void newProjectMenuItemActionPerformed(ActionEvent evt) {
        if (hanTuneProject.isProceedOKCheckModifiedProjectSave()) {
            hanTuneProject.newProject();
        }
    }


    private void openMenuItemActionPerformed(ActionEvent evt) {
        if (hanTuneProject.isProceedOKCheckModifiedProjectSave()) {
            hanTuneProject.selectAndOpenProjectFile();
        }
    }


    private void closeMenuItemActionPerformed(ActionEvent evt) {
        if (hanTuneProject.isProceedOKCheckModifiedProjectSave()) {
            hanTuneProject.closeProject();
        }
    }


    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
          hanTuneProject.saveCurrentProject(true);
    }


    private void saveAsMenuItemActionPerformed(ActionEvent evt) {
        hanTuneProject.saveAsCurrentProject();
    }


    private void exportServiceProjectMenuItemActionPerformed(ActionEvent evt) {
        hanTuneProject.requestSaveAsServiceProject();
    }


    private void newDaqlistMenuItemActionPerformed(ActionEvent evt) {
        createNewDAQlist();
    }


    private void newLayoutMenuItemActionPerformed(ActionEvent evt) {
        String name = HANtune.showQuestion("New layout", "Name:", "");
        if (name == null) {
            return; // cancel has been pressed.
        }

        CurrentConfig.getInstance().createNewLayout(name);
    }

    private void newCalibrationMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        CreateCalibration createCalib = new CreateCalibration(hanTune);
        createCalib.createNewCalibration();
    }

    private void importASAP2MenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        hanTune.requestOpenASAP2();
    }


    private void importDbcActionPerformed(java.awt.event.ActionEvent evt) {
        hanTuneProject.importDbcData();
    }


    private void importDataMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        if (hanTuneProject.isProceedOKCheckModifiedProjectSave()) {
            hanTuneProject.importProjectData();
        }
    }


    private void flashTargetActionPerformed(java.awt.event.ActionEvent evt) {
        // Flash a file
        FlashCalibration flashSRec = new FlashCalibration(hanTune);
        flashSRec.chooseFilenameAndFlash();
    }

    private void readLogActionPerformed(java.awt.event.ActionEvent evt) {
        // read a logfile
        LogReaderAction.getInstance().openReaderActionDialog();
    }


    private void readWhileWriteLogActionPerformed() {
        LogDataWriterCsv writer = LogDataWriterCsv.getInstance();
        writer.setReadWhileWriteStandby(readWhileWriteCheckboxMenuItem.isSelected());
        writer.enableDatalogging(writer.isWritingStandby(), writer.isReadWhileWriteStandby(), false);

        hanTune.updateDatalogGuiComponents();
    }


    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        hanTune.requestExitHANtune();
    }


    private void createNewDAQlist() {
        // ASAP2 file loaded?
        if (hanTune.getActiveAsap2Id() == -1) {
            MessagePane.showInfo("Please load an ASAP2 file first!");
            return;
        }

        // show popup to enter application
        String name = "";
        boolean validName = false;
        while (!validName) {
            name = HANtune.showQuestion("New DAQ list - Enter name", "Enter the name of the DAQ list:", "");
            if (name == null)
                break;
            validName = true;
            if (name.equals("")) {
                MessagePane.showError("Please enter a valid name");
                validName = false;
            }
            for (DAQList.DAQList DAQList : hanTune.daqLists) {
                if (DAQList.getName().equals(name)) {
                    MessagePane
                        .showError("A DAQlist with this name already exists\n please choose another name");
                    validName = false;
                }
            }
        }

        // perform add action
        if (name != null) {
            hanTune.currentConfig.getHANtuneManager().newDaqlist(name, true);
        }

    }

    private MenuListener fileMenuListener = new MenuListener() {
        @Override
        public void menuSelected(MenuEvent e) {
            readWhileWriteCheckboxMenuItem.setSelected(LogDataWriterCsv.getInstance().isReadWhileWriteStandby());
        }

        @Override
        public void menuDeselected(MenuEvent e) {
            // nothing to be done here
        }

        @Override
        public void menuCanceled(MenuEvent e) {
            // nothing to be done here
        }
    };
}
