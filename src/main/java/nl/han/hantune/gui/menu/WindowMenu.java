/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JOptionPane;

import HANtune.HANtune;
import HANtune.HANtuneManager;
import HANtune.HANtuneTab;
import HANtune.HANtuneWindowFactory;
import HANtune.HANtuneWindowFactory.HANtuneWindowType;
import datahandling.CurrentConfig;
import util.MessagePane;

// suppress: S110, Inheritance tree of classes should not be too deep
// suppress: S1612 Lambdas should be replaced with method references
// suppress: S1172 Unused method parameters should be removed
@SuppressWarnings({"java:S110", "java:S1612", "java:S1172"})
class WindowMenu extends JMenu {
    private HANtune hanTune;
    private MenuItem newTabMenuItem;
    private MenuItem newViewerMenuItem;
    private MenuItem newEditorMenuItem;
    private MenuItem newTableEditorMenuItem;
    private MenuItem newTextLabelMenuItem;
    private MenuItem newImageMarkupMenuItem;
    private MenuItem newErrorViewer;
    private MenuItem newScriptWindowMenuItem;
    private MenuItem resizeMenuItem;
    private CheckBoxMenuItem chromelessMenuItem;
    private CheckBoxMenuItem chromelessTransparentMenuItem;


    WindowMenu(HANtune hantune) {
        this.hanTune = hantune;
        setText("Window");
        setMnemonic('W');


        newTabMenuItem = new MenuItem.Builder("New Tab")
            .acceleratorKey(KeyEvent.VK_T, InputEvent.CTRL_DOWN_MASK)
            .actionListener(evt -> newTabMenuItemActionPerformed(evt))
            .build();
        add(newTabMenuItem);

        newViewerMenuItem = new MenuItem.Builder("New Viewer")
            .actionListener(evt -> newViewerMenuItemActionPerformed(evt))
            .build();
        add(newViewerMenuItem);

        newEditorMenuItem = new MenuItem.Builder("New Editor")
            .actionListener(evt -> newEditorMenuItemActionPerformed(evt))
            .build();
        add(newEditorMenuItem);

        newTableEditorMenuItem = new MenuItem.Builder("New TableEditor")
            .actionListener(evt -> newTableEditorMenuItemActionPerformed(evt))
            .build();
        add(newTableEditorMenuItem);

        newTextLabelMenuItem = new MenuItem.Builder("New TextMarkup")
            .acceleratorKey(KeyEvent.VK_L, InputEvent.CTRL_DOWN_MASK)
            .actionListener(evt -> newTextLabelMenuItemActionPerformed(evt))
            .build();
        add(newTextLabelMenuItem);

        newImageMarkupMenuItem = new MenuItem.Builder("New ImageMarkup")
            .acceleratorKey(KeyEvent.VK_I, InputEvent.CTRL_DOWN_MASK)
            .actionListener(evt -> newImageMarkupMenuItemActionPerformed(evt))
            .build();
        add(newImageMarkupMenuItem);

        newErrorViewer = new MenuItem.Builder("New Error Viewer")
            .acceleratorKey(KeyEvent.VK_E, InputEvent.CTRL_DOWN_MASK)
            .actionListener(evt -> newErrorViewerActionPerformed(evt))
            .build();
        add(newErrorViewer);

        newScriptWindowMenuItem = new MenuItem.Builder("New Script Window")
                .actionListener(e -> {
                    HANtuneManager manager = hanTune.currentConfig.getHANtuneManager();
                    int currentTabIndex = hanTune.getTabbedPane().getSelectedIndex();
                    manager.addWindowToTab(currentTabIndex, HANtuneWindowType.ScriptWindow);
                })
                .build();
        add(newScriptWindowMenuItem);

        addSeparator();

        resizeMenuItem = new MenuItem.Builder("Resize and Move mode")
            .acceleratorKey(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK)
            .actionListener(evt -> resizeMenuItemActionPerformed(evt))
            .build();
        add(resizeMenuItem);

        addSeparator();

        chromelessMenuItem = new CheckBoxMenuItem.Builder("Chromeless")
            .acceleratorKey(KeyEvent.VK_F11, 0)
            .actionListener(evt -> hantune.setFullScreen(false))
            .toolTiptext("Chromeless mode for current tab. Back with Esc key.")
            .build();
        add(chromelessMenuItem);

        chromelessTransparentMenuItem = new CheckBoxMenuItem.Builder("Chromeless transparent")
            .acceleratorKey(KeyEvent.VK_F11, InputEvent.SHIFT_DOWN_MASK)
            .actionListener(evt -> hantune.setFullScreen(true))
            .toolTiptext("Transparent chromeless mode for current tab. Back with Esc key.")
            .build();
        add(chromelessTransparentMenuItem);
    }


    void updateItemsServiceModeVisibility() {
        this.setVisible(!CurrentConfig.getInstance().isServiceToolMode());
    }


    void enableLayoutItems(boolean enabled) {
        newTabMenuItem.setEnabled(enabled);
        newViewerMenuItem.setEnabled(enabled);
        newEditorMenuItem.setEnabled(enabled);
        newTableEditorMenuItem.setEnabled(enabled);
        newTextLabelMenuItem.setEnabled(enabled);
        newImageMarkupMenuItem.setEnabled(enabled);
        newErrorViewer.setEnabled(enabled);
        newScriptWindowMenuItem.setEnabled(enabled);
        chromelessMenuItem.setEnabled(enabled);
        chromelessTransparentMenuItem.setEnabled(enabled);
    }


    void updateChromelessStateItems(boolean isChromeless, boolean isTransparent) {
        if (isChromeless) {
            if (isTransparent) {
                chromelessTransparentMenuItem.setSelected(true);
                chromelessMenuItem.setSelected(false);
            }
        } else {
            chromelessMenuItem.setSelected(false);
            chromelessTransparentMenuItem.setSelected(false);
        }
    }


    private void newTabMenuItemActionPerformed(ActionEvent evt) {
        // add new tab
        hanTune.currentConfig.getHANtuneManager().addTab();
    }


    private void newViewerMenuItemActionPerformed(ActionEvent evt) {
        // add a new viewer component
        HANtuneWindowFactory.HANtuneWindowType opt =
            (HANtuneWindowFactory.HANtuneWindowType)JOptionPane.showInputDialog(this,
                "Select a viewer type:", HANtune.applicationName + " - Add Viewer",
                JOptionPane.QUESTION_MESSAGE, null, HANtuneWindowFactory.HANtuneViewerType.toArray(), null);
        if (opt != null) {
            hanTune.currentConfig.getHANtuneManager()
                .addWindowToTab(hanTune.getTabbedPane().getSelectedIndex(), opt);
        }
    }


    private void newEditorMenuItemActionPerformed(ActionEvent evt) {
        // add new editor component
        HANtuneWindowFactory.HANtuneWindowType opt =
            (HANtuneWindowFactory.HANtuneWindowType)JOptionPane.showInputDialog(this,
                "Select a editor type:", HANtune.applicationName + " - Add Editor",
                JOptionPane.QUESTION_MESSAGE, null, HANtuneWindowFactory.HANtuneEditorType.toArray(), null);
        if (opt != null) {
            hanTune.currentConfig.getHANtuneManager()
                .addWindowToTab(hanTune.getTabbedPane().getSelectedIndex(), opt);
        }
    }


    private void newTableEditorMenuItemActionPerformed(ActionEvent evt) {
        // add new editor component
        HANtuneWindowFactory.HANtuneWindowType opt =
            (HANtuneWindowFactory.HANtuneWindowType)JOptionPane.showInputDialog(this,
                "Select a TableEditor type:", HANtune.applicationName + " - Add TableEditor",
                JOptionPane.QUESTION_MESSAGE, null, HANtuneWindowFactory.HANtuneTableEditorType.toArray(),
                null);
        if (opt != null) {
            hanTune.currentConfig.getHANtuneManager()
                .addWindowToTab(hanTune.getTabbedPane().getSelectedIndex(), opt);
        }
    }


    private void newTextLabelMenuItemActionPerformed(ActionEvent evt) {
        // add new textlabel component
        hanTune.currentConfig.getHANtuneManager().addWindowToTab(hanTune.getTabbedPane().getSelectedIndex(),
            HANtuneWindowType.TextMarkup);
    }


    private void newImageMarkupMenuItemActionPerformed(ActionEvent evt) {
        // add new imageMarkup
        hanTune.currentConfig.getHANtuneManager().addWindowToTab(hanTune.getTabbedPane().getSelectedIndex(),
            HANtuneWindowType.ImageMarkup);
    }


    private void newErrorViewerActionPerformed(ActionEvent evt) {
        if (hanTune.isConnected() && hanTune.currentConfig.getXcpsettings().isErrorMonitoringEnabled()) {
            if (hanTune.currentConfig.getXcpsettings().isErrorMonitoringSupported()) {
                hanTune.currentConfig.getHANtuneManager().addWindowToTab(
                    hanTune.getTabbedPane().getSelectedIndex(), HANtuneWindowType.ErrorViewer);
            } else {
                MessagePane.showInfo("Controller does not support error monitoring.");
            }
        } else {
            MessagePane.showInfo("Please reconnect with error monitoring checked");
        }
    }


    private void resizeMenuItemActionPerformed(ActionEvent evt) {
        int index = hanTune.getTabbedPane().getSelectedIndex();
        HANtuneTab tab = hanTune.currentConfig.getHANtuneManager().getTab(index);
        tab.enableResizeAndMoveMode(!tab.isResizeAndMoveModeEnabled());
    }

}
