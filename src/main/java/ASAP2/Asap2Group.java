/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import java.util.ArrayList;
import java.util.List;

/**
 * Groups are typically used to bundle characteristics and measurements that have a common meaning or are used for a specific view.
 * Group hierarchies can be expressed that include a root and sub-groups.
 * @author Michiel Klifman
 */
public class Asap2Group extends ASAP2Object {

    private List<Asap2Group> subGroups = new ArrayList<>();
    private final List<ASAP2Characteristic> characteristics = new ArrayList<>();
    private final List<ASAP2Measurement> measurements = new ArrayList<>();
    private boolean root = false;
    private boolean simulinkStructure = false;

    /**
     * @return the subGroups
     */
    public List<Asap2Group> getSubGroups() {
        return subGroups;
    }

    public void setSubGroups(List<Asap2Group> subGroups) {
        this.subGroups = subGroups;
    }

    /**
     * @param subGroup the subGroups to add to this group
     */
    public void addSubGroup(Asap2Group subGroup) {
        subGroups.add(subGroup);
    }

    /**
     * @return the characteristics
     */
    public List<ASAP2Characteristic> getCharacteristics() {
        return characteristics;
    }

    /**
     * @param characteristic the characteristic to add to this group
     */
    public void addCharacteristic(ASAP2Characteristic characteristic) {
        characteristics.add(characteristic);
    }

    /**
     * @return the measurements
     */
    public List<ASAP2Measurement> getMeasurements() {
        return measurements;
    }

    /**
     * @param measurement the measurement to add to this group
     */
    public void addMeasurement(ASAP2Measurement measurement) {
        measurements.add(measurement);
    }

    /**
     * @return whether this group is the root of the hierarchy or not
     */
    public boolean isRoot() {
        return root;
    }

    /**
     * @param root whether this group is the root of the hierarchy
     */
    public void setRoot(boolean root) {
        this.root = root;
    }

    /**
     * @return whether this group uses a Simulink structure or not
     */
    public boolean isSimulinkStructure() {
        return simulinkStructure;
    }

    /**
     * @param simulinkStructure the simulinkStructure to set
     */
    public void setSimulinkStructure(boolean simulinkStructure) {
        this.simulinkStructure = simulinkStructure;
    }

    /**
     *
     * @return all characteristics of this group and its subgroups
     */
    public List<ASAP2Characteristic> getAllChildCharacteristics() {
        List<ASAP2Characteristic> allCharacteristics = addAllChildCharacteristics(new ArrayList<>());
        ASAP2Data.sortAsap2Objects(allCharacteristics);
        return allCharacteristics;
    }

    private List<ASAP2Characteristic> addAllChildCharacteristics(List<ASAP2Characteristic> allCharacteristics) {
        characteristics.stream().forEach((characteristic) -> allCharacteristics.add(characteristic));
        subGroups.stream().forEach((subGroup) -> subGroup.addAllChildCharacteristics(allCharacteristics));
        return allCharacteristics;
    }

    /**
     * @return all measurements of this group and its subgroups
     */
    public List<ASAP2Measurement> getAllChildMeasurements() {
        List<ASAP2Measurement> allChildMeasurements = addAllChildMeasurements(new ArrayList<>());
        ASAP2Data.sortAsap2Objects(allChildMeasurements);
        return allChildMeasurements;
    }

    private List<ASAP2Measurement> addAllChildMeasurements(List<ASAP2Measurement> allMeasurements) {
        measurements.stream().forEach((measurement) -> allMeasurements.add(measurement));
        subGroups.stream().forEach((subGroup) -> subGroup.addAllChildMeasurements(allMeasurements));
        return allMeasurements;
    }

    public int getGroupDepth() {
        int depth = 1;
        for (Asap2Group subGroup : getSubGroups()) {
            if (!subGroup.getSubGroups().isEmpty()) {
                int subGroupDepth = subGroup.getGroupDepth();
                depth = subGroupDepth > depth ? subGroupDepth : depth;
            }
        }
        return depth + 1;
    }

    public int getDepth() {
        int depth = 1;
//        for (Asap2Group subGroup : getSubGroups()) {
//
//        }
        return depth;
    }

    @Override
    public String toString() {
        if (simulinkStructure) {
            String[] splitDescription = this.getDescription().split("/");
            return splitDescription[splitDescription.length-1];
        } else {
            return this.getName();
        }
    }
}
