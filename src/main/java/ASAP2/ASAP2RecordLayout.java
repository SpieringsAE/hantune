/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

/**
 * Contains information about a RECORD_LAYOUT.
 *
 * @author Aart-Jan
 */
public class ASAP2RecordLayout extends ASAP2Object {

    private int position = 1;
    private String indexMode = "COLUMN_DIR";    // FNC_VALUES
    private String indexIncr = "INDEX_INCR";    // AXIS_PTS_X
    private String addresstype = "DIRECT";      // FNC_VALUES
    private String addressing = "DIRECT";       // AXIS_PTS_X

    public String getAddressing() {
        return addressing;
    }

    public void setAddressing(String addressing) {
        String[] Addressings = {"PBYTE", "PWORD", "PLONG", "DIRECT"};
        boolean found = false;
        for(int i=0; i<Addressings.length; i++){
            if(Addressings[i].equals(addressing)){
                found = true;
                break;
            }
        }
        if(found) this.addressing = addressing;
    }

    public String getAddresstype() {
        return addresstype;
    }

    public void setAddresstype(String addresstype) {
        String[] Addresstypes = {"PBYTE", "PWORD", "PLONG", "DIRECT"};
        boolean found = false;
        for(int i=0; i<Addresstypes.length; i++){
            if(Addresstypes[i].equals(addresstype)){
                found = true;
                break;
            }
        }
        if(found) this.addresstype = addresstype;
    }

    public String getIndexIncr() {
        return indexIncr;
    }

    public void setIndexIncr(String IndexIncr) {
        String[] IndexIncrs = {"INDEX_INCR", "INDEX_DECR"};
        boolean found = false;
        for(int i=0; i<IndexIncrs.length; i++){
            if(IndexIncrs[i].equals(IndexIncr)){
                found = true;
                break;
            }
        }
        if(found) this.indexIncr = IndexIncr;
    }

    public String getIndexMode() {
        return indexMode;
    }

    public void setIndexMode(String indexMode) {
        String[] IndexModes = {"ALTERNATE_CURVES", "ALTERNATE_WITH_X", "ALTERNATE_WITH_Y", "COLUMN_DIR", "ROW_DIR"};
        boolean found = false;
        for(int i=0; i<IndexModes.length; i++){
            if(IndexModes[i].equals(indexMode)){
                found = true;
                break;
            }
        }
        if(found) this.indexMode = indexMode;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

}
