/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import asap2XmlData.Asap2AxisPts;
import asap2XmlData.Asap2Characteristic;
import asap2XmlData.Asap2CompuMethod;
import asap2XmlData.Asap2Measurement;
import asap2XmlData.Asap2ModPar;
import asap2XmlData.Asap2RecordLayout;
import asap2XmlData.Asap2XmlData;

public class ASAP2XmlImport extends ASAP2Import {
    private Asap2XmlData asapXmlData = null;


    public ASAP2XmlImport(Asap2XmlData asapXmlData) {
        super();
        this.asapXmlData = asapXmlData;
    }


    public ASAP2Data convertXmlAsapToASAP2() {
        if (!xmlImportCompumethods())
            return null;
        if (!xmlImportAxisPts())
            return null;
        if (!xmlImportMeasurements())
            return null;
        if (!xmlImportCharacteristics())
            return null;
        if (!xmlImportRecordLayout())
            return null;
        if (!xmlImportModPar())
            return null;


        data.setASAP2RecordLayouts(record_layout);
        data.setASAP2Characteristics(characteristic);
        data.setCharacteristics(characteristics);
        data.setASAP2AxisPts(axis_pts);
        data.setASAP2Measurement(measurementsMap);
        data.setMeasurements(measurementsList);
        data.setASAP2CompuMethods(compu_method);
        data.setModPar(modPar);

        postprocessMeasurements();
        postprocessAxisPts();
        postprocessCharacteristics();

        return data;
    }


    private boolean xmlImportRecordLayout() {
        for (Asap2RecordLayout xmlRL : asapXmlData.getRecordLayoutList()) {
            ASAP2RecordLayout as2RL = asapXmlToASAP2RecordLayout(xmlRL);

            record_layout.put(as2RL.getName(), as2RL);
        }
        return true;
    }


    private boolean xmlImportModPar() {
        Asap2ModPar xmlMP = asapXmlData.getModPar();
        if (xmlMP == null) {
            return false;
        }
        modPar = new ASAP2ModPar();
        if (xmlMP.isSetAddrEpk()) {
            modPar.setAddrEpk(xmlMP.getAddrEpk());
        }
        if (xmlMP.isSetCpuType()) {
            modPar.setCpuType(xmlMP.getCpuType());
        }
        if (xmlMP.isSetEcu()) {
            modPar.setEcu(xmlMP.getEcu());
        }
        if (xmlMP.isSetRamFlashOffset()) {
            modPar.setRamFlashOffset(xmlMP.getRamFlashOffset());
        }
        modPar.setEpk(xmlMP.getEpk());

        return true;
    }


    private boolean xmlImportCompumethods() {
        for (Asap2CompuMethod xmlCM : asapXmlData.getCompuMethodList()) {
            ASAP2CompuMethod as2CM = asapXmlToASAP2CompuMethod(xmlCM);
            compu_method.put(as2CM.getName(), as2CM);
        }
        return true;
    }


    private boolean xmlImportAxisPts() {
        for (Asap2AxisPts xmlAP : asapXmlData.getAxisPtsList()) {
            ASAP2AxisPts as2 = asapXmlToASAP2AxisPts(xmlAP);

            axis_pts.put(as2.getName(), as2);
            characteristic.put(as2.getName(), (ASAP2Characteristic)as2);

        }
        return true;
    }


    private boolean xmlImportMeasurements() {
        for (Asap2Measurement xmlMm : asapXmlData.getMeasurementsList()) {
            ASAP2Measurement as2Mm = asapXmlToASAP2Measurement(xmlMm);

            measurementsMap.put(as2Mm.getName(), as2Mm);
            measurementsList.add(as2Mm);
        }

        return true;
    }


    private boolean xmlImportCharacteristics() {
        for (Asap2Characteristic xmlCh : asapXmlData.getCharacteristicsList()) {
            ASAP2Characteristic as2Ch = asapXmlToASAP2Characteristic(xmlCh);

            characteristic.put(as2Ch.getName(), as2Ch);
            characteristics.add(as2Ch);
        }

        return true;
    }


    private ASAP2CompuMethod asapXmlToASAP2CompuMethod(asap2XmlData.Asap2CompuMethod a2Xml) {
        ASAP2CompuMethod as2 = new ASAP2CompuMethod();

        as2.setASAP2ObjType(ASAP2Object.ASAP2ObjType.COMPU_METHOD);
        as2.setName(a2Xml.getName());
        as2.setDescription(a2Xml.getDescription());
        as2.setConversionType(a2Xml.getConversion());
        as2.setDecimals(a2Xml.getDecimals());
        as2.setUnit(a2Xml.getUnit());

        float[] coeffs = new float[a2Xml.getCoeffsList().size()];
        for (int idx = 0; idx < coeffs.length; idx++) {
            coeffs[idx] = a2Xml.getCoeffsArray(idx);
            as2.setCoeffs(coeffs);
        }

        return as2;
    }


    private ASAP2AxisPts asapXmlToASAP2AxisPts(asap2XmlData.Asap2AxisPts a2Xml) {
        ASAP2AxisPts as2 = new ASAP2AxisPts();
        as2.setType(ASAP2Characteristic.Type.AXIS_PTS);
        as2.setName(a2Xml.getName());
        as2.setDescription(a2Xml.getDescription());
        as2.setAddress(a2Xml.getAddress());
        as2.setInputQuantity(a2Xml.getInputQuantity());
        as2.setDeposit(a2Xml.getDeposit());
        as2.setMaxDiff(a2Xml.getMaxDiff());
        as2.setConversion(a2Xml.getConversion());
        as2.setMaxAxisPoints(a2Xml.getMaxAxisPoints());
        as2.setColumnCount(a2Xml.getColumnCount());
        as2.setMinimum(a2Xml.getMinimum());
        as2.setLowerLimitEnabled(a2Xml.getLowerLimitEnabled());
        as2.setMaximum(a2Xml.getMaximum());
        as2.setUpperLimitEnabled(a2Xml.getUpperLimitEnabled());

        return as2;
    }


    private ASAP2RecordLayout asapXmlToASAP2RecordLayout(asap2XmlData.Asap2RecordLayout a2Xml) {
        ASAP2RecordLayout as2 = new ASAP2RecordLayout();

        as2.setName(a2Xml.getName());
        as2.setPosition(a2Xml.getPosition());
        as2.setDatatype(ASAP2Datatype.valueOf(a2Xml.getDatatype().toString()));
        as2.setIndexMode(a2Xml.getIndexMode());
        as2.setAddresstype(a2Xml.getAddresstype());
        as2.setIndexIncr(a2Xml.getIndexIncr());
        as2.setAddressing(a2Xml.getAddressing());

        return as2;
    }


    private ASAP2Measurement asapXmlToASAP2Measurement(asap2XmlData.Asap2Measurement a2Xml) {
        ASAP2Measurement as2 = new ASAP2Measurement();

        as2.setASAP2ObjType(ASAP2Object.ASAP2ObjType.SIGNAL);
        as2.setName(a2Xml.getName());
        as2.setDescription(a2Xml.getDescription());
        as2.setDatatype(ASAP2Datatype.valueOf(a2Xml.getDatatype().toString()));
        as2.setConversion(a2Xml.getConversion());
        as2.setResolution(a2Xml.getResolution());
        as2.setAccuracy(a2Xml.getAccuracy());
        as2.setMinimum(a2Xml.getMinimum());
        as2.setLowerLimitEnabled(a2Xml.getLowerLimitEnabled());
        as2.setMaximum(a2Xml.getMaximum());
        as2.setUpperLimitEnabled(a2Xml.getUpperLimitEnabled());
        as2.setAddress(a2Xml.getAddress());


        return as2;
    }


    private ASAP2Characteristic asapXmlToASAP2Characteristic(asap2XmlData.Asap2Characteristic a2Xml) {
        ASAP2Characteristic as2;

        if (a2Xml.getType() == asap2XmlData.Type.VALUE) {
            as2 = new Asap2Parameter();
        } else {
            as2 = new Asap2Table();
        }
        as2.setASAP2ObjType(ASAP2Object.ASAP2ObjType.CHARACTERISTIC);
        as2.setType(ASAP2Characteristic.Type.valueOf(a2Xml.getType().toString()));
        as2.setName(a2Xml.getName());
        as2.setDescription(a2Xml.getDescription());
        as2.setAddress(a2Xml.getAddress());
        as2.setDeposit(a2Xml.getDeposit());
        as2.setMaxDiff(a2Xml.getMaxDiff());
        as2.setConversion(a2Xml.getConversion());
        as2.setMinimum(a2Xml.getMinimum());
        as2.setLowerLimitEnabled(a2Xml.getLowerLimitEnabled());
        as2.setMaximum(a2Xml.getMaximum());
        as2.setUpperLimitEnabled(a2Xml.getUpperLimitEnabled());
        as2.getAxisDescr().add(a2Xml.getAxisColumnDescr());
        as2.getAxisDescr().add(a2Xml.getAxisRowDescr());

        return as2;
    }

}
