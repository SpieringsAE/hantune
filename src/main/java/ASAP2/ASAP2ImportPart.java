/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class ASAP2ImportPart {

    private static final String START_DELIMITER = "/begin";
    private static final String END_DELIMITER = "/end";
    private static final String PROJECT_KEYWORD = "PROJECT";

    private class DelimiterPosition {

        boolean isStartDelimiter; // true if next delimiter is START_DELIMITER, false if END_DELIMITER
        int delimiterIdx; // index in rawString at start of delimiter
    }

    private static final int MAX_ASAP2_STRUCTURAL_LEVEL = 8;

    // array for the split file parts
    private ArrayList<String>[] part;
    private ArrayList<Integer>[] part_parent;

    private String rawString; // contains complete file as 1 string;
    private int rawStringSearchPos;
    private File asap2File;

    @SuppressWarnings({"rawtypes", "unchecked"})
    public ASAP2ImportPart(File asap2File) throws IOException {
        this.asap2File = asap2File;
        this.part = (ArrayList<String>[]) new ArrayList[MAX_ASAP2_STRUCTURAL_LEVEL];
        this.part_parent = (ArrayList<Integer>[]) new ArrayList[MAX_ASAP2_STRUCTURAL_LEVEL];

        // initialize data arrays
        for (int i = 0; i < part.length; i++) {
            this.part[i] = new ArrayList<>();
        }
        for (int i = 0; i < part_parent.length; i++) {
            part_parent[i] = new ArrayList<>();
        }

        this.rawString = readAllImportData();

    }

    private String readAllImportData() throws IOException {
        int dataLength = (int) asap2File.length();

        FileInputStream fis = new FileInputStream(asap2File);
        byte[] dataBytes = new byte[dataLength];

        fis.read(dataBytes);
        fis.close();

        rawStringSearchPos = 0; // Reset search position when a new file has been loaded.
        return new String(dataBytes);
    }


    /*
     * search for first occurrence of  "/begin" or "/end" in rawString at given searchIdx
     * @param startidx holds index within rawString at start of current search
     * @param delim holds delimiter position after successful return
     * @return true if found. False if not found
     */
    private boolean searchForNearestDelimiter(int searchIdx, DelimiterPosition delim) {

        int endIdx = rawString.indexOf(END_DELIMITER, searchIdx);
        if (endIdx < 0) {
            return false;
        }

        int beginIdx = rawString.indexOf(START_DELIMITER, searchIdx);
        if (beginIdx > 0) {
            if (beginIdx < endIdx) {
                delim.isStartDelimiter = true;
                delim.delimiterIdx = beginIdx;
            } else {
                delim.isStartDelimiter = false;
                delim.delimiterIdx = endIdx;
            }
        } else {
            delim.isStartDelimiter = false;
            delim.delimiterIdx = endIdx;
        }
        return true;
    }

    /**
     * Parse all data chunks in file and place them in part attribute
     *
     * @return
     */
    public ArrayList<String>[] parseDelimitedDataChunks() {
        if (rawStringSearchPos == 0) {
            parseImportData(0);
        }
        return part;
    }

    public ArrayList<Integer>[] getParsedPartParent() {
        return part_parent;
    }

    /**
     * parse import data recursively and store in part variable
     *
     * @param level holds current in part data
     */
    private void parseImportData(int level) {
        DelimiterPosition delimPos = new DelimiterPosition();
        boolean expectStartDelimiter = false;
        int firstSectionStartIdx = 0;
        int firstSectionEndIdx = 0;

        // use + 4 to prevent searching the same '/begin' or '/end'
        while (searchForNearestDelimiter(rawStringSearchPos + 4, delimPos)) {
            if (delimPos.isStartDelimiter) {
                if (expectStartDelimiter) {
                    // new sections complete, store it in part
                    storeInPart(level, firstSectionStartIdx, firstSectionEndIdx, rawStringSearchPos,
                            delimPos.delimiterIdx);

                    rawStringSearchPos = delimPos.delimiterIdx;
                    expectStartDelimiter = false;
                } else {
                    // store positions of section just found.
                    firstSectionStartIdx = rawStringSearchPos;
                    firstSectionEndIdx = delimPos.delimiterIdx;

                    rawStringSearchPos = delimPos.delimiterIdx;
                    expectStartDelimiter = true;

                    // go to next level, recursive
                    parseImportData(level + 1);
                }

            } else {
                if (expectStartDelimiter) {
                    // sections complete, store it in part
                    storeInPart(level, firstSectionStartIdx, firstSectionEndIdx, rawStringSearchPos,
                            delimPos.delimiterIdx);
                    rawStringSearchPos = delimPos.delimiterIdx;

                    // finished on this level, return to previous level
                    return;
                } else {
                    // store positions of section just found.
                    firstSectionStartIdx = rawStringSearchPos;
                    firstSectionEndIdx = delimPos.delimiterIdx;

                    rawStringSearchPos = delimPos.delimiterIdx;
                    expectStartDelimiter = true;
                }

            }

        }

        if (level == 1) {
            int idx = rawString.indexOf(PROJECT_KEYWORD, rawStringSearchPos);
            storeInPart(level, firstSectionStartIdx, firstSectionEndIdx, rawStringSearchPos,
                    idx + PROJECT_KEYWORD.length());
            rawStringSearchPos = idx + PROJECT_KEYWORD.length();
        } else if (level == 0) {
            int idx = rawString.indexOf(START_DELIMITER, firstSectionStartIdx);
            storeInPart(level, firstSectionStartIdx, idx, rawStringSearchPos, rawString.length());
        }
    }

    /**
     * Store data at given indexes from rawString in part and part_parent
     *
     * @param level
     * @param firstSectionStartIdx
     * @param firstSectionEndIdx
     * @param lastSectionStartIdx
     * @param lastSectionEndIdx
     */
    private void storeInPart(int level, int firstSectionStartIdx, int firstSectionEndIdx,
            int lastSectionStartIdx, int lastSectionEndIdx) {
        String str = (rawString.substring(firstSectionStartIdx, firstSectionEndIdx)
                + rawString.substring(lastSectionStartIdx, lastSectionEndIdx)).trim();
        part[level].add(str);

        if (level > 0) {
            part_parent[level].add((part[level - 1].size()));
        } else {
            part_parent[level].add(0);
        }

    }

}
