/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import datahandling.CurrentConfig;
import datahandling.SendParameterDataHandler;
import datahandling.Table;
import datahandling.TableListener;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Michiel Klifman
 */
public class Asap2Table extends ASAP2Characteristic implements Table, AxisPointsListener {

    private final List<TableListener> listeners = new ArrayList<>();
    private final List<ASAP2AxisPts> axisPoints = new ArrayList<>();
    private double[][] data;
    private boolean[][] syncs;
    private int rows = 1;
    private int columns = 1;
    private long timeStamp;

    @Override
    public double getValueAt(int row, int column) {
        return data[row][column];
    }

    @Override
    public void setValueAt(double value, int row, int column) {
        this.timeStamp = System.currentTimeMillis();
        data[row][column] = value;
        updateListeners(value, row, column);
    }

    public boolean isInSync() {
        for (boolean[] row : syncs) {
            for (boolean column : row) {
                if (column == false) return false;
            }
        }
        return true;
    }

    @Override
    public boolean isInSyncAt(int row, int column) {
        return syncs[row][column];
    }

    @Override
    public void setInSyncAt(boolean inSync, int row, int column) {
        if (this.isInSyncAt(row, column) != inSync) {
            syncs[row][column] = inSync;
            updateStateListeners();
        }
    }

    @Override
    public int getRowCount() {
        return rows;
    }

    @Override
    public void setRowCount(int rows) {
        this.rows = rows;
        data = new double[rows][columns];
        syncs = new boolean[rows][columns];
    }

    @Override
    public int getColumnCount() {
        return columns;
    }

    @Override
    public void setColumnCount(int columns) {
        this.columns = columns;
        data = new double[rows][columns];
        syncs = new boolean[rows][columns];
    }


    @Override
    public void send(double value, int row, int column) {
        this.setInSyncAt(false, row, column);
        double validatedValue = validateValue(value);
        SendParameterDataHandler dataHandler =
            CurrentConfig.getInstance().getHANtuneManager().getSendParameterDataHandler();
        switch (getType()) {
            case CURVE:
                dataHandler.setParameter(getName(), validatedValue, column);
                break;

            case MAP:
                dataHandler.setParameter(getName(), validatedValue, row, column);
                break;

            case AXIS_PTS:
                dataHandler.setParameter(getName(), validatedValue, row);
                break;

            case VALUE:
                // not used, prevent warning
                break;
        }
    }

    @Override
    public void sendAxisPoint(double value, int axis, int index) {
        double validatedValue = axisPoints.get(axis).validateValue(value);
        SendParameterDataHandler dataHandler = CurrentConfig.getInstance().getHANtuneManager().getSendParameterDataHandler();
        dataHandler.setParameter(getAxisDescr().get(axis), validatedValue, 0, index);
    }

    @Override
    public void addListener(TableListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    @Override
    public void removeListener(TableListener listener) {
        listeners.remove(listener);
    }

    public void updateListeners(double value, int row, int column) {
        for (TableListener listener : listeners) {
            listener.updateTable(value, row, column);
        }
    }

    @Override
    public boolean isConnectedToEditor() {
        return listeners.size() > 0;
    }

    @Override
    public void axisPointsUpdate(ASAP2AxisPts axisPoints, double value, int index) {
        for (TableListener listener : listeners) {
            int axis = this.axisPoints.indexOf(axisPoints);
            listener.updateAxisPoints(value, axis, index);
        }
    }

    @Override
    public void axisPointsStateUpdate() {
        updateStateListeners();
    }

    @Override
    public void addAxisPoint(ASAP2AxisPts axisPts) {
        this.axisPoints.add(axisPts);
        axisPts.addListener(this);
    }

    @Override
    public double getAxisPointsValueAt(int axis, int index) {
        return axisPoints.get(axis).getValueAt(index);
    }

    @Override
    public boolean isAxisPointInSyncAt(int axis, int index) {
        return axisPoints.get(axis).isInSyncAt(index);
    }

    @Override
    public boolean isValueValid(double value) {
        return value >= getMinimum() && value <= getMaximum();
    }

    /**
     * Only used by calibration management. Do not use this method. Use
     * getValueAt() instead.
     * @return
     * @deprecated
     */
    @Deprecated
    @Override
    public ASAP2Data.Data getData() {
        if (isInSync() || isConnectedToEditor()) {
            if (getType() == Type.MAP) {
                return new ASAP2Data.Data(timeStamp, get2dData());
            } else {
                return new ASAP2Data.Data(timeStamp, get1dData());
            }
        } else {
            return null;
        }
    }

    private Object[][] get2dData() {
        Object[][] mapData = new Object[columns][rows];
        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                mapData[i][j] = data[j][i];
            }
        }
        return mapData;
    }

    private Object[] get1dData() {
        Object[] curveData = new Object[columns];
        for (int i = 0; i < columns; i++) {
            curveData[i] = (Double) data[0][i];
        }
        return curveData;
    }
}
