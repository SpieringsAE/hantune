/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JTextField;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class HintedTextField extends JTextField {

    private final String hint;
    private boolean showingHint;
    private boolean painting = false;

    public HintedTextField(String hint) {
        super(hint);
        this.hint = hint;
        showingHint = true;

        addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
                if (HintedTextField.this.getText().isEmpty()) {
                    showingHint = false;
                    HintedTextField.super.setText("");
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if (HintedTextField.this.getText().isEmpty()) {
                    showingHint = true;
                    HintedTextField.super.setText(hint);
                }
            }
        });
    }

    @Override
    public String getText() {
        return showingHint ? "" : super.getText();
    }

    @Override
    public void setText(String text) {
        showingHint = text.isEmpty();
        if (showingHint) {
            super.setText(hint);
        } else {
            super.setText(text);
        }
    }

    @Override
    public Color getForeground() {
        return showingHint && painting ? Color.GRAY : super.getForeground();
    }

    @Override
    public Font getFont() {
        return showingHint && painting ? new Font("Tahoma", Font.ITALIC, 11) : super.getFont();
    }

    @Override
    protected void paintComponent(Graphics g) {
        painting = true;
        super.paintComponent(g);
        painting = false;
    }
}
