/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import util.MouseEventDispatcher;

/**
 * This class can be used as an alternative to the default Swing tooltips. Set
 * the text of the tooltip either by calling the constructor, setText(text) or
 * overriding getText(). The main differences are:
 * 1. A tooltip will only be
 * shown when the timer ends. Whenever a Swing tooltip is displayed, Swing goes
 * into "tooltip mode" and will show another components tooltip immediately upon
 * entering.
 * 2. Allows forwarding of MouseEvents to another component.
 *
 * @author Michiel Klifman
 */
public class DynamicTooltip {

    private int delay = 1000; //miliseconds

    private final JComponent component;
    private final Timer tooltipTimer;
    private String text;
    private MouseEvent mouseEvent;

    /**
     * Creates a tooltip for the provided component with the provided text.
     * Initializes the timer that upon ending shows the tooltip. Each tooltip
     * runs on its own timer, as oposed to Swing tooltips. Adds MouseListeners that will start and stop
     * the timer. All MouseEvents will be forwarded to the provided target.
     *
     * @param component - the component to which the tooltip must be added.
     * @param text - the text of the tooltip.
     * @param target - the container to which the events must be dispatched.
     * @throws IllegalArgumentException when component == target
     */
    public DynamicTooltip(JComponent component, String text, Container target) {
        if (component == target) {
            throw new IllegalArgumentException("component must not equal target");
        }

        this.component = component;
        this.text = text;

        tooltipTimer = new Timer(delay, (ActionEvent e) -> showTooltip());
        tooltipTimer.setRepeats(false);
        component.addMouseMotionListener(new MouseEventDispatcher(target) {
            @Override
            public void mouseMoved(MouseEvent event) {
                mouseEvent = event;
                startTimer();
                dispatchEventToTarget(event);
            }
        });
        component.addMouseListener(new MouseEventDispatcher(target) {
            @Override
            public void mouseExited(MouseEvent event) {
                tooltipTimer.stop();
                ToolTipManager.sharedInstance().setEnabled(true);
                dispatchEventToTarget(event);
            }
        });
    }

    /**
     * Creates a tooltip for the provided component with the provided text.
     * Initializes the timer that upon ending shows the tooltip. Each tooltip
     * runs on its own timer, as oposed to Swing tooltips.
     *
     * @param component - the component to which the tooltip must be added.
     * @param text - the text of the tooltip.
     */
    public DynamicTooltip(JComponent component, String text) {
        this(component, text, null);
    }

    /**
     * Starts the tooltipTimer. If a timer is already running it will be restarted.
     * This methods is called whenever the mouse is over the component and moves.
     * The timer is stopped when the mouse exits the component.
     */
    private void startTimer() {
        if (tooltipTimer.isRunning()) {
            tooltipTimer.restart();
            ToolTipManager.sharedInstance().setEnabled(false);
        } else {
            tooltipTimer.start();
        }
    }

    /**
     * Shows the tooltip. This method is called when the tooltipTimer ends.
     */
    private void showTooltip() {
        if (mouseEvent != null) {
            component.setToolTipText(getText());
            ToolTipManager.sharedInstance().setEnabled(true);
            ToolTipManager.sharedInstance().mouseMoved(mouseEvent);
        }
    }

    /**
     * Override this method to return something more meaningful.
     * @return the text to be displayed by the tooltip.
     */
    public String getText() {
        return text;
    }

    /**
     * @param text - the tex tto be shown by the tooltip.
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the delay of the tooltip
     */
    public int getDelay() {
        return delay;
    }

    /**
     * @param delay - the delay of the tooltip
     */
    public void setDelay(int delay) {
        this.delay = delay;
        tooltipTimer.setInitialDelay(delay);
    }
}
