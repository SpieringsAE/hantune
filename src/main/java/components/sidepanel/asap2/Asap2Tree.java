/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel.asap2;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import ASAP2.ASAP2Characteristic;
import ASAP2.ASAP2Data;
import ASAP2.ASAP2Measurement;
import ASAP2.Asap2Function;
import ASAP2.Asap2Group;
import ASAP2.Asap2Table;
import components.sidepanel.AbstractTree;
import components.sidepanel.DescriptionNode;
import components.sidepanel.DescriptionTreeMenuManager;
import components.sidepanel.TreeTransferHandler;
import datahandling.CurrentConfig;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class Asap2Tree extends AbstractTree {

    private final DescriptionNode root = new DescriptionNode();
    private TreeType type = TreeType.ALL;
    private int depth = 0;

    public Asap2Tree() {
        this.setRootVisible(false);
        this.setShowsRootHandles(true);
        this.setDragEnabled(true);
        this.setTransferHandler(new TreeTransferHandler(this));
        this.setModel(new DefaultTreeModel(root));
        addCustomRenderer();
        addMenuManager();
        setDynamicToolTip();
    }

    private void addCustomRenderer() {

        this.setCellRenderer(new DefaultTreeCellRenderer() {
            private final ImageIcon defaultIcon = createImageIcon("/images/tree/folder.png");
            private final ImageIcon measurementIcon = createImageIcon("/images/tree/SIGNAL.png");
            private final ImageIcon measurementDisabledIcon = createImageIcon("/images/tree/SIGNAL_disabled.png");
            private final ImageIcon characteristDisabledIcon = createImageIcon("/images/tree/VALUE_disabled.png");

            @Override
            public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean isLeaf, int row, boolean focused) {
                Component c = super.getTreeCellRendererComponent(tree, value, selected, expanded, isLeaf, row, focused);
                if (value instanceof DescriptionNode) {
                    DescriptionNode node = (DescriptionNode) value;
                    Object object = node.getUserObject();
                    if (object instanceof ASAP2Data) {
                        setIcon(defaultIcon);
                    } else if (object instanceof ASAP2Characteristic) {
                        ASAP2Characteristic.Type type = ((ASAP2Characteristic) object).getType();
                        ImageIcon icon = createImageIcon("/images/tree/" + type.toString() + ".png");
                        if (icon != null) {
                            setIcon(icon);
                        }
                    } else if (object instanceof ASAP2Measurement) {
                        setIcon(measurementIcon);
                        setDisabledIcon(measurementDisabledIcon);
                    } else if (node.isType(DescriptionNode.Type.AXIS_POINT)) {
                        setIcon(null);
                        setForeground(Color.LIGHT_GRAY);
                    } else if (!isLeaf) {
                        setIcon(defaultIcon);
                        if (object != null) {
                            node.setUserObject(((String) object).replaceAll("\\(\\d+\\)", "(" + node.getLeafCount() + ")"));
                        }
                    }
                    setEnabled(node.isEnabled());
                    setDragEnabled(node.isDragEnabled());
                }
                return c;
            }
        });
    }

    private void addMenuManager() {
        DescriptionTreeMenuManager menuManager = new DescriptionTreeMenuManager(this);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                if (SwingUtilities.isRightMouseButton(event)) {
                    menuManager.showMenu(event);
                } else if (event.getClickCount() == 2) {
                    menuManager.quickAddItem(event);
                }
            }
        });
    }

    public void buildTree(ASAP2Data asap2) {
        root.removeAllChildren();
        if (asap2 != null) {
            DescriptionNode asapNode = getTreeOfType(asap2);
            root.add(asapNode);
        }
        this.setModel(new DefaultTreeModel(root));
        expandTree();
    }

    private DescriptionNode getTreeOfType(ASAP2Data asap2) {
        DescriptionNode asapNode;
        switch (type) {
            default:
                asapNode = buildNormalTree(asap2);
                break;
            case GROUPS:
                asapNode = buildGroupTree(asap2.getGroupRoot(), depth);
                break;
            case FUNCTIONS:
                asapNode = buildFunctionTree(asap2.getFunctionRoot(), depth);
                break;
        }
        asapNode.setUserObject(asap2.getName() + getNodeSuffix(asapNode));
        return asapNode;
    }

    private DescriptionNode buildNormalTree(ASAP2Data asap2) {
        DescriptionNode asap2TreeNode = new DescriptionNode();
        DescriptionNode characteristicsNode = getCharacteristicsNode(asap2.getCharacteristics());
        DescriptionNode measurementsNode = getMeasurementNodes(asap2.getMeasurements());
        asap2TreeNode.add(characteristicsNode);
        asap2TreeNode.add(measurementsNode);
        return asap2TreeNode;
    }

    private DescriptionNode buildGroupTree(Asap2Group group, int depth) {
        DescriptionNode groupNode = new DescriptionNode();
        if (depth > 1) {
            for (Asap2Group subGroup : group.getSubGroups()) {
                DescriptionNode subGroupNode = buildGroupTree(subGroup, depth - 1);
                subGroupNode.setUserObject(subGroup + getNodeSuffix(subGroupNode));
                if (!subGroupNode.isLeaf())groupNode.add(subGroupNode);
            }
            addCharacteristicsToNode(groupNode, group.getCharacteristics());
            addMeasurementsToNode(groupNode, group.getMeasurements());
        } else {
            addCharacteristicsToNode(groupNode, group.getAllChildCharacteristics());
            addMeasurementsToNode(groupNode, group.getAllChildMeasurements());
        }
        return groupNode;
    }

    private DescriptionNode buildFunctionTree(Asap2Function function, int depth) {
        DescriptionNode functionNode = new DescriptionNode();
        if (depth > 1) {
            for (Asap2Function subFunction : function.getSubFunctions()) {
                DescriptionNode subFunctionNode = buildFunctionTree(subFunction, depth - 1);
                subFunctionNode.setUserObject(subFunction + getNodeSuffix(subFunctionNode));
                if (!subFunctionNode.isLeaf()) functionNode.add(subFunctionNode);
            }
            addCharacteristicsToNode(functionNode, function.getCharacteristics());
            addMeasurementsToNode(functionNode, function.getInAndOutputMeasurements());
        } else {
            addCharacteristicsToNode(functionNode, function.getAllChildCharacteristics());
            addMeasurementsToNode(functionNode, function.getAllChildMeasurements());
        }
        return functionNode;
    }

    private void addCharacteristicsToNode(DescriptionNode node, List<ASAP2Characteristic> characteristics) {
        if (characteristics.isEmpty()) return;
        DescriptionNode characteristicsNode = getCharacteristicsNode(characteristics);
        if (!characteristicsNode.isLeaf()) {
            node.add(characteristicsNode);
        }
    }

    private DescriptionNode getCharacteristicsNode(List<ASAP2Characteristic> characteristics) {
        DescriptionNode characteristicsNode = addNodesFromCollection(characteristics, DescriptionNode.Type.PARAMETER);
        characteristicsNode.setType(DescriptionNode.Type.PARAMETER_PARENT);
        characteristicsNode.setUserObject("Parameters" + getNodeSuffix(characteristicsNode));
        return characteristicsNode;
    }

    private void addMeasurementsToNode(DescriptionNode node, List<ASAP2Measurement> measurements) {
        if (measurements.isEmpty()) return;
        DescriptionNode measurementsNode = getMeasurementNodes(measurements);
        if (!measurementsNode.isLeaf()) {
            node.add(measurementsNode);
        }
    }

    private DescriptionNode getMeasurementNodes(List<ASAP2Measurement> measurements) {
        DescriptionNode measurementsNode = addNodesFromCollection(measurements, DescriptionNode.Type.SIGNAL);
        measurementsNode.setType(DescriptionNode.Type.SIGNAL_PARENT);
        measurementsNode.setUserObject("Signals" + getNodeSuffix(measurementsNode));
        return measurementsNode;
    }

    private DescriptionNode addNodesFromCollection(Collection<?> collection, DescriptionNode.Type type) {
        DescriptionNode node = new DescriptionNode();
        for (Object userObject : collection) {
            DescriptionNode childNode = new DescriptionNode(userObject);
            childNode.setType(userObject.getClass());
            if (type.equals(DescriptionNode.Type.SIGNAL)) {
                childNode.setEnabled(((ASAP2Measurement)userObject).isAllowed());
                childNode.setDragEnable(((ASAP2Measurement)userObject).isAllowed());
            } else if (type.equals(DescriptionNode.Type.PARAMETER)){
                childNode.setEnabled(((ASAP2Characteristic)userObject).isActive());
                childNode.setDragEnable(((ASAP2Characteristic)userObject).isActive());
            }
            node.add(childNode);
        }
        return node;
    }

    public void setType(TreeType type) {
        this.type = type;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    @Override
    public void expandTree() {
        for (int i = 0; i < this.getRowCount(); i++) {
            TreePath path = this.getPathForRow(i);
            if (path != null) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
                if (node.getUserObject() instanceof Asap2Table) {
                    continue;
                }
            }
            this.expandRow(i);
        }
    }

    @Override
    public String getToolTipTextForNode(DefaultMutableTreeNode node) {
        if (node.toString().contains(CurrentConfig.getInstance().getASAP2Data().getName())) {
            return "Software ID: " + CurrentConfig.getInstance().getASAP2Data().getEpromID();
        } else {
            return node.toString();
        }
    }

    public enum TreeType {

        ALL("All"), GROUPS("Groups"), FUNCTIONS("Functions");

        private String value;

        private TreeType(String val) {
            this.value = val;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
}
