/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel.asap2;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableRowSorter;

import ASAP2.ASAP2Measurement;
import DAQList.DAQList;
import DAQList.DaqListListener;
import HANtune.HANtune;
import components.sidepanel.AbstractTable;
import datahandling.CurrentConfig;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class DaqListTable extends AbstractTable implements DaqListListener {

    private DaqListTableModel tableModel;
    private HANtune hanTune = HANtune.getInstance();
    public DaqListTable() {

        createTable();
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                int row = convertRowIndexToModel(rowAtPoint(event.getPoint()));
                int column = columnAtPoint(event.getPoint());
                ASAP2Measurement measurement = (ASAP2Measurement)tableModel.getValueAt(row, 0);

                if (tableModel.isColumnEnabled(column) && measurement.isAllowed()) {
                    boolean value = !(boolean) tableModel.getValueAt(row, column);
                    if (event.isShiftDown()) {
                        for (int selectedRow : DaqListTable.this.getSelectedRows()) {
                            selectedRow = convertRowIndexToModel(selectedRow);
                            tableModel.setValueAt(value, selectedRow, column);
                        }
                    } else {
                        tableModel.setValueAt(value, row, column);
                    }
                }
            }
        });
    }

    @Override
    public void daqListUpdate() {
        createTable();
        fillTable();
        this.addSorter(new TableRowSorter<>(tableModel));
        this.ignoreColumnWhileFiltering(1);
    }

    private void createTable() {
        tableModel = new DaqListTableModel();
        tableModel.disableColumn(0);
        tableModel.disableColumn(1);
        List<Object> columns = buildColumns();



        tableModel.setColumnNames(columns);

        this.setModel(tableModel);

        tableModel.addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent e) {
                int row = e.getFirstRow();
                int column = e.getColumn();
                boolean value = (boolean)tableModel.getValueAt(row, column);
                ASAP2Measurement measurement = (ASAP2Measurement)tableModel.getValueAt(row, 0);
                String screenId = (String)tableModel.getColumnObject(column);
                int id = Integer.parseInt(screenId.replace("D", ""));
                CurrentConfig.getInstance().getHANtuneManager().modifyDaqlist(id - 1, measurement.getName(),
                    value);
            }
        });

        for (int i = 1; i < columnModel.getColumnCount(); i++) {
            this.getColumnModel().getColumn(i).setPreferredWidth(30);
            this.getColumnModel().getColumn(i).setMinWidth(30);
            this.getColumnModel().getColumn(i).setMaxWidth(30);
            this.getColumnModel().getColumn(i).setCellRenderer(new TableCellRenderer() {
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                               boolean isFocused, int row, int column) {
                    Component component =
                        table.getDefaultRenderer(Boolean.class).getTableCellRendererComponent(table, value, isSelected,
                            isFocused, row, column);

                    ASAP2Measurement measurement = (ASAP2Measurement) tableModel.getValueAt(row, 0);
                    component.setEnabled(tableModel.isColumnEnabled(column) && measurement.isAllowed());

                    return component;
                }
            });
        }

        this.getColumnModel().getColumn(0).setCellRenderer(new TableCellRenderer() {
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                           boolean isFocused, int row, int column) {
                Component component =
                    table.getDefaultRenderer(String.class).getTableCellRendererComponent(table, value, isSelected,
                        isFocused, row, column);

                ASAP2Measurement measurement = (ASAP2Measurement) tableModel.getValueAt(row, 0);
                component.setEnabled(measurement.isAllowed());

                return component;
            }
        });




        this.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        headerManager.addTableHeaderMenu();
        headerManager.excludeColumnFromMenu(0);
    }

    private List<Object> buildColumns() {
        List<Object> columns = new ArrayList<>();
        columns.add("Signals");
        hanTune.daqLists.forEach(daqList -> columns.add("D" + (hanTune.daqLists.indexOf(daqList) + 1)));
        return columns;
    }

    private void fillTable() {
        if (CurrentConfig.getInstance().getASAP2Data() != null) {
            for (ASAP2Measurement measurement : CurrentConfig.getInstance().getASAP2Data().getMeasurements()) {
                Object[] row = new Object[hanTune.daqLists.size() + 1];
                row[0] = measurement;
                for (DAQList daqList : hanTune.daqLists) {
                    if (daqList.getASAP2Measurements().contains(measurement) && measurement.isAllowed()) {
                        row[hanTune.daqLists.indexOf(daqList) + 1] = Boolean.TRUE;
                    } else {
                        row[hanTune.daqLists.indexOf(daqList) + 1] = Boolean.FALSE;
                    }
                }
                tableModel.addRow(row);
            }
        }
    }
}
