/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel;

import HANtune.HANtune;
import HANtune.HANtuneWindow;
import static HANtune.HANtuneWindowFactory.HANtuneEditorType;
import static HANtune.HANtuneWindowFactory.HANtuneMultiEditorType;
import static HANtune.HANtuneWindowFactory.HANtuneMultiViewerType;
import static HANtune.HANtuneWindowFactory.HANtuneTableEditorType;
import static HANtune.HANtuneWindowFactory.HANtuneViewerType;
import HANtune.HANtuneWindowFactory.HANtuneWindowType;
import static HANtune.HANtuneWindowFactory.HANtuneWindowType.ScriptWindow;
import HANtune.ReferenceWindow;
import datahandling.CurrentConfig;
import datahandling.Parameter;
import datahandling.Reference;
import datahandling.Signal;
import datahandling.Table;
import java.awt.event.MouseEvent;
import java.util.EnumSet;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import nl.han.hantune.scripting.Script;

/**
 * A convenience class that provides access to several menu's containing the
 * required signals or parameters as menu items. NOTE: In this class MultiViewer
 * refers to all viewers that can handle multiple signals. Not the MultiViewer
 * class itself. The same goes for editors. Currently all editors are
 * "MultiEditors".
 *
 * @author Michiel Klifman
 */
public class NewWindowMenuManager {

    public JPopupMenu getAllWindowsMenu() {
        JPopupMenu menu = getViewerAndEditorMenu();
        JMenuItem menuItem = getMenuItemOfType(ScriptWindow);
        menuItem.setText("Add Script Window");
        menu.add(menuItem);
        return menu;
    }

    /**
     * Convenience method for creating a menu containing all viewer types (i.e.
     * MultiViewer or BooleanViewer) as menu items.
     *
     * @return a pop-up menu containing all viewers as menu items.
     */
    public JPopupMenu getViewerMenu() {
        return getPopupMenuOfWindowTypes(HANtuneViewerType);
    }

    public JPopupMenu getEditorMenu() {
        return getPopupMenuOfWindowTypes(HANtuneEditorType);
    }

    public JPopupMenu getTableEditorMenu() {
        return getPopupMenuOfWindowTypes(HANtuneTableEditorType);
    }

    public JPopupMenu getViewerAndEditorMenu() {
        JPopupMenu menu = new JPopupMenu();
        menu.add(getMenuOfWindowTypes("Add Viewer", HANtuneViewerType));
        menu.add(getMenuOfWindowTypes("Add Editor", HANtuneEditorType));
        return menu;
    }

    public JPopupMenu getMultiViewerMenu() {
        return getPopupMenuOfWindowTypes(HANtuneMultiViewerType);
    }

    public JPopupMenu getMultiEditorMenu() {
        return getPopupMenuOfWindowTypes(HANtuneMultiEditorType);
    }

    public JPopupMenu getMultiViewerAndEditorMenu() {
        JPopupMenu menu = new JPopupMenu();
        menu.add(getMenuOfWindowTypes("Add Viewer", HANtuneMultiViewerType));
        menu.add(getMenuOfWindowTypes("Add Editor", HANtuneMultiEditorType));
        return menu;
    }

    public JPopupMenu getPopupMenuOfWindowTypes(EnumSet<HANtuneWindowType> types) {
        JPopupMenu menu = new JPopupMenu();
        for (HANtuneWindowType type : types) {
            menu.add(getMenuItemOfType(type));
        }
        return menu;
    }

    public JMenu getMenuOfWindowTypes(String text, EnumSet<HANtuneWindowType> types) {
        JMenu menu = new JMenu(text);
        for (HANtuneWindowType type : types) {
            menu.add(getMenuItemOfType(type));
        }
        return menu;
    }

    /**
     * Creates a menu item for the given type and the set of types it belongs
     * to.
     *
     * @param type - the type of window (i.e. MultiViewer).
     * @return a menu item
     */
    public JMenuItem getMenuItemOfType(HANtuneWindowType type) {
        JMenuItem item = new JMenuItem(type.toString());
        if (HANtuneViewerType.contains(type)) {
            item.addActionListener(e -> handleMenuItemAction(type, Signal.class));
        } else if (HANtuneEditorType.contains(type)) {
            item.addActionListener(e -> handleMenuItemAction(type, Parameter.class));
        } else if (HANtuneTableEditorType.contains(type)) {
            item.addActionListener(e -> handleMenuItemAction(type, Table.class));
        } else if (type == ScriptWindow) {
            item.addActionListener(e -> handleMenuItemAction(type, Script.class));
        }
        return item;
    }

    @SuppressWarnings("unchecked")
    public <T extends Reference> ReferenceWindow<T> handleMenuItemAction(HANtuneWindowType windowType, Class<T> referenceType) {
        return (ReferenceWindow<T>) addWindow(windowType);
    }

    private HANtuneWindow addWindow(HANtuneWindowType type) {
        int index = HANtune.getInstance().getTabbedPane().getSelectedIndex();
        return CurrentConfig.getInstance().getHANtuneManager().addWindowToTab(index, type);
    }

    /**
     * Shows the given menu. The menu items will be disabled if there is no tab
     * active to add the viewers or editors to.
     *
     * @param menu - the menu to show
     * @param e
     */
    public void showMenu(JPopupMenu menu, MouseEvent e) {
        if (CurrentConfig.getInstance().isServiceToolMode()) {
            return;
        }

        if (HANtune.getInstance().getTabbedPane().getTabCount() == 0) {
            disableMenuItems(menu);
        } else {
            enableMenuItems(menu);
        }
        menu.show(e.getComponent(), e.getX(), e.getY());
    }

    /**
     * Enables all menu items of a pop-up menu.
     *
     * @param menu - the menu containing the menu items to enable.
     */
    public void enableMenuItems(JPopupMenu menu) {
        for (int i = 0; i < menu.getComponentCount(); i++) {
            menu.getComponent(i).setEnabled(true);
        }
    }

    /**
     * Disables and grays out all menu items of a pop-up menu.
     *
     * @param menu - the menu containing the items to disable.
     */
    public void disableMenuItems(JPopupMenu menu) {
        for (int i = 0; i < menu.getComponentCount(); i++) {
            menu.getComponent(i).setEnabled(false);
        }
    }
}
