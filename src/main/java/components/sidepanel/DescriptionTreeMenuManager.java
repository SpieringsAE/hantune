/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel;

import static components.sidepanel.DescriptionNode.Type.PARAMETER;
import static components.sidepanel.DescriptionNode.Type.PARAMETER_PARENT;
import static components.sidepanel.DescriptionNode.Type.SIGNAL;
import static components.sidepanel.DescriptionNode.Type.SIGNAL_PARENT;
import static components.sidepanel.DescriptionNode.Type.TABLE;

import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JPopupMenu;
import javax.swing.tree.TreePath;

import HANtune.HANtuneWindowFactory.HANtuneWindowType;
import HANtune.ReferenceWindow;
import datahandling.Parameter;
import datahandling.Reference;
import datahandling.Signal;
import datahandling.Table;

/**
 *
 * @author Michiel Klifman
 */
public class DescriptionTreeMenuManager extends NewWindowMenuManager {

    private final AbstractTree tree;

    public DescriptionTreeMenuManager(AbstractTree tree) {
        this.tree = tree;
    }

    public void showMenu(MouseEvent event) {
        int row = tree.getRowForLocation(event.getX(), event.getY());

        if (event.isControlDown()) {
            tree.addSelectionRow(row);
        } else if (!tree.isRowSelected(row)) {
            tree.setSelectionRow(row);
        }

        TreePath path = tree.getPathForRow(row);
        if (path != null) {
            DescriptionNode node = (DescriptionNode) path.getLastPathComponent();

            if (!node.isEnabled()) {
                return;
            }

            JPopupMenu menu = checkPopupMenuType(node);
            if (menu != null) {
                showMenu(menu, event);
            }
        }
    }

    private JPopupMenu checkPopupMenuType(DescriptionNode node) {
        int selectionCount = tree.getSelectionCount();
        JPopupMenu menu = null;
        if (node.isType(SIGNAL_PARENT) && node.isType(PARAMETER_PARENT)) {
            menu = getMultiViewerAndEditorMenu();
        } else if (node.isType(SIGNAL_PARENT)) {
            menu = getMultiViewerMenu();
        } else if (node.isType(PARAMETER_PARENT)) {
            menu = getMultiEditorMenu();
        } else if (node.isType(SIGNAL) && node.isType(PARAMETER)) {
            menu = selectionCount == 1 ? getViewerAndEditorMenu() : getMultiViewerAndEditorMenu();
        } else if (node.isType(SIGNAL)) {
            menu = selectionCount == 1 ? getViewerMenu() : getMultiViewerMenu();
        } else if (node.isType(PARAMETER)) {
            menu = selectionCount == 1 ? getEditorMenu() : getMultiEditorMenu();
        } else if (node.isType(TABLE)) {
            menu = getTableEditorMenu();
        }
        return menu;
    }

    public void quickAddItem(MouseEvent event) {
        DescriptionNode node = (DescriptionNode) tree.getSelectedNode();
        if (node != null && node.isEnabled()) {
            if (node.isType(SIGNAL) && !event.isAltDown()) {
                handleMenuItemAction(HANtuneWindowType.MultiViewer, Signal.class);
            } else if (node.isType(PARAMETER)) {
                handleMenuItemAction(HANtuneWindowType.MultiEditor, Parameter.class);
            } else if (node.isType(TABLE)) {
                handleMenuItemAction(HANtuneWindowType.TableEditor, Table.class);
            }
        }
    }

    @Override
    public <T extends Reference> ReferenceWindow<T> handleMenuItemAction(HANtuneWindowType windowType, Class<T> referenceType) {
        ReferenceWindow<T> window = super.handleMenuItemAction(windowType, referenceType);
        List<T> references = tree.getSelectedUserObjects(referenceType);
        if (window != null && !references.isEmpty()) {
            window.addMultipleReferences(references);
        }
        return window;
    }
}
