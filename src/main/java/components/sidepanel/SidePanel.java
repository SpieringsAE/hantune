/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel;

import HANtune.HANtune;
import components.CustomButton;
import components.HintedTextField;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Michiel
 */
@SuppressWarnings("serial")
public class SidePanel extends JPanel {

    public final static Color BORDER_COLOR = new Color(172, 172, 172);
    public JPanel contentPanel = new JPanel();
    public SpringLayout layout = new SpringLayout();
    private final SubPanelListeners subPanelListeners = new SubPanelListeners();

    public SidePanel(String title) {
        setLayout(new BorderLayout());
        SidePanelManager manager = SidePanelManager.getInstance();
        manager.addSidePanel(this);
        setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        this.add(contentPanel);
        contentPanel.setLayout(layout);
        this.add(createSidePanelMainHeader(title), BorderLayout.NORTH);
        this.setOpaque(false);
        //setMaximumSize(new Dimension(900, 900));
    }

    private Box createSidePanelMainHeader(String title) {
        Box header = createSidePanelHeader(title);

        CustomButton pinButton = new CustomButton("/images/sidepanel/pin2.png");
        pinButton.setMouseOverImage("/images/sidepanel/pin.png");
        pinButton.setButtonActionPerformed(e -> HANtune.getInstance().setSnapLayoutToSidePanel(!HANtune.getInstance().isSnapLayoutToSidePanelSet()));
        header.add(pinButton);

        CustomButton minimizeButton = new CustomButton("/images/sidepanel/minimize_horizontal2.png");
        minimizeButton.setMouseOverImage("/images/sidepanel/minimize_horizontal_hover2.png");
        minimizeButton.setButtonActionPerformed(e -> HANtune.getInstance().showPanel(this, false));
        header.add(minimizeButton);
        return header;
    }

        public static Box createSidePanelHeader(String title) {
        JLabel subHeaderTitle = new JLabel(title);
        subHeaderTitle.setFont(new Font("Tahoma", Font.BOLD, 10));

        Border border = BorderFactory.createMatteBorder(1, 1, 0, 1, BORDER_COLOR);
        Border innerBorder = BorderFactory.createEmptyBorder(0, 2, 0, 0);

        Box subHeader = Box.createHorizontalBox();
        subHeader.setBorder(BorderFactory.createCompoundBorder(border, innerBorder));
        subHeader.add(subHeaderTitle);
        subHeader.add(Box.createHorizontalGlue());
        subHeader.addMouseListener(new MouseAdapter() {});
        subHeader.setOpaque(true);
        return subHeader;
    }

    @Override
    public void setSize(int width, int height) {
        //System.out.println(width);
        if (width > 254 && width <= getMaximumSize().width) {
            super.setSize(width, height);
        } else {
            super.setSize(254, height);
        }
    }

    public JPanel getContentPanel() {
        return contentPanel;
    }

    public void addSearchField(Searchable searchable, String hint) {
        this.add(createSearchField(searchable, hint), BorderLayout.SOUTH);
    }

    public static Box createSearchField(Searchable searchable, String hint) {
        JTextField searchField = new HintedTextField(hint);
        Border outerBorder = BorderFactory.createLineBorder(BORDER_COLOR, 1);
        Border innerBorder = BorderFactory.createEmptyBorder(2, 2, 2, 2);
        searchField.setBorder(BorderFactory.createCompoundBorder(outerBorder, innerBorder));
        searchField.setMaximumSize(new Dimension(Integer.MAX_VALUE,searchField.getPreferredSize().height));

        searchField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent event) {
                searchable.find(searchField.getText());
            }
        });

        ActionListener searchFieldActionListener = (ActionEvent event) -> searchable.find(searchField.getText());
        searchField.addActionListener(searchFieldActionListener);

        CustomButton icon = new CustomButton("/images/sidepanel/Search_Icon2.png");
        icon.setButtonActionPerformed(searchFieldActionListener);
        icon.setMouseOverImage("/images/sidepanel/Search_Icon3.png");
        icon.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, BORDER_COLOR));

        Box wrapper = Box.createHorizontalBox();
        wrapper.setBorder(new EmptyBorder(3, 0, 0, 0));
        wrapper.add(searchField);
        wrapper.add(icon);
        return wrapper;
    }

    public void add(SubPanel subPanel) {
        contentPanel.add(subPanel);
        subPanel.addMouseListener(subPanelListeners);
        subPanel.addMouseMotionListener(subPanelListeners);
        subPanelListeners.addSubpanel(subPanel);
    }
}
