/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel;

import HANtune.HANtune;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.MouseInfo;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.MouseInputAdapter;

/**
 *
 * @author Michiel
 * singleton class
 */
public class SidePanelManager extends MouseInputAdapter {

    private final List<SidePanel> sidePanels;
    private Component component;
    private int horizontalDifference;
    private boolean isResizing;

    private static SidePanelManager instance;

    private SidePanelManager() {
        sidePanels = new ArrayList<>();
    }

    public static SidePanelManager getInstance() {
        if (instance == null) {
            instance = new SidePanelManager();
        }
        return instance;
    }

    public void addSidePanel(SidePanel sidePanel) {
        if (!sidePanels.contains(sidePanel)) {
            sidePanel.addMouseListener(this);
            sidePanel.addMouseMotionListener(this);
            sidePanels.add(sidePanel);
        }
    }

    @Override
    public void mouseEntered(MouseEvent event) {
        SidePanel sidePanel = (SidePanel) event.getSource();
        Container panel = (Container) sidePanel;

        if (event.getX() >= panel.getWidth() - panel.getInsets().right) {
            sidePanel.setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
        }
    }

    @Override
    public void mouseExited(MouseEvent event) {
        if (!isResizing) {
            component = (Component) event.getSource();
            component.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }

    @Override
    public void mousePressed(MouseEvent event) {
        component = (Component) event.getSource();
        horizontalDifference = MouseInfo.getPointerInfo().getLocation().x - component.getWidth();
    }

    @Override
    public void mouseReleased(MouseEvent event) {
        SidePanel panel = (SidePanel) event.getSource();
        isResizing = false;
        if (event.getX() < panel.getWidth() - panel.getInsets().right) {
            panel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }

    @Override
    public void mouseDragged(MouseEvent event) {
        isResizing = true;
        for (SidePanel sidePanel : sidePanels) {
            sidePanel.setSize(MouseInfo.getPointerInfo().getLocation().x - horizontalDifference, sidePanel.getHeight());
            sidePanel.setPreferredSize(sidePanel.getSize());
            sidePanel.validate();
        }
        if (HANtune.getInstance().isSnapLayoutToSidePanelSet()) {
            HANtune.getInstance().adjustLayoutSize(true);
        }
    }

    public int getSidePanelWidth() {
        if (!sidePanels.isEmpty()) {
            return sidePanels.get(0).getWidth();
        } else {
            return 0;
        }
    }

    public boolean isSidePanelVisible() {
        for (SidePanel sidePanel : sidePanels) {
            if (sidePanel.isVisible()) {
                return true;
            }
        }
        return false;
    }

    public void setSidePanelVisible(boolean visible) {
        for (SidePanel sidePanel : sidePanels) {
            sidePanel.setVisible(visible);
        }
    }

    public void setSidePanelBounds(int x, int y, int width, int height) {
        for (SidePanel sidePanel : sidePanels) {
            sidePanel.setBounds(x, y, width, height);
            sidePanel.updateUI();
        }
    }
}
