/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel.dbc;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.tree.DefaultMutableTreeNode;

import can.CanMessage;
import can.CanSignal;
import can.DbcObject;
import components.sidepanel.AbstractTable;
import components.sidepanel.AbstractTreeListener;

/**
 *
 * @author Michiel
 */
@SuppressWarnings("serial")
public class SignalTable extends AbstractTable implements AbstractTreeListener {

    private DefaultTableModel tableModel;

    public SignalTable() {
        createTable();
    }

    private void createTable() {

        tableModel = new DefaultTableModel();
        tableModel.setColumnIdentifiers(new Object[]{"Name", "Type", "Byte Order", "Multiplexer", "Start Bit", "Length", "Factor", "Offset", "Mnimum", "Maximum", "Unit", "Comment"});

        this.setModel(tableModel);
        this.setRowSorter(new TableRowSorter<>(tableModel));

        headerManager.addHiddenColumn(columnModel.getColumn(1));
        headerManager.addHiddenColumn(columnModel.getColumn(2));
        headerManager.addHiddenColumn(columnModel.getColumn(3));
        headerManager.addHiddenColumn(columnModel.getColumn(4));
        headerManager.addHiddenColumn(columnModel.getColumn(5));
        headerManager.addHiddenColumn(columnModel.getColumn(6));
        headerManager.addHiddenColumn(columnModel.getColumn(7));
        headerManager.addHiddenColumn(columnModel.getColumn(8));
        headerManager.addHiddenColumn(columnModel.getColumn(9));
        //headerManager.addHiddenColumn(columnModel.getColumn(11));
        headerManager.addTableHeaderMenu();
    }

    private void fillTable(List<CanSignal> signals) {
        for (CanSignal signal : signals) {
            tableModel.addRow(new Object[]{signal.getShortName(), signal.getType(), signal.getFormat(), signal.getMultiplexer(), signal.getStartBit(), signal.getLength(),
                signal.getFactor(), signal.getOffset(), signal.getMinimum(), signal.getMaximum(), signal.getUnit(), signal.getComment()});
        }
    }

    @Override
    public void treeUpdate(DefaultMutableTreeNode node) {
        tableModel.setRowCount(0);
        if (node != null) {
            if (node.getUserObject() instanceof DbcObject) {
                fillTable(getSignalsFromDbcNode(node));
                scrollToAndSelectRow(0);
            } else if (node.getUserObject() instanceof CanMessage) {
                CanMessage message = (CanMessage) node.getUserObject();
                List<CanSignal> signals = getSignalsFromMessageNode(node);
                fillTable(signals);
                scrollToAndSelectRow(0);
            } else if (node.getUserObject() instanceof CanSignal) {
                CanSignal signal = (CanSignal) node.getUserObject();
                List<CanSignal> signals = getSignalsFromMessageNode((DefaultMutableTreeNode) node.getParent());
                fillTable(signals);
                scrollToAndSelectRow(signals.indexOf(signal));
            }
        }
    }

    private List<CanSignal> getSignalsFromMessageNode(DefaultMutableTreeNode node) {
        List<CanSignal> signals = new ArrayList<>();
        for (int i = 0; i < node.getChildCount(); i++) {
            DefaultMutableTreeNode signalNode = (DefaultMutableTreeNode) node.getChildAt(i);
            signals.add((CanSignal) signalNode.getUserObject());
        }
        return signals;
    }

    private List<CanSignal> getSignalsFromDbcNode(DefaultMutableTreeNode node) {
        List<CanSignal> signals = new ArrayList<>();
        for (int i = 0; i < node.getChildCount(); i++) {
            DefaultMutableTreeNode messageNode = (DefaultMutableTreeNode) node.getChildAt(i);
            for (int j = 0; j < messageNode.getChildCount(); j++) {
                DefaultMutableTreeNode signalNode = (DefaultMutableTreeNode) messageNode.getChildAt(j);
                signals.add((CanSignal) signalNode.getUserObject());
            }
        }
        return signals;
    }

    /**
     * Editing for this table is disabled. Remove this function
     * or have it return true, to make it editable.
     * @param row the row of the cell
     * @param column the column of the cell
     * @return
     */
    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
