/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel.dbc;

import components.sidepanel.SidePanel;
import components.sidepanel.SubPanel;
import can.DbcManager;

import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Michiel
 */
@SuppressWarnings("serial")
public class DbcPanel extends SidePanel {

    public DbcPanel(String title) {
        super(title);
        SubPanel dbcTreePanel = new SubPanel();
        SubPanel signalPanel = new SubPanel("CAN Signals");

        JScrollPane scrollPane = new JScrollPane();
        DbcTree tree = new DbcTree();

        scrollPane.setViewportView(tree);
        scrollPane.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
        dbcTreePanel.setBorder(new EmptyBorder(0, 0, 3, 0));
        dbcTreePanel.add(scrollPane);
        this.add(dbcTreePanel);

        SubPanel messagePanel = new SubPanel("CAN Messages");
        MessageTable messageTable = new MessageTable();
        JScrollPane scrollPane2 = new JScrollPane();
        scrollPane2.setViewportView(messageTable);
        scrollPane2.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
        messagePanel.add(scrollPane2);

        messagePanel.setPreferredSize(new Dimension(240, 190));
        messagePanel.setBorder(new EmptyBorder(0, 0, 3, 0));
        messagePanel.setMinimumSize(new Dimension(240, 14));
        this.add(messagePanel);

        SignalTable signalTable = new SignalTable();
        JScrollPane scrollPane3 = new JScrollPane();
        scrollPane3.setViewportView(signalTable);
        scrollPane3.setBorder(BorderFactory.createLineBorder(BORDER_COLOR));
        signalPanel.add(scrollPane3);

        signalPanel.setPreferredSize(new Dimension(240, 190));
        signalPanel.setMinimumSize(new Dimension(240, 15));
        this.add(signalPanel);

        DbcManager.addDbcContainerListener(tree);

        addSearchField(tree, "Search DBC Files");
        tree.addListener(messageTable);
        tree.addListener(signalTable);
        messageTable.setComponentListener(messagePanel);
        signalTable.setComponentListener(signalPanel);

        layout.putConstraint(SpringLayout.NORTH, dbcTreePanel, 0, SpringLayout.NORTH, contentPanel);
        layout.putConstraint(SpringLayout.WEST, dbcTreePanel, 0, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.EAST, dbcTreePanel, 0, SpringLayout.EAST, contentPanel);
        layout.putConstraint(SpringLayout.SOUTH, dbcTreePanel, 0, SpringLayout.NORTH, messagePanel);

        layout.putConstraint(SpringLayout.WEST, messagePanel, 0, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.EAST, messagePanel, 0, SpringLayout.EAST, contentPanel);
        layout.putConstraint(SpringLayout.SOUTH, messagePanel, 0, SpringLayout.NORTH, signalPanel);

        layout.putConstraint(SpringLayout.WEST, signalPanel, 0, SpringLayout.WEST, contentPanel);
        layout.putConstraint(SpringLayout.EAST, signalPanel, 0, SpringLayout.EAST, contentPanel);
        layout.putConstraint(SpringLayout.SOUTH, signalPanel, 0, SpringLayout.SOUTH, contentPanel);

    }
}
