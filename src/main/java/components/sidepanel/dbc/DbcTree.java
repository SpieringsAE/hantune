/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components.sidepanel.dbc;

import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;

import can.CanMessage;
import can.CanSignal;
import can.DbcManager;
import can.DbcManagerListener;
import can.DbcObject;
import components.sidepanel.AbstractTree;
import components.sidepanel.DescriptionNode;
import components.sidepanel.DescriptionTreeMenuManager;
import components.sidepanel.TreeTransferHandler;

/**
 *
 * @author Michiel
 */
@SuppressWarnings("serial")
public class DbcTree extends AbstractTree implements DbcManagerListener {
    private DescriptionNode root = new DescriptionNode();
    public static DataFlavor flavor;
    public static DataFlavor listFlavor;


    public DbcTree() {

        this.setRootVisible(false);
        this.setShowsRootHandles(true);
        this.setDragEnabled(true);

        this.setTransferHandler(new TreeTransferHandler(this));
        this.setShowsRootHandles(true);
        this.setDynamicToolTip();

        this.setCellRenderer(new DefaultTreeCellRenderer() {
            private final ImageIcon dbcIcon = createImageIcon("/images/sidepanel/dbc.png");
            private final ImageIcon messageIcon = createImageIcon("/images/sidepanel/dbc_message.png");
            private final ImageIcon messageDisabledIcon = createImageIcon("/images/sidepanel/dbc_messageDisabled.png");
            private final ImageIcon signalIcon = createImageIcon("/images/sidepanel/dbc_signal.png");
            private final ImageIcon signalDisabledIcon = createImageIcon("/images/sidepanel/dbc_signalDisabled.png");
            private final ImageIcon signalInOutIcon =
                createImageIcon("/images/sidepanel/dbc_signalInOut.png");
            private final ImageIcon signalInIcon = createImageIcon("/images/sidepanel/dbc_signalIn.png");
            private final ImageIcon signalOutIcon = createImageIcon("/images/sidepanel/dbc_signalOut.png");

            @Override
            public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected,
                                                          boolean expanded, boolean isLeaf, int row,
                                                          boolean focused) {
                super.getTreeCellRendererComponent(tree, value, selected, expanded, isLeaf, row, focused);
                if (!(value instanceof DescriptionNode)) {
                    return this;
                }
                DescriptionNode node = (DescriptionNode)value;

                if (node.getUserObject() instanceof CanSignal) {
                    setEnabled(node.isEnabled());

                    CanSignal signal = (CanSignal)node.getUserObject();
                    if (signal.isConnectedToEditor() && signal.isConnectedToViewer()) {
                        setIcon(signalInOutIcon);
                    } else if (signal.isConnectedToEditor()) {
                        setIcon(signalOutIcon);
                    } else if (signal.isConnectedToViewer()) {
                        setIcon(signalInIcon);
                    } else {
                        setIcon(signalIcon);
                    }
                    setDisabledIcon(signalDisabledIcon);

                } else if (node.getUserObject() instanceof CanMessage) {
                    setEnabled(node.isEnabled());
                    setIcon(messageIcon);
                    setDisabledIcon(messageDisabledIcon);

                } else if (node.getUserObject() instanceof DbcObject) {
                    setIcon(dbcIcon);
                }


                return this;
            }
        });

        this.setModel(new DefaultTreeModel(root));

        addEventListeners();
    }


    @Override
    public void dbcUpdate() {
        root = buildDbcTree(DbcManager.getDbcList());
        this.setModel(new DefaultTreeModel(root));
        updateListeners(null);
    }


    @Override
    public void messageUpdate() {
        this.updateUI();
    }


    @Override
    public void signalUpdate() {
        this.updateUI();
    }


    private DescriptionNode buildDbcTree(List<DbcObject> dbcList) {
        DescriptionNode newRoot = new DescriptionNode();
        for (DbcObject dbc : dbcList) {
            DescriptionNode dbcNode = new DescriptionNode(dbc);
            for (CanMessage message : dbc.getMessages()) {
                DescriptionNode messageNode = new DescriptionNode(message);
                messageNode.setType(DescriptionNode.Type.SIGNAL_PARENT);
                messageNode.setType(DescriptionNode.Type.PARAMETER_PARENT);
                messageNode.setEnabled(!message.isExtendedDLC());
                messageNode.setDragEnable(!message.isExtendedDLC());

                for (CanSignal signal : message.getSignals()) {
                    DescriptionNode signalNode = new DescriptionNode(signal);
                    signalNode.setType(DescriptionNode.Type.SIGNAL);
                    signalNode.setType(DescriptionNode.Type.PARAMETER);
                    signalNode.setEnabled(messageNode.isEnabled());
                    signalNode.setDragEnable(messageNode.isDragEnabled());

                    messageNode.add(signalNode);
                }
                dbcNode.add(messageNode);
            }
            newRoot.add(dbcNode);
        }
        return newRoot;
    }


    private void addEventListeners() {
        this.addTreeSelectionListener((TreeSelectionEvent e) -> {
            DescriptionNode node = (DescriptionNode)this.getLastSelectedPathComponent();
            updateListeners(node);
        });

        DescriptionTreeMenuManager menuManager = new DescriptionTreeMenuManager(this);
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                if (SwingUtilities.isRightMouseButton(event)) {
                    menuManager.showMenu(event);
                } else if (event.getClickCount() == 2) {
                    menuManager.quickAddItem(event);
                }
            }
        });
    }
}
