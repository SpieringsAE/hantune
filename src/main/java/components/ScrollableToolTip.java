/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolTip;
import javax.swing.KeyStroke;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

public class ScrollableToolTip extends JToolTip implements MouseWheelListener, AncestorListener {
    private static final long serialVersionUID = 1L;

    private JTextArea tipArea;
    private JScrollPane scrollpane;
    private JComponent comp;
    private KeyStroke kUp = null;
    private KeyStroke kDown = null;
    private KeyStroke kPgUp = null;
    private KeyStroke kPgDn = null;
    private KeyStroke kHome = null;
    private KeyStroke kEnd = null;
    private KeyStroke kLeft = null;
    private KeyStroke kRight = null;


    /** Creates a scrollable tool tip. */
    @SuppressWarnings("serial")
    public ScrollableToolTip(final int width, final int height, final JComponent comp) {
        this.comp = comp;
        setPreferredSize(new Dimension(width, height));
        setLayout(new BorderLayout());
        tipArea = new JTextArea();
        tipArea.setEditable(false);
        tipArea.setLineWrap(true);
        tipArea.setWrapStyleWord(true);
        tipArea.setBackground(Color.WHITE);
        tipArea.setFont(new java.awt.Font("Tahoma", 0, 11));

        scrollpane = new JScrollPane(tipArea);
        scrollpane.setBorder(BorderFactory.createEmptyBorder());
        this.add(scrollpane);
        kUp = KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0);
        comp.getInputMap(JComponent.WHEN_FOCUSED).put(kUp, "actionWhenKeyUp");
        comp.getActionMap().put("actionWhenKeyUp", new AbstractAction("keyUpAction") {
            public void actionPerformed(ActionEvent e) {
                FontMetrics metrics = getFontMetrics(tipArea.getFont());
                handleKeyActionPerformed(-metrics.getHeight(), e);
            }
        });

        kDown = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0);
        comp.getInputMap(JComponent.WHEN_FOCUSED).put(kDown, "actionWhenKeyDown");
        comp.getActionMap().put("actionWhenKeyDown", new AbstractAction("keyDownAction") {
            public void actionPerformed(ActionEvent e) {
                FontMetrics metrics = getFontMetrics(tipArea.getFont());
                handleKeyActionPerformed(metrics.getHeight(), e);
            }
        });

        kPgUp = KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, 0);
        comp.getInputMap(JComponent.WHEN_FOCUSED).put(kPgUp, "actionWhenKeyPgUp");
        comp.getActionMap().put("actionWhenKeyPgUp", new AbstractAction("keyPgUpAction") {
            public void actionPerformed(ActionEvent e) {
                handleKeyActionPerformed(-(int)scrollpane.getViewportBorderBounds().getHeight(), e);
            }
        });

        kPgDn = KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, 0);
        comp.getInputMap(JComponent.WHEN_FOCUSED).put(kPgDn, "actionWhenKeyPgDn");
        comp.getActionMap().put("actionWhenKeyPgDn", new AbstractAction("keyPgDnAction") {
            public void actionPerformed(ActionEvent e) {
                handleKeyActionPerformed((int)scrollpane.getViewportBorderBounds().getHeight(), e);
            }
        });

        kHome = KeyStroke.getKeyStroke(KeyEvent.VK_HOME, 0);
        comp.getInputMap(JComponent.WHEN_FOCUSED).put(kHome, "actionWhenKeyHome");
        comp.getActionMap().put("actionWhenKeyHome", new AbstractAction("keyHomeAction") {
            public void actionPerformed(ActionEvent e) {
                handleKeyActionPerformed(Integer.MIN_VALUE, e);
            }
        });

        kEnd = KeyStroke.getKeyStroke(KeyEvent.VK_END, 0);
        comp.getInputMap(JComponent.WHEN_FOCUSED).put(kEnd, "actionWhenKeyEnd");
        comp.getActionMap().put("actionWhenKeyEnd", new AbstractAction("keyEndAction") {
            public void actionPerformed(ActionEvent e) {
                handleKeyActionPerformed(Integer.MAX_VALUE, e);
            }
        });

        kLeft = KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0);
        comp.getInputMap(JComponent.WHEN_FOCUSED).put(kLeft, "actionWhenKeyLeft");
        comp.getActionMap().put("actionWhenKeyLeft", new AbstractAction("keyLeftAction") {
            public void actionPerformed(ActionEvent e) {
                handleKeyActionPerformed(-1, e);
            }
        });

        kRight = KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0);
        comp.getInputMap(JComponent.WHEN_FOCUSED).put(kRight, "actionWhenKeyRight");
        comp.getActionMap().put("actionWhenKeyRight", new AbstractAction("keyRightAction") {
            public void actionPerformed(ActionEvent e) {
                handleKeyActionPerformed(1, e);
            }
        });


        addAncestorListener(this);
    }

    private void handleKeyActionPerformed(int increment, ActionEvent e) {
        final JScrollBar bar = scrollpane.getVerticalScrollBar();
        int currentValue = bar.getValue();
        if (increment == Integer.MAX_VALUE || increment == Integer.MIN_VALUE) {
            bar.setValue(increment);
        } else {
            bar.setValue(currentValue + increment);
        }
    }


    @Override
    public void mouseWheelMoved(final MouseWheelEvent e) {
        scrollpane.dispatchEvent(e);
    }


    @Override
    public void ancestorRemoved(AncestorEvent e) {
        comp.removeMouseWheelListener(this);

        // return all keys to parent
        comp.getInputMap(JComponent.WHEN_FOCUSED).remove(kUp);
        comp.getInputMap(JComponent.WHEN_FOCUSED).remove(kDown);
        comp.getInputMap(JComponent.WHEN_FOCUSED).remove(kPgUp);
        comp.getInputMap(JComponent.WHEN_FOCUSED).remove(kPgDn);
        comp.getInputMap(JComponent.WHEN_FOCUSED).remove(kHome);
        comp.getInputMap(JComponent.WHEN_FOCUSED).remove(kEnd);
        comp.getInputMap(JComponent.WHEN_FOCUSED).remove(kLeft);
        comp.getInputMap(JComponent.WHEN_FOCUSED).remove(kRight);

    }


    @Override
    public void ancestorAdded(AncestorEvent e) {
        comp.addMouseWheelListener(this);
    }


    @Override
    public void ancestorMoved(AncestorEvent e) {
    }


    @Override
    public void setTipText(final String tipText) {
        String oldValue = this.tipArea.getText();
        FontMetrics metrics = getFontMetrics(tipArea.getFont());
        tipArea.setText(" " + tipText);
        if (tipText.indexOf("<br>") == -1 && tipText.indexOf('\n') == -1 && tipText.length() < 100) {
            setPreferredSize(new Dimension(metrics.stringWidth(tipText) + 12, metrics.getHeight() + 8));
        } else {
            setPreferredSize(new Dimension(400, 10 * metrics.getHeight() + 4));
        }

        tipArea.setCaretPosition(0);
        firePropertyChange("tiptext", oldValue, tipText);
    }


    @Override
    public String getTipText() {
        return tipArea == null ? "" : tipArea.getText();
    }


    @Override
    protected String paramString() {
        String tipTextString = (tipArea.getText() != null ? tipArea.getText() : "");

        return super.paramString() + ",tipText=" + tipTextString;
    }
}

