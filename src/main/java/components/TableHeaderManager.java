/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package components;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author Michiel
 */
@SuppressWarnings("serial")
public class TableHeaderManager {

    private final JTableHeader header;
    private final List<TableColumn> hiddenColumns = new ArrayList<>();
    private final List<Integer> excludedColumns = new ArrayList<>();
    private final TableColumnModel columnModel;
    private JPopupMenu menu;
    private boolean isMenuEnabled;

    public TableHeaderManager(JTableHeader header) {
        this.header = header;
        columnModel = header.getColumnModel();
        menu = new JPopupMenu();
    }

    public void addTableHeaderMenu() {
        createHeaderMenu();
        isMenuEnabled = true;
        header.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if(isMenuEnabled) showPopup(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(isMenuEnabled) showPopup(e);
            }

            private void showPopup(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    menu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });
    }

    private void createHeaderMenu() {
        menu = new JPopupMenu();
        for (int i = 0; i < columnModel.getColumnCount(); i++) {
            if (excludedColumns.contains(i)) continue;
            JCheckBoxMenuItem item = new JCheckBoxMenuItem((String) columnModel.getColumn(i).getHeaderValue());
            item.addActionListener(e -> menuActionPerformed(e));
            if (!hiddenColumns.contains(columnModel.getColumn(i))) {
                item.setState(true);
            }
            menu.add(item);
        }
    }

    public void addMenuItem(Component menuItem) {
        menu.insert(menuItem, 0);
    }

    public void setMenuEnabled(boolean isEnabled) {
        isMenuEnabled = isEnabled;
    }

    private void menuActionPerformed(ActionEvent event) {
        JCheckBoxMenuItem menuItem = (JCheckBoxMenuItem) event.getSource();
        if (columnModel.getColumnCount() == 1 && !menuItem.getState()) {
            menuItem.setState(true);
        } else {
            if (menuItem.getState()) {
                for (TableColumn col : hiddenColumns) {
                    if (col.getHeaderValue().equals(menuItem.getText())) {
                        showColumn(col);
                        break;
                    }
                }
            } else if (!menuItem.getState()) {
                int index = columnModel.getColumnIndex(menuItem.getText());
                TableColumn col1 = columnModel.getColumn(index);
                hideColumn(col1);
            }
        }
    }

    private void showColumn(TableColumn column) {
        columnModel.addColumn(column);
        hiddenColumns.remove(column);
    }

    private void hideColumn(TableColumn column) {
        columnModel.removeColumn(column);
        hiddenColumns.add(column);
    }

    public void addHiddenColumn(TableColumn column) {
        hiddenColumns.add(column);
    }

    public void hideColumns() {
        for (TableColumn column : hiddenColumns) {
            columnModel.removeColumn(column);
        }
    }

    public void showColumns() {
        for (TableColumn column : hiddenColumns) {
            columnModel.addColumn(column);
        }
    }

    public void excludeColumnFromMenu(int index) {
        if (!excludedColumns.contains(index)) excludedColumns.add(index);
    }

    public void addHexOptionToMenu(TableColumn column) {
        JCheckBoxMenuItem menuItem = new JCheckBoxMenuItem("Hexadecimal " + column.getHeaderValue() + "s");
        menuItem.addActionListener(e -> toggleHexFormat(e, column));
        menu.insert(new JSeparator(), 0);
        menu.insert(menuItem, 0);
    }

    public void toggleHexFormat(ActionEvent event, TableColumn column) {
        JCheckBoxMenuItem menuItem = (JCheckBoxMenuItem) event.getSource();
        if (menuItem.getState()) {
            column.setCellRenderer(new DefaultTableCellRenderer() {
                @Override
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    value = "0x" + String.format("%X", Long.valueOf(String.valueOf(value)));
                    return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                }
            });
        } else {
            column.setCellRenderer(null);
        }
        header.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        header.updateUI();
    }
}
