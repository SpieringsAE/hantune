/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ErrorMonitoring;

import java.util.Objects;

/**
 *
 * @author Mathijs
 */
public class ErrorObject {

    private String code = "";
    private int parameter = 0;
    private int occurrence = 0;
    private String timestamp = "";
    private String info = "";

    public ErrorObject(String code, int parameter, int occurrence, String timestamp, String info) {
        this.parameter = parameter;
        this.occurrence = occurrence;
        this.timestamp = timestamp;
        this.info = info;
        setCode(code);
    }

    public ErrorObject() {
        this("", 0, 0, "", "");
    }

    /**
     * Constructor for copying ErrorObjects
     * @param obj
     */
    public ErrorObject(ErrorObject obj) {
        this(obj.code, obj.parameter, obj.occurrence, obj.timestamp, obj.info);
    }

    /**
     * Returns the error code
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the error code
     * @param code
     */
    public void setCode(String str) {
        this.code = str;
        // check if info is empty. in this case try to do a smart info fill
        if (info.isEmpty() && !this.code.isEmpty()) {
            // convert hex string back to integer
            int codeValue = Integer.parseUnsignedInt(code,16);
            // specific info for a RC_30, but also valid for other targets
            if (codeValue > 0 && codeValue <= 32767) { // 0 - 0x7FFF
                setInfo("System error");
            } else if (codeValue >= 32768 && codeValue <= 61439) { // 0x8000 - 0xEFFF
                setInfo("Application error");
            } else if (codeValue >= 61440 && codeValue <= 65535) { // 0xF000 - 0xFFFF
                setInfo("Target error");
            } else {
                setInfo("-");
            }
        }
    }

    /**
     * Returns the error parameter
     * @return
     */
    public int getParameter() {
        return parameter;
    }

    /**
     * Sets the error parameter
     * @param parameter
     */
    public void setParameter(int parameter) {
        this.parameter = parameter;
    }

    /**
     * Returns the error occurrence
     * @return
     */
    public int getOccurrence() {
        return occurrence;
    }

    /**
     * Sets the error occurrence
     * @param occurrence
     */
    public void setOccurrence(int occurrence) {
        this.occurrence = occurrence;
    }

    /**
     * Returns the error timestamp
     * @return
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * Sets the error timestamp
     * @param timestamp
     */
    public void setTimestamp(String time) {
        this.timestamp = time;

    }

    /**
     * Gets the error info
     * @return
     */
    public String getInfo() {
        return info;
    }

    /**
     * Sets the error info
     * @param info
     */
    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.code);
        hash = 97 * hash + this.parameter;
        hash = 97 * hash + this.occurrence;
        hash = 97 * hash + Objects.hashCode(this.timestamp);
        hash = 97 * hash + Objects.hashCode(this.info);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ErrorObject other = (ErrorObject) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (this.parameter != other.parameter) {
            return false;
        }
        if (this.occurrence != other.occurrence) {
            return false;
        }
        if (!Objects.equals(this.timestamp, other.timestamp)) {
            return false;
        }
        if (!Objects.equals(this.info, other.info)) {
            return false;
        }
        return true;
    }
}
