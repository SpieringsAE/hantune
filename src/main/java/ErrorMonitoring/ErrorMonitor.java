/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ErrorMonitoring;

import ErrorLogger.AppendToLogfile;
import ErrorLogger.defaultExceptionHandler;

import java.util.ArrayList;

/**
 * Error Monitor Class. Class that implements an abstraction layer for reading
 * and clearing errors (diagnostic trouble codes) from a target.
 *
 * @author Frank Voorburg
 */
public class ErrorMonitor implements IErrorInfoProcessor {
    // =========================================================================
    // Fields
    // =========================================================================
    /**
     * Interval time between requests, sent to the target, to update the error
     * info.
     */
    private final int THREAD_PERIOD_MS = 500;

    /**
     * Flag with the enabled status of the error monitor.
     */
    private boolean mEnabled;

    /**
     * Periodic worker thread of the error monitor.
     */
    private ErrorMonitorThread mThread;

    /**
     * The controller through which error clear and read requests can be
     * submitted.
     */
    private IErrorInfoController mController;

    /**
     * List with observers that need to be updated each time new error
     * information becomes available.
     */
    private ArrayList<IErrorObserver> mObservers;

    // =========================================================================
    // Constructors
    // =========================================================================
    /**
     * Constructor for creating the error monitor.
     */
    public ErrorMonitor() {
        /* initialize members */
        mEnabled = false;
        mController = null;
        mObservers = new ArrayList<>();
    }

    // =========================================================================
    // Methods
    // =========================================================================
    /**
     * Enables the error monitor making it possible to interact with the target.
     */
    public void enable() {
        // only enable if not already enabled
        if (!mEnabled) {
            // create the thread
            mThread = new ErrorMonitorThread();
            // set enabled flag
            mEnabled = true;
            // start the thread
            mThread.start();
            // initialize observers with empty error list
            errorInfoAvailable(new ArrayList<ErrorObject>(), new ArrayList<ErrorObject>());
        }
    }

    /**
     * Disables the error monitor.
     */
    public void disable() {
        // only disable if not already disabled
        if (mEnabled) {
            // interrupt the thread to stop it
            mThread.interrupt();
            // wait for the thread to stop
            while (mThread.isAlive()) {
                try {
                    // give other processes a change to run
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    // stop the loop if something interrupted us and make sure
                    // the interrupting is processed by setting the flag
                    AppendToLogfile.appendError(Thread.currentThread(), e);
                    Thread.currentThread().interrupt();
                    break;
                }
            }
            // reset enabled flag
            mEnabled = false;
            // release the thread object
            mThread = null;
        }
    }

    /**
     * Submit the request to clear the errors on a target.
     *
     * @param active True if the active (RAM) errors should be cleared, false otherwise.
     * @param stored True if the stored (EEPROM) errors should be cleared, false otherwise.
     */
    public void clearErrors(boolean active, boolean stored) {
        // pass the request on to the controller, if set
        if (mController != null) {
          mController.clearErrorInfo(active, stored);
      }
    }

    /**
     * initialises the observes by clearing the error info.
     */
    public void initObservers() {
        for (IErrorObserver ob : mObservers) {
            ob.update(new ArrayList<ErrorObject>(), new ArrayList<ErrorObject>());
        }
    }

    /**
     * Registers an observer that wants to be kept up-to-date regarding
     * error information.
     *
     * @param observer The observer to register.
     */
    public void registerObserver(IErrorObserver observer) {
        mObservers.add(observer);
    }

    /**
     * Removes an observer.
     *
     * @param observer The observer to remove.
     */
    public void removeObserver(IErrorObserver observer) {
        mObservers.remove(observer);
    }

    /**
     * Removes all observers.
     */
    public void clearObservers() {
        mObservers.clear();
    }

    // =========================================================================
    // Getter & Setter
    // =========================================================================
    /**
     * Sets the controller that can then be used to submit error read and clear
     * requests when the error monitor is enabled.
     *
     * @param controller Specifies the controller to use. Can also be null to
     *                   explicitely reset the controller.
     */
    public void setController(IErrorInfoController controller) {
        mController = controller;
    }

    // =========================================================================
    // Overrides
    // =========================================================================
    /**
     * Callback that gets called by an error info controller each time new
     * error information became available.
     *
     * @param activeErrors List with active (RAM) errors.
     * @param storedErrors List with stored (EEPROM) errors.
     */
    @Override
    public void errorInfoAvailable(ArrayList<ErrorObject> activeErrors, ArrayList<ErrorObject> storedErrors) {
        // send an update to all observers to keep them up-to-date with the latest
        // error code info.
        for (IErrorObserver ob : mObservers) {
            ob.update(activeErrors, storedErrors);
        }
    }

    // =========================================================================
    // Inner classes
    // =========================================================================
    /**
     * Command class for reading the number of diagnostic trouble codes.
     */
    private class ErrorMonitorThread extends Thread {
        // =====================================================================
        // Constructors
        // =====================================================================
        /**
         * Constructor for creating the error monitor.
         */
        public ErrorMonitorThread() {
            /* set the thread title */
            super("ErrorMonitor Thread");
            /* set the threads exception handler */
            Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
            /* change priority to normal */
            this.setPriority(Thread.NORM_PRIORITY);
        }

        // =====================================================================
        // Overrides
        // =====================================================================
        /**
         * Periodic background method for requesting updated error info from the target.
         */
        @Override
        public void run() {
            Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    // submit request to read error info from the target
                    if (ErrorMonitor.this.mController != null) {
                        ErrorMonitor.this.mController.readErrorInfo(ErrorMonitor.this);
                    }
                    // delay a bit between error info update requests
                    try {
                        Thread.sleep(THREAD_PERIOD_MS);
                    } catch (InterruptedException exit) {
                        AppendToLogfile.appendError(Thread.currentThread(), exit);
                        // filter out the interrupt exception as this one is expected
                        // when the thread is stopped. note that this exception also
                        // clears the interrupted status of the thread, so we need to
                        // explicitely break the loop here to actually stop the
                        // thread. another option would be so set the interrupt flag
                        // again: Thread.currentThread().interrupt();
                        break;
                    }
                }
            } catch (Exception e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
            }
        }
    }
}
