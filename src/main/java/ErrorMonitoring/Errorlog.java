/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ErrorMonitoring;

import ErrorLogger.AppendToLogfile;
import datahandling.CurrentConfig;
import util.Util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JTable;

/**
 *
 * @author Mathijs
 */
public class Errorlog {

    private CurrentConfig currentConfig = CurrentConfig.getInstance();
    private String file_ext = "csv";
    private String prefix = "";
    private String prefix2 = "ERRLOG";
    private String error = "";

    private boolean active = false;

    private FileOutputStream os = null;
    private PrintStream out = null;


    public Errorlog(){
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public String getError(){
        return error;
    }

    public void setError(String error){
        this.error = error;
    }

    public String getPrefix(){
        return prefix;
    }

    public void setPrefix(String prefix){
        this.prefix = prefix;
    }

    /**
     * Opens a new datalog file in the 'log' directory of HANtune. The filename
     * is based on the current date and time.
     * @return
     */
    public boolean openDatalog(JTable errTableA, JTable errTableS){
        String file_path = currentConfig.getLogFile_Path();
        try {
            // construct filename
            Calendar c = Calendar.getInstance();
            String filename = c.get(Calendar.YEAR)+""+((((c.get(Calendar.MONTH)+1))<10)?"0":"")+(c.get(Calendar.MONTH)+1)+""+(((c.get(Calendar.DAY_OF_MONTH))<10)?"0":"")+c.get(Calendar.DAY_OF_MONTH)+"_"+(((c.get(Calendar.HOUR_OF_DAY))<10)?"0":"")+c.get(Calendar.HOUR_OF_DAY)+""+(((c.get(Calendar.MINUTE))<10)?"0":"")+c.get(Calendar.MINUTE)+""+(((c.get(Calendar.SECOND))<10)?"0":"")+c.get(Calendar.SECOND);
            if(prefix.equals("")) prefix = Util.getFilename(currentConfig.getProjectFile().getName()).replace(".hml", "");
            String fn = prefix + "_" + prefix2 + "_" + filename + "." + file_ext;
            File directory = new File(file_path);

            // if the directory does not exist, create it
            if (!directory.exists()) {
                directory.mkdirs();
            }
            filename = directory.getAbsolutePath() + "\\" + fn;

            // open new file
            os = new FileOutputStream(filename);
            if(os == null){
                setError("unable to create output stream to filesystem");
                closeDatalog();
                return false;
            }
            out = new PrintStream(os);
            if(out == null){
                setError("unable to create print stream to filesystem");
                closeDatalog();
                return false;
            }

            // add header information
            out.println("Project: " + Util.getFilename(currentConfig.getProjectFile().getName()));
            out.println("ECU: " + currentConfig.getXcpsettings().slaveid);
            out.println("Date: " + new Date());
            out.println("");

            // print table Active
            out.println("Active Error List:");
            out.println("Code;Parameter;Occurrence;Time;Info");
            //ErrorViewer errorViewer = new ErrorViewer();
            for (int i = 0; i < errTableA.getRowCount(); i++) {
                StringBuilder sbA = new StringBuilder();
                for (int j = 0; j < errTableA.getColumnCount(); j++) {
                    sbA.append(errTableA.getValueAt(i, j)).append(";");

                }
                out.print(sbA);
                out.print(System.getProperty("line.separator"));
            }

            out.print("\r\n");
            out.println("Stored Error List:");
            out.println("Code;Parameter;Occurrence;Time;Info");

            // print table Stored
            for (int i = 0; i < errTableS.getRowCount(); i++) {
                StringBuilder sbS = new StringBuilder();
                for (int j = 0; j < errTableS.getColumnCount(); j++) {
                    sbS.append(errTableS.getValueAt(i, j)).append(";");
                }
                out.print(sbS);
                out.print(System.getProperty("line.separator"));
            }

        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            setError(e.getMessage());
            closeDatalog();
            return false;
        }
        return true;
    }

    public boolean closeDatalog(){
        try {
            if(out != null){
                out.close();
            }
            if(os != null){
                os.close();
            }
            out = null;
            os = null;
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            setError(e.getMessage());
            return false;
        }
        return true;
    }

}
