/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import HANtune.viewers.TextViewer;
import components.HintedTextField;
import components.JFontChooser;
import datahandling.Signal;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;

import util.Util;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class TextAndThresholdDialog extends HanTuneDialog {

    private final TextViewer window;
    private final Signal signal;
    private final List<TextAndThresholdElement> elements;
    private Box textAndThresholdBox;
    private JLabel infoLabel;
    private JButton okButton;

    private static final String INTEGER_WARNING = "f != java.lang.Integer";
    private static final String FLOAT_WARNING = "d != java.lang.Double";

    private boolean thresholdsValid = true;
    private String thresholdWarningText = "";
    private boolean formatsValid = true;
    private String formatWarningText = "";

    public TextAndThresholdDialog(TextViewer window, Signal signal) {
        this.window = window;
        this.signal = signal;
        elements = new ArrayList<>();
        createHeader();
        createFooter();
        createThresholds();
        setTitle("HANtune - Text and Threshold Dialog");
        setResizable(false);



        //set the location to the center of the screen
        setLocationRelativeTo(null);

        //sometimes pack() needs to be called twice for it to work properly
        pack();
    }

    private void createHeader() {
        JTextArea infoArea = new JTextArea("Set the ranges and enter the text that should be shown when the signal is within that range."
                + " String formats can be used to show the value of the signal, instead of or in combination with text.");
        infoArea.setLineWrap(true);
        infoArea.setWrapStyleWord(true);
        infoArea.setEditable(false);
        infoArea.setRows(3);
        infoArea.setBackground(this.getBackground());
        infoArea.setFont(new Font("Tahoma", 0, 11));
        JLabel iconLabel = new JLabel(UIManager.getIcon("OptionPane.informationIcon"));
        iconLabel.setBorder(BorderFactory.createEmptyBorder(0, 3, 8, 3));
        iconLabel.setToolTipText(createInfoTooltip());

        Box infoBox = Box.createHorizontalBox();
        infoBox.add(infoArea);
        infoBox.add(iconLabel);

        JButton addButton = new JButton("Add Range");
        addButton.addActionListener(e -> addTextAndThresholdElement());
        JButton removeButton = new JButton("Remove Range");
        removeButton.addActionListener(e -> removeTextAndThresholdElement());

        Box addRemoveBox = Box.createHorizontalBox();
        addRemoveBox.add(addButton);
        addRemoveBox.add(removeButton);
        addRemoveBox.add(Box.createHorizontalGlue());

        Box header = Box.createVerticalBox();
        header.add(infoBox);
        header.add(addRemoveBox);
        header.setBorder(BorderFactory.createEmptyBorder(3, 3, 0, 3));
        add(header, BorderLayout.NORTH);
    }

    private String createInfoTooltip() {
        return "<html>"
                + "String formats can be used to show the value of a signal and determine how it is represented.<br>"
                + "Each format used must be prefixed with \"%\". It is possible to use multiple formats in a single <br>"
                + "line of text. If one of the formats within a single line contains an error, none of them will work.<br>"
                + "<br>"
                + "A few examples of formats are:<br>"
                + "<br>"
                + "<b>%f</b>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(floating point numbers)<br>"
                + "<b>%d</b> 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(integer numbers)<br>"
                + "<b>%h</b> 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(hexadecimal numbers, %H for uppercase)<br>"
                + "<b>%.2f</b>	&nbsp;&nbsp;(floating point number with two decimals)<br>"
                + "<b>%%</b>	&nbsp;&nbsp;&nbsp;&nbsp;(displays a percentage sign)<br>"
                + "<b>08</b>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(prefix the value with leading zeros to 8 digits)<br>"
                + "<br>"
                + "A few examples of how formats can be used in combination with text or eachother and what<br>"
                + "the result will look like for a value of <b>100</b>.<br>"
                + "<br>"
                + "<i>Input:</i> Decimal:<b>%04d</b> Hexadecimal:0x<b>%h</b><br>"
                + "<i>Result:</i> Decimal:<b>0100</b> Hexadecimal: 0x<b>64</b><br>"
                + "<br>"
                + "<i>Input:</i> <b>%H</b>h<br>"
                + "<i>Result:</i> <b>64</b>h<br>"
                + "<br>"
                + "<i>Input:</i> Capacity: <b>%d%%</b><br>"
                + "<i>Result:</i> Capacity: <b>100%</b><br>"
                + "<br>"
                + "<i>Input:</i> <b>%.2f%%</b><br>"
                + "<i>Result:</i> <b>100,00%</b>"
                + "</html>";
    }

    private void createFooter() {
        okButton = new JButton("Ok");
        okButton.addActionListener(e -> close(true));
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> close(false));
        infoLabel = new JLabel();
        infoLabel.setFont(new Font("Tahoma", 2, 11));

        Box footer = Box.createHorizontalBox();
        footer.add(infoLabel);
        footer.add(Box.createHorizontalGlue());
        footer.add(okButton);
        footer.add(cancelButton);
        footer.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        add(footer, BorderLayout.SOUTH);
    }

    private void createThresholds() {
        textAndThresholdBox = Box.createVerticalBox();
        textAndThresholdBox.setBorder(BorderFactory.createEmptyBorder(3, 3, 0, 3));
        add(textAndThresholdBox, BorderLayout.CENTER);

        if (!window.hasSetting(signal.getName(), "thresholdCount")) {
            addTextAndThresholdElement();
            addTextAndThresholdElement();
        } else {
            loadSettings();
        }
    }

    private TextAndThresholdElement addTextAndThresholdElement() {
        TextAndThresholdElement element = new TextAndThresholdElement(elements.size() + 1);
        element.spinner.addChangeListener(e -> validateThresholds());
        elements.add(element);
        textAndThresholdBox.add(element);
        if (elements.size() > 1) {
            TextAndThresholdElement previousElement = elements.get(elements.size() - 2);
            previousElement.setValue(0.0);
        }
        setLastThresholdInvisible();
        pack();
        return element;
    }

    private void removeTextAndThresholdElement() {
        if (elements.size() > 2) {
            TextAndThresholdElement lastElement = elements.get(elements.size() - 1);
            elements.remove(lastElement);
            textAndThresholdBox.remove(lastElement);
            elements.get(elements.size() - 1).setValue(signal.getMaximum());
            setLastThresholdInvisible();
            pack();
        }
    }

    private void validateThresholds() {
        double previousValue = 0;
        for (TextAndThresholdElement element : elements) {
            double currentValue = element.getValue();
            int index = elements.indexOf(element);
            if (index != 0) {
                thresholdsValid = currentValue > previousValue;
                if (!thresholdsValid) {
                    thresholdWarningText = getThresholdWarningText(index, currentValue, previousValue);
                    break;
                }
            }
            previousValue = currentValue;
        }
        updateInfoMessage();
        okButton.setEnabled(thresholdsValid);
    }

    private String getThresholdWarningText(int index, double currentValue, double previousValue) {
        String format = "%.2f";
        if (!signal.hasDecimals()) {
            format = "%.0f";
        }
        return String.format("Limit " + index + " must be smaller than Limit " + (index + 1) + " (" + format + " >= " + format + ")", previousValue, currentValue);
    }

    private void validateFormats() {
        for (TextAndThresholdElement element : elements) {
            try {
                String text = element.getText();
                if (!signal.hasDecimals() && signal.getMaximum() <= Integer.MAX_VALUE) {
                    formatWarningText = String.format(text, Util.getFormatArgumentArray(0, text));
                } else {
                    formatWarningText = String.format(text, Util.getFormatArgumentArray(0d, text));
                }
                formatsValid = true;
            } catch (IllegalFormatException exception) {
                //Tests user input, no need to log
                formatsValid = false;
                switch (exception.getMessage()) {
                    case INTEGER_WARNING:
                        formatWarningText = "Invalid format: Signal is not a floating point number";
                        break;
                    case FLOAT_WARNING:
                        formatWarningText = "Invalid format: Signal is not an integer number";
                        break;
                    default:
                        formatWarningText = "Invalid format: " + element.getText();
                        break;
                }
                break;
            }
        }
        updateInfoMessage();
    }

    private void updateInfoMessage() {
        String info;
        if (!thresholdsValid) {
            infoLabel.setForeground(Color.RED);
            info = thresholdWarningText;
        } else if (!formatsValid) {
            infoLabel.setForeground(Color.RED);
            info = formatWarningText;
        } else {
            infoLabel.setForeground(Color.BLACK);
            info = "All ranges and formats are valid ";
        }
        //add extra space or the last character will be cut off due to cursive font
        infoLabel.setText(info + " ");
    }

    private void setLastThresholdInvisible() {
        for (Component component : textAndThresholdBox.getComponents()) {
            TextAndThresholdElement element = (TextAndThresholdElement) component;
            boolean visible = element != textAndThresholdBox.getComponent(textAndThresholdBox.getComponentCount() - 1);
            element.thresholdBox.setVisible(visible);
        }
    }

    private void loadSettings() {
        Map<String, String> settings = window.settings.get(signal.getName());
        int thresholdCount = Integer.parseInt(settings.get("thresholdCount"));
        for (int i = 1; i <= thresholdCount; i++) {
            if (settings.containsKey("threshold" + i)) {
                TextAndThresholdElement element = new TextAndThresholdElement(i);
                element.spinner.addChangeListener(e -> validateThresholds());
                elements.add(element);
                textAndThresholdBox.add(element);
                double value = Double.parseDouble(settings.get("threshold" + i));
                element.setValue(value);
                String text = settings.get("text" + i);
                element.setText(text);
                Color color = Color.decode(settings.get("color" + i));
                element.setColor(color);
                element.font = Font.decode(settings.get("font" + i));
            }
        }
        setLastThresholdInvisible();
        validateThresholds();
        pack();
    }

    private void saveSettings() {
        clearSettings();
        for (TextAndThresholdElement element : elements) {
            int index = elements.indexOf(element) + 1;

            String value = String.valueOf(element.getValue());
            window.addSetting(signal.getName(), "threshold" + index, value);

            String text = element.getText();
            window.addSetting(signal.getName(), "text" + index, text);

            String color = String.valueOf(element.getColor().hashCode());
            window.addSetting(signal.getName(), "color" + index, color);

            String font = Util.getFontString(element.font);
            window.addSetting(signal.getName(), "font" + index, font);
        }
        window.addSetting(signal.getName(), "thresholdCount", String.valueOf(elements.size()));
    }

    private void clearSettings() {
        if (window.settings.containsKey(signal.getName())) {
            Map<String, String> settings = window.settings.get(signal.getName());
            settings.entrySet().removeIf(e -> e.getKey().contains("threshold")
                    || e.getKey().contains("text")
                    || e.getKey().contains("color")
                    || e.getKey().contains("font"));
        }
    }

    private void close(boolean save) {
        if (save) {
            saveSettings();
            window.loadSettings();
            window.repaint();
        } else {
            window.refreshLabel();
        }
        dispose();
    }

    private class TextAndThresholdElement extends Box {

        private JTextField textField;
        private String text;
        private Font font;
        private final Box colorBox;
        private Color color;

        private final Box thresholdBox;
        private final JSpinner spinner;

        public TextAndThresholdElement(int index) {
            super(BoxLayout.Y_AXIS);

            Box textBox = Box.createHorizontalBox();
            createTextField(index);
            textBox.add(textField);
            text = "";
            colorBox = Box.createVerticalBox();
            colorBox.setPreferredSize(new Dimension(21, 21));
            colorBox.setMaximumSize(colorBox.getPreferredSize());
            colorBox.setBackground(Color.BLACK);
            colorBox.setOpaque(true);
            color = colorBox.getBackground();
            textBox.add(colorBox);
            JButton editColorButton = new JButton("Edit Color");
            editColorButton.addActionListener(e -> editColor());
            textBox.add(editColorButton);
            JButton editFontButton = new JButton("Edit font");
            editFontButton.addActionListener(e -> editFont());
            textBox.add(editFontButton);
            add(textBox);

            thresholdBox = Box.createHorizontalBox();
            spinner = new JSpinner(new SpinnerNumberModel(signal.getMaximum(), signal.getMinimum(), signal.getMaximum(), signal.getFactor()));
            spinner.setPreferredSize(new Dimension(200, 21));
            spinner.setMaximumSize(spinner.getPreferredSize());
            thresholdBox.add(spinner);
            JLabel thresholdLabel = new JLabel("Limit " + index);
            thresholdBox.add(thresholdLabel);
            thresholdBox.add(Box.createHorizontalGlue());
            add(thresholdBox);
        }

        private void createTextField(int index) {
            String hint = index == 1 ? "Text shown when value <= limit 1" : "Text shown when value > limit " + (index - 1);
            textField = new HintedTextField(hint);
            textField.addFocusListener(new FocusAdapter() {
                @Override
                public void focusGained(FocusEvent event) {
                    validateFormats();
                    showPreview();
                }
            });
            textField.addKeyListener(new KeyAdapter() {
                @Override
                public void keyReleased(KeyEvent event) {
                    text = textField.getText();
                    validateFormats();
                    showPreview();
                }
            });
            textField.setPreferredSize(new Dimension(200, 21));
            textField.setMaximumSize(textField.getPreferredSize());
            font = textField.getFont();
        }

        private String getText() {
            return text;
        }

        private void setText(String text) {
            this.text = text;
            textField.setText(text);
        }

        private Color getColor() {
            return color;
        }

        private void setColor(Color color) {
            this.color = color;
            colorBox.setBackground(color);
        }

        private double getValue() {
            return (double) spinner.getValue();
        }

        private void setValue(double value) {
            spinner.setValue(value);
        }

        private void editColor() {
            Color newColor = JColorChooser.showDialog(this, "Choose font color", colorBox.getBackground());
            if (newColor != null) {
                setColor(newColor);
                showPreview();
            }
        }

        private void editFont() {
            JFontChooser fontChooser = new JFontChooser();
            if (fontChooser.showDialog(null, text, font) == JFontChooser.OK_OPTION) {
                font = fontChooser.getSelectedFont();
                showPreview();
            }
        }

        private void showPreview() {
            JLabel label = new JLabel(!text.isEmpty() ? text : "?");
            label.setForeground(colorBox.getBackground());
            label.setFont(font);
            window.setLabel(label);
            window.repaint();
        }
    }
}
