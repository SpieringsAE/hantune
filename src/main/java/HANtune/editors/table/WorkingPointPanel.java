/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.editors.table;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class WorkingPointPanel extends JPanel {

    private final int radius = 11;
    private final int offset = radius / 2;
    private final int alpha = 75;

    private Color color = new Color(20, 20, 20, alpha);
    private int x = 0;
    private int y = 0;
    private boolean drawX = false;
    private boolean drawY = false;
    private boolean show = false;

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        if (show) {
            g.setColor(color);
            ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            if (drawX && drawY) {
                //vertical
                g.drawLine(x, 0, x, y - offset);
                g.drawLine(x, y + offset, x, y + offset + getHeight());

                //horizontal
                g.drawLine(0, y, x - offset, y);
                g.drawLine(x + offset, y, x + offset + getWidth(), y);

                //workingpoint
                g.fillOval(x - offset, y - offset, radius, radius);
            } else if (drawX) {
                g.drawLine(x, 0, x, getHeight());
            } else if (drawY) {
                g.drawLine(0, y, getWidth(), y);
            }
        }
    }

    public void setPointX(int x) {
        this.x = x;
    }

    public void setPointY(int y) {
        this.y = y;
    }

    public void setPointColor(Color color) {
        this.color = new Color(color.getRed(), color.getGreen(), color.getBlue(), alpha);
    }

    public boolean isWithinPoint(int x, int y) {
        return (x >= this.x - offset && x <= this.x + offset) && (y >= this.y - offset && y <= this.y + offset);
    }

    /**
     * @param drawX the drawX to set
     */
    public void setDrawPointX(boolean drawX) {
        this.drawX = drawX;
    }

    /**
     * @param drawY the drawY to set
     */
    public void setDrawPointY(boolean drawY) {
        this.drawY = drawY;
    }

    /**
     * @param show the show to set
     */
    public void setShowPoint(boolean show) {
        this.show = show;
    }
}
