/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.editors.multiEditor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author Aart-Jan made usable by Michel de Beijer and Mike van Laar
 */
public class SpinnerListener implements ChangeListener, ActionListener {
    private static final int MIN_TIME = 100;
    private long lastChange = System.currentTimeMillis();
    private boolean valueChanged = false;

    /**
     * Constructor
     */
    public SpinnerListener() {
        Timer repeatTimer;
        repeatTimer = new Timer(500, this);
        repeatTimer.start();
    }

    /**
     * Default implementation of the stateChanged event of a spinnerListener.
     * @param e
     */
    @Override
    public void stateChanged(ChangeEvent e) {
        valueChanged = true;
        lastChange = System.currentTimeMillis();
    }

    /**
     * Default implementation of the actionPerformed event of a spinnerListener.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (valueChanged && (System.currentTimeMillis() - lastChange) > MIN_TIME) {
            valueChanged = false;
        }
    }
}
