/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.editors;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import ErrorLogger.AppendToLogfile;
import HANtune.OptionDialog;
import HANtune.editors.RadioButtonEditor.RadioButtonSet;
import datahandling.CurrentConfig;
import datahandling.Parameter;
import datahandling.ParameterListener;
import util.Util;

/**
 *
 * @author Jason van Kolfschoten, Michiel Klifman
 */
@SuppressWarnings("serial")
public class RadioButtonEditor extends HANtuneEditor<RadioButtonSet> {

    protected class RadioButtonSet extends HANtuneEditorElement implements ParameterListener {

        private final Integer maxNumberOfOptions = 5;
        private Integer numberOfOptions = 2;
        private final ButtonGroup radioButtonGroup = new ButtonGroup();
        private final RadioButton[] radioButtons = new RadioButton[maxNumberOfOptions];
        private double setValue = 0;

        public RadioButtonSet(Parameter parameter) {
            super(parameter, RadioButtonEditor.this);
        }

        @Override
        public void valueUpdate(double value) {
            getValueLabel().setText(Util.getStringValue(value, "0.##########"));
            boolean match = false;
            for (RadioButton radioButton : radioButtons) {
                if (radioButton != null) {
                    match = radioButton.getValue() == value;
                    if (match) {
                        radioButton.setSelected(match);
                        setValue = value;
                        break;
                    }
                }
            }
            if (!match) {
                radioButtonGroup.clearSelection();
            }
        }
    }



    // Constructor of the RadioButtonEditor
    public RadioButtonEditor() {
        setVerticalResizable(false);
        setTitle("RadioButtonEditor");
    }

    @Override
    public void rebuild() {
        super.rebuild();
        packAndSetMinMax();
    }

    @Override
    protected RadioButtonSet getListenerForParameter(Parameter parameter) {
        return createRadioButtonSet(parameter);
    }


    protected RadioButtonSet createRadioButtonSet(Parameter parameter) {
        int row = getContentPane().getComponentCount();
        GridBagConstraints gridBagConstraints;
        final RadioButtonSet radioButtonSet = new RadioButtonSet(parameter);

        // Get the number of options in order to set the right amount of RadioButtons
        if (settings.containsKey(parameter.getName())
            && settings.get(parameter.getName()).containsKey("numberOfOptions")) {
            radioButtonSet.numberOfOptions =
                Integer.parseInt(settings.get(parameter.getName()).get("numberOfOptions"));
        }

        gridBagConstraints = new GridBagConstraints();

        radioButtonSet.getNameLabel().setToolTipText(parameter.getSource());
        gridBagConstraints.weightx = 1;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = row;
        gridBagConstraints.insets = new Insets(5, 5, 5, 0);
        getContentPane().add(radioButtonSet.getNameLabel(), gridBagConstraints);

        JLabel valueJLabel = new JLabel("");
        valueJLabel.setHorizontalAlignment(SwingConstants.CENTER);
        valueJLabel.setPreferredSize(new Dimension(70, 18));
        radioButtonSet.setValueLabel(valueJLabel);
        gridBagConstraints.weightx = 0;
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = row;
        gridBagConstraints.insets = new Insets(5, 0, 5, 0);
        getContentPane().add(valueJLabel, gridBagConstraints);

        if (parameter.getUnit() != null && !parameter.getUnit().equals("")) {
            gridBagConstraints.gridx = 2;
            gridBagConstraints.gridy = row;
            gridBagConstraints.insets = new Insets(5, 5, 5, 0);
            getContentPane().add(radioButtonSet.getUnitLabel(), gridBagConstraints);

        }

        // Generate the correct number of radiobuttons
        for (int i = 0; i < radioButtonSet.numberOfOptions; i++) {
            radioButtonSet.radioButtons[i] = new RadioButton("Option " + (i + 1), false);
            radioButtonSet.radioButtons[i].setAsap2Ref(parameter.getName());
            gridBagConstraints.gridx = 3;
            gridBagConstraints.gridy = row++;
            gridBagConstraints.insets = new Insets(5, 5, 5, 5);
            gridBagConstraints.anchor = GridBagConstraints.WEST;
            getContentPane().add(radioButtonSet.radioButtons[i], gridBagConstraints);
            radioButtonSet.radioButtonGroup.add(radioButtonSet.radioButtons[i]);

            radioButtonSet.radioButtons[i].addActionListener(e -> {
                RadioButton radioButton = (RadioButton)e.getSource();
                radioButtonSet.setValue = radioButton.getValue();
                parameter.send(radioButtonSet.setValue);
            });

            if (CurrentConfig.getInstance().isServiceToolMode()) {
                radioButtonSet.radioButtons[i].setComponentPopupMenu(null);
            } else {
                addPopupMenuItems(parameter, radioButtonSet, i);
            }

            radioButtonSet.addStateComponent(radioButtonSet.radioButtons[i]);
        }
        return radioButtonSet;
    }

    private void addPopupMenuItems(Parameter parameter, final RadioButtonSet radioButtonSet, int i) {
        JPopupMenu popupMenu = new JPopupMenu();

        // Menu item for calling OptionDialog, copy and edit this for editors that use the dialog
        JMenuItem optionsMenuItem = new JMenuItem("Modify options");
        optionsMenuItem.addActionListener(e -> new OptionDialog(RadioButtonEditor.this, parameter.getName()));
        popupMenu.add(optionsMenuItem);

        radioButtonSet.radioButtons[i].setComponentPopupMenu(popupMenu);
    }

    @Override
    public void loadSettings() {

        for (Map.Entry<Parameter, RadioButtonSet> entry : parameters.entrySet()) {
            Parameter parameter = entry.getKey();
            RadioButtonSet radioButtonSet = entry.getValue();
            NumberFormat inputFormat = NumberFormat.getInstance(Locale.getDefault());

            for (int i = 1; i <= radioButtonSet.numberOfOptions; i++) {
                if (settings.containsKey(parameter.getName()) && settings.get(parameter.getName()).containsKey("inputValue" + i)) {
                    Double inputValue;
                    try {
                        inputValue = inputFormat.parse(settings.get(parameter.getName()).get("inputValue" + i)).doubleValue();
                        radioButtonSet.radioButtons[i - 1].setValue(inputValue);
                    } catch (ParseException ex) {
                        AppendToLogfile.appendError(Thread.currentThread(), ex);
                        Logger.getLogger(RadioButtonEditor.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                if (settings.containsKey(parameter.getName()) && settings.get(parameter.getName()).containsKey("textValue" + i)) {
                    radioButtonSet.radioButtons[i - 1].setText(settings.get(parameter.getName()).get("textValue" + i));
                }
            } //end of for loop

        } //end of iterator
        repaint();
    }

    /**
     * RadioButton class
     *
     */
    private class RadioButton extends JRadioButton {

        private static final long serialVersionUID = 7526471155622776170L;

        private Double value = 0.0;
        private String asap2Ref;

        //Constructor
        RadioButton(String setText, boolean selected) {
            setText(setText);
            setSelected(selected);
        }

        public String getTitle() {
            return title;
        }

        public void setValue(double value) {
            this.value = value;
        }

        public double getValue() {
            return value;
        }

        public void setAsap2Ref(String asap2Ref) {
            this.asap2Ref = asap2Ref;
        }

        public String getAsap2Ref() {
            return asap2Ref;
        }
    }
}

