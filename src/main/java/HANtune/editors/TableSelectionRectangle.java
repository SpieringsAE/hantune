/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.editors;

public class TableSelectionRectangle {

    private int startColumn = 0;
    private int startRow = 0;
    private int endColumn = 0;
    private int endRow = 0;

    public void setStart(int column, int row) {
        this.startColumn = column;
        this.startRow = row;
    }

    public void setEnd(int column, int row) {
        this.endColumn = column;
        this.endRow = row;
    }

    public int getColumnStart() {
        return startColumn < endColumn ? startColumn : endColumn;
    }

    public int getRowStart() {
        return startRow < endRow ? startRow : endRow;
    }

    public int getColumnStop() {
        return endColumn > startColumn ? endColumn : startColumn;
    }

    public int getRowStop() {
        return endRow > startRow ? endRow : startRow;
    }

    public int getStartColumn() {
        return startColumn;
    }

    public int getStartRow() {
        return startRow;
    }

    public boolean isStart(int column, int row) {
        return column == startColumn && row == startRow;
    }

    public boolean isInSelection(int col, int row) {
        return col >= getColumnStart() && col <= getColumnStop() && row >= getRowStart() && row <= getRowStop();
    }
}
