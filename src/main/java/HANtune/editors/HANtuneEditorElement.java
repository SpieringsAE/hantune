/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.editors;

import HANtune.ReferenceWindow;
import components.ReferenceLabel;
import datahandling.Parameter;
import datahandling.ReferenceStateListener;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JLabel;

/**
 * A base class for viewer and editor elements. Most viewers and editors share
 * common GUI components (i.e. a JLabel to represent the name or unit of a
 * signal or parameter). The JComponent valueComponent is used to store
 * implementation specific components (i.e. a JSpinner for MultiEditor or a
 * JSlider for the SliderEditor.
 *
 * @author Michiel Klifman
 */
public class HANtuneEditorElement implements ReferenceStateListener {

    private final Parameter parameter;
    private final ReferenceLabel<Parameter> nameLabel;
    private JLabel valueLabel = null;
    private JComponent valueComponent = null;
    private final List<JComponent> stateComponents = new ArrayList<>();

    public HANtuneEditorElement(Parameter parameter, ReferenceWindow<Parameter> window) {
        this.parameter = parameter;
        nameLabel = new ReferenceLabel<>(parameter, window);
    }

    /**
     * @return the name label
     */
    public JLabel getNameLabel() {
        return nameLabel;
    }

    /**
     * @return the unit label
     */
    public JLabel getUnitLabel() {
        return new JLabel(parameter.getUnit());
    }

    public JLabel getValueLabel() {
        return valueLabel;
    }

    public void setValueLabel(JLabel valueLabel) {
        this.valueLabel = valueLabel;
    }

    /**
     * @return the valueJSpinner
     */
    public JComponent getValueComponent() {
        return valueComponent;
    }

    /**
     * @param valueComponent the valueJSpinner to set
     */
    public void setValueComponent(JComponent valueComponent) {
        this.valueComponent = valueComponent;
    }

    public void addStateComponent(JComponent component) {
        stateComponents.add(component);
    }

    public void removeStateComponent(JComponent component) {
        stateComponents.remove(component);
    }

    @Override
    public void stateUpdate() {
        for (JComponent stateComponent : stateComponents) {
            if (!parameter.isActive()) {
                stateComponent.setEnabled(false);
                stateComponent.setBackground(ReferenceWindow.ACTIVE_COLOR);
                stateComponent.setToolTipText("N/A");
            } else {
                stateComponent.setEnabled(true);
                if (!parameter.isInSync()) {
                    stateComponent.setBackground(ReferenceWindow.WARNING_COLOR);
                    stateComponent.setToolTipText("Value could not be sent");
                } else {
                    stateComponent.setBackground(ReferenceWindow.ACTIVE_COLOR);
                    stateComponent.setToolTipText(null);
                }
            }
        }
    }
}
