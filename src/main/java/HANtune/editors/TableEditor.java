/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.editors;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.Box;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.NumberFormatter;

import HANtune.ReferenceWindow;
import components.VerticalLabelUI;
import datahandling.Signal;
import datahandling.Table;
import datahandling.TableListener;
import HANtune.editors.table.WorkingPointPanel;
import datahandling.SignalAdapter;
import nl.han.hantune.config.ApplicationProperties;
import util.Util;

import javax.swing.JTextField;
import static nl.han.hantune.config.ConfigProperties.TABLEEDITOR_LABEL_POSITION;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MAX;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MID;
import static nl.han.hantune.config.ConfigProperties.TABLE_FIELD_COLOR_MIN;
import nl.han.hantune.gui.dialogs.GetReferenceDialog;

/**
 * Shows the TableEditor for editing CURVE and MAP values of asap2Data
 * CHARACTERISTIC's.
 *
 * @author Aart-Jan, Michel de Beijer, Mike van Laar, Michiel Klifman
 */
@SuppressWarnings("serial")
public class TableEditor extends ReferenceWindow<Table> implements TableListener {

    /**
     * @param interpolation the interpolation to set
     */
    public void setInterpolation(String interpolation) {
        this.interpolation = interpolation;
    }

    /**
     * @param minTableValue the minTableValue to set
     */
    public void setMinTableValue(double minTableValue) {
        this.minTableValue = minTableValue;
    }

    /**
     * @param midTableValue the midTableValue to set
     */
    public void setMidTableValue(double midTableValue) {
        this.midTableValue = midTableValue;
    }

    /**
     * @param maxTableValue the maxTableValue to set
     */
    public void setMaxTableValue(double maxTableValue) {
        this.maxTableValue = maxTableValue;
    }

    private final GridBagConstraintsCreator gridBagConstraintsCreator;
    private static final int TEXTFIELD_WIDTH = 7;
    private static final String TABLE_TEXTFIELD = "table_textfield";
    private static final String AXIS_POINT_TEXTFIELD = "axis_point";
    private static final int COLUMN_AXIS = 0;
    private static final int ROW_AXIS = 1;
    private String multiplier = "1.1";
    private String divider = "1.1";
    private String adder = "1.0";
    private String subtractor = "1.0";
    private WorkingPointPanel panel;
    private JScrollPane scrollPane;
    private TableEditorCell[][] textFields;
    private TableEditorCell[][] axisPoints;
    private Table table;
    private JLabel YAxisLabel;
    private final TableSelectionRectangle selection = new TableSelectionRectangle();

    private AxisSignalManager xAxisManager;
    private AxisSignalManager yAxisManager;

    private double minTableValue = 0;
    private double midTableValue = 0;
    private double maxTableValue = 0;
    private boolean autoCalculation = false;
    private String interpolation = "none";

    /**
     * The constructor of the MultiLedViewer class
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public TableEditor() {
        super(Table.class, false);
        gridBagConstraintsCreator = new GridBagConstraintsCreator();
        setTitle("HANtune - TableEditor");
        setResizable(false);
        repaintPeriod = 500;
        setDefaultLabelPosition(ApplicationProperties.getTopBottomLabelPos(TABLEEDITOR_LABEL_POSITION).getLabelPositionString());

    }

    @Override
    public void rebuild() {
        super.rebuild();
        saveAndShowBorder(getWindowSetting("showBorder", DEFAULT_BORDER));
        pack();
        if (YAxisLabel != null && YAxisLabel.getHeight() > scrollPane.getHeight()) {
            YAxisLabel.setPreferredSize(new Dimension(YAxisLabel.getWidth(), scrollPane.getHeight()));
            YAxisLabel.setSize(new Dimension(YAxisLabel.getWidth(), scrollPane.getHeight()));
        }
        packAndSetMinMax();

        JMenu addMenuItem = new JMenu("Axis signals");
        JMenuItem selectXSignalMenuItem = new JMenuItem("Select X axis signal");
        selectXSignalMenuItem.addActionListener(e -> selectXAxisSignal());

        JMenuItem selectYSignalMenuItem = new JMenuItem("Select Y axis signal");
        selectYSignalMenuItem.addActionListener(e -> selectYAxisSignal());

        JMenuItem modMenuItem = new JMenuItem("Select modification factors");
        modMenuItem.addActionListener(e -> selectModifcationFactors());

        addMenuItem.add(selectXSignalMenuItem);
        addMenuItem.add(selectYSignalMenuItem);

        addMenuToPopupMenu(addMenuItem);
        addMenuItemToPopupMenu(modMenuItem);

        JMenuItem editColorsAndRangesMenuItem = new JMenuItem("Edit Table Settings");
        editColorsAndRangesMenuItem.addActionListener(e -> {
            new TableSettingsDialog(this).setVisible(true);
            loadSettings();
        });
        addMenuItemToPopupMenu(editColorsAndRangesMenuItem);

    }

    private void selectXAxisSignal() {
        Signal signal = new GetReferenceDialog<>(Signal.class).showDialog();
        xAxisManager.setSignal(signal);
    }

    private void selectYAxisSignal() {
        Signal signal = new GetReferenceDialog<>(Signal.class).showDialog();
        yAxisManager.setSignal(signal);
    }

    private void selectModifcationFactors() {

        JPanel contentPanel = new JPanel(new BorderLayout(5, 5));

        JPanel labels = new JPanel(new GridLayout(4, 1, 2, 2));
        labels.add(new JLabel("Multipier *", SwingConstants.RIGHT));
        labels.add(new JLabel("Divider /", SwingConstants.RIGHT));
        labels.add(new JLabel("Adder +", SwingConstants.RIGHT));
        labels.add(new JLabel("Subtractor -", SwingConstants.RIGHT));
        contentPanel.add(labels, BorderLayout.WEST);

        JPanel controls = new JPanel(new GridLayout(4, 1, 2, 2));
        JFormattedTextField field1 = new JFormattedTextField(createNumberFormatter());
        JFormattedTextField field2 = new JFormattedTextField(createNumberFormatter());
        JFormattedTextField field3 = new JFormattedTextField(createNumberFormatter());
        JFormattedTextField field4 = new JFormattedTextField(createNumberFormatter());

        field1.setText(multiplier);
        field2.setText(divider);
        field3.setText(adder);
        field4.setText(subtractor);
        controls.add(field1);
        controls.add(field2);
        controls.add(field3);
        controls.add(field4);
        contentPanel.add(controls, BorderLayout.CENTER);

        int option = JOptionPane.showConfirmDialog(this, contentPanel, "Choose your modification factors", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            multiplier = field1.getText();
            divider = field2.getText();
            adder = field3.getText();
            subtractor = field4.getText();
            addWindowSetting("Multiplier", multiplier);
            addWindowSetting("Divider", divider);
            addWindowSetting("Adder", adder);
            addWindowSetting("Subtractor", subtractor);

        }
    }

    /**
     * Creates a Content panel to add a table to.
     */
    private void createContentPanel() {
        scrollPane = new JScrollPane();
        scrollPane.setOpaque(false);
        scrollPane.setComponentPopupMenu(TableEditor.this.getMenu());
        panel = new WorkingPointPanel();
        getPanel().setLayout(new GridBagLayout());
        getPanel().setBackground(Color.WHITE);
        scrollPane.setViewportView(getPanel());

        // add scrollpane
        scrollPane.setBorder(null);
        GridBagConstraints gridBagConstraints = gridBagConstraintsCreator.createGridBagConstraints(1, 1);
        getContentPane().add(scrollPane, gridBagConstraints);

    }

    /**
     * Creates the table content. Adds the cells and axis points for the table
     * based on its type.
     *
     * @param table reference to parameter on the ecu
     */
    private void createTable(Table table) {
        int cols = table.getColumnCount();
        int rows = table.getRowCount();
        textFields = new TableEditorCell[cols][rows];
        setTitle(table.getName());
        createTableMap(table, cols, rows);
        axisPoints = new TableEditorCell[table.getAxisDescr().size()][];
        switch (table.getAxisDescr().size()) {
            case 1:
                createTableAxis(table.getAxisColumnDescr(), cols);
                xAxisManager = new AxisSignalManager(COLUMN_AXIS, table.getColumnCount());
                getPanel().setDrawPointX(true);
                break;
            case 2:
                createTableAxis(table.getAxisColumnDescr(), cols);
                xAxisManager = new AxisSignalManager(COLUMN_AXIS, table.getColumnCount());
                getPanel().setDrawPointX(true);
                createTableAxis(table.getAxisRowDescr(), rows);
                yAxisManager = new AxisSignalManager(ROW_AXIS, table.getRowCount());
                getPanel().setDrawPointY(true);
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }

    /**
     * Create table cells for a table with MAP type
     *
     * @param cols number of columns in table
     * @param rows number of rows in table
     * @param asap2Ref reference to parameter on the ECU
     */
    private void createTableMap(Table asap2Ref, int cols, int rows) {
        NumberFormatter valueNumberFormat = createNumberFormatter();

        for (int i = 0; i < cols; i++) {
            for (int j = 0; j < rows; j++) {
                textFields[i][j] = createAxisTextField(TABLE_TEXTFIELD, valueNumberFormat, i, j);
                textFields[i][j].setValue(table.getValueAt(j, i));              // j = row, i = column
                Insets insets = new Insets(1, 1, 0, 0);
                GridBagConstraints gridBagConstraints = gridBagConstraintsCreator.createGridBagConstraints(i, j, insets);       //i = gridX, j = gridY
                getPanel().add(textFields[i][j], gridBagConstraints);
            }
        }
    }

    /**
     * Creates the axes for the table (e.g. the group of text fields to the top
     * and left side of the table.
     *
     * @param description - the description(name) of the axis
     * @param size - the size of the axis
     */
    private void createTableAxis(String description, int size) {
        List<String> axisDescr = table.getAxisDescr();
        int axis = axisDescr.indexOf(description);

        axisPoints[axis] = createTableAxisTextfields(axis, size);
        createTableLabels(description, axis);
    }

    /**
     * Creates an array of text fields for an axis of the table.
     *
     * @param axis - for which axis this is. 0 for columns(top) 1 for rows(left)
     * @param size - the size of the axis
     * @return an array containing text fields for the axis
     */
    private TableEditorCell[] createTableAxisTextfields(int axis, int size) {
        TableEditorCell[] axisTextFields = new TableEditorCell[size];
        NumberFormatter axisNumberFormat = createNumberFormatter();
        JPanel axisPanel = new JPanel();
        axisPanel.setLayout(new GridBagLayout());

        for (int i = 0; i < size; i++) {
            axisTextFields[i] = createAxisTextField(AXIS_POINT_TEXTFIELD, axisNumberFormat, i, axis);
            axisTextFields[i].setValue(table.getAxisPointsValueAt(axis, i));
            Insets insets = new Insets(1, 1, 0, 0);
            int[] grid = checkAxisIndexForGridBagConstraints(axis, i);
            GridBagConstraints gridBagConstraints = gridBagConstraintsCreator.createGridBagConstraints(grid[0], grid[1], insets);
            Box wrapper = Box.createHorizontalBox();
            wrapper.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            wrapper.add(axisTextFields[i]);
            axisPanel.add(wrapper, gridBagConstraints);
            setHeaderView(axis, axisPanel);
        }
        return axisTextFields;
    }

    /**
     * Creates a text field for the table or one of its axes.
     *
     * @param type - the type of text field (TABLE_TEXTFIELD or
     * AXIS_POINT_TEXTFIELD).
     * @param numberFormat
     * @param column
     * @param row
     * @return a TableEditorCell for the table or one of its axes.
     */
    private TableEditorCell createAxisTextField(String type, NumberFormatter numberFormat, int column, int row) {
        final TableEditorCell textField = new TableEditorCell(numberFormat);

        textField.setValue(0);
        textField.setColumns(TEXTFIELD_WIDTH);
        textField.setHorizontalAlignment(JFormattedTextField.RIGHT);
        textField.setActionCommand(type);
        textField.addActionListener(e -> actionPerformed(e, row, column));
        textField.setComponentPopupMenu(TableEditor.this.getMenu());
        textField.setHorizontalAlignment(JTextField.CENTER);

        if (type.equals(TABLE_TEXTFIELD)) {

            textField.setEditting(false);
            textField.setBorder(createDefaultBorder());

            textField.addMouseListener(new MouseAdapter() {

                @Override
                public void mousePressed(MouseEvent e) {
                    if (SwingUtilities.isLeftMouseButton(e)) {

                        if (selection.isStart(column, row)) {
                            textField.setReadyForEditing(true);
                        } else {
                            textFields[selection.getStartColumn()][selection.getStartRow()].setEditting(false);
                            textFields[selection.getStartColumn()][selection.getStartRow()].setReadyForEditing(false);

                            if (!e.isShiftDown()) {
                                selection.setStart(column, row);
                            }
                            selection.setEnd(column, row);
                            paintTableRange(selection);
                        }
                    }
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    if (SwingUtilities.isLeftMouseButton(e)) {
                        if (textField.isReadyForEditing() && isInTextFieldBounds(e.getX(), e.getY())) {
                            textField.setEditting(true);
                        }
                        paintTableRange(selection);
                    }
                }

                private boolean isInTextFieldBounds(int x, int y) {
                    return x >= 0 && x <= textField.getWidth() && y >= 0 && y <= textField.getHeight();
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    if ((e.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK) == MouseEvent.BUTTON1_DOWN_MASK) {
                        selection.setEnd(column, row);
                        paintTableRange(selection);
                    }
                }

            });

            textField.addKeyListener(new KeyAdapter() {
                @Override
                public void keyTyped(KeyEvent e) {
                    if (!textField.isEditable()) {
                        if (selection != null) {
                            // Do something
                            char c = e.getKeyChar();
                            switch (c) {
                                case ('*'):
                                    e.consume();
                                    editTableRange(selection, "*" + multiplier);
                                    break;
                                case ('/'):
                                    e.consume();
                                    editTableRange(selection, "/" + divider);
                                    break;
                                case ('+'):
                                    e.consume();
                                    editTableRange(selection, "+" + adder);
                                    break;
                                case ('-'):
                                    e.consume();
                                    editTableRange(selection, "-" + subtractor);
                                    break;
                                case ('m'):
                                    e.consume();
                                    setTableRange(selection, textField.getText());
                                    break;
                                default:
                            }
                        }
                    }

                }

                @Override
                public void keyPressed(KeyEvent e) {
                    if (!textField.isEditable()) {
                        if ((e.getKeyCode() == KeyEvent.VK_C) && ((e.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0)) {
                            if (selection != null) {
                                StringSelection selection = new StringSelection(convertSelectionToString());
                                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                                clipboard.setContents(selection, null);
                                e.consume();
                            }
                        }

                        if ((e.getKeyCode() == KeyEvent.VK_V) && ((e.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0)) {
                            if (selection != null) {
                                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                                Transferable t = clipboard.getContents(null);

                                if (t.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                                    String data = "";
                                    try {
                                        data = (String) t.getTransferData(DataFlavor.stringFlavor);
                                    } catch (UnsupportedFlavorException | IOException ex) {
                                        Logger.getLogger(TableEditor.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    if (!"".equals(data)) {
                                        textField.setText(data);
                                    }
                                    distributeStringOverSelection(data);
                                    e.consume();
                                }
                            }
                        }
                    }

                }
            });

        }

        textField.setFocusLostBehavior(JFormattedTextField.REVERT);
        return textField;
    }

    private boolean editTableRange(TableSelectionRectangle sel, String action) {

        if (action == null) {
            return false; // cancel key pressed
        }

        if (action.length() > 0) {
            // for correct parsing of double: either a '.' or a DEC_SEPARATOR_LOCALE will be used as decimal
            // separator
            action = action.replace(Util.DEC_SEPARATOR_LOCALE, Util.DEC_SEPARATOR_HML);

            for (int column = sel.getColumnStart(); column <= sel.getColumnStop(); column++) {
                for (int row = sel.getRowStart(); row <= sel.getRowStop(); row++) {
                    double actionValue;
                    double tempValue;

                    switch (action.charAt(0)) {
                        case '/':
                            tempValue = Double.parseDouble(textFields[column][row].getValue().toString());
                            actionValue = Double.parseDouble(action.substring(1));
                            if (actionValue != 0) {
                                tempValue /= actionValue;
                            }
                            textFields[column][row].setValue(tempValue);
                            break;

                        case '*':
                            tempValue = Double.parseDouble(textFields[column][row].getValue().toString());
                            actionValue = Double.parseDouble(action.substring(1));
                            tempValue *= actionValue;
                            textFields[column][row].setValue(tempValue);
                            break;

                        case '+':
                            tempValue = Double.parseDouble(textFields[column][row].getValue().toString());
                            actionValue = Double.parseDouble(action.substring(1));
                            tempValue += actionValue;
                            textFields[column][row].setValue(tempValue);
                            break;

                        case '-':
                            tempValue = Double.parseDouble(textFields[column][row].getValue().toString());
                            actionValue = Double.parseDouble(action.substring(1));
                            tempValue -= actionValue;
                            textFields[column][row].setValue(tempValue);
                            break;

                        default:
                            actionValue = Double.parseDouble(action);
                            tempValue = actionValue;
                            textFields[column][row].setValue(tempValue);
                            break;
                    }
                }
            }

            for (int column = sel.getColumnStart(); column <= sel.getColumnStop(); column++) {
                for (int row = sel.getRowStart(); row <= sel.getRowStop(); row++) {
                    double tempValue = Double.parseDouble(textFields[column][row].getValue().toString());
                    table.send(tempValue, row, column);
                }
            }
        }

        return true;
    }

    private boolean setTableRange(TableSelectionRectangle sel, String action) {

        if (action == null) {
            return false; // cancel key pressed
        }

        if (action.length() > 0) {
            // for correct parsing of double: either a '.' or a DEC_SEPARATOR_LOCALE will be used as decimal
            // separator
            action = action.replace(Util.DEC_SEPARATOR_LOCALE, Util.DEC_SEPARATOR_HML);

            for (int column = sel.getColumnStart(); column <= sel.getColumnStop(); column++) {
                for (int row = sel.getRowStart(); row <= sel.getRowStop(); row++) {
                    double actionValue;

                    actionValue = Double.parseDouble(action);
                    textFields[column][row].setValue(actionValue);
                    table.send(actionValue, row, column);
                }
            }
        }
        return true;
    }

    private void paintTableRange(TableSelectionRectangle selRect) {
        for (int row = 0; row < table.getRowCount(); row++) {
            for (int column = 0; column < table.getColumnCount(); column++) {
                if (column == selRect.getStartColumn() && row == selRect.getStartRow()) {
                    textFields[column][row].setBorder(createSelectedBorder());
                } else if (selRect.isInSelection(column, row)) {
                    textFields[column][row].setBorder(createSelectionBorder(row, column, selRect));
                } else {
                    textFields[column][row].setBorder(createDefaultBorder());
                }
            }
        }
    }

    private Border createDefaultBorder() {
        Border line = BorderFactory.createLineBorder(new Color(171, 173, 179));
        Border empty = BorderFactory.createEmptyBorder(3, 3, 3, 3);
        return BorderFactory.createCompoundBorder(line, empty);
    }

    private Border createSelectedBorder() {
        Border line = BorderFactory.createLineBorder(Color.BLACK, 2);
        Border empty = BorderFactory.createEmptyBorder(2, 2, 2, 2);
        return BorderFactory.createCompoundBorder(line, empty);
    }

    private Border createSelectionBorder(int row, int column, TableSelectionRectangle selRect) {
        int leftSelected = 0, topSelected = 0, bottomSelected = 0, rightSelected = 0;
        int leftEmpty, topEmpty, bottomEmpty, rightEmpty;
        int leftNormal = 0, topNormal = 0, bottomNormal = 0, rightNormal = 0;
        if (column == selRect.getColumnStart()) {
            leftSelected = 2;
            leftEmpty = 2;
        } else {
            leftNormal = 1;
            leftEmpty = 3;
        }

        if (column == selRect.getColumnStop()) {
            rightSelected = 2;
            rightEmpty = 2;
        } else {
            rightNormal = 1;
            rightEmpty = 3;
        }

        if (row == selRect.getRowStart()) {
            topSelected = 2;
            topEmpty = 2;
        } else {
            topNormal = 1;
            topEmpty = 3;
        }

        if (row == selRect.getRowStop()) {
            bottomSelected = 2;
            bottomEmpty = 2;
        } else {
            bottomNormal = 1;
            bottomEmpty = 3;
        }

        Border selected = BorderFactory.createMatteBorder(topSelected, leftSelected, bottomSelected, rightSelected, Color.BLACK);
        Border normal = BorderFactory.createMatteBorder(topNormal, leftNormal, bottomNormal, rightNormal, new Color(171, 173, 179));
        Border empty = BorderFactory.createEmptyBorder(topEmpty, leftEmpty, bottomEmpty, rightEmpty);
        return BorderFactory.createCompoundBorder(selected, BorderFactory.createCompoundBorder(normal, empty));
    }

    private String convertSelectionToString() {
        String s = new String();
        if (selection != null) {
            for (int row = selection.getRowStart(); row <= selection.getRowStop(); row++) {
                for (int column = selection.getColumnStart(); column <= selection.getColumnStop(); column++) {
                    s = s.concat(textFields[column][row].getText());
                    s = s.concat("\t");
                }
                s = s.concat("\n");
            }
        }
        return s;
    }

    private void distributeWordOverSelection(String s, int row, int column) {
        double value;
        if (row <= selection.getRowStop()
                && column <= selection.getColumnStop()) {
            System.err.println(row + " " + column + " pasting \"" + s + "\"");
            value = Double.parseDouble(s);
            System.err.println(row + " " + column + " setting to \"" + value + "\"");
            textFields[column][row].setValue(value);
            table.send(value, row, column);
        }

    }

    private void distributeLineOverSelection(String s, int row, int column) {
        String substr;
        while (s.indexOf('\t') >= 0) {
            substr = s.substring(0, s.indexOf('\t'));
            distributeWordOverSelection(substr, row, column);
            s = s.substring(s.indexOf('\t') + 1);
            column++;
        }
        // Distribute the last one
        distributeWordOverSelection(s, row, column);
    }

    private void distributeStringOverSelection(String s) {
        int row, column;
        String substr;

        if (selection != null) {
            // Will do this counterwise: consume the input string
            row = selection.getRowStart();
            column = selection.getColumnStart();

            while (s.indexOf('\n') >= 0) {   // May be -1 if there is only one line
                substr = s.substring(0, s.indexOf('\n'));
                distributeLineOverSelection(substr, row, column);
                s = s.substring(s.indexOf('\n') + 1);
                row++;
            }
            // Distribute the last line
            distributeLineOverSelection(s, row, column);
        }
    }

    /**
     * This method checks if the column or the row needs to be set and returns
     * the grid.
     *
     * @param axisIndex :The index of the axis of a table. example: 0 is for the
     * column and 1 is for the row.
     */
    private int[] checkAxisIndexForGridBagConstraints(final int axisIndex, final int cols) {
        int[] grid = new int[2];

        if (axisIndex == COLUMN_AXIS) {
            grid[0] = cols;
            grid[1] = 0;
        } else {
            grid[0] = 0;
            grid[1] = cols;
        }
        return grid;
    }

    /**
     * This method sets the columnHeaderView or rowHeaderView depending on the
     * axisIndex.
     *
     * @param axisIndex :The index of the axis of a table. example: 0 is for the
     * column and 1 is for the row.
     * @param axisPanel :The axisPanel that needs to be set.
     */
    private void setHeaderView(final int axisIndex, final JPanel axisPanel) {
        if (axisIndex == COLUMN_AXIS) {
            scrollPane.setColumnHeaderView(axisPanel);
        } else {
            scrollPane.setRowHeaderView(axisPanel);
        }
    }

    /**
     * Creates the labels for the table.
     *
     * @param asap2Ref reference to parameter on the ecu.
     * @param axisIndex AXIS_PTS index
     */
    public void createTableLabels(String asap2Ref, int axisIndex) {
        JLabel axisLabel = new JLabel(asap2Ref);
        if (axisIndex == ROW_AXIS) {
            axisLabel.setUI(new VerticalLabelUI(false));
            YAxisLabel = axisLabel;
        }
        Insets insets = new Insets(2, 2, 2, 2);
        int[] grid = checkAxisIndexForGridBagConstraints(axisIndex, 1);
        GridBagConstraints gridBagConstraints = gridBagConstraintsCreator.createGridBagConstraints(grid[0], grid[1], insets);

        getContentPane().add(axisLabel, gridBagConstraints);
    }

    /**
     * This method load the HANtuneWindow specific settings for each asap2
     * reference. This method should be called only if settings have been
     * changed. The method automatically triggers a repaint after the settings
     * are loaded.
     *
     * NOTE: Currently Nothing to be done here!
     */
    @Override
    public void loadSettings() {

        String xSignalName = getWindowSetting("XSignal", null);
        if (xSignalName != null) {
            Signal signal = currentConfig.getReferenceByName(xSignalName, Signal.class);
            if (signal != null) {
                xAxisManager.setSignal(signal);
            }
        }
        String ySignalName = getWindowSetting("YSignal", null);
        if (ySignalName != null) {
            Signal signal = currentConfig.getReferenceByName(ySignalName, Signal.class);
            if (signal != null) {
                yAxisManager.setSignal(signal);
            }
        }
        multiplier = getWindowSetting("Multiplier", "1.1");
        divider = getWindowSetting("Divider", "1.1");
        adder = getWindowSetting("Adder", "1.0");
        subtractor = getWindowSetting("Subtractor", "1.0");

        setInterpolation(getWindowSetting("interpolation", "none"));

        boolean useMid = getBooleanWindowSetting("useMid", false);

        autoCalculation = "auto".equals(getWindowSetting("calculation", "auto"));
        if (autoCalculation) {
            findMinAndMax();
        } else {
            try {
                String minValue = getWindowSetting("min", "auto");
                setMinTableValue(Double.parseDouble(minValue));
                if (useMid) {
                    String midValue = getWindowSetting("mid", "auto");
                    setMidTableValue(Double.parseDouble(midValue));
                }
                String maxValue = getWindowSetting("max", "auto");
                setMaxTableValue(Double.parseDouble(maxValue));
            } catch (NumberFormatException exception) {
                autoCalculation = true;
                findMinAndMax();
            }
        }

        String minColorString = getWindowSetting("minColor", "");
        Color minColor = !minColorString.isEmpty() ? Color.decode(minColorString) : ApplicationProperties.getColor(TABLE_FIELD_COLOR_MIN);

        String midColorString = getWindowSetting("midColor", "");
        Color midColor = !midColorString.isEmpty() ? Color.decode(midColorString) : ApplicationProperties.getColor(TABLE_FIELD_COLOR_MID);

        String maxColorString = getWindowSetting("maxColor", "");
        Color maxColor = !maxColorString.isEmpty() ? Color.decode(maxColorString) : ApplicationProperties.getColor(TABLE_FIELD_COLOR_MAX);

        String workingPointColorString = getWindowSetting("workingPointColor", "");
        Color workingPointColor = !workingPointColorString.isEmpty() ? Color.decode(workingPointColorString) : new Color(20, 20, 20);
        getPanel().setPointColor(workingPointColor);

        boolean enableWorkingPoint = getBooleanWindowSetting("showWorkingPoint", false);
        getPanel().setShowPoint(enableWorkingPoint);

        TableFieldColor.initColors(minColor, midColor, maxColor, useMid, interpolation.equals("hsb"), autoCalculation, minTableValue, midTableValue, maxTableValue);

        stateUpdate();
    }

    /**
     * Creates a NumberFormatter that will restrict the input of a cell to
     * numbers within the range (minimum - maximum) of the table.
     *
     * @return the NumberFormatter
     */
    public NumberFormatter createNumberFormatter() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setGroupingUsed(false);
        numberFormat.setMinimumFractionDigits(0);
        numberFormat.setMaximumFractionDigits(4);
        return new NumberFormatter(numberFormat) {
            @Override
            public Object stringToValue(String text) throws ParseException {
                Number number = (Number) super.stringToValue(text);
                if (number != null) {
                    double d = number.doubleValue();
                    if (!table.isValueValid(d)) {
                        throw new ParseException("Invalid Value", 0);
                    }
                }
                return number;
            }
        };
    }

    public void actionPerformed(ActionEvent e, int row, int column) {
        TableEditorCell textField = (TableEditorCell) e.getSource();
        String type = e.getActionCommand();

        double value = ((Number) textField.getValue()).doubleValue();
        switch (type) {
            case TABLE_TEXTFIELD:
                table.send(value, row, column);
                textField.setEditting(false);
                break;
            case AXIS_POINT_TEXTFIELD:
                table.sendAxisPoint(value, row, column);
                break;
        }
    }

    @Override
    protected void addReferenceContent() {
        table = references.get(0);
        createContentPanel();
        createTable(table);
        table.addListener(this);
        table.addStateListener(this);
        stateUpdate();
    }

    @Override
    public void updateTable(double value, int row, int column) {
        textFields[column][row].setValue(value);
        if (autoCalculation == true) {
            findMinAndMax();
        }
        stateUpdate();
    }

    public void findMinAndMax() {
        minTableValue = table.getValueAt(0, 0);
        maxTableValue = table.getValueAt(0, 0);
        for (int row = 0; row < table.getRowCount(); row++) {
            for (int column = 0; column < table.getColumnCount(); column++) {
                double textfieldValue = table.getValueAt(row, column);
                if (textfieldValue < minTableValue) {
                    setMinTableValue(textfieldValue);
                }
                if (textfieldValue > maxTableValue) {
                    setMaxTableValue(textfieldValue);
                }
            }
        }
    }

    @Override
    public void updateAxisPoints(double value, int axis, int index) {
        axisPoints[axis][index].setValue(value);
    }

    @Override
    public void stateUpdate() {
        for (int column = 0; column < table.getColumnCount(); column++) {
            for (int row = 0; row < table.getRowCount(); row++) {
                setTextFieldState(textFields[column][row], true, true);
            }
        }

        for (int axis = 0; axis < axisPoints.length; axis++) {
            for (int index = 0; index < axisPoints[axis].length; index++) {
                setTextFieldState(axisPoints[axis][index], table.isAxisPointInSyncAt(axis, index), false);
            }
        }
    }

    private void setTextFieldState(TableEditorCell textField, boolean inSync, boolean tf) {
        if (!table.isActive() || inactiveReferences.contains(table)) {
            textField.setToolTipText("N/A");
            textField.setEnabled(false);
        } else if (!inSync) {
            textField.setBackground(ACTIVE_COLOR);
            textField.setForeground(WARNING_COLOR);
            textField.setToolTipText("Value not in sync with target. Last known value: ?");
            textField.setEnabled(true);
        } else if (tf && !"none".equals(interpolation)) {
            textField.setBackground(getTextFieldColor(Double.parseDouble(textField.getValue().toString())));
            textField.setForeground(ACTIVE_TEXT_COLOR);
            textField.setToolTipText("");
            textField.setEnabled(true);
        } else {
            textField.setBackground(ACTIVE_COLOR);
            textField.setForeground(ACTIVE_TEXT_COLOR);
            textField.setToolTipText("");
            textField.setEnabled(true);
        }
    }

    private Color getTextFieldColor(double value) {
        int colorindex;
        if (value <= minTableValue) {
            colorindex = 0;
        } else if (value >= maxTableValue) {
            colorindex = TableFieldColor.MAX_COLOR_VALUES - 1;
        } else if (maxTableValue > minTableValue) {
            colorindex = (int) (((value - minTableValue) / (maxTableValue - minTableValue)) * (
                TableFieldColor.MAX_COLOR_VALUES - 1));
        } else {
            colorindex = 0;
        }
        return TableFieldColor.getColor(colorindex);
    }

    @Override
    public void clearWindow() {
        if (table != null) {
            table.removeListener(this);
        }
        if (xAxisManager != null) {
            xAxisManager.setSignal(null);
        }
        if (yAxisManager != null) {
            yAxisManager.setSignal(null);
        }
        super.clearWindow();
    }

    private class AxisSignalManager {

        private final SignalAdapter signalAdapter;
        private Signal signal;

        public AxisSignalManager(int axis, int size) {
            signalAdapter = new SignalAdapter() {
                @Override
                public void valueUpdate(double value, long timeStamp) {
                    double percentage = interpolateSignalValue(value, axis, size);
                    if (axis == COLUMN_AXIS) {
                        int x = (int) (getPanel().getWidth() * percentage);
                        getPanel().setPointX(x);
                    } else if (axis == ROW_AXIS) {
                        int y = (int) (getPanel().getHeight() * percentage);
                        getPanel().setPointY(y);
                    }
                    panel.repaint();
                }
            };
        }

        private Signal getSignal() {
            return signal;
        }

        private void setSignal(Signal axisSignal) {
            if (signal != null) {
                signal.removeListener(signalAdapter);
            }
            signal = axisSignal;
            if (signal != null) {
                signal.addListener(signalAdapter);
            }
        }

        private double interpolateSignalValue(double value, int axis, int size) {
            double percentage;
            if (size > 1) {
                if (value < table.getAxisPointsValueAt(0, 0)) {
                    percentage = 0;

                } else if (value > table.getAxisPointsValueAt(axis, size - 1)) {
                    percentage = 1;
                } else {
                    int pos;
                    for (pos = 1; pos < size - 1; pos++) {
                        if (value < table.getAxisPointsValueAt(axis, pos)) {
                            break;
                        }
                    }
                    if (table.getAxisPointsValueAt(axis, pos) == table.getAxisPointsValueAt(axis, pos - 1)) {
                        // should not happen...
                        percentage = ((double) pos - 0.5) / size;
                    } else {
                        percentage = ((double) pos - 0.5
                                + (value - table.getAxisPointsValueAt(axis, pos - 1)) / (table.getAxisPointsValueAt(axis, pos) - table.getAxisPointsValueAt(axis, pos - 1)))
                                / (double) size;
                    }
                }
            } else {
                // Only 1 item. Nothing to interpolate
                percentage = 0.5;   // Line in the middle
            }
            return percentage;
        }
    }

    private class TableEditorCell extends JFormattedTextField {

        private boolean readyForEditing;

        public TableEditorCell(NumberFormatter format) {
            super(format);
        }

        public boolean isReadyForEditing() {
            return readyForEditing;
        }

        public void setReadyForEditing(boolean readyForEditing) {
            this.readyForEditing = readyForEditing;
        }

        public void setEditting(boolean editting) {
            getCaret().setVisible(editting);
            setEditable(editting);
        }

        @Override
        public Color getBackground() {
            return isEditable() ? Color.WHITE : super.getBackground();
        }

        @Override
        public void repaint() {
            panel.repaint();
        }

        @Override
        public String getToolTipText(MouseEvent event) {
            if (getPanel().isWithinPoint(event.getX() + this.getX(), event.getY() + this.getY())) {
                return "<html>X-Axis: " + xAxisManager.getSignal().getValue()
                        + " <br> Y-Axis: " + yAxisManager.getSignal().getValue() + "</html";
            }
            return !super.getToolTipText(event).isEmpty() ? super.getToolTipText(event) : null;
        }
    }

    /**
     * @return the panel
     */
    public WorkingPointPanel getPanel() {
        return panel;
    }

}
