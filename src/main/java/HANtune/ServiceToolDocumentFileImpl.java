/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import ErrorLogger.AppendToLogfile;
import org.apache.xmlbeans.XmlError;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;

import haNtuneSHML.ShmlDocument;
import util.EncryptDecrypt;

public class ServiceToolDocumentFileImpl implements DocumentFile<ShmlDocument> {
    private static final String SERVICE_FILE_STARTSTRING = "SHMLenc";
    private static final String PASSPHRASE_SERVICE_ENCR = "1248934561234567";
    private static String errorString = "";

    private static final boolean doEncrypt = true; // for debugging purposes ONLY: set false to
    // disable encryption and read/write human readable shml files. Set true for production

    private ShmlDocument shmlDoc = null;


    public ShmlDocument loadProjectData(File projectFile) {
        try {
            if (doEncrypt) {
                // Service tool SHML file is encrypted
                byte[] rawBytes = readServiceHMLData(projectFile);

                EncryptDecrypt decr = new EncryptDecrypt();
                byte[] plainBytes = decr.decryptData(rawBytes, PASSPHRASE_SERVICE_ENCR);
                shmlDoc = ShmlDocument.Factory.parse(new String(plainBytes));
            } else {
                shmlDoc = ShmlDocument.Factory.parse(projectFile);
            }
        } catch (IOException | XmlException | NoSuchAlgorithmException | NoSuchPaddingException
            | InvalidKeyException | InvalidAlgorithmParameterException | IllegalBlockSizeException
            | BadPaddingException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            errorString = e.getMessage();
            return null;
        }
        return shmlDoc;
    }


    public boolean saveProjectData(File projectFile, ShmlDocument shmlDoc) {
        FileOutputStream os;
        try {
            os = new FileOutputStream(projectFile);

            XmlOptions options = new XmlOptions();
            options.setUseDefaultNamespace();

            if (doEncrypt) {
                // save as Service Project File: a Starting string followed by Encrypted data
                EncryptDecrypt encr = new EncryptDecrypt();

                os.write(SERVICE_FILE_STARTSTRING.getBytes());
                os.write(encr.encryptData(shmlDoc.xmlText(options).getBytes(), PASSPHRASE_SERVICE_ENCR));
            } else {
                options.setSavePrettyPrint();
                shmlDoc.save(os, options);
            }

            os.close();
        } catch (IOException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
            | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            errorString = e.getMessage();
            return false;
        }

        return true;
    }


    /**
     * @param shmlDoc
     */
    @Override
    public boolean isValidDocument() {
        ////// validate
        // Create an XmlOptions instance and set the error listener.
        XmlOptions validateOptions = new XmlOptions();
        ArrayList<XmlError> errorList = new ArrayList<XmlError>();
        validateOptions.setErrorListener(errorList);

        // Validate the XML.
        boolean isValid = shmlDoc.validate(validateOptions);

        // If the XML isn't valid, loop through the listener's contents and put result in errorString
        if (!isValid) {
            StringBuilder stringBldr = new StringBuilder();
            for (int i = 0; i < errorList.size(); i++) {
                XmlError error = errorList.get(i);
                stringBldr.append("\n" + error.getMessage());
                stringBldr
                    .append("\nLocation of invalid XML: " + error.getCursorLocation().xmlText() + "\n");
            }
            errorString = stringBldr.toString();
        }

        return isValid;
    }


    /**
     * Checks version and returns false if not correct version.
     *
     * @param shmlDoc
     * @return true if version OK. false if not, returns message in errorstring
     */
    @Override
    public boolean isDocVersionOK() {

        // get SHML version from xsd file
        String applVersion = ShmlDocument.Factory.newInstance().addNewShml().getSHMLVersion();
        String docVersion = shmlDoc.getShml().getSHMLVersion();

        if (!HANtuneDocumentFileImpl.compareVersionStrings(applVersion, docVersion)) {
            String docHANtuneVersion = Float.toString(shmlDoc.getShml().getVersion());
            errorString = "Warning: current Service Tool SHML file does not match the version this HANtune expects.\n"
                + "SHML file version: " + docVersion
                + ", this HANtune expects SHML version: " + applVersion + "\n"
                + "Please obtain HANtune version " + docHANtuneVersion + " or higher\n" + "\n"
                + "Opening this SHML file might give unexpected results.\n" + "Continue anyway?";
            return false;
        }

        return true;
    }



    /**
     * Check if service SHML file
     *
     * @param file
     * @return true: file is a Service SHML file
     * @throws IOException
     * @throws XmlException
     */
    static public boolean isServiceSHML(File file) {
        if (doEncrypt) {
            try {
                byte[] startbytes = new byte[SERVICE_FILE_STARTSTRING.length()];
                FileInputStream is;
                is = new FileInputStream(file);
                int len = is.read(startbytes);
                is.close();
                String tmpStr = new String(startbytes);
                if (len > 0 && tmpStr.startsWith(SERVICE_FILE_STARTSTRING)) {
                    return true;
                }
            } catch (IOException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
                errorString = e.getMessage();
            }
        } else {
            if (file.getAbsolutePath().endsWith("shml")) {
                return true;
            }
        }
        return false;
    }


    static private byte[] readServiceHMLData(File file) throws IOException {
        int startStringLength = SERVICE_FILE_STARTSTRING.length();
        int dataLength = (int)file.length() - startStringLength;

        FileInputStream fis = new FileInputStream(file);
        byte[] dataBytes = new byte[dataLength];

        // skip startstring
        fis.skip(startStringLength);

        fis.read(dataBytes);
        fis.close();

        return dataBytes;
    }


    public String getError() {
        return errorString;
    }
}
