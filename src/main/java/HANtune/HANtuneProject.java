/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.io.File;
import java.util.Date;
import java.util.prefs.Preferences;
import javax.swing.JOptionPane;

import ErrorLogger.AppendToLogfile;
import datahandling.CurrentConfig;
import haNtuneHML.HANtuneDocument;
import nl.han.hantune.config.VersionInfo;
import nl.han.hantune.datahandling.DataLogger.LogDataWriterCsv;
import util.MessagePane;
import util.Util;

public class HANtuneProject {
    static final String LAST_OPEN_PROJECT_PREFERENCE = "lastOpenProject";
    static final String LOAD_PREV_PROJECT_PREFERENCE = "loadPreviousProject";
    static final String IMPORT_TXT = "Import";
    // Singleton class
    private static final CurrentConfig currentConfig = CurrentConfig.getInstance();
    private static final HANtuneProject instance = new HANtuneProject();
    HANtune hanTune = null;

    // Singleton private constructor
    private HANtuneProject() {

    }


    /**
     * Get the instance of HANtuneProjectFile
     *
     * @return instance
     */
    public static HANtuneProject getInstance() {
        return instance;
    }


    public void loadProject(HANtune hTune, String[] args, Preferences usrPrefs) {
        hanTune = hTune;

        String projectPath = currentConfig.getHANtuneManager().getProjectPathFromArguments(args);
        if (projectPath != null) {
            currentConfig.getHANtuneManager().openProject(new File(projectPath), false);
        } else if (usrPrefs.getBoolean(LOAD_PREV_PROJECT_PREFERENCE, false)
            && !usrPrefs.get(LAST_OPEN_PROJECT_PREFERENCE, "").isEmpty()) {
            if (!currentConfig.getHANtuneManager()
                .openProject(new File(usrPrefs.get(LAST_OPEN_PROJECT_PREFERENCE, "")), false)) {
                MessagePane.showError("Error while opening previous project:\n\n"
                    + currentConfig.getHANtuneManager().getError());
                currentConfig.getHANtuneManager().newProject();

                // remove lastOpenProject from userPrefs
                usrPrefs.remove(LAST_OPEN_PROJECT_PREFERENCE);
            }
        } else {
            currentConfig.getHANtuneManager().newProject();
        }
    }

    public void importProjectData() {
        CustomFileChooser fileChooser = new CustomFileChooser(IMPORT_TXT, CustomFileChooser.FileType.PROJECT_FILE);
        File projectFile = fileChooser.chooseOpenDialog(hanTune, IMPORT_TXT);

        if (projectFile != null) {
            hanTune.currentConfig.getHANtuneManager().openProject(projectFile, true);
            hanTune.showProjectPanel(true);
        }
    }

    public void importDbcData() {
        CustomFileChooser fileChooser = new CustomFileChooser(IMPORT_TXT, CustomFileChooser.FileType.DBC_FILE);
        File dbcFile = fileChooser.chooseOpenDialog(hanTune, IMPORT_TXT);

        if (dbcFile != null) {
            hanTune.currentConfig.getHANtuneManager().newDbc(dbcFile);
            hanTune.showProjectPanel(true);
        }
    }


    private boolean isProjectEmpty(HANtuneDocument htDoc) {
        return !(htDoc != null && (htDoc.getHANtune().sizeOfASAP2FileArray() > 0
            || htDoc.getHANtune().sizeOfDaqlistArray() > 0
            || htDoc.getHANtune().sizeOfLayoutArray() > 0
            || htDoc.getHANtune().sizeOfCalibrationArray() > 0));
    }


    public void saveCurrentProject(boolean showSavedSuccessMsg) {
        File projectFile = currentConfig.getProjectFile();
        if (projectFile != null) {
            // Project already exists, overwrite
            saveProjectFile(projectFile, CustomFileChooser.FileType.PROJECT_FILE, showSavedSuccessMsg);
        } else {
            saveAsCurrentProject(); // Project does not already exist,
        }
    }


    public void newProject() {
        currentConfig.getHANtuneManager().newProject();
        currentConfig.createNewLayout("Unnamed");
        currentConfig.getHANtuneManager().loadLayout(0);
        hanTune.showProjectPanel(true);
    }


    public void closeProject() {
        currentConfig.getHANtuneManager().closeProject();
        hanTune.showProjectPanel(true);
    }

    /**
     * Checks if the project is modified and/or if the connection is active
     * If modified/active user will be asked to Save and Proceed, Proceed without save, or cancel
     * File will be saved when requested
     * @return true: proceed. false: abort further action
     */
    public boolean isProceedOKCheckModifiedProjectSave() {
        if (CurrentConfig.getInstance().isServiceToolMode()) {
            return true; // Cannot save current project in service tool mode, always proceed
        }
        if (isProjectEmpty(currentConfig.getHANtuneDocument())) {
            return true;
        }
        if (isHANtuneDocumentModified(currentConfig)) {
            return isProceedOkModifiedProjectSaveDlg();
        } else {
            return isProceedOkConnectedDlg();
        }
    }


    private boolean isProceedOkConnectedDlg() {
        String dialogStr;
        String[] options;
        if (hanTune.isConnected()) {
            dialogStr = "This will close your current connection";
            dialogStr += LogDataWriterCsv.getInstance().isWritingActive() ? " and logging." : ".";
            dialogStr += "\nContinue anyway?\n";

            options = new String[]{"OK", "Cancel"};
            int rtn = MessagePane.showOptionsDialog(HANtune.getInstance(), dialogStr, "Confirm close current project",
                options);
            return rtn == 0;
        }
        return true;
    }

    private boolean isProceedOkModifiedProjectSaveDlg() {
        String dialogStr;
        String[] options;
        dialogStr = "Do you want to save the current project before proceeding?";
        if (hanTune.isConnected()) {
            dialogStr += "\nProceeding will also close your current connection";
            dialogStr += LogDataWriterCsv.getInstance().isWritingActive() ? " and logging." : ".";
        }
        options = new String[]{"Save and proceed", "Proceed without save", "Cancel"};
        int rtn = MessagePane.showOptionsDialog(HANtune.getInstance(), dialogStr, "Confirm close Project",
            options);

        if (rtn == 0) {
            saveCurrentProject(false);
            return true;
        } else {
            return rtn == 1;
        }
    }


    public void selectAndOpenProjectFile() {
        CustomFileChooser fileChooser;
        if (CurrentConfig.getInstance().isServiceToolMode()) {
            // When in service mode, prefer to open files in service mode (first FileType)
            fileChooser = new CustomFileChooser("Open", CustomFileChooser.FileType.SERVICE_PROJECT_FILE, CustomFileChooser.FileType.PROJECT_FILE);
        } else {
            fileChooser = new CustomFileChooser("Open", CustomFileChooser.FileType.PROJECT_FILE, CustomFileChooser.FileType.SERVICE_PROJECT_FILE);
        }
        File projectFile = fileChooser.chooseOpenDialog(hanTune, "Open");

        if (projectFile != null) {
            openProjectFile(projectFile);
        }
    }


    public void openProjectFile(File prjFile) {
        currentConfig.getHANtuneManager().closeProject();
        if (!currentConfig.getHANtuneManager().openProject(prjFile, false)) {
            MessagePane.showError("Error: " + currentConfig.getHANtuneManager().getError()
                + "\nCould not open file: " + prjFile.getName());
            newProject();
        }
    }


    /**
     * Checks whether the current HANtune project has been modified. First it gets the last loaded file from
     * memory, which resembles the file on disk. Then it executes a project-save action without writing the
     * result to disk, and saves that project into a runtime object. Those two are compared which results in
     * a boolean isModified to be true or false.
     * <p>
     * It excludes some data in the comparison such as date and date_mod as this changes at every save
     * action. The excluded data is erased from the runtime objects using regex.
     *
     * @return isModified, true when the HANtune project is modified but unsaved, false otherwise
     */
    private boolean isHANtuneDocumentModified(CurrentConfig config) {

        HANtuneDocument lastSavedHantuneDocument;
        File currentProject = config.getProjectFile();

        boolean isModified = true;
        try {

            if (currentProject == null) { // avoiding null pointer exceptions here
                return true;
            }

            // Get the last saved HANtuneDocument from memory. This effectively is the last loaded
            // HANtune project from disk. Therefore equal to the file on disk.
            lastSavedHantuneDocument =
                config.getHANtuneManager().loadProjectDataFromHmlData(currentProject);

            // replace relative paths by absolute paths
            Util.resolvePaths(currentProject, lastSavedHantuneDocument.getHANtune().getASAP2FileList());
            for (HANtuneDocument.HANtune.DbcFile dbcFile : lastSavedHantuneDocument.getHANtune().getDbcFileList()) {
                dbcFile.setPath(Util.resolvePath(currentProject, dbcFile.getPath()));
            }

            // Assemble all the objects into one HANtune project, but don't write it to disk, rather
            // use it for comparison
            config.getHANtuneManager().saveProject(currentProject, CustomFileChooser.FileType.PROJECT_FILE, false);


            // Get the String representation of the XML object to be able to conveniently modify it.
            String lastSavedHantuneDocumentString = lastSavedHantuneDocument.toString();

            lastSavedHantuneDocumentString =
                lastSavedHantuneDocumentString.replaceAll(" date=\"(.*?)\"", ""); // Delete the date
            lastSavedHantuneDocumentString =
                lastSavedHantuneDocumentString.replaceAll(" date_mod=\"(.*?)\"", ""); // Delete the
            // date_modified
            // Delete the HANtune version, as this could change without the project being changed
            lastSavedHantuneDocumentString =
                lastSavedHantuneDocumentString.replaceAll("<HANtune version=\".*?>", "<HANtune>");

            // Same accounts for the hml version
            lastSavedHantuneDocumentString =
                lastSavedHantuneDocumentString.replaceAll("<Setting name=\"HMLversion\">.*?>", "");

            lastSavedHantuneDocumentString =
                lastSavedHantuneDocumentString.replaceAll("<activeLayout>.*?>", ""); // Same...

            // Active DAQ lists will be set upon project load, which is no indication of a user change to
            // the project
            lastSavedHantuneDocumentString =
                lastSavedHantuneDocumentString.replaceAll("<activeDAQlist>.*?>", "");

            // Delete all newlines, which are left behind by the replacement actions above
            lastSavedHantuneDocumentString = lastSavedHantuneDocumentString.replaceAll("\\r?\\n", "");

            // Delete all white spaces and tabs, which can magically appear...
            lastSavedHantuneDocumentString = lastSavedHantuneDocumentString.replaceAll("\\s", "");


            // Now get the newly assembled HANtune project and save it into an object
            String currentHantuneDocumentString = config.getHANtuneDocument().copy().toString();

            // And more of the same...
            currentHantuneDocumentString = currentHantuneDocumentString.replaceAll(" date=\"(.*?)\"", "");
            currentHantuneDocumentString =
                currentHantuneDocumentString.replaceAll(" date_mod=\"(.*?)\"", "");
            currentHantuneDocumentString =
                currentHantuneDocumentString.replaceAll("<HANtune version=\".*?>", "<HANtune>");
            currentHantuneDocumentString =
                currentHantuneDocumentString.replaceAll("<Setting name=\"HMLversion\">.*?>", "\n");
            currentHantuneDocumentString =
                currentHantuneDocumentString.replaceAll("<activeLayout>.*?>", "");
            currentHantuneDocumentString =
                currentHantuneDocumentString.replaceAll("<activeDAQlist>.*?>", "");
            currentHantuneDocumentString = currentHantuneDocumentString.replaceAll("\\r?\\n", "");
            currentHantuneDocumentString = currentHantuneDocumentString.replaceAll("\\s", "");

            // Set modified to false when the previously loaded project is not equal to the current
            // project
            if (lastSavedHantuneDocumentString.equalsIgnoreCase(currentHantuneDocumentString)) {
                isModified = false;
            }
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
        }
        return isModified;
    }


    /**
     * For test purposes ONLY. Do NOT use in production, only to be called in testcode: @Test!!
     */
    public boolean testIsDocumentModified(CurrentConfig config) {
        return isHANtuneDocumentModified(config);
    }


    /**
     * Initializes the filechooser window to save a new project file and saves when necessary
     */
    public void saveAsCurrentProject() {
        CustomFileChooser fileChooser =
            new CustomFileChooser("Save As", CustomFileChooser.FileType.PROJECT_FILE, CustomFileChooser.FileType.SERVICE_PROJECT_FILE);

        File projectFile = fileChooser.chooseSaveDialogConfirmOverwrite(hanTune, "Save");

        if (projectFile != null) {
            if (!projectFile.exists()) {
                hanTune.getMainMenuBar().updateItemsServiceModeVisibility();
            }

            saveProjectFile(projectFile,
                Util.getExtension(projectFile.getName())
                    .equals(CustomFileChooser.FileType.SERVICE_PROJECT_FILE.getExtension()) ? CustomFileChooser.FileType.SERVICE_PROJECT_FILE
                    : CustomFileChooser.FileType.PROJECT_FILE, false);
        }
    }


    /**
     * Initializes the filechooser to save a new Service tool project file
     */
    public void requestSaveAsServiceProject() {
        CustomFileChooser fileChooser = new CustomFileChooser("Export", CustomFileChooser.FileType.SERVICE_PROJECT_FILE);
        File projectFile = fileChooser.chooseSaveDialogConfirmOverwrite(hanTune, "Export");

        if (projectFile != null) {
            if (!projectFile.exists()) {
                hanTune.getMainMenuBar().updateItemsServiceModeVisibility();
            }

            saveProjectFile(projectFile, CustomFileChooser.FileType.SERVICE_PROJECT_FILE, true);
        }
    }


    private boolean saveProjectFile(File projectFile, CustomFileChooser.FileType ftype, boolean showSavedSuccesMsg) {
        String projectName = "";
        CurrentConfig current = CurrentConfig.getInstance();
        if (current.getProjectFile() != null) {
            projectName = current.getProjectFile().getName();
        }

        float projectVersion = current.getHANtuneDocument().getHANtune().getVersion();
        if (projectName.equals(projectFile.getName())
            && projectVersion < VersionInfo.getInstance().getVersionFloat()) {
            boolean continueSaving = askUserToBackupProject(projectFile, projectVersion);
            if (!continueSaving) {
                return false;
            }
        }

        boolean isSaved = current.getHANtuneManager().saveProject(projectFile, ftype, true);
        current.getHANtuneDocument().getHANtune().setVersion(VersionInfo.getInstance().getVersionFloat());

        // Check if the overwrite action was successful.
        long lastModified = projectFile.lastModified(); // Get elapsed time (ms) since 1 jan. 1970
        Date date = new Date();
        long currentDate = date.getTime();
        long timeBetween = currentDate - lastModified;
        if (timeBetween > 10000 || !isSaved) { // Last modification has not been in the last 10000 ms
            String errStr = "The file specified could not be overwritten";
            if (!isSaved) {
                errStr += "\n" + current.getHANtuneManager().getError();
            }
            MessagePane.showError(errStr);
            return false; // Unsuccessful save action
        } else {
            if (showSavedSuccesMsg) {
                MessagePane.showInfo(
                    ftype.getDescription() + ": " + projectFile.getName() + " has been saved successfully");
            }
            return true; // Successful save action
        }

    }


    private boolean askUserToBackupProject(File file, float version) {
        String backupPath = HANtuneDocumentFileImpl.getBackupPath(file.getPath(), version);
        int result = JOptionPane.showConfirmDialog(hanTune,
            "The project you're trying to save was created in HANtune " + version + ". "
                + "Saving the project in " + VersionInfo.getInstance().getHANtuneXYVersion()
                + " will render it unable \nto be opened in a previous version of HANtune. "
                + "Would you like to create a backup of the project at:\n\n" + backupPath,
            "Create backup of project file?", JOptionPane.YES_NO_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            boolean backupCreated = HANtuneDocumentFileImpl.createBackupOfProjectFile(file, version);
            if (!backupCreated) {
                JOptionPane.showMessageDialog(hanTune,
                    "HANtune was unable to create a backup of the project file", "Unable to create backup",
                    JOptionPane.ERROR_MESSAGE);
                result = askUserToBackupProject(file, version) ? JOptionPane.YES_OPTION
                    : JOptionPane.CANCEL_OPTION;
            }
        }
        return result == JOptionPane.YES_OPTION || result == JOptionPane.NO_OPTION;
    }


}
