/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.io.File;
import java.util.List;

import javax.swing.JToolTip;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import ASAP2.ASAP2Data;
import DAQList.DAQList;
import components.ScrollableToolTip;
import components.sidepanel.AbstractTree;
import datahandling.CurrentConfig;
import haNtuneHML.Calibration;
import haNtuneHML.HANtuneDocument;
import nl.han.hantune.scripting.Script;
import nl.han.hantune.scripting.ScriptingManager;
import util.Util;

@SuppressWarnings("serial")
public class ProjectTree extends AbstractTree {

    public static final String WORKING_PARAMETER_SET = "Working Parameter Set";

    public static DefaultTreeModel getTreeModel(File projectFile, HANtuneDocument doc, List<DAQList> h_daqlists, int h_layout, int h_calibration) {
        String projectName = Util.getProjectName(projectFile);
        ProjectInfo project, projectItem, projectItemContent;

        project = new ProjectInfo(ProjectInfo.Type.Root, projectName, 0, false, -1);

        // New project?
        if (doc != null) {

            // ASAP2 files
            int cnt_asap2 = doc.getHANtune().sizeOfASAP2FileArray();
            projectItem = new ProjectInfo(ProjectInfo.Type.Folder, "ASAP2 files", 0, false, -1);

            // loop through individual asap2 files
            for (int i = 0; i < cnt_asap2; i++) {
                File asap2File = new File(doc.getHANtune().getASAP2FileArray(i));
                FileWatcher.addFile(asap2File);
                boolean active = CurrentConfig.getInstance().hasDescriptionFile(asap2File);
                ProjectInfo info = new ProjectInfo(ProjectInfo.Type.Asap2File, asap2File, i, active, -1);
                if (FileWatcher.isFileModified(asap2File)) {
                    info.setModified(true);
                }

                ASAP2Data asap2 = CurrentConfig.getInstance().getASAP2Data();
                String softwareID = asap2 != null && active ? asap2.getEpromID() : "not available";
                info.add(new ProjectInfo(ProjectInfo.Type.Info, "Software ID: " + softwareID, 0, false, -1));
                projectItem.add(info);
            }
            project.add(projectItem);

            projectItem = new ProjectInfo(ProjectInfo.Type.Folder, "DBC files", 0, false, -1);
            for (int i = 0; i < doc.getHANtune().sizeOfDbcFileArray(); i++) {
                File dbcFile = new File(doc.getHANtune().getDbcFileArray(i).getPath());
                FileWatcher.addFile(dbcFile);
                boolean active = CurrentConfig.getInstance().hasDescriptionFile(dbcFile.getName());
                ProjectInfo info = new ProjectInfo(ProjectInfo.Type.DbcFile, dbcFile, i, active, -1);
                if (FileWatcher.isFileModified(dbcFile)) {
                    info.setModified(true);
                }
                projectItem.add(info);
            }
            project.add(projectItem);

            // DAQ-Lists
            projectItem = new ProjectInfo(ProjectInfo.Type.Folder, "DAQ lists", 0, false, -1);

            // loop through individual DAQ-lists
            for (int iCnt = 0; iCnt < doc.getHANtune().sizeOfDaqlistArray(); iCnt++) {
                boolean activeFlag = false;
                String name = doc.getHANtune().getDaqlistArray(iCnt).getTitle();

                if (h_daqlists != null && iCnt < h_daqlists.size()) {
                    name = h_daqlists.get(iCnt).getScreenId() + " - " + name;
                    activeFlag = h_daqlists.get(iCnt).isActive();
                } else {
                    name = "XX - " + name;
                }

                projectItemContent = new ProjectInfo(ProjectInfo.Type.Daqlist, name, iCnt, activeFlag, iCnt);
                projectItemContent.add(new ProjectInfo(ProjectInfo.Type.Info,
                        "Signals: " + doc.getHANtune().getDaqlistArray(iCnt).sizeOfSignalArray(), 0, false,
                        -1));
                projectItemContent.add(new ProjectInfo(ProjectInfo.Type.Info,
                        "Created: " + Util.getDateString(doc.getHANtune().getDaqlistArray(iCnt).getDate()),
                        0, false, -1));
                projectItemContent.add(new ProjectInfo(ProjectInfo.Type.Info,
                        "Last Modified: "
                        + Util.getDateString(doc.getHANtune().getDaqlistArray(iCnt).getDateMod()),
                        0, false, -1));
                projectItem.add(projectItemContent);
            }
            project.add(projectItem);

            // Layouts
            int cnt_layout = doc.getHANtune().sizeOfLayoutArray();
            projectItem = new ProjectInfo(ProjectInfo.Type.Folder, "Layouts", 0, false, -1);
            // loop through individual layouts
            for (int i = 0; i < cnt_layout; i++) {
                projectItemContent = new ProjectInfo(ProjectInfo.Type.Layout,
                        doc.getHANtune().getLayoutArray(i).getTitle(), i, (i == h_layout), -1);
                projectItemContent.add(new ProjectInfo(ProjectInfo.Type.Info,
                        "Created: " + Util.getDateString(doc.getHANtune().getLayoutArray(i).getDate()), 0,
                        false, -1));
                projectItemContent.add(new ProjectInfo(ProjectInfo.Type.Info,
                        "Last Modified: "
                        + Util.getDateString(doc.getHANtune().getLayoutArray(i).getDateMod()),
                        0, false, -1));
                projectItem.add(projectItemContent);
            }
            project.add(projectItem);

            // Calibrations
            int cnt_calibration = doc.getHANtune().sizeOfCalibrationArray();
            projectItem = new ProjectInfo(ProjectInfo.Type.Folder, "Calibrations", 0, false, -1);

            // Add Working parameter set
            projectItemContent = new ProjectInfo(ProjectInfo.Type.Calibration, WORKING_PARAMETER_SET, 0, true, -1);
            projectItem.add(projectItemContent);

            // loop through individual calibrations
            for (int i = 0; i < cnt_calibration; i++) {
                int calibrationParameterCount = doc.getHANtune().getCalibrationArray(i).sizeOfASAP2Array();
                String parametersString = "Parameters: " + calibrationParameterCount;
                String calibrationString = doc.getHANtune().getCalibrationArray(i).getTitle();
                if (CurrentConfig.getInstance().getASAP2Data() != null) {
                    int parameterTotal
                            = CurrentConfig.getInstance().getASAP2Data().getASAP2Characteristics().size();
                    if (calibrationParameterCount < parameterTotal) {
                        calibrationString += " (partial)";
                        parametersString += " out of " + parameterTotal;
                    }
                }
                projectItemContent = new ProjectInfo(ProjectInfo.Type.Calibration,
                        calibrationString, i, (i == h_calibration), -1);
                projectItemContent.add(new ProjectInfo(ProjectInfo.Type.Info, parametersString, 0, false, -1));
                projectItemContent.add(new ProjectInfo(ProjectInfo.Type.Info,
                        "Created: "
                        + Util.getDateString(doc.getHANtune().getCalibrationArray(i).getDate()),
                        0, false, -1));
                projectItemContent.add(new ProjectInfo(ProjectInfo.Type.Info,
                        "Last Modified: "
                        + Util.getDateString(doc.getHANtune().getCalibrationArray(i).getDateMod()),
                        0, false, -1));

                projectItem.add(projectItemContent);
            }
            project.add(projectItem);
            addScriptingItems(doc, project);
        }
        return new DefaultTreeModel(project);
    }

    private static void addScriptingItems(HANtuneDocument doc, ProjectInfo projectNode) {
        ProjectInfo scriptsNode = new ProjectInfo(ProjectInfo.Type.Folder, "Scripts", 0, false, -1);
        int id = 0;
        for (Script script : ScriptingManager.getInstance().getScripts()) {
            String name = script.isRunOnStartup() ? "> " + script.getName() : script.getName();
            ProjectInfo info = new ProjectInfo(ProjectInfo.Type.Script, name, ++id, false, -1);
            info.setUserObject(script);
            scriptsNode.add(info);
        }
        projectNode.add(scriptsNode);
    }

    @Override
    public JToolTip createToolTip() {
        DefaultMutableTreeNode node = getLastPathComponent();
        if (node != null && node instanceof ProjectInfo && ((ProjectInfo) node).getType().equals(ProjectInfo.Type.Calibration)) {
            ScrollableToolTip tip = new ScrollableToolTip(200, 80, this);
            tip.setComponent(this);
            return tip;
        } else {
            return super.createToolTip();
        }
    }

    @Override
    public String getToolTipTextForNode(DefaultMutableTreeNode node) {
        String text = null;
        if (node instanceof ProjectInfo) {
            ProjectInfo projectNode = (ProjectInfo) node;
            ProjectInfo.Type type = projectNode.getType();
            switch (type) {
                case Root:
                    text = CurrentConfig.getInstance().getProjectFile().getPath();
                    break;

                case Asap2File:
                    text = projectNode.getFile().toString();
                    break;

                case DbcFile:
                    text = projectNode.getFile().toString();
                    break;

                case Script:
                    text = ((Script) node.getUserObject()).getPath();
                    break;

                case Calibration:
                    text = getCalibrationOrWorkingParameterSetTooltip(node);
                    break;

                default:
                    break;
            }
        }
        return text;
    }

    private String getCalibrationOrWorkingParameterSetTooltip(DefaultMutableTreeNode node) {
        String text = null;
        HANtuneDocument hanDoc = CurrentConfig.getInstance().getHANtuneDocument();
        if (hanDoc != null && hanDoc.getHANtune() != null) {
            if (((ProjectInfo) node).getName().equals(ProjectTree.WORKING_PARAMETER_SET)) {
                text = getWorkingParamSetTooltipText();
            } else {
                text = getCalibrationTooltipText(node, hanDoc);
            }
        }
        return text;
    }

    private String getCalibrationTooltipText(DefaultMutableTreeNode node, HANtuneDocument hanDoc) {
        String text = "<Not found>";
        if (CurrentConfig.getInstance().getASAP2Data() != null
                && CurrentConfig.getInstance().getASAP2Data().getASAP2Characteristics() != null) {

            int calibIdx = ((ProjectInfo) node).getId();
            if (hanDoc != null && hanDoc.getHANtune() != null) {
                text = createCalibText(hanDoc, calibIdx);
            }
        }
        return text;
    }

    private String createCalibText(HANtuneDocument hanDoc, int calibIdx) {
        String text;
        int iTotal
                = CurrentConfig.getInstance().getASAP2Data().getASAP2Characteristics().keySet().size();
        Calibration calib = hanDoc.getHANtune().getCalibrationArray(calibIdx);
        if (calib != null) {
            int iCalibSize = calib.sizeOfASAP2Array();
            if (iTotal == iCalibSize) {
                text = "Calibration Parameters (total " + iTotal + "):";
            } else {
                text
                        = "Calibration Parameters (partial " + iCalibSize + " out of " + iTotal + "):";
            }
            if (iCalibSize > 0) {
                for (int iCnt = 0; iCnt < iCalibSize; iCnt++) {
                    text += "\n" + calib.getASAP2Array(iCnt).getName();
                }
            }
        } else {
            text = "Calibration Parameters (partial 0 out of " + iTotal + ")\n"
                    + "<Not found>";
        }
        return text;
    }

    private String getWorkingParamSetTooltipText() {
        String text = null;
        if (CurrentConfig.getInstance().getASAP2Data() != null
                && CurrentConfig.getInstance().getASAP2Data().getASAP2Characteristics() != null) {
            int iTotal = CurrentConfig.getInstance().getASAP2Data().getASAP2Characteristics().keySet().size();
            text = "Working Parameters (total " + iTotal + "):";
            for (String key : CurrentConfig.getInstance().getASAP2Data().getASAP2Characteristics().keySet()) {
                text += "\n" + key;
            }
        }
        return text;
    }

}
