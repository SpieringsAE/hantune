/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.BorderFactory;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import ASAP2.ASAP2Measurement;
import DAQList.DAQList;
import DAQList.DaqListListener;
import ErrorLogger.AppendToLogfile;
import ErrorLogger.ErrorLogRestrictor;
import ErrorLogger.defaultExceptionHandler;
import ErrorMonitoring.ErrorMonitor;
import HANtune.actions.CreateCalibration;
import HANtune.actions.DaqlistAction;
import HANtune.actions.ExportCalibration;
import HANtune.actions.ExportFlashIHexAction;
import HANtune.actions.ExportFlashSRecAction;
import HANtune.actions.ImportCalibration;
import HANtune.actions.LogReaderAction;
import HANtune.actions.ProjectTreeRenderer;
import XCP.XCP;
import XCP.XCPDaqInfo;
import XCP.XCPSettings;
import XCP.XCPonCANBasic;
import XCP.XCPonSocketCan;
import XCP.XCPonEthernet;
import XCP.XCPonUART;
import can.CanConnectionHandler;
import components.VerticalButton;
import components.sidepanel.SidePanel;
import components.sidepanel.SidePanelManager;
import components.sidepanel.TreeTransferHandler;
import components.sidepanel.asap2.Asap2Panel;
import components.sidepanel.dbc.DbcPanel;
import datahandling.CalibrationHandler;
import datahandling.CurrentConfig;
import haNtuneHML.Calibration;
import haNtuneHML.Layout;
import nl.han.hantune.config.ApplicationProperties;
import nl.han.hantune.config.VersionInfo;
import nl.han.hantune.datahandling.DataLogger.LogDataWriterCsv;
import nl.han.hantune.gui.menu.HANtuneMainMenuBar;
import nl.han.hantune.gui.sidepanel.ConsolePanel;
import nl.han.hantune.gui.sidepanel.ScriptedElementsPanel;
import nl.han.hantune.gui.statusBar.HANtuneMainStatusBar;
import nl.han.hantune.scripting.Console;
import nl.han.hantune.scripting.Script;
import nl.han.hantune.scripting.ScriptingManager;
import nl.han.hantune.scripting.jython.JythonScript;
import util.MessagePane;
import util.ResettableTimer;
import util.Util;

import static HANtune.actions.DaqlistAction.LOAD_DAQ_LIST;
import static HANtune.actions.DaqlistAction.UNLOAD_DAQ_LIST;
import static util.Util.isLinux;

/**
 * @author Aart-Jan and Roel van den Boom
 */
public class HANtune extends JFrame {

    ScheduledExecutorService scheduler = new ScheduledThreadPoolExecutor(1);
    public static final String shortApplicationName = "HANtune";
    public static String applicationName = VersionInfo.getInstance().getHANtuneConditionalBranchNameBuildCommitCount();
    // error monitor supported controllers
    public static final ArrayList<String> errorMonitorSupport
        = new ArrayList<>(Arrays.asList("XCP_RC30", "RC28-14", "RC12-10", "STM32"));
    private static final long serialVersionUID = 1L;
    // (red)
    public static final int CONNECTION_TIMEOUT = 2000; // 2 second timeout on missing packets
    public CurrentConfig currentConfig = CurrentConfig.getInstance();
    private HANtuneProject hanTuneProject = HANtuneProject.getInstance();

    private ErrorMonitor errorMonitor;
    private CalibrationHandler calibrationHandler;

    private Preferences userPrefs = Preferences.userNodeForPackage(UserPreferences.class);
    public static final int DAQ_MAX_DEFAULT = 3; // default amount of allowed
    // DAQ lists
    private int activeDaqlistIdCnt = 0;
    private int activeAsap2Id = -1;
    private int activeLayoutId = -1;
    private int activeCalibrationId = -1;

    private HANtuneMainMenuBar mainMenuBar = null;
    private HANtuneMainStatusBar mainStatusBar = null;

    // windows & managers
    private CommunicationSettings communicationSettings = null;

    private ResettableTimer daqListReceiveTimoutTimer;
    private Timer connectionTimoutTimer;

    // Sidepanels
    private SidePanel projectPanel;
    private Asap2Panel asap2Panel;
    private DbcPanel dbcPanel;
    private ScriptedElementsPanel scriptedElementsPanel;
    private VerticalButton scriptedElementsButton;
    private ConsolePanel consolePanel;

    // Vertical buttons sidepanels
    private javax.swing.JButton projectButton;
    private javax.swing.JButton asap2Button;
    private javax.swing.JButton dbcButton;

    // Map containing all sidepanels plus their corresponding vertical buttons
    private Map<VerticalButton, SidePanel> buttonSidePanelMap = new HashMap<>();

    private ProjectTree projectTree;
    private JScrollPane projectPanelScrollPane;
    // DAQ lists
    public List<DAQList> daqLists = new ArrayList<>();

    // status colors
    public static ArrayList<String> missingElements = new ArrayList<>();
    public static Calendar licenceStartDate = null;
    public static Calendar licenceExpiryDate = null;
    private final static HANtune instance = new HANtune();
    private static String[] arguments;
    SidePanelManager sidePanelManager = SidePanelManager.getInstance();
    private final List<DaqListListener> daqListListeners = new ArrayList<>();
    private boolean snapLayoutToSidePanel = true;

    public ConnectionDialog connectionDialog;

    private HANtune() {
        Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());

        // Check if user has accepted EULA.
        if (!userPrefs.getBoolean("eulaAccept", false)) {
            EulaDialog eula = new EulaDialog(this, true, true);
            eula.setVisible(true);
        }
        if (!userPrefs.getBoolean("eulaAccept", false)) {
            System.exit(0);
        }
        // instantiate error monitor
        errorMonitor = new ErrorMonitor();
        calibrationHandler = CalibrationHandler.getInstance();
    }

    public static HANtune getInstance() {
        return instance;
    }

    public JTabbedPane getTabbedPane() {
        return tabbedPane;
    }

    public JPopupMenu getTabPopupMenu() {
        return tabPopupMenu;
    }

    public CommunicationSettings getCommunicationSettings() {
        return communicationSettings;
    }

    public void setCommunicationSettings(CommunicationSettings communicationSettings) {
        this.communicationSettings = communicationSettings;
    }

    public HANtuneMainMenuBar getMainMenuBar() {
        return mainMenuBar;
    }

    public HANtuneMainStatusBar getMainStatusBar() {
        return mainStatusBar;
    }

    public HANtuneProject getHanTuneProject() {
        return hanTuneProject;
    }

    public Preferences getUserPrefs() {
        return userPrefs;
    }

    /**
     * Shows question dialog
     *
     * @param title Question to be shown
     * @param value Default value to be shown
     */
    public static String showQuestion(String title, String msg, String value) {
        return (String) JOptionPane.showInputDialog(HANtune.getInstance(), msg, title,
            JOptionPane.QUESTION_MESSAGE, null, null, value);
    }

    /**
     * Shows confirmation dialog
     *
     * @param msg Confirmation message to be shown
     */
    public static int showConfirm(String msg) {
        return JOptionPane.showConfirmDialog(HANtune.getInstance(), msg, applicationName,
            JOptionPane.YES_NO_OPTION);
    }

    /**
     * Get current_layout
     *
     * @return current_layout
     */
    public int getActiveLayoutId() {
        return activeLayoutId;
    }

    public void setActiveLayoutId(int activeLayoutId) {
        this.activeLayoutId = activeLayoutId;
        mainMenuBar.enableLayoutItems(activeLayoutId != -1);

        updateProjectTree();
    }

    /**
     * @return the number of active DAQ lists
     */
    public int getActiveDaqlistIdCnt() {
        return activeDaqlistIdCnt;
    }

    /**
     * Resets activeDaqlistIdCnt
     */
    public void resetActiveDaqlistIdCnt() {
        int relId = 0;

        // loop through DAQ list
        for (int i = 0; i < daqLists.size(); i++) {
            if (daqLists.get(i).isActive()) {
                // set relative identifier
                daqLists.get(i).setRelativeId(relId);

                // increment counter
                relId++;
            } else {
                // reset relative identifier
                daqLists.get(i).setRelativeId(-1);
            }
        }

        // store value
        activeDaqlistIdCnt = relId;
    }

    /**
     * Returns the array index for the specified relative identifier
     */
    public int getDaqListId(int relativeId) {
        for (int i = 0; i < daqLists.size(); i++) {
            if (daqLists.get(i).getRelativeId() == relativeId) {
                return i;
            }
        }

        return -1;
    }

    public void addDaqListListener(DaqListListener listener) {
        if (!daqListListeners.contains(listener)) {
            daqListListeners.add(listener);
        }
    }

    public void removeDaqListListener(DaqListListener listener) {
        daqListListeners.remove(listener);
    }

    public void updateDaqListListeners() {
        daqListListeners.forEach(listener -> listener.daqListUpdate());
    }

    /**
     * Get current_asap2
     *
     * @return current_asap2
     */
    public int getActiveAsap2Id() {
        return activeAsap2Id;
    }

    public void setActiveAsap2Id(int activeAsap2Id) {

        if (this.activeAsap2Id != activeAsap2Id) {
            if (activeAsap2Id != -1) {

                // reconnect if necessary
                if (isConnected()) {
                    connectProtocol(false);
                    connectProtocol(true);
                }

                enableExport(true);

            } else {
                enableExport(false);
            }
        }

        this.activeAsap2Id = activeAsap2Id;
        if (activeAsap2Id == -1) {
            mASAP2Load.setText("Load File");
        } else {
            mASAP2Load.setText("Unload File");
        }
        updateProjectTree();
    }

    /**
     * Resets the pointers of the currently in used asap2 file, DAQlist, layout
     * and calibration.
     */
    public void clearActiveIds() {
        activeAsap2Id = -1;
        activeLayoutId = -1;
        activeCalibrationId = -1;
        currentConfig.getHANtuneManager().storeAndCloseDaqLists();
        updateProjectTree();
    }

    /**
     * Initializes the HANtune functionality
     */
    public void initHANtune() {

        initComponents();
        initCustomComponents();
        addFullScreenKeyHandler();

        UIManager.put("ToolTip.background", Color.WHITE);
        ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);

        // set program window icon
        Image img = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/HANlogo.png"));
        setIconImage(img);

        mainStatusBar.init();
        connectProtocol(false);

        // get last directory from userPrefs
        currentConfig.setPreviousPath(CustomFileChooser.FileType.ASAP2_FILE,
            (new File(userPrefs.get("lastAsap2Path", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(CustomFileChooser.FileType.DBC_FILE,
            (new File(userPrefs.get("lastDBCPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(CustomFileChooser.FileType.LOG_FILE,
            (new File(userPrefs.get("lastLogPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(CustomFileChooser.FileType.MAP_FILE,
            (new File(userPrefs.get("lastMapPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(CustomFileChooser.FileType.MATLAB_FILE,
            (new File(userPrefs.get("lastMatlabPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(CustomFileChooser.FileType.DCM_FILE,
            (new File(userPrefs.get("lastDcmPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(CustomFileChooser.FileType.SREC_FILE,
            (new File(userPrefs.get("lastSRecPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(CustomFileChooser.FileType.IHEX_FILE,
            (new File(userPrefs.get("lastHexPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(CustomFileChooser.FileType.PNG_FILE,
            (new File(userPrefs.get("lastPngPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(CustomFileChooser.FileType.PROJECT_FILE,
            (new File(userPrefs.get("lastProjectPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(CustomFileChooser.FileType.SERVICE_PROJECT_FILE,
            (new File(userPrefs.get("lastServiceProjectPath", System.getProperty("user.dir")))));
        currentConfig.setPreviousPath(CustomFileChooser.FileType.PYTHON_FILE,
            (new File(userPrefs.get("lastPythonPath", System.getProperty("user.dir")))));

        currentConfig.setFlashSRecBaseFileName(userPrefs.get(CurrentConfig.LAST_FLASH_SREC_BASE_FILE, ""));
        currentConfig.setFlashIHexBaseFileName(userPrefs.get(CurrentConfig.LAST_FLASH_IHEX_BASE_FILE, ""));

        // init project panel
        initProjectTreePopupMenus();

        showProjectPanel(true);
        boolean pinLayout = userPrefs.getBoolean("pinLayout", true);
        setSnapLayoutToSidePanel(pinLayout);

        if (userPrefs.getBoolean("minimizeConsole", true)) {
            consolePanel.toggleSize();
        }

        // set preferred sizes and resize to maximized state
        setMinimumSize(new Dimension(640, 480));
        setExtendedState(this.getExtendedState() | HANtune.MAXIMIZED_BOTH);

        // configure closing of window
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        // reset export function
        enableExport(false);

        // load projectFile
        hanTuneProject.loadProject(this, arguments, userPrefs);
        new JythonScript("Startup.py", "./scripts/Startup.py").start();

        mainStatusBar.updateMaxDaqFrequency();

        mainStatusBar.updateDaqFrequency();


        // put list of user altered properties in logfile
        String[] applicProperties = ApplicationProperties.getAllPropertyStringArray();
        if (applicProperties.length > 0) {
            //AppendToLogfile.appendMessage(applicProperties);
        }
    }

    @SuppressWarnings("serial")
    private void initCustomComponents() {

        projectPanelScrollPane = new JScrollPane();
        projectTree = new ProjectTree();
        projectTree.setDynamicToolTip();
        projectTree.setTransferHandler(new TreeTransferHandler(projectTree));
        projectPanelScrollPane.setViewportView(projectTree);
        projectPanelScrollPane.setBorder(BorderFactory.createLineBorder(SidePanel.BORDER_COLOR));

        javax.swing.JMenuItem menuTabAdd = new javax.swing.JMenuItem();
        menuTabAdd.setText("Add Tab");
        menuTabAdd.addActionListener(this::menuTabAddActionPerformed);
        tabPopupMenu.add(menuTabAdd, 1);

        projectPanel = new SidePanel("Project Data");
        projectPanel.add(projectPanelScrollPane);
        layeredPane.add(projectPanel, JLayeredPane.PALETTE_LAYER);
        buttonSidePanelMap.put((VerticalButton) projectButton, projectPanel);

        asap2Panel = new Asap2Panel("ASAP2 File");
        currentConfig.addAsap2Listener(asap2Panel);
        layeredPane.add(asap2Panel, JLayeredPane.PALETTE_LAYER);
        buttonSidePanelMap.put((VerticalButton) asap2Button, asap2Panel);

        dbcPanel = new DbcPanel("DBC Files");
        layeredPane.add(dbcPanel, JLayeredPane.PALETTE_LAYER);
        buttonSidePanelMap.put((VerticalButton) dbcButton, dbcPanel);

        scriptedElementsButton = new VerticalButton("Scripted elements", false);
        scriptedElementsButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        scriptedElementsButton.addActionListener(e -> scriptedElementsButtonActionPerformed());
        layeredPane.add(scriptedElementsButton);
        scriptedElementsButton.setBounds(2, 480, 24, 140);

        scriptedElementsPanel = new ScriptedElementsPanel("Scripted Elements");
        layeredPane.add(scriptedElementsPanel, JLayeredPane.PALETTE_LAYER);
        buttonSidePanelMap.put(scriptedElementsButton, scriptedElementsPanel);
        ScriptingManager.getInstance().addListener(scriptedElementsPanel);

        sidePanelManager.setSidePanelBounds(24, 22, 250, layeredPane.getHeight() - 25);

        tabbedPane.setFocusable(false);
        tabbedPane.setVisible(false);

        consolePanel = new ConsolePanel();
        consolePanel.initialize();
        consolePanel.setBounds(24, 763, 1200, 200);
        layeredPane.add(consolePanel, JLayeredPane.PALETTE_LAYER);
    }

    /**
     * Initializes ProjectTreePopupMenus
     */
    private void initProjectTreePopupMenus() {
        // register selection listener
        projectTree.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                if (evt.getButton() == 1 && evt.getClickCount() == 2) {
                    // select closest
                    projectTree
                        .setSelectionRow(projectTree.getClosestRowForLocation(evt.getX(), evt.getY()));

                    // retrieve info
                    DefaultMutableTreeNode node
                        = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
                    if (node == null) {
                        return;
                    }

                    ProjectInfo.Type type = ((ProjectInfo) node).getType();
                    switch (type) {
                        case Asap2File:
                            mASAP2LoadActionPerformed(new ActionEvent(node, 0, "dblclick"));
                            break;

                        case Layout:
                            mLayoutLoadActionPerformed(new ActionEvent(node, 0, "dblclick"));
                            break;

                        case Daqlist:
                            mDaqlistLoadActionPerformed(new ActionEvent(node, 0, "dblclick"));
                            break;

                        case Calibration:
                            if (!((ProjectInfo) node).getName()
                                .equals(ProjectTree.WORKING_PARAMETER_SET)) {
                                mCalibrationLoadActionPerformed(new ActionEvent(node, 0, "dblclick"));
                            }

                            break;

                        case DbcFile:
                            loadDbcActionPerformed(new ActionEvent(node, 0, "dblclick"));
                            break;

                        case Script:
                            Script script = (Script) node.getUserObject();
                            Util.openFile(script.getPath());
                            break;

                        default:
                            break;
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent evt) {
                TreePath path = projectTree.getClosestPathForLocation(evt.getX(), evt.getY());
                if (path != null) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
                    if (node != null) {
                        ProjectInfo.Type type = ((ProjectInfo) node).getType();
                        projectTree.setDragEnabled(type == ProjectInfo.Type.Script);
                    }
                }

                if (evt.isPopupTrigger()) {
                    showPopup(evt);
                }
            }

            @Override
            public void mouseReleased(MouseEvent evt) {
                if (evt.isPopupTrigger()) {
                    showPopup(evt);
                }
            }

            private void showPopup(MouseEvent evt) {
                // select closest
                projectTree.setSelectionRow(projectTree.getClosestRowForLocation(evt.getX(), evt.getY()));

                // Retrieve info
                DefaultMutableTreeNode node
                    = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
                if (node == null) {
                    return;
                }
                String name = ((ProjectInfo) node).getName();
                ProjectInfo.Type type = ((ProjectInfo) node).getType();

                // right-click
                if (evt.getButton() == MouseEvent.BUTTON3) {
                    if (type == ProjectInfo.Type.Folder) {
                        if (name.equals("ASAP2 files")) {
                            asap2sPopupMenu.show(projectTree, evt.getX(), evt.getY());
                        }
                        if (name.equals("DBC files")) {
                            dbcsPopupMenu.show(projectTree, evt.getX(), evt.getY());
                        }
                        if (name.equals("DAQ lists")) {
                            daqlistsPopupMenu.show(projectTree, evt.getX(), evt.getY());
                        }
                        if (name.equals("Layouts")) {
                            layoutsPopupMenu.show(projectTree, evt.getX(), evt.getY());
                        }
                        if (name.equals("Calibrations")) {
                            calibrationsPopupMenu.show(projectTree, evt.getX(), evt.getY());
                        }
                        if (name.equals("Scripts")) {
                            getScriptsPopupMenu().show(projectTree, evt.getX(), evt.getY());
                        }
                    }
                    if (type == ProjectInfo.Type.Asap2File) {
                        mASAP2Load.setText(activeAsap2Id == ((ProjectInfo) node).getId() ? "Unload File" : "Load File");
                        asap2PopupMenu.show(projectTree, evt.getX(), evt.getY());
                    }
                    if (type == ProjectInfo.Type.DbcFile) {
                        String dbcId = ((ProjectInfo) node).getFile().getName();
                        String menuText
                            = currentConfig.hasDescriptionFile(dbcId) ? "Unload File" : "Load File";
                        mDbcLoad.setText(menuText);
                        dbcPopupMenu.show(projectTree, evt.getX(), evt.getY());
                    }
                    if (type == ProjectInfo.Type.Daqlist) {
                        mDaqlistLoad.setText(
                            daqLists.get(((ProjectInfo) node).getId()).isActive() ? UNLOAD_DAQ_LIST : LOAD_DAQ_LIST);
                        daqlistPopupMenu.show(projectTree, evt.getX(), evt.getY());
                    }
                    if (type == ProjectInfo.Type.Layout) {
                        mLayoutLoad.setText(((ProjectInfo) node).isActive() ? "Unload Layout" : "Load Layout");
                        layoutPopupMenu.show(projectTree, evt.getX(), evt.getY());
                    }
                    if (type == ProjectInfo.Type.Calibration) {
                        if (name.equals(ProjectTree.WORKING_PARAMETER_SET)) {
                            workingParameterPopupMenu.show(projectTree, evt.getX(), evt.getY());
                        } else {
                            calibrationPopupMenu.show(projectTree, evt.getX(), evt.getY());
                        }
                    }
                    if (type == ProjectInfo.Type.Script) {
                        getScriptPopupMenu().show(projectTree, evt.getX(), evt.getY());
                    }
                }
            }
        });
    }

    public JPopupMenu getScriptsPopupMenu() {
        JPopupMenu scriptsMenu = new JPopupMenu();
        JMenuItem addScriptMenuItem = new JMenuItem("Add Script");
        addScriptMenuItem.addActionListener(e -> addScript());
        scriptsMenu.add(addScriptMenuItem);
        return scriptsMenu;
    }

    public JPopupMenu getScriptPopupMenu() {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        Script script = (Script) node.getUserObject();
        JPopupMenu scriptMenu = new JPopupMenu();
        JMenuItem runOrStopMenuItem = new JMenuItem(script.isRunning() ? "Stop" : "Start");
        runOrStopMenuItem.addActionListener(e -> toggleScript(script));
        scriptMenu.add(runOrStopMenuItem);
        JCheckBoxMenuItem runOnStartupMenuItem = new JCheckBoxMenuItem("Run on startup");
        runOnStartupMenuItem.addActionListener(e -> setProjectStartupScript(script));
        runOnStartupMenuItem.setSelected(script.isRunOnStartup());
        scriptMenu.add(runOnStartupMenuItem);
        JMenuItem editFileMenuItem = new JMenuItem("Edit");
        editFileMenuItem.addActionListener(e -> Util.openFile(script.getPath()));
        scriptMenu.add(editFileMenuItem);
        JMenuItem openFileLocationMenuItem = new JMenuItem("Open File Location");
        openFileLocationMenuItem.addActionListener(e -> Util.openFileLocation(script.getPath()));
        scriptMenu.add(openFileLocationMenuItem);
        JMenuItem removeScriptMenuItem = new JMenuItem("Remove");
        removeScriptMenuItem.addActionListener(e -> removeScript(script));
        scriptMenu.add(removeScriptMenuItem);
        return scriptMenu;
    }

    public void addScript() {
        CustomFileChooser fileChooser = new CustomFileChooser("Add Script", CustomFileChooser.FileType.PYTHON_FILE);
        File scriptFile = fileChooser.chooseOpenDialog(this, "Add Script");
        if (scriptFile != null) {
            currentConfig.getHANtuneManager().addScriptToProject(scriptFile);
        }
    }

    private void toggleScript(Script script) {
        if (script.isRunning()) {
            script.stop();
        } else {
            script.start();
        }
    }

    public void setProjectStartupScript(Script startupScript) {
        List<Script> scripts = ScriptingManager.getInstance().getScripts();
        scripts.forEach(s -> {
            if (s == startupScript) {
                s.setRunOnStartup(!s.isRunOnStartup());
            } else {
                s.setRunOnStartup(false);
            }
        });
        scripts.remove(startupScript);
        scripts.add(0, startupScript);
        updateProjectTree();
    }

    public void removeScript(Script script) {
        if (showConfirm("Are you sure you want to remove \"" + script.getName()
            + "\" from this project?") == 0) {
            currentConfig.getHANtuneManager().removeScriptFromProject(script);
        }
    }

    /**
     * Updates the project content in the project treeview
     */
    public void updateProjectTree() {

        // create tree
        if (currentConfig.getHANtuneDocument() == null) {
            projectTree.setModel(null);
        } else {
            projectTree.setModel(ProjectTree.getTreeModel(currentConfig.getProjectFile(), currentConfig.getHANtuneDocument(), daqLists, activeLayoutId, activeCalibrationId));
        }
        projectTree.setCellRenderer(new ProjectTreeRenderer());
        projectTree.setToggleClickCount(0);

        // expand all items
        for (int i = 0; i < projectTree.getRowCount(); i++) {
            projectTree.expandRow(i);
        }

        ToolTipManager.sharedInstance().registerComponent(projectTree);
    }

    public void updateProjectTreeUI() {
        projectTree.updateUI();
    }

    /**
     * show/hide all service tool related Gui components
     */
    public void updateServiceToolGuiComponents() {
        setAllSidePanelsVisible(!CurrentConfig.getInstance().isServiceToolMode());

        mainMenuBar.updateItemsServiceModeVisibility();
    }

    /**
     * Update title bar of main window
     */
    public void updateTitleBar() {
        String projName = null;
        if (currentConfig.getProjectFile() != null) {
            projName = currentConfig.getProjectFile().getName();
        }
        if (projName != null) {
            setTitle(applicationName + " - " + projName);
        } else {
            setTitle(applicationName);
        }
    }

    /**
     * Shows projectPanel2
     *
     * @param visible
     */
    public void showProjectPanel(boolean visible) {
        showPanel(projectPanel, visible);
    }

    /**
     * Shows or hides all side panels including their vertical buttons
     *
     * @param visible true: show
     */
    public void setAllSidePanelsVisible(boolean visible) {
        sidePanelManager.setSidePanelVisible(visible);

        for (Map.Entry<VerticalButton, SidePanel> entry : buttonSidePanelMap.entrySet()) {
            entry.getKey().setVisible(visible);
        }

        adjustLayoutSize(visible);
    }

    /**
     * Sets the visibility of the provided side panel and the appearance of the
     * corresponding button. All other side panels will be hidden. *
     *
     * @param sidePanel - the side panel to show or hide
     * @param show      - whether to hide or show the side panel
     */
    public void showPanel(SidePanel sidePanel, boolean show) {
        for (Map.Entry<VerticalButton, SidePanel> entry : buttonSidePanelMap.entrySet()) {
            boolean active = entry.getValue() == sidePanel && show;
            entry.getKey().setActive(active);
            entry.getValue().setVisible(active);
        }
        adjustLayoutSize(show);
    }

    public boolean isSnapLayoutToSidePanelSet() {
        return snapLayoutToSidePanel;
    }

    public void setSnapLayoutToSidePanel(boolean snapLayoutToSidePanel) {
        if (!CurrentConfig.getInstance().isServiceToolMode()) {
            this.snapLayoutToSidePanel = snapLayoutToSidePanel;
            userPrefs.put("pinLayout", String.valueOf(snapLayoutToSidePanel));
            setSidePanelSize();
            adjustLayoutSize(sidePanelManager.isSidePanelVisible());
        }
    }

    private void setSidePanelSize() {
        if (snapLayoutToSidePanel) {
            sidePanelManager.setSidePanelBounds(20, -1, sidePanelManager.getSidePanelWidth(),
                (layeredPane.getHeight() - 23) + 24);
        } else {
            sidePanelManager.setSidePanelBounds(20, 19, sidePanelManager.getSidePanelWidth(),
                layeredPane.getHeight() - 19);
        }
    }

    private boolean isFullScreen = false;
    private HANtuneTab fullScreenTab;

    public void addFullScreenKeyHandler() {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher((KeyEvent e) -> {
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
            {
                // handle ESC-key only when in 'fullscreen' mode
                if (isFullScreen()) {
                    setFullScreen(false); // don't care if transparent
                }
            }
            return false;
        });
    }

    public void setFullScreen(boolean transparent) {
        if (Util.isWindows()) {
            setFullScreenWindows(transparent);
        } else if (Util.isLinux()) {
            setFullScreenLinux(transparent);
        }
    }


    private void setFullScreenWindows(boolean transparent) {
        this.dispose();
        if (!isFullScreen) {
            fullScreenTab
                = (HANtuneTab) ((JScrollPane) tabbedPane.getSelectedComponent()).getViewport().getComponent(0);
            layeredPane.add(fullScreenTab);
            mainMenuBar.minimize();
            tabbedPane.setVisible(false);
            mainStatusBar.setVisible(false);
            consolePanel.setVisible(false);
            setAllSidePanelsVisible(false);
            this.setUndecorated(true);
            if (transparent) {
                fullScreenTab.setOpaque(false);
                this.setBackground(new Color(1.0f, 1.0f, 1.0f, 0.0f));
                this.setAlwaysOnTop(true);
            }
        } else {
            ((JScrollPane) tabbedPane.getComponentAt(tabbedPane.getSelectedIndex())).getViewport().add(fullScreenTab);
            mainMenuBar.restore();
            tabbedPane.setVisible(true);
            mainStatusBar.setVisible(true);
            consolePanel.setVisible(true);
            setAllSidePanelsVisible(true);
            fullScreenTab.setOpaque(true);
            this.setBackground(new Color(1.0f, 1.0f, 1.0f, 1.0f));
            this.setUndecorated(false);
            this.setAlwaysOnTop(false);
        }
        isFullScreen = !isFullScreen;
        this.setVisible(true);
        this.requestFocus();
    }


    //TODO: sometimes this results in 2 views visible simultaneously. Should be improved. Occurs in Linux (Fedora 28)
    // When 2 views are visible, a HANtune restart is required to remove them
    private void setFullScreenLinux(boolean transparent) {
        this.dispose();
        if (!isFullScreen) {
            fullScreenTab
                    = (HANtuneTab) ((JScrollPane) tabbedPane.getSelectedComponent()).getViewport().getComponent(0);
            ((JScrollPane) tabbedPane.getSelectedComponent()).getViewport().remove(fullScreenTab);

            layeredPane.add(fullScreenTab);

            mainMenuBar.minimize();
            tabbedPane.setVisible(false);
            mainStatusBar.setVisible(false);
            consolePanel.setVisible(false);
            setAllSidePanelsVisible(false);
            this.setUndecorated(true);
            if (transparent) {
                fullScreenTab.setOpaque(false);
                this.setBackground(new Color(1.0f, 1.0f, 1.0f, 0.0f));
                this.setAlwaysOnTop(true);
            }
        } else {
            synchronized (fullScreenTab) {
                ((JScrollPane) tabbedPane.getSelectedComponent()).getViewport().add(fullScreenTab);
                mainMenuBar.restore();
                tabbedPane.setVisible(true);
                mainStatusBar.setVisible(true);
                consolePanel.setVisible(true);
                setAllSidePanelsVisible(true);
                fullScreenTab.setOpaque(true);
                this.setBackground(new Color(1.0f, 1.0f, 1.0f, 1.0f));
                this.setUndecorated(false);
                this.setAlwaysOnTop(false);
                layeredPane.remove(fullScreenTab);
                fullScreenTab = null;
            }
        }
        this.isFullScreen = !isFullScreen;
        getMainMenuBar().updateWindowMenuChromelessState(isFullScreen, transparent);
        this.setVisible(true);
        this.requestFocus();
    }

    public boolean isFullScreen() {
        return isFullScreen;
    }

    public HANtuneTab getFullScreenTab() {
        return fullScreenTab;
    }

    public void adjustLayoutSize(boolean sidePanelVisible) {
        if (isFullScreen) {
            fullScreenTab.setBounds(0, 0, HANtune.this.getWidth(), HANtune.this.getHeight());
        } else {
            int layoutWidth = layeredPane.getWidth() - 21;
            int layoutHeight = layeredPane.getHeight() - consolePanel.getHeight() - 3;
            if (sidePanelVisible && snapLayoutToSidePanel) {
                tabbedPane.setBounds(20 + sidePanelManager.getSidePanelWidth(), 1,
                    layoutWidth - sidePanelManager.getSidePanelWidth(), layoutHeight);
                consolePanel.setBounds(20 + sidePanelManager.getSidePanelWidth(), layoutHeight,
                    layoutWidth - sidePanelManager.getSidePanelWidth() - 2, consolePanel.getHeight());
            } else {
                tabbedPane.setBounds(23, 1, layoutWidth, layoutHeight);
                consolePanel.setBounds(23, layoutHeight, layoutWidth - 2, consolePanel.getHeight());
            }
            consolePanel.validate();
        }
    }

    /**
     * Getter for the error monitor object.
     *
     * @return Error monitor object.
     */
    public ErrorMonitor getErrorMonitor() {
        return errorMonitor;
    }

    /**
     * Manages the XCP connection
     *
     * @param conn true = new connection, false = close current connection
     */
    public void connectProtocol(boolean conn) {
        // connect or disconnect
        if (conn) {
            // preconfigure error monitoring support if the configuration
            // contains an error viewer window
            if (currentConfig.getHANtuneManager().isErrorViewerPresent()) {
                if (currentConfig.getXcpsettings() != null) {
                    currentConfig.getXcpsettings().setErrorMonitoringEnabled(true);
                }
            }
            // Handle Read log dialog before connecting
            if (LogReaderAction.getInstance().isActive()) {
                LogReaderAction.getInstance().dispose();
            }
            connect();
        } else {
            disconnect();
        }

        updateConnectionGUI();
    }

    private void updateConnectionGUI() {
        // at this point a connection might have been established through the
        // connection dialog but it is not yet know through wich protocol
        // because the protocol can be changed on the connection dialog

        // ---------------------------------------------------------------------
        // connection established with the XCP protocol?
        // ---------------------------------------------------------------------
        if (currentConfig.getXcp() != null && currentConfig.getXcp().isConnected()) {
            // init error monitor observers to clear errors from error viewers
            errorMonitor.initObservers();
            // reset the error monitoring label on the status bar
            XCPSettings xcpSettings = currentConfig.getXcpsettings();

            // yes, connected XCP
            mainStatusBar.setConnected(true, xcpSettings.slaveid);

            if (xcpSettings.isErrorMonitoringEnabled()) {
                if (xcpSettings.isErrorMonitoringSupported()) {
                    errorMonitor.setController(currentConfig.getXcp());
                    currentConfig.getHANtuneManager().registerErrorObservers();
                    errorMonitor.enable();
                } else {
                    MessagePane.showInfo("Controller does not support error monitoring.");
                }
            }

        } // Generic CAN connection active?
        else if (currentConfig.getCan() != null && currentConfig.getCan().isConnected()) {
            // reset the error monitoring label on the status bar
            mainStatusBar.setConnected(true, "CAN-bus");
            updateProjectTree();
            currentConfig.getHANtuneManager().updateAllWindows();

        } else {
            cancelConnectionTimoutTimer();
            cancelDaqListReceiveTimoutTimer();

            errorMonitor.setController(null);
            mainStatusBar.setConnected(false, "OFF");
            updateDAQlistGUI();
        }
    }

    public void quickConnect(boolean calibrate) {
        switch (currentConfig.getProtocol()) {
            case XCP_ON_CAN:
                if (isLinux()) {
                    quickstartXCP(new XCPonSocketCan(), calibrate);
                } else {
                    quickstartXCP(new XCPonCANBasic(), calibrate);
                }
                break;
            case XCP_ON_ETHERNET:
                quickstartXCP(new XCPonEthernet(), calibrate);
                break;
            case XCP_ON_UART:
                quickstartXCP(new XCPonUART(), calibrate);
                break;
            case CAN:
                CanConnectionHandler canConnectionHandler = currentConfig.getCan();
                canConnectionHandler.startSession();
                Console.println("Can session started");
                break;
        }
        updateConnectionGUI();
    }

    private void quickstartXCP(XCP xcp, boolean calibrate) {
        for (int i = 0; i < daqLists.size(); i++) {
            // currently active?
            if (daqLists.get(i).isActive()) {
                // set reload flag
                daqLists.get(i).setReload(true);
            }
        }

        for (int i = 0; i < daqLists.size(); i++) {
            if (daqLists.get(i).isActive()) {
                currentConfig.getHANtuneManager().unloadDaqlist(i, false, false);
            }
        }
        currentConfig.setXcp(xcp);
        xcp.start();
        xcp.startSession();
        xcp.requestInfo();
        currentConfig.getXcpsettings().slaveid = String.valueOf(xcp.getInfo().slave_identifier);
        for (DAQList daqList : daqLists) {
            if (daqList.getPrescaler() == 0) {
                daqList.setPrescaler((char) Math.ceil(currentConfig.getXcpsettings().getSlaveFrequency()
                    / currentConfig.getXcpsettings().sample_freq_init));
            }
        }

        if (calibrate) {
            CalibrationHandler.getInstance().handleParameterCalibration();
        }
        currentConfig.getHANtuneManager().requestParameters();
        XCPDaqInfo daqinfo = xcp.getDaqInfo();

        HANtuneManager manager = currentConfig.getHANtuneManager();
        for (int i = 0; i < daqLists.size(); i++) {
            if (daqLists.get(i).mustReload()) {
                manager.loadDaqlist(i, false);
            }
        }

        if (getActiveDaqlistIdCnt() == 0) {
            manager.loadDaqlist(0, false);
        }

        manager.setDaqlistSize(daqinfo.max_event_channel);
        manager.startDaqlists();
        manager.checkDaqListTimeOut();

        mainStatusBar.enableDaqPrescalers();
    }

    /**
     * Establishes a new XCP connection
     */
    public void connect() {

        connectionDialog = new ConnectionDialog();
        connectionDialog.setVisible(true);
    }

    /**
     * Determines if the program currently has a connection through one of the
     * supported communication protocols.
     *
     * @return True if connected, false otherwise.
     */
    public boolean isConnected() {
        if (currentConfig.getXcp() != null) {
            return currentConfig.getXcp().isConnected();
        }
        if (currentConfig.getCan() != null) {
            return currentConfig.getCan().isConnected();
        }
        return false;
    }

    /**
     * Closes the current XCP connection
     */
    public void disconnect() {
        // first disable the error monitor so it can no longer submit
        // requests to the communication protocol.
        errorMonitor.disable();

        try {
            // object initialized?
            if (currentConfig.getXcp() != null) {
                currentConfig.getXcp().stopSession();
                Thread.sleep(0, 500000);
            }

            if (currentConfig.getCan() != null) {
                currentConfig.getCan().stopSession();
            }

            // clear object
            currentConfig.setXcp(null);
            currentConfig.setCan(null);

            // stop datalog, if necessary
            LogDataWriterCsv.getInstance().enableDatalogging(false, false, false);
//            LogDataWriterCsv datalog = LogDataWriterCsv.getInstance();
//            if (datalog != null && datalog.isWritingActive()) {
//                datalog.closeDatalog();
//                updateDatalogGuiComponents();
//            }

            // reset the daqlist size
            currentConfig.getHANtuneManager().setDaqlistSize(DAQ_MAX_DEFAULT);
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
        }
    }

    public void updateDatalogGuiComponents() {
        mainStatusBar.updateDataLogComponents();
    }

    /**
     * Performs the required DAQ list GUI updates
     */
    public void updateDAQlistGUI() {
        mainStatusBar.enableDaqPrescalers();
        mainStatusBar.updateDaqFrequency();

        setDaqListReceiveTimoutTimer();
        checkMeasurementActivity();
    }

    public void checkMeasurementActivity() {
        if (currentConfig.getASAP2Data() != null) {
            for (ASAP2Measurement measurement : currentConfig.getASAP2Data().getMeasurements()) {
                boolean active = false;
                for (DAQList daqList : daqLists) {
                    if (daqList.isActive() && daqList.contains(measurement)) {
                        active = true;
                        break;
                    }
                }
                measurement.setActive(active);
            }
        }
    }

    /**
     * enables or disables given DAQ lists
     *
     * @param mode     (0=stop all; 1=start list; 2=stop list)
     * @param daqlists the DAQ lists which are stopped or started
     * @return whether this command was executed successfully
     */
    public boolean toggleDaqlists(char mode, List<Character> daqlists) {
        // start or stop DAQ lists
        if (currentConfig.getXcp() != null) {
            if (currentConfig.getXcp().isConnected() && currentConfig.getXcp().getDaqLists().length > 0) {
                char[] lists = new char[daqlists.size()];
                for (int i = 0; i < daqlists.size(); i++) {
                    lists[i] = daqlists.get(i);
                }
                if (!currentConfig.getXcp().toggleDaq(mode, lists)) {
                    MessagePane.showError(
                        "Could not toggle the DAQ list(s)!\n" + currentConfig.getXcp().getErrorMsg());
                    return false;
                }
            }
        }

        // update frequency labels in statusbar
        mainStatusBar.updateDaqFrequency();
        mainStatusBar.updateBusloadIndicator();

        return true;
    }

    /**
     * Modifies prescaler used for DAQ list
     *
     * @param id
     * @param ps
     * @param update_slave
     * @return
     */
    public boolean changePrescaler(int id, char ps, boolean update_slave) {

        // calculate and store prescaler and derived sample frequency
        daqLists.get(id).setPrescaler(ps);
        mainStatusBar.resetLagMovingAvrg();

        mainStatusBar.updateBusloadIndicator();
        // configure prescaler at slave
        if (update_slave && daqLists.get(id).isActive() && currentConfig.getXcp() != null
            && currentConfig.getXcp().isConnected() && currentConfig.getXcp().getDaqLists().length > 0) {
            // relative id
            int relativeId = daqLists.get(id).getRelativeId();

            // request DAQ info (for timestamp information)
            XCPDaqInfo daqinfo = currentConfig.getXcp().getDaqInfo();
            if (daqinfo == null) {
                MessagePane.showError("Could not request DAQ processor information.\n"
                    + currentConfig.getXcp().getErrorMsg());
                return false;
            }

            // set prescaler
            if (!currentConfig.getXcp().setPrescaler((char) relativeId, (char) daqLists.get(id).getPrescaler())) {
                MessagePane.showError("Could not configure the specified prescaler at the slave device!\n"
                    + currentConfig.getXcp().getErrorMsg());
                return false;
            } else {
                LogDataWriterCsv datalog = LogDataWriterCsv.getInstance();
                // restart datalog
                if (datalog.isWritingActive()) {
                    if (!datalog.restart()) {
                        MessagePane.showError("Could not restart logfile.\n\n" + datalog.getError());
                    }
                    HANtune.getInstance().updateDatalogGuiComponents();

                }
            }
        }

        // store prescaler in DAQ list
        currentConfig.getHANtuneManager().storeDaqlist(id);

        // modify related statusbar elements
        mainStatusBar.updateDaqFrequency();

        // update GUI
        updateDAQlistGUI();

        updateDaqListListeners();

        return true;
    }

    private void handleFrequencyLabelEvent(MouseEvent event) {
        if (currentConfig.isServiceToolMode()) {
            return;
        }
        String name = ((JLabel) event.getSource()).getName();
        if (name != null) {
            int id = Integer.parseInt(name);
            currentConfig.getHANtuneManager().modifyDaqlist(id);
        }
    }

    public void setDaqListReceiveTimoutTimer() {
        long timeout = (long) (1000 * currentConfig.getXcpsettings().getTPrescaler()); // return timeout
        // setting in ms

        daqListReceiveTimoutTimer
            = new ResettableTimer(scheduler, 2 * timeout, TimeUnit.MILLISECONDS, new Runnable() {
            @Override
            public void run() {
                Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
                mainStatusBar.rotatorWarning();

                if (connectionTimoutTimer == null) {
                    connectionTimoutTimer = new Timer();

                    connectionTimoutTimer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            Thread.currentThread()
                                .setUncaughtExceptionHandler(new defaultExceptionHandler());
                            HANtune.this.connectProtocol(false);
                            MessagePane.showError("Connection has been lost!");
                        }
                    }, CONNECTION_TIMEOUT);
                }
            }
        });
    }

    private void cancelDaqListReceiveTimoutTimer() {
        if (daqListReceiveTimoutTimer != null) {
            daqListReceiveTimoutTimer.clear();
            daqListReceiveTimoutTimer = null;
        }
    }

    private void cancelConnectionTimoutTimer() {
        if (connectionTimoutTimer != null) {
            connectionTimoutTimer.cancel();
            connectionTimoutTimer = null;
        }
    }

    /**
     * Enables the export related items
     */
    public void enableExport(boolean enabled) {
        boolean asap2RamFlashOffsetPresent = false;

        // change tooltip
        if (enabled) {
            mCalibrationUpdate
                .setToolTipText("Update calibration with data from current Working Parameter Set");
            mCalibrationLoad.setToolTipText("Write calibration into current Working Parameter Set");

            String basedOn = "Create a new calibration based on ";
            mCalibrationCreate.setToolTipText(basedOn + "current Working Parameter Set");
            mCalibrationCreateFromDCM.setToolTipText(basedOn + CustomFileChooser.FileType.DCM_FILE);
            mCalibrationCreateFromSRec.setToolTipText(basedOn + CustomFileChooser.FileType.SREC_FILE);
            mCalibrationCreateFromIHex.setToolTipText(basedOn + CustomFileChooser.FileType.IHEX_FILE);
            mWorkingParameterCreate.setToolTipText(basedOn + "current Working Parameter Set");

            String exportTo = "Export current calibration data to ";
            mCalibrationExportMatlab.setToolTipText(exportTo + CustomFileChooser.FileType.MATLAB_FILE.getDescription());
            mCalibrationExportDcm.setToolTipText(exportTo + CustomFileChooser.FileType.DCM_FILE.getDescription());

            mCalibrationImportDcm.setToolTipText(
                "Import an " + CustomFileChooser.FileType.DCM_FILE.getDescription() + " into current snapshot");
            mCalibrationImportSRec.setToolTipText(
                "Import a " + CustomFileChooser.FileType.SREC_FILE.getDescription() + " into current snapshot");
            mCalibrationImportIHex.setToolTipText(
                "Import a " + CustomFileChooser.FileType.IHEX_FILE.getDescription() + " into current snapshot");

            mCalibrationWriteIntoAndFlashSRec.setToolTipText("Write current snapshot data into an existing "
                + CustomFileChooser.FileType.SREC_FILE.getDescription() + " followed by flashing all in target");
            mCalibrationWriteIntoAndFlashIHex.setToolTipText("Write current snapshot data into an existing "
                + CustomFileChooser.FileType.IHEX_FILE.getDescription() + " followed by flashing all in target");

            if (currentConfig.getASAP2Data() != null) {
                asap2RamFlashOffsetPresent = currentConfig.getASAP2Data().getModPar().isRamFlashOffsetPresent();
            }

        } else {
            String asapFirst = "Please open an ASAP2 file first";
            mCalibrationUpdate.setToolTipText(asapFirst);
            mCalibrationLoad.setToolTipText(asapFirst);

            mCalibrationCreate.setToolTipText(asapFirst);
            mCalibrationCreateFromDCM.setToolTipText(asapFirst);
            mCalibrationCreateFromSRec.setToolTipText(asapFirst);
            mCalibrationCreateFromIHex.setToolTipText(asapFirst);
            mWorkingParameterCreate.setToolTipText(asapFirst);

            mCalibrationExportMatlab.setToolTipText(asapFirst);
            mCalibrationExportDcm.setToolTipText(asapFirst);

            mCalibrationImportDcm.setToolTipText(asapFirst);
            mCalibrationImportSRec.setToolTipText(asapFirst);
            mCalibrationImportIHex.setToolTipText(asapFirst);
        }

        // change enabled state of export menuitem
        mCalibrationUpdate.setEnabled(enabled);
        mCalibrationLoad.setEnabled(enabled);

        mCalibrationExportMatlab.setEnabled(enabled);
        mCalibrationExportDcm.setEnabled(enabled);
        mCalibrationImportDcm.setEnabled(enabled);

        mCalibrationCreate.setEnabled(enabled);
        mCalibrationCreateFromDCM.setEnabled(enabled);
        mCalibrationCreateFromSRec.setEnabled(enabled && asap2RamFlashOffsetPresent);
        mCalibrationCreateFromIHex.setEnabled(enabled && asap2RamFlashOffsetPresent);
        mWorkingParameterCreate.setEnabled(enabled);

        mCalibrationImportSRec.setEnabled(enabled && asap2RamFlashOffsetPresent);
        mCalibrationImportIHex.setEnabled(enabled && asap2RamFlashOffsetPresent);
        mCalibrationWriteIntoAndFlashSRec.setEnabled(enabled);
        mCalibrationWriteIntoAndFlashIHex.setEnabled(enabled);
    }

    /**
     * Terminates HANtune.
     */
    public void exitHANtune() {
        // check length of logfile, if nessecary, trim logfile.
        ErrorLogRestrictor.Restrictor(); // Check errorLog.txt file size and trim file if nesseccary

        connectProtocol(false);
        System.exit(0);
    }

    /**
     * Starts exit procedure for HANtune
     */
    public void requestExitHANtune() {

        // save last used file paths
        userPrefs.put("lastAsap2Path", currentConfig.getPreviousPath(CustomFileChooser.FileType.ASAP2_FILE).getPath());
        userPrefs.put("lastDBCPath", currentConfig.getPreviousPath(CustomFileChooser.FileType.DBC_FILE).getPath());
        userPrefs.put("lastLogPath", currentConfig.getPreviousPath(CustomFileChooser.FileType.LOG_FILE).getPath());
        userPrefs.put("lastMapPath", currentConfig.getPreviousPath(CustomFileChooser.FileType.MAP_FILE).getPath());
        userPrefs.put("lastMatlabPath", currentConfig.getPreviousPath(CustomFileChooser.FileType.MATLAB_FILE).getPath());
        userPrefs.put("lastDcmPath", currentConfig.getPreviousPath(CustomFileChooser.FileType.DCM_FILE).getPath());
        userPrefs.put("lastSRecPath", currentConfig.getPreviousPath(CustomFileChooser.FileType.SREC_FILE).getPath());
        userPrefs.put("lastHexPath", currentConfig.getPreviousPath(CustomFileChooser.FileType.IHEX_FILE).getPath());
        userPrefs.put("lastPngPath", currentConfig.getPreviousPath(CustomFileChooser.FileType.PNG_FILE).getPath());
        userPrefs.put("lastProjectPath", currentConfig.getPreviousPath(CustomFileChooser.FileType.PROJECT_FILE).getPath());
        userPrefs.put("lastServiceProjectPath", currentConfig.getPreviousPath(CustomFileChooser.FileType.SERVICE_PROJECT_FILE).getPath());
        userPrefs.put("lastPythonPath", currentConfig.getPreviousPath(CustomFileChooser.FileType.PYTHON_FILE).getPath());

        userPrefs.put(CurrentConfig.LAST_FLASH_SREC_BASE_FILE, currentConfig.getFlashSRecBaseFileName());
        userPrefs.put(CurrentConfig.LAST_FLASH_IHEX_BASE_FILE, currentConfig.getFlashIHexBaseFileName());
        userPrefs.put("minimizeConsole", String.valueOf(consolePanel.isMinimized()));

        userPrefs.put("loggerEnabled", String.valueOf(LogDataWriterCsv.getInstance().isWritingStandby()));
        userPrefs.put("readWhileWriteLoggerEnabled", String.valueOf(LogDataWriterCsv.getInstance()
            .isReadWhileWriteStandby()));

        if (currentConfig.getProjectFile() != null) {
            userPrefs.put("lastOpenProject", currentConfig.getProjectFile().getAbsolutePath());
        }

        if (hanTuneProject.isProceedOKCheckModifiedProjectSave()) {
            exitHANtune();
        }
    }

    /**
     * Starts open ASAP2 procedure.
     */
    public void requestOpenASAP2() {
        CustomFileChooser fileChooser = new CustomFileChooser("Open", CustomFileChooser.FileType.ASAP2_FILE);
        File asap2File = fileChooser.chooseOpenDialog(this, "Open");

        if (asap2File != null) {
            currentConfig.getHANtuneManager().newASAP2(asap2File);
            showProjectPanel(true);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated
    // Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabPopupMenu = new javax.swing.JPopupMenu();
        menuTabEdit = new javax.swing.JMenuItem();
        menuTabDelete = new javax.swing.JMenuItem();
        asap2sPopupMenu = new javax.swing.JPopupMenu();
        mASAP2Add = new javax.swing.JMenuItem();
        asap2PopupMenu = new javax.swing.JPopupMenu();
        mASAP2Load = new javax.swing.JMenuItem();
        mASAP2Remove = new javax.swing.JMenuItem();
        daqlistsPopupMenu = new javax.swing.JPopupMenu();
        mDaqlistNew = new javax.swing.JMenuItem();
        daqlistPopupMenu = new javax.swing.JPopupMenu();
        mDaqlistLoad = new javax.swing.JMenuItem();
        mDaqlistModify = new javax.swing.JMenuItem();
        mDaqlistRename = new javax.swing.JMenuItem();
        mDaqlistCopy = new javax.swing.JMenuItem();
        mDaqlistRemove = new javax.swing.JMenuItem();
        layoutsPopupMenu = new javax.swing.JPopupMenu();
        mLayoutNew = new javax.swing.JMenuItem();
        layoutPopupMenu = new javax.swing.JPopupMenu();
        mLayoutLoad = new javax.swing.JMenuItem();
        mLayoutRename = new javax.swing.JMenuItem();
        mLayoutCopy = new javax.swing.JMenuItem();
        mLayoutRemove = new javax.swing.JMenuItem();
        calibrationsPopupMenu = new javax.swing.JPopupMenu();
        mCalibrationCreate = new javax.swing.JMenuItem();
        mCalibrationCreateFromSRec = new javax.swing.JMenuItem();
        mCalibrationCreateFromIHex = new javax.swing.JMenuItem();
        mCalibrationCreateFromDCM = new javax.swing.JMenuItem();
        workingParameterPopupMenu = new JPopupMenu();
        mWorkingParameterCreate = new JMenuItem();
        calibrationPopupMenu = new JPopupMenu();
        mCalibrationUpdate = new javax.swing.JMenuItem();
        mCalibrationLoad = new javax.swing.JMenuItem();
        mCalibrationRename = new javax.swing.JMenuItem();
        mCalibrationCopy = new JMenuItem();
        mCalibrationRemove = new JMenuItem();
        mCalibrationSubExport = new JMenu();
        mCalibrationSubImport = new JMenu();
        mCalibrationExportMatlab = new JMenuItem();
        mCalibrationExportDcm = new JMenuItem();
        mCalibrationImportDcm = new JMenuItem();
        mCalibrationImportSRec = new JMenuItem();
        mCalibrationImportIHex = new JMenuItem();
        mCalibrationWriteIntoAndFlashSRec = new javax.swing.JMenuItem();
        mCalibrationWriteIntoAndFlashIHex = new javax.swing.JMenuItem();
        dbcsPopupMenu = new javax.swing.JPopupMenu();
        mDbcAdd = new javax.swing.JMenuItem();
        dbcPopupMenu = new JPopupMenu();
        mDbcLoad = new javax.swing.JMenuItem();
        mDbcRemove = new javax.swing.JMenuItem();
        layeredPane = new javax.swing.JLayeredPane();
        tabbedPane = new javax.swing.JTabbedPane();
        projectButton = new VerticalButton("Project data", false);
        asap2Button = new VerticalButton("ASAP2 elements", false);
        dbcButton = new VerticalButton("CAN elements", false);

        // Init and fill main status bar
        mainStatusBar = new HANtuneMainStatusBar(this);
        mainStatusBar = new HANtuneMainStatusBar(this);

        // Init and fill main menu bar here
        mainMenuBar = new HANtuneMainMenuBar(this);
        setJMenuBar(mainMenuBar.getMenuBar());


        menuTabEdit.setText("Rename Tab");
        menuTabEdit.addActionListener(this::menuTabEditActionPerformed);
        tabPopupMenu.add(menuTabEdit);

        menuTabDelete.setText("Delete Tab");
        menuTabDelete.addActionListener(this::menuTabDeleteActionPerformed);
        tabPopupMenu.add(menuTabDelete);

        mASAP2Add.setText("Add ASAP2 file");
        mASAP2Add.addActionListener(this::mASAP2AddActionPerformed);
        asap2sPopupMenu.add(mASAP2Add);

        mASAP2Load.setText("Load File");
        mASAP2Load.addActionListener(this::mASAP2LoadActionPerformed);
        asap2PopupMenu.add(mASAP2Load);

        mASAP2Remove.setText("Remove");
        mASAP2Remove.addActionListener(this::mASAP2RemoveActionPerformed);
        asap2PopupMenu.add(mASAP2Remove);

        mDaqlistNew.setText("New DAQ list");
        mDaqlistNew.addActionListener(this::mDaqlistNewActionPerformed);
        daqlistsPopupMenu.add(mDaqlistNew);

        mDaqlistLoad.addActionListener(this::mDaqlistLoadActionPerformed);
        daqlistPopupMenu.add(mDaqlistLoad);

        mDaqlistModify.setText("Modify DAQ list");
        mDaqlistModify.addActionListener(this::mDaqlistModifyActionPerformed);
        daqlistPopupMenu.add(mDaqlistModify);

        mDaqlistRename.setText("Rename DAQ list");
        mDaqlistRename.addActionListener(this::mDaqlistRenameActionPerformed);
        daqlistPopupMenu.add(mDaqlistRename);

        mDaqlistCopy.setText("Copy DAQ list");
        mDaqlistCopy.addActionListener(this::mDaqlistCopyActionPerformed);
        daqlistPopupMenu.add(mDaqlistCopy);

        mDaqlistRemove.setText("Remove DAQ list");
        mDaqlistRemove.addActionListener(this::mDaqlistRemoveActionPerformed);
        daqlistPopupMenu.add(mDaqlistRemove);

        mLayoutNew.setText("New Layout");
        mLayoutNew.addActionListener(this::mLayoutNewActionPerformed);
        layoutsPopupMenu.add(mLayoutNew);

        mLayoutLoad.setText("Load Layout");
        mLayoutLoad.addActionListener(this::mLayoutLoadActionPerformed);
        layoutPopupMenu.add(mLayoutLoad);

        mLayoutRename.setText("Rename");
        mLayoutRename.addActionListener(this::mLayoutRenameActionPerformed);
        layoutPopupMenu.add(mLayoutRename);

        mLayoutCopy.setText("Copy");
        mLayoutCopy.addActionListener(this::mLayoutCopyActionPerformed);
        layoutPopupMenu.add(mLayoutCopy);

        mLayoutRemove.setText("Remove");
        mLayoutRemove.addActionListener(this::mLayoutRemoveActionPerformed);
        layoutPopupMenu.add(mLayoutRemove);


        // CalibrationSSSS PopUp
        mCalibrationCreate.setText("Create calibration from Working Parameter Set");
        mCalibrationCreate.addActionListener(this::mCalibrationCreateActionPerformed);
        calibrationsPopupMenu.add(mCalibrationCreate);

        mCalibrationCreateFromDCM.setText("Create calibration from DCM...");
        mCalibrationCreateFromDCM.addActionListener(this::mCalibrationCreateFromDCMActionPerformed);
        calibrationsPopupMenu.add(mCalibrationCreateFromDCM);

        mCalibrationCreateFromSRec.setText("Create calibration from SREC...");
        mCalibrationCreateFromSRec.addActionListener(this::mCalibrationCreateFromSRecActionPerformed);
        calibrationsPopupMenu.add(mCalibrationCreateFromSRec);

        mCalibrationCreateFromIHex.setText("Create calibration from HEX...");
        mCalibrationCreateFromIHex.addActionListener(this::mCalibrationCreateFromHexActionPerformed);
        calibrationsPopupMenu.add(mCalibrationCreateFromIHex);


        // Working parameter set PopUp
        mWorkingParameterCreate.setText("Create calibration from Working Parameter Set");
        mWorkingParameterCreate.addActionListener(this::mCalibrationCreateActionPerformed);
        workingParameterPopupMenu.add(mWorkingParameterCreate);

        // CalibratioNNN PopUp
        mCalibrationUpdate.setText("Update from Working Parameter Set");
        mCalibrationUpdate.addActionListener(this::mCalibrationUpdateActionPerformed);
        calibrationPopupMenu.add(mCalibrationUpdate);

        mCalibrationLoad.setText("Load into Working Parameter Set");
        mCalibrationLoad.addActionListener(this::mCalibrationLoadActionPerformed);
        calibrationPopupMenu.add(mCalibrationLoad);

        calibrationPopupMenu.addSeparator();

        mCalibrationRename.setText("Rename");
        mCalibrationRename.addActionListener(this::mCalibrationRenameActionPerformed);
        calibrationPopupMenu.add(mCalibrationRename);

        mCalibrationCopy.setText("Copy");
        mCalibrationCopy.addActionListener(this::mCalibrationCopyActionPerformed);
        calibrationPopupMenu.add(mCalibrationCopy);

        mCalibrationRemove.setText("Remove");
        mCalibrationRemove.addActionListener(this::mCalibrationRemoveActionPerformed);
        calibrationPopupMenu.add(mCalibrationRemove);

        calibrationPopupMenu.addSeparator();

        // submenu: export
        mCalibrationSubExport.setText("Export/Flash");

        mCalibrationExportMatlab.setText("to MATLAB file...");
        mCalibrationExportMatlab.addActionListener(this::mCalibrationExportMatlabActionPerformed);
        mCalibrationSubExport.add(mCalibrationExportMatlab);

        mCalibrationExportDcm.setText("to DCM file...");
        mCalibrationExportDcm.addActionListener(this::mCalibrationExportDcmActionPerformed);
        mCalibrationSubExport.add(mCalibrationExportDcm);


        mCalibrationWriteIntoAndFlashSRec.setText("Export/Flash SREC...");
        mCalibrationWriteIntoAndFlashSRec.setEnabled(false);
        mCalibrationWriteIntoAndFlashSRec.addActionListener(this::mCalibrationExportIntoSRecAndFlashActionPerformed);
        mCalibrationSubExport.add(mCalibrationWriteIntoAndFlashSRec);

        mCalibrationWriteIntoAndFlashIHex.setText("Export/Flash IHEX...");
        mCalibrationWriteIntoAndFlashIHex.setEnabled(false);
        mCalibrationWriteIntoAndFlashIHex.addActionListener(this::mCalibrationExportIntoIHexAndFlashActionPerformed);
        mCalibrationSubExport.add(mCalibrationWriteIntoAndFlashIHex);

        calibrationPopupMenu.add(mCalibrationSubExport);
        // end: export submenu

        // submenu: import
        mCalibrationSubImport.setText("Import");

        mCalibrationImportDcm.setText("from DCM file...");
        mCalibrationImportDcm.addActionListener(this::mCalibrationImportDcmActionPerformed);
        mCalibrationSubImport.add(mCalibrationImportDcm);

        mCalibrationImportSRec.setText("from SREC file...");
        mCalibrationImportSRec.addActionListener(this::mCalibrationImportSRecActionPerformed);
        mCalibrationSubImport.add(mCalibrationImportSRec);

        mCalibrationImportIHex.setText("from HEX file...");
        mCalibrationImportIHex.addActionListener(this::mCalibrationImportHexActionPerformed);
        mCalibrationSubImport.add(mCalibrationImportIHex);

        calibrationPopupMenu.add(mCalibrationSubImport);
        // end: import submenu


        mDbcAdd.setText("Add DBC file");
        mDbcAdd.addActionListener(this::importDbcActionPerformed);
        dbcsPopupMenu.add(mDbcAdd);

        mDbcLoad.setText("Load File");
        mDbcLoad.addActionListener(this::loadDbcActionPerformed);
        dbcPopupMenu.add(mDbcLoad);

        mDbcRemove.setText("Remove File");
        mDbcRemove.addActionListener(this::removeDbcActionPerformed);
        dbcPopupMenu.add(mDbcRemove);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle(shortApplicationName);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        layeredPane.addComponentListener(new java.awt.event.ComponentAdapter() {
            @Override
            public void componentResized(java.awt.event.ComponentEvent evt) {
                layeredPaneComponentResized(evt);
            }
        });
        layeredPane.add(tabbedPane);
        tabbedPane.setBounds(24, 10, 1040, 550);

        projectButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        projectButton.addActionListener(this::projectButtonActionPerformed);
        layeredPane.add(projectButton);
        projectButton.setBounds(2, 60, 24, 140);

        asap2Button.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        asap2Button.addActionListener(this::asap2ButtonActionPerformed);
        layeredPane.add(asap2Button);
        asap2Button.setBounds(2, 200, 24, 140);

        dbcButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        dbcButton.addActionListener(this::dbcButtonActionPerformed);
        layeredPane.add(dbcButton);
        dbcButton.setBounds(2, 340, 24, 140);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(layeredPane, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(mainStatusBar.getStatusPanel(), javax.swing.GroupLayout.DEFAULT_SIZE, 1308, Short.MAX_VALUE));
        layout
            .setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addComponent(layeredPane, javax.swing.GroupLayout.DEFAULT_SIZE, 518, Short.MAX_VALUE)
                    .addGap(0, 0, 0).addComponent(mainStatusBar.getStatusPanel(), javax.swing.GroupLayout.PREFERRED_SIZE,
                        javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void asap2ButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_asap2ButtonActionPerformed
        showPanel(asap2Panel, !asap2Panel.isVisible());
    }// GEN-LAST:event_asap2ButtonActionPerformed

    private void layeredPaneComponentResized(java.awt.event.ComponentEvent evt) {// GEN-FIRST:event_layeredPaneComponentResized
        setSidePanelSize();
        adjustLayoutSize(sidePanelManager.isSidePanelVisible());
    }// GEN-LAST:event_layeredPaneComponentResized

    private void menuTabEditActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_menuTabEditActionPerformed
        String name = "";

        // show popup to enter applicationName of layout
        while (name != null && name.equals("")) {
            name
                = showQuestion("Rename Tab", "Name:", tabbedPane.getTitleAt(tabbedPane.getSelectedIndex()));
        }
        if (name != null) {
            currentConfig.getHANtuneManager().renameTab(tabbedPane.getSelectedIndex(), name);
        }
    }// GEN-LAST:event_menuTabEditActionPerformed

    private void menuTabDeleteActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_menuTabDeleteActionPerformed
        // delete the specified tab
        if (HANtune.showConfirm("Are you sure you want to delete this Tab and all its content?") == 0) {
            currentConfig.getHANtuneManager().deleteTab(tabbedPane.getSelectedIndex());
        }
    }// GEN-LAST:event_menuTabDeleteActionPerformed

    private void menuTabAddActionPerformed(java.awt.event.ActionEvent evt) {
        currentConfig.getHANtuneManager().addTab();
    }

    private void formWindowClosing(java.awt.event.WindowEvent evt) {// GEN-FIRST:event_formWindowClosing
        // close current layout
        requestExitHANtune();
    }// GEN-LAST:event_formWindowClosing

    private void projectButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_projectButtonActionPerformed
        showProjectPanel(!projectPanel.isVisible());
    }// GEN-LAST:event_projectButtonActionPerformed

    private void mCalibrationCreateActionPerformed(java.awt.event.ActionEvent evt) {
        CreateCalibration createCalib = new CreateCalibration(this);
        createCalib.createNewCalibration();
    }

    private void mCalibrationCreateFromSRecActionPerformed(ActionEvent evt) {
        CreateCalibration createCalib = new CreateCalibration(this);
        createCalib.doActionCreateCalibFromSRec();
    }

    private void mCalibrationCreateFromHexActionPerformed(ActionEvent evt) {
        CreateCalibration createCalib = new CreateCalibration(this);
        createCalib.doActionCreateCalibFromIHex();
    }

    private void mCalibrationCreateFromDCMActionPerformed(ActionEvent evt) {
        CreateCalibration createCalib = new CreateCalibration(this);
        createCalib.doActionCreateCalibFromDCM();
    }

    private void mASAP2LoadActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mASAP2LoadActionPerformed
        if (mASAP2Load.getText().equals("Load File")) {
            DefaultMutableTreeNode node
                = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
            if (node == null) {
                return;
            }

            currentConfig.getHANtuneManager().loadASAP2(((ProjectInfo) node).getId());
        } else {
            DefaultMutableTreeNode node
                = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
            if (node == null) {
                return;
            }

            if (((ProjectInfo) node).getId() == activeAsap2Id) {
                currentConfig.getHANtuneManager().loadASAP2(-1);
            } else {
                currentConfig.getHANtuneManager().loadASAP2(((ProjectInfo) node).getId());
            }
        }

    }// GEN-LAST:event_mASAP2LoadActionPerformed

    private void mASAP2AddActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mASAP2AddActionPerformed
        requestOpenASAP2();
    }// GEN-LAST:event_mASAP2AddActionPerformed

    private void mASAP2RemoveActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mASAP2RemoveActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }
        if (showConfirm("Are you sure you want to remove \"" + ((ProjectInfo) node).getName()
            + "\" from this project?") != 0) {
            return;
        }
        if (((ProjectInfo) node).getId() == activeAsap2Id) {
            mASAP2Load.setText("Load File");
        }
        currentConfig.getHANtuneManager().removeASAP2(((ProjectInfo) node).getId());
    }// GEN-LAST:event_mASAP2RemoveActionPerformed

    private void mLayoutRemoveActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mLayoutRemoveActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }
        if (showConfirm("Are you sure you want to remove \"" + ((ProjectInfo) node).getName()
            + "\" from this project?") != 0) {
            return;
        }
        currentConfig.getHANtuneManager().removeLayout(((ProjectInfo) node).getId());
    }// GEN-LAST:event_mLayoutRemoveActionPerformed

    private void mLayoutRenameActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mLayoutRenameActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }

        String name = "";

        // show popup to enter applicationName of DAQ list
        boolean validName = false;
        while (!validName) {
            name = HANtune.showQuestion("Rename layout", "Name:", "");
            validName = true;
            if (name.equals("")) {
                MessagePane.showError("Please enter a valid name");
                validName = false;
            }
            for (Layout HANtuneLayout : currentConfig.getHANtuneDocument().getHANtune().getLayoutList()) {
                if (HANtuneLayout.getTitle().equals(name)) {
                    MessagePane
                        .showError("A layout with this name already exists\n please choose another name");
                    validName = false;
                }
            }
        }
        currentConfig.getHANtuneManager().renameLayout(((ProjectInfo) node).getId(), name);
    }// GEN-LAST:event_mLayoutRenameActionPerformed

    private void mLayoutLoadActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mLayoutLoadActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }
        if (((ProjectInfo) node).getId() == activeLayoutId) {
            currentConfig.getHANtuneManager().loadLayout(-1);
        } else {
            currentConfig.getHANtuneManager().loadLayout(((ProjectInfo) node).getId());
        }
    }// GEN-LAST:event_mLayoutLoadActionPerformed

    private void mLayoutNewActionPerformed(java.awt.event.ActionEvent evt) {
        String name = HANtune.showQuestion("New layout", "Name:", "");
        if (name == null) {
            return; // cancel has been pressed.
        }

        currentConfig.createNewLayout(name);
    }

    private void mCalibrationRenameActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mCalibrationRenameActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }

        String name = "";
        // show popup to enter applicationName of DAQ list
        boolean validName = false;
        while (!validName) {
            name = HANtune.showQuestion("Rename calibration", "Name:", "");
            validName = true;
            if (name.equals("")) {
                MessagePane.showError("Please enter a valid name");
                validName = false;
            }
            for (Calibration calibration : currentConfig.getHANtuneDocument().getHANtune()
                .getCalibrationList()) {
                if (calibration.getTitle().equals(name)) {
                    MessagePane.showError(
                        "A calibration with this name already exists\n please choose another name");
                    validName = false;
                }
            }
        }
        calibrationHandler.renameCalibration(((ProjectInfo) node).getId(), name);
        // update projectTree
        updateProjectTree();
    }// GEN-LAST:event_mCalibrationRenameActionPerformed

    private void mCalibrationRemoveActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mCalibrationRemoveActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }
        if (showConfirm("Are you sure you want to remove \"" + ((ProjectInfo) node).getName()
            + "\" from this project?") != 0) {
            return;
        }
        calibrationHandler.removeCalibration(((ProjectInfo) node).getId());
        // reload project
        updateProjectTree();
    }// GEN-LAST:event_mCalibrationRemoveActionPerformed

    private void mCalibrationLoadActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mCalibrationLoadActionPerformed
        if (activeAsap2Id == -1) {
            MessagePane.showInfo("Please open an ASAP2 file first!");
            return;
        }

        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }

        int id = ((ProjectInfo) node).getId();

        calibrationHandler.loadCalibration(id, this);
    }// GEN-LAST:event_mCalibrationLoadActionPerformed

    private void mCalibrationExportMatlabActionPerformed(java.awt.event.ActionEvent evt) {
        ExportCalibration exportCalib = new ExportCalibration(this);
        exportCalib.doActionExportCalibrationMatlab((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }

    private void mCalibrationExportDcmActionPerformed(java.awt.event.ActionEvent evt) {
        ExportCalibration exportCalib = new ExportCalibration(this);
        exportCalib.doActionExportCalibrationDCM((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }

    private void mCalibrationImportDcmActionPerformed(java.awt.event.ActionEvent evt) {
        ImportCalibration importCalib = new ImportCalibration(this);
        importCalib.doActionImportDCM((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }

    private void mCalibrationImportSRecActionPerformed(java.awt.event.ActionEvent evt) {
        ImportCalibration importCalib = new ImportCalibration(this);
        importCalib.doActionImportSRec((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }

    private void mCalibrationImportHexActionPerformed(java.awt.event.ActionEvent evt) {
        ImportCalibration importCalib = new ImportCalibration(this);
        importCalib.doActionImportHex((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }

    private void mCalibrationExportIntoSRecAndFlashActionPerformed(java.awt.event.ActionEvent evt) {
        ExportFlashSRecAction action = new ExportFlashSRecAction(this);
        action.handleExportFlashDialog((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }

    private void mCalibrationExportIntoIHexAndFlashActionPerformed(java.awt.event.ActionEvent evt) {
        ExportFlashIHexAction action = new ExportFlashIHexAction(this);
        action.handleExportFlashAction((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }

    private void mLayoutCopyActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mLayoutCopyActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }
        if (showConfirm("Are you sure you want to copy \"" + ((ProjectInfo) node).getName()
            + "\"") != 0) {
            return;
        }
        currentConfig.getHANtuneManager().copyLayout(((ProjectInfo) node).getId());
    }// GEN-LAST:event_mLayoutCopyActionPerformed

    private void mCalibrationCopyActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mCalibrationCopyActionPerformed
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }
        if (showConfirm("Are you sure you want to copy \"" + ((ProjectInfo) node).getName()
            + "\"") != 0) {
            return;
        }
        calibrationHandler.copyCalibration(((ProjectInfo) node).getId());
        // reload project
        updateProjectTree();
    }// GEN-LAST:event_mCalibrationCopyActionPerformed

    private void mCalibrationUpdateActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mCalibrationUpdateActionPerformed
        if (activeAsap2Id == -1) {
            MessagePane.showInfo("Please open an ASAP2 file first!");
            return;
        }

        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (node == null) {
            return;
        }

        calibrationHandler.updateCalibration(((ProjectInfo) node).getId());
        // reload projecttree
        updateProjectTree();
    }// GEN-LAST:event_mCalibrationUpdateActionPerformed

    private void mDaqlistNewActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mDaqlistNewActionPerformed
        DaqlistAction daqlistAction = new DaqlistAction(this);
        daqlistAction.daqlistCreate();
    }// GEN-LAST:event_mDaqlistNewActionPerformed

    private void mDaqlistLoadActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mDaqlistLoadActionPerformed
        DaqlistAction daqlistAction = new DaqlistAction(this);
        daqlistAction.daqlistToggleLoad((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }// GEN-LAST:event_mDaqlistLoadActionPerformed

    private void mDaqlistRenameActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mDaqlistRenameActionPerformed
        DaqlistAction daqlistAction = new DaqlistAction(this);
        daqlistAction.daqlistRename((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }// GEN-LAST:event_mDaqlistRenameActionPerformed

    private void mDaqlistCopyActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mDaqlistCopyActionPerformed
        DaqlistAction daqlistAction = new DaqlistAction(this);
        daqlistAction.daqlistCopy((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }// GEN-LAST:event_mDaqlistCopyActionPerformed

    private void mDaqlistRemoveActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mDaqlistRemoveActionPerformed
        DaqlistAction daqlistAction = new DaqlistAction(this);
        daqlistAction.daqlistRemove((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }// GEN-LAST:event_mDaqlistRemoveActionPerformed

    private void mDaqlistModifyActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_mDaqlistModifyActionPerformed
        DaqlistAction daqlistAction = new DaqlistAction(this);
        daqlistAction.daqlistModify((DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent());
    }// GEN-LAST:event_mDaqlistModifyActionPerformed

    private void loadDbcActionPerformed(java.awt.event.ActionEvent evt) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        File dbcFile = ((ProjectInfo) node).getFile();
        if (!currentConfig.hasDescriptionFile(dbcFile)) {
            currentConfig.getHANtuneManager().loadDbcFile(dbcFile);
        } else {
            currentConfig.getHANtuneManager().unLoadDbcFile(dbcFile);
        }
        updateProjectTree();
        currentConfig.getHANtuneManager().updateAllWindows();
    }

    private void importDbcActionPerformed(java.awt.event.ActionEvent evt) {
        CustomFileChooser fileChooser = new CustomFileChooser("Import", CustomFileChooser.FileType.DBC_FILE);
        File dbcFile = fileChooser.chooseOpenDialog(this, "Import");

        if (dbcFile != null) {
            currentConfig.getHANtuneManager().newDbc(dbcFile);
            showProjectPanel(true);
        }
    }

    private void removeDbcActionPerformed(java.awt.event.ActionEvent evt) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) projectTree.getLastSelectedPathComponent();
        if (showConfirm("Are you sure you want to remove \"" + ((ProjectInfo) node).getName()
            + "\" from this project?") == 0) {
            String dbcId = ((ProjectInfo) node).getFile().getName();
            currentConfig.getHANtuneManager().removeDbcFile(dbcId);
        }
    }

    private void dbcButtonActionPerformed(java.awt.event.ActionEvent evt) {
        showPanel(dbcPanel, !dbcPanel.isVisible());
    }

    public void daqListReceivedUpdate(long arg) {
        mainStatusBar.updateDaqListReceived();

        if (daqListReceiveTimoutTimer != null) {
            cancelConnectionTimoutTimer();
            daqListReceiveTimoutTimer.reset(false);
        }
    }

    private void scriptedElementsButtonActionPerformed() {
        showPanel(scriptedElementsPanel, !scriptedElementsPanel.isVisible());
    }


    // provide faster startup while developing, use this 'hidden' commandline argument
    private static boolean isSkipSleepPresent(String[] args) {
        for (String arg : args) {
            if (arg.equals("-SkipSleep")) {
                return true;
            }
        }
        return false;
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
        arguments = args;

        // set LookAndFeel depending on OS
        try {
            if (Util.isWindows()) {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } else if (Util.isLinux()) {
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } else {
                // All other not supported OS's
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                AppendToLogfile.appendMessage("OS not supported, using default LookAndFeel" + System.getProperty("os.name"));
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            AppendToLogfile.appendError(Thread.currentThread(), ex);
        }

        HANtuneSplash.splashInit();
        try {
            if (isSkipSleepPresent(args)) {
                Thread.sleep(5);
            } else {
                Thread.sleep(1500);
            }
        } catch (InterruptedException ex) {
            AppendToLogfile.appendError(Thread.currentThread(), ex);
            System.out.println(ex.toString());
        }

        SwingUtilities.invokeLater(() -> {
            Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
            HANtuneManager manager = new HANtuneManager(HANtune.getInstance());
            manager.startHantune();
        });
    }

    private javax.swing.JPopupMenu asap2PopupMenu;
    private javax.swing.JPopupMenu asap2sPopupMenu;
    private javax.swing.JPopupMenu calibrationPopupMenu;
    private javax.swing.JPopupMenu calibrationsPopupMenu;
    private javax.swing.JPopupMenu workingParameterPopupMenu;
    private javax.swing.JPopupMenu daqlistsPopupMenu;
    private javax.swing.JPopupMenu daqlistPopupMenu;
    private javax.swing.JPopupMenu dbcPopupMenu;
    private javax.swing.JPopupMenu dbcsPopupMenu;
    private javax.swing.JLayeredPane layeredPane;
    private javax.swing.JPopupMenu layoutPopupMenu;
    private javax.swing.JPopupMenu layoutsPopupMenu;
    private javax.swing.JMenuItem mASAP2Add;
    private javax.swing.JMenuItem mASAP2Load;
    private javax.swing.JMenuItem mASAP2Remove;
    private javax.swing.JMenuItem mCalibrationCopy;
    private javax.swing.JMenuItem mCalibrationSubExport;
    private javax.swing.JMenuItem mCalibrationExportMatlab;
    private javax.swing.JMenuItem mCalibrationExportDcm;
    private javax.swing.JMenuItem mCalibrationSubImport;
    private javax.swing.JMenuItem mCalibrationImportDcm;
    private javax.swing.JMenuItem mCalibrationImportSRec;
    private javax.swing.JMenuItem mCalibrationImportIHex;
    private javax.swing.JMenuItem mCalibrationWriteIntoAndFlashSRec;
    private javax.swing.JMenuItem mCalibrationWriteIntoAndFlashIHex;
    private javax.swing.JMenuItem mCalibrationLoad;
    private javax.swing.JMenuItem mCalibrationCreate;
    private javax.swing.JMenuItem mCalibrationCreateFromSRec;
    private javax.swing.JMenuItem mCalibrationCreateFromIHex;
    private javax.swing.JMenuItem mCalibrationCreateFromDCM;
    private javax.swing.JMenuItem mWorkingParameterCreate;
    private javax.swing.JMenuItem mCalibrationRemove;
    private javax.swing.JMenuItem mCalibrationRename;
    private javax.swing.JMenuItem mCalibrationUpdate;
    private javax.swing.JMenuItem mDaqlistCopy;
    private javax.swing.JMenuItem mDaqlistLoad;
    private javax.swing.JMenuItem mDaqlistModify;
    private javax.swing.JMenuItem mDaqlistNew;
    private javax.swing.JMenuItem mDaqlistRemove;
    private javax.swing.JMenuItem mDaqlistRename;
    private javax.swing.JMenuItem mDbcAdd;
    private javax.swing.JMenuItem mDbcLoad;
    private javax.swing.JMenuItem mDbcRemove;
    private javax.swing.JMenuItem mLayoutCopy;
    private javax.swing.JMenuItem mLayoutLoad;
    private javax.swing.JMenuItem mLayoutNew;
    private javax.swing.JMenuItem mLayoutRemove;
    private javax.swing.JMenuItem mLayoutRename;
    private javax.swing.JMenuItem menuTabDelete;
    private javax.swing.JMenuItem menuTabEdit;
    private javax.swing.JPopupMenu tabPopupMenu;
    private javax.swing.JTabbedPane tabbedPane;
}
