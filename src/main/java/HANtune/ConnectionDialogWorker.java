/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package HANtune;

import DAQList.DAQList;
import ErrorLogger.AppendToLogfile;
import ErrorLogger.defaultExceptionHandler;
import datahandling.CalibrationHandler;
import datahandling.CurrentConfig;
import XCP.XCP;
import XCP.XCPDaqInfo;
import XCP.XCPEvent;
import XCP.XCPInfo;
import XCP.XCPonCANBasic;
import XCP.XCPonSocketCan;
import XCP.XCPonEthernet;
import XCP.XCPonUART;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import util.MessagePane;

import static util.Util.isLinux;

/**
 *
 * @author la_alaap
 */
public class ConnectionDialogWorker extends SwingWorker<Integer, Integer>{

    private static final int MAX_HDR_PID_DAQ_CNT = 251; // Max PID value when sending DAQ items
    private XCPEvent[] events;
    private XCPDaqInfo daqinfo;
    private final HANtune hantune = HANtune.getInstance();
    private final CurrentConfig currentConfig = CurrentConfig.getInstance();
    private final ConnectionDialog connectionDialog;
    private boolean finished;
    private String parameterAction;

    public ConnectionDialogWorker(ConnectionDialog dialog) {
        this.connectionDialog = dialog;
    }

    public void connect(String parameterAction) {
        this.parameterAction = parameterAction;
        this.execute();
    }
    /**
     *
     * @return String: used for status updates and error messages
     * @throws Exception
     */
    @Override
    protected Integer doInBackground() throws Exception {
        Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
        // check if using too many signals
        int signalCounter = 0;
        for (DAQList daqList : hantune.daqLists) {
            if (daqList.isActive()) {
                signalCounter += daqList.getASAP2Measurements().size();
            }
        }


        // store the active daqlist, so they can be reset after the new XCPdaqlists are set
        memorizeDaqlist();

        // STEP 1: Connect to XCP network
        publish(1); //update progressbar and indicate which is the current step
        // decide about driver to use
        XCP xcp;
        switch (currentConfig.getProtocol()) {
            case XCP_ON_CAN:
                if (isLinux()) {
                    xcp = new XCPonSocketCan();
                    System.out.println("Starting XCPonSocketCAN driver...");
                } else {
                    xcp = new XCPonCANBasic();
                    System.out.println("Starting XCPonCANBasic driver...");
                }
                break;
            case XCP_ON_ETHERNET:
                xcp = new XCPonEthernet();
                System.out.println("Starting XCPonEthernet driver...");
                break;
            case XCP_ON_UART:
                xcp = new XCPonUART();
                System.out.println("Starting XCPonUART driver...");
                break;
            default:
                xcp = null;
                MessagePane.showError("Unsupported driver mode: " + currentConfig.getProtocol().name());
                System.out.println("Unsupported driver, xcp = null");
        }
        currentConfig.setXcp(xcp);

        // start session only if xcp != null
        if (xcp != null) {
            xcp.setName("XCPThread");
            xcp.start();
            try {
                if (!xcp.startSession()) { //Ticket #281 Exception occurs after issueing this command
                connectionDialog.showError("Could not connect to XCP device:\n\n" + xcp.getErrorMsg());
                return 0;
            }
            } catch (Exception e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
                connectionDialog.showError("Could not connect to XCP device:\n\n" + e.getMessage());
                return 0;
            }


            // STEP 2: Initialize XCP session
            publish(2);     //update progressbar and indicate which is the current step

            // request slave information
            XCPInfo info = xcp.requestInfo();
            currentConfig.getXcpsettings().slaveid = String.valueOf(info.slave_identifier);

            // STEP 3: validate if identifiers match, connection may only continue if they match
            publish(3);
            String xcpSlaveId = currentConfig.getXcpsettings().slaveid;
            if (!xcpSlaveId.equals(currentConfig.getASAP2Data().getEpromID()) && !currentConfig.getASAP2Data().getEpromID().equals("Project Comment Goes Here")) {
                boolean dialogDone = false;
                while (!dialogDone) {
                //display an error, connection should not be set up because the ASAP2 file
                    //ID does not match the id from the ECU device.
                    JOptionPane warningPane = new JOptionPane("Warning: Current ASAP2 application ID does not match "
                            + "the application ID obtained from the ECU.\n"
                            + "This may cause instability or may crash the ECU software.\n\n"
                            + "ASAP2 Application ID: " + currentConfig.getASAP2Data().getEpromID()
                            + "\nECU Application ID:     " + xcpSlaveId
                            + "\n\nDo you wish to continue?");
                    Object[] options = new String[]{"Yes", "No"};
                    warningPane.setOptions(options);
                    JDialog warningDialog = warningPane.createDialog(new JFrame(), "Warning!");
                    //warningDialog.show();
                    warningDialog.setVisible(true);
                    Object warningObj = warningPane.getValue();
                    int choice = -1;
                    for (int i = 0; i < options.length; i++) {
                        if (options[i].equals(warningObj)) {
                            choice = i;
                        }
                    }
                    if (choice == 0) {
                        warningDialog.dispose();
                        dialogDone = true;
                    }
                    if (choice == 1) {
                        warningDialog.dispose();
                        connectionDialog.showError("ASAP2 ID does not match and user cancelled connection.\n" + xcp.getErrorMsg());
                        return 0;
                    }
                }
            }

            // STEP 4: Request event information
            publish(4);
            events = xcp.getEvents();
            if (events == null || events.length == 0 || events[0] == null) {
                connectionDialog.showError("Could not request event information.\n" + xcp.getErrorMsg());
                return 0;
            }
            if (events.length == 0 || !events[0].getDaqAllowed() || events[0].getTimeUnit() == 0) {
                connectionDialog.showError("Available event with ID 0 does not allow DAQ list usage.\n" + xcp.getErrorMsg());
                return 0;
            }

            currentConfig.getXcpsettings().setSlaveFrequency(events[0].calcFrequency());
            for (DAQList daqList : hantune.daqLists) {
                if (daqList.getPrescaler() == 0) {
                    daqList.setPrescaler((char) Math.ceil(currentConfig.getXcpsettings().getSlaveFrequency() / currentConfig.getXcpsettings().sample_freq_init));
                }
            }

            // STEP 5: Request / Calibrate parameter values
            publish(5);
            if (parameterAction.equals(ConnectionDialog.REQUEST)) {
                // request parameters
                currentConfig.getHANtuneManager().requestParameters();
            } else {
                // calibrate parameters
                CalibrationHandler.getInstance().handleParameterCalibration();
                System.err.println("CALIBRATED");
                currentConfig.getHANtuneManager().requestParameters();
                System.err.println("REQUESTING PARAMETERS");
            }

            // STEP 6: Initialize DAQ list
            publish(6);
            if (!configureDaqLists(signalCounter)) {
                return 0;
            } else {
                finished = true;
                return 1;
            }
        }
        connectionDialog.showError("XCP = null");
        return 0;
    }

    /**
     * Necessary for Ethernet, not for USB.
     */
    private void memorizeDaqlist() {
        SwingUtilities.invokeLater(() -> {
            for (int i = 0; i < hantune.daqLists.size(); i++) {
                // currently active?
                if (hantune.daqLists.get(i).isActive()) {
                    // set reload flag
                    hantune.daqLists.get(i).setReload(true);
                }
            }

            for (int i = 0; i < hantune.daqLists.size(); i++) {
                if (hantune.daqLists.get(i).isActive()) {
                    currentConfig.getHANtuneManager().unloadDaqlist(i, false, false);
                }
            }
        });
    }

    private boolean configureDaqLists(int signalCounter) {
        XCP xcp = currentConfig.getXcp();
        daqinfo = xcp.getDaqInfo();
        if (daqinfo == null) {
            connectionDialog.showError("Could not request DAQ processor information.\n" + xcp.getErrorMsg());
            return false;
        }

        // check if using too many signals
        if (!xcp.isDaqlistNumberInXcpHdr()) {
            if (signalCounter > MAX_HDR_PID_DAQ_CNT) {
                connectionDialog.showError("Target currently connected can only send " + MAX_HDR_PID_DAQ_CNT
                    + " signals. You are requesting a total of " + signalCounter + " signals.\n"
                    + "Reduce total number of DAQ signals and reconnect.");
                return false;
            }
        }


        HANtuneManager manager = currentConfig.getHANtuneManager();
//todo: is this OK?????
//        SwingUtilities.invokeLater(() -> {
            for (int i = 0; i < hantune.daqLists.size(); i++) {
                // reload if necessary
                if (hantune.daqLists.get(i).mustReload()) {
                    manager.loadDaqlist(i, false);  //1st arg: id, 2nd arg: update slave?
                }
            }
            if (hantune.getActiveDaqlistIdCnt() == 0) {
                //load the default DAQ list if none are loaded
                manager.loadDaqlist(0, false);
            }

            manager.setDaqlistSize(daqinfo.max_event_channel);
            manager.startDaqlists();
            manager.checkDaqListTimeOut();
//            hantune.enablePrescalers();
            hantune.getMainStatusBar().enableDaqPrescalers();

//        });
        return true;
    }


    @Override
    protected void done() {
        connectionDialog.finished(finished);
    }

    @Override
    protected void process(List<Integer> steps) {
        for (int step : steps) {
            connectionDialog.updateProgress(step);
        }
    }
}



