/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.markup;

import ErrorLogger.AppendToLogfile;
import HANtune.HANtune;
import HANtune.HANtuneWindow;
import components.ImageFilter;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

import util.MessagePane;

/**
 *
 * @author Mathijs, Roel van den Boom
 */
public class ImageMarkup extends HANtuneWindow {

    private static final long serialVersionUID = 1L;
    private static final Dimension DEFAULT_SIZE = new Dimension(200, 200);
    private static final String REFERENCE = "";
    private Container container;
    private BackgroundPanel panel = new BackgroundPanel(null, BackgroundPanel.SCALED);
    //private Boolean visible;
    private Boolean border = true;
    private JLabel notfound;
    private JLabel startimage;
    private int originalHeight;
    private int originalWidth;

    public ImageMarkup() {
        defaultMinimumSize = new Dimension(70, 50);
        this.getContentPane().setPreferredSize(DEFAULT_SIZE);
        showBorder(getWindowSetting("showBorder", DEFAULT_BORDER));
        initializePopupMenu();
        addPopupMenuItems();
    }

    /**
     * This method rebuilds the content of this HANtuneWindow and should be
     * called only if the content changed (adding or removing asap2 references).
     * There is no need to call this method after a value or setting has
     * changed.
     *
     * The method loops through all asap2 references and will build the
     * components required for each asap2 reference.
     *
     * NOTE: Although the method loops through all asap2 references, this is not
     * yet suported. This HANtuneWindow allows for just a single asap2 reference
     * to be present. The developer will informed by a AssertException otherwise
     *
     */
    @Override
    public void rebuild() {
        //Start with an empty label
        getContentPane().removeAll();
        GridBagConstraints c;
        container = this.getRootPane();

        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        c.fill = GridBagConstraints.BOTH;
        getContentPane().add(panel, c);
        setAllOpaque(container, false);

        ImageIcon icon = createImageIcon("/images/HANlogo.png", "HANlogo");
        startimage = new JLabel("Use the contextmenu to choose an image", icon, JLabel.CENTER);
        startimage.setHorizontalTextPosition(JLabel.CENTER);
        startimage.setVerticalTextPosition(JLabel.BOTTOM);
        panel.add(startimage);
        loadSettings();
    }

    private void addPopupMenuItems() {

        JMenuItem imageLoader = new JMenuItem("Choose an image");
        imageLoader.addActionListener(e -> loadImage());
        addMenuItemToPopupMenu(imageLoader);

        JMenuItem resetSizeMenuItem = new JMenuItem("Scale image size");
        resetSizeMenuItem.addActionListener(e -> resetSize());
        addMenuItemToPopupMenu(resetSizeMenuItem);

        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    showPopupMenu(panel, e.getX(), e.getY());
                }
            }
        });

    }

    private void loadImage() {
        File root = new File(System.getProperty("user.dir") + "\\images\\");
        final JFileChooser fc = new JFileChooser(root);
        fc.setFileFilter(new ImageFilter());
        int returnValue = fc.showOpenDialog(ImageMarkup.this);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            BufferedImage img = null;
            try {
                img = ImageIO.read(fc.getSelectedFile());
            } catch (IOException ex) {
                AppendToLogfile.appendError(Thread.currentThread(), ex);
                MessagePane.showError("Error opening image");
            }
            JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(panel);
            if (img.getHeight() >= topFrame.getHeight() || img.getWidth() >= topFrame.getHeight()) {
                if (HANtune.showConfirm("Image is larger than the current tab which will result in scrollbars in the layout, do you wish to continue?") != 0) {
                    return;
                }
            }
            panel.setImage(img);
            panel.remove(startimage);
            try {
                panel.remove(notfound);
            } catch (Exception ex) {
                AppendToLogfile.appendError(Thread.currentThread(), ex);
            }
            setSize(img.getWidth(), img.getHeight());
            // compensate for the window frame and reset size
            int w = (img.getWidth() - getContentPane().getWidth()) + img.getWidth();
            int h = (img.getHeight() - getContentPane().getHeight()) + img.getHeight();
            setSize(w, h);
            originalHeight = getHeight();
            originalWidth = getWidth();
            addSetting(REFERENCE, "image", String.valueOf(fc.getSelectedFile()));
            addSetting(REFERENCE, "originalHeight", String.valueOf(originalHeight));
            addSetting(REFERENCE, "originalWidth", String.valueOf(originalWidth));
        }
    }

    private void resetSize() {
        String scale = HANtune.showQuestion("Scale dialog", "<html>Enter a percentage for resizing the image.<br>(100 = original size)</html>", "100");
        if (scale != null) {
            try {
                Double dValue = Double.parseDouble(Integer.toString(Integer.parseInt(scale)));
                setSize((int) Math.round((double) originalWidth * dValue / 100),
                        (int) Math.round((double) originalHeight * dValue / 100));
            } catch (Exception ex) {
                AppendToLogfile.appendMessage("Input is not a valid number");
                AppendToLogfile.appendError(Thread.currentThread(), ex);
                MessagePane.showError("Input was not a valid number");
            }
        }
        repaint();
    }

    /**
     * This method load the HANtuneWindow specific settings for each asap2
     * reference. This method should be called only if settings have been
     * changed. The method automatically triggers a repaint after the settings
     * are loaded.
     *
     * The method loops through all asap2 references and will load the settings
     * for each asap2 reference.
     *
     * NOTE: Although the method loops through all asap2 references, this is not
     * yet supported. This HANtuneWindow allows for just a single asap2 reference
     * to be present. The developer will informed by a AssertException otherwise
     */
    @Override
    public void loadSettings() {
        if (settings.containsKey(REFERENCE) && settings.get(REFERENCE).containsKey("width") && settings.get(REFERENCE).containsKey("height")) {
            //setSize(Integer.parseInt(settings.get(REFERENCE).get("width")), Integer.parseInt(settings.get(REFERENCE).get("height")));
            settings.get(REFERENCE).remove("width");                            // Settings "width" and "height" are deprecated as of HANtune 2.1. (duplicate properties) Need to delete them in order to prevent issues.
            settings.get(REFERENCE).remove("height");
        }
        if (settings.containsKey(REFERENCE) && settings.get(REFERENCE).containsKey("originalHeight")) {
            originalHeight = Integer.parseInt(settings.get(REFERENCE).get("originalHeight"));
        }
        if (settings.containsKey(REFERENCE) && settings.get(REFERENCE).containsKey("originalWidth")) {
            originalWidth = Integer.parseInt(settings.get(REFERENCE).get("originalWidth"));
        }
        if (settings.containsKey(REFERENCE) && settings.get(REFERENCE).containsKey("image")) {
            panel.remove(startimage);
            try {
                panel.setImage(ImageIO.read(new File(settings.get(REFERENCE).get("image"))));
            } catch (IOException ex) {
                AppendToLogfile.appendError(Thread.currentThread(), ex);
                ImageIcon icon = createImageIcon("/images/notfound.png", "Image not found");
                notfound = new JLabel("<html><p>" + settings.get(REFERENCE).get("image") + "</p></html>", icon, JLabel.CENTER);
                notfound.setHorizontalTextPosition(JLabel.CENTER);
                notfound.setVerticalTextPosition(JLabel.BOTTOM);
                panel.add(notfound);
            }
        }
        if (settings.containsKey(REFERENCE) && settings.get(REFERENCE).containsKey("showBorder")) {
            if (Boolean.parseBoolean(settings.get(REFERENCE).get("border")) == false) {
                panel.setBorder(null);
                border = false;
            } else {
                panel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
                border = true;
            }
        }
        if (settings.containsKey(REFERENCE) && settings.get(REFERENCE).containsKey("visible")) {
            //if (Boolean.parseBoolean(settings.get(REFERENCE).get("visible")) == false) {
            //    this.setUI(null);
            //    visible = false;
            //} else {
            //    visible = true;
            //}
            settings.get(REFERENCE).remove("visible");                          // Setting "visible" is deprecated in HANtune 2.1. Need to delete this key to avoid image rendering problems
        }
        super.loadWindowSettings();
        repaint();
    }


    /**
     * Sets opaque of all the components in the container to true or false
     *
     * @param c the container on witch to perform the action
     * @param opaque boolean to use
     */
    public final void setAllOpaque(Container c, boolean opaque) {
        if (c instanceof JComponent) {
            ((JComponent) c).setOpaque(opaque);
            for (int i = 0; i < c.getComponentCount(); i++) {
                Component comp = c.getComponent(i);
                if (comp instanceof Container) {
                    setAllOpaque((Container) comp, opaque);
                }
            }
        }
    }

    protected ImageIcon createImageIcon(String path,
            String description) {
        java.net.URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL, description);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

    @Override
    public void clearWindow() {

    }
}
