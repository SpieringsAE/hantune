/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.viewers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.JMenuItem;

import HANtune.SubrangeDialog;
import datahandling.CurrentConfig;
import datahandling.Signal;
import datahandling.SignalListener;
import nl.han.hantune.config.ApplicationProperties;

import static nl.han.hantune.config.ConfigProperties.LEDVIEWER_LABEL_POSITION;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class LedViewer extends HANtuneViewer<LedViewer> implements SignalListener {

    private static final Dimension DEFAULT_SIZE = new Dimension(100, 100);
    public static final int MINIMUM_THRESHOLDS = 1;
    public static final int MAXIMUM_THRESHOLDS = 4;
    private final Map<Double, Color> colorAndThresholdMap = new LinkedHashMap<>();
    private Signal signal;
    private Led led;
    private double lastValue;

    public LedViewer() {
        super(false);
        defaultMinimumSize = new Dimension(70, 80);
        setPreferredSize(DEFAULT_SIZE);
        setDefaultLabelPosition(ApplicationProperties.getTopBottomLabelPos(LEDVIEWER_LABEL_POSITION).getLabelPositionString());
    }

    private LedViewer createLedViewer(Signal signal) {
        this.signal = signal;
        setTitle(signal.getName());
        if (!windowSettings.containsKey("showBackground")) showBackground(false);
        initializePopupMenu();

        if (CurrentConfig.getInstance().isServiceToolMode()) {
            setComponentPopupMenu(null);
        }
        else {
            addPopupMenuItems();
        }


        GridBagConstraints gridBagConstraints;
        led = new Led();
        led.setOn(currentConfig.getXcp() != null);
        currentConfig.addXcpListener(connected -> led.setOn(connected));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.weighty = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        getContentPane().add(led, gridBagConstraints);
        return this;
    }

    @Override
    protected LedViewer getListenerForSignal(Signal signal) {
        return createLedViewer(signal);
    }

    private void addPopupMenuItems() {

        JMenuItem resetSizeMenuItem = new JMenuItem("Reset to default size");
        resetSizeMenuItem.addActionListener(e -> setSize(DEFAULT_SIZE));
        addMenuItemToWindowMenu(resetSizeMenuItem);

        JMenuItem editColorAndThresholdMenuItem = new JMenuItem("Edit colors and thresholds");
        editColorAndThresholdMenuItem.addActionListener(e -> setColorsAndThresholds());
        addMenuItemToPopupMenu(editColorAndThresholdMenuItem);
    }

    private void setColorsAndThresholds() {
        SubrangeDialog subrangeDialog = new SubrangeDialog(this, signal, MINIMUM_THRESHOLDS, MAXIMUM_THRESHOLDS, true);
        subrangeDialog.setVisible(true);
    }

    @Override
    public void loadSettings() {
        if (signal != null && settings.containsKey(signal.getName())) {
            Map<String, String> signalSettings = settings.get(signal.getName());

            colorAndThresholdMap.clear();
            for (int i = 0; i <= MAXIMUM_THRESHOLDS; i++) {
                if (signalSettings.containsKey("limit" + i) && signalSettings.containsKey("color" + i)) {
                    Double threshold = Double.parseDouble(signalSettings.get("limit" + i));
                    Color color = Color.decode(signalSettings.get("color" + i));
                    colorAndThresholdMap.put(threshold, color);
                } else if (signalSettings.containsKey("color" + i)) {
                    Color color = Color.decode(signalSettings.get("color" + i));
                    colorAndThresholdMap.put(signal.getMaximum(), color);
                }
            }
        }

        if (colorAndThresholdMap.isEmpty()) {
            setDefaultThresholdAndColors();
        }
        Color color = getColorForValue(lastValue);
        led.setColor(color);
    }

    private void setDefaultThresholdAndColors() {
        colorAndThresholdMap.put(signal.getMinimum(), Color.GREEN);
        colorAndThresholdMap.put(signal.getMaximum(), Color.RED);
    }

    @Override
    public void valueUpdate(double value, long timestamp) {
        Color color = getColorForValue(value);
        led.setColor(color);
        lastValue = value;
        repaint();
    }

    private Color getColorForValue(double value) {
        for (Map.Entry<Double, Color> entry : colorAndThresholdMap.entrySet()) {
            if (value <= entry.getKey()) {
                return entry.getValue();
            }
        }
        return null;
    }

    @Override
    public void stateUpdate() {

    }
}
