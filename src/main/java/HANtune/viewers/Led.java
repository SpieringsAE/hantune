/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.viewers;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MultipleGradientPaint;
import java.awt.RadialGradientPaint;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;
import javax.swing.JPanel;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class Led extends JPanel {

    private final RenderingHints rh;
    private Color color;
    private boolean on;

    public Led() {
        rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

    public boolean isOn() {
        return on;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHints(rh);

        int panelWidth = getWidth()-1;
        int panelHeight = getHeight()-1;
        int offsetX = 0;
        int offsetY = 0;

        if (panelWidth > panelHeight) {
            offsetX = (panelWidth - panelHeight) / 2;
        } else if (panelHeight > panelWidth) {
            offsetY = (panelHeight - panelWidth) / 2;
        }

        int diameter = panelWidth - (2 * offsetX);
        if (on) {
            paintLedOn(g2d, diameter, offsetX, offsetY);
        } else {
            paintLedOff(g2d, diameter, offsetX, offsetY);
        }
    }

    private void paintLedOn(Graphics2D g2d, int diameter, int offsetX, int offsetY) {
        Point2D center = new Point2D.Float((diameter / 2) + offsetX, (diameter / 2) + offsetY);
        float radius = diameter / 2;
        float[] dist = {0.5f, 1.0f};
        Color[] colors = {color, color.darker().darker()};
        RadialGradientPaint p = new RadialGradientPaint(center, radius, center, dist, colors, MultipleGradientPaint.CycleMethod.NO_CYCLE);
        g2d.setPaint(p);
        g2d.fillOval(offsetX, offsetY, diameter, diameter);
    }

    private void paintLedOff(Graphics2D g2d, int diameter, int offsetX, int offsetY) {
        g2d.setColor(color.darker().darker());
        g2d.fillOval(offsetX, offsetY, diameter, diameter);
    }
}
