/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.viewers;

import HANtune.TextAndThresholdDialog;
import components.DynamicTooltipManager;
import datahandling.CurrentConfig;
import datahandling.Signal;
import datahandling.SignalListener;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.IllegalFormatException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingUtilities;

import util.Util;

/**
 *
 * @author Michiel Klifman
 */
@SuppressWarnings("serial")
public class TextViewer extends HANtuneViewer<TextViewer> implements SignalListener {

    private Signal signal;
    private JLabel textLabel;
    private double lastValue = 0;
    private final Map<Double, JLabel> thresholdAndLabelMap = new LinkedHashMap<>();

    public TextViewer() {
        super(false);
        defaultMinimumSize = new Dimension(70, 50);
        setPreferredSize(new Dimension(200, 80));
    }

    @Override
    protected TextViewer getListenerForSignal(Signal signal) {
        createTextViewer(signal);
        return this;
    }


    private void createTextViewer(Signal signal) {
        this.signal = signal;
        saveAndShowTitleLabel(getBooleanWindowSetting("showTitleLabel", false));
        if (!CurrentConfig.getInstance().isServiceToolMode()) {
            initializePopupMenu();
        }

        createLabel();
        loadSettings();
        if (!CurrentConfig.getInstance().isServiceToolMode()) {
            createMenu(signal);
        }

        DynamicTooltipManager.setTooltip(textLabel, getReferenceTooltipText(signal), this.getRootPane());
    }

    private void createLabel() {
        textLabel = createDefaultLabel();
        textLabel.setHorizontalAlignment(SwingUtilities.CENTER);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.weightx = 1;
        gridBagConstraints.weighty = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        getContentPane().add(textLabel, gridBagConstraints);
    }

    private JLabel createDefaultLabel() {
        String defaultTitle = "<html><b>TextViewer:</b> \"Right click to edit\"</html>";
        JLabel defaultLabel = new JLabel(defaultTitle);
        defaultLabel.setFont(new Font("Tahoma", 0, 12));
        return defaultLabel;
    }

    private void createMenu(Signal signal) {
        JMenuItem editTextandRangeItem = new JMenuItem("Edit text and ranges");
        editTextandRangeItem.addActionListener(e -> new TextAndThresholdDialog(this, signal).setVisible(true));
        addMenuItemToPopupMenu(editTextandRangeItem);

        JMenu editAlignmentMenu = new JMenu("Edit alignment");
        ButtonGroup group = new ButtonGroup();
        JRadioButtonMenuItem leftAlignmentMenuItem = new JRadioButtonMenuItem("Align left");
        leftAlignmentMenuItem.addActionListener(e -> setAlignment(SwingUtilities.LEFT));
        leftAlignmentMenuItem.setSelected(textLabel.getHorizontalAlignment() == SwingUtilities.LEFT);
        group.add(leftAlignmentMenuItem);
        editAlignmentMenu.add(leftAlignmentMenuItem);

        JRadioButtonMenuItem centerAlignmentMenuItem = new JRadioButtonMenuItem("Align center");
        centerAlignmentMenuItem.addActionListener(e -> setAlignment(SwingUtilities.CENTER));
        centerAlignmentMenuItem.setSelected(textLabel.getHorizontalAlignment() == SwingUtilities.CENTER);
        group.add(centerAlignmentMenuItem);
        editAlignmentMenu.add(centerAlignmentMenuItem);

        JRadioButtonMenuItem rightAlignmentMenuItem = new JRadioButtonMenuItem("Align right");
        rightAlignmentMenuItem.addActionListener(e -> setAlignment(SwingUtilities.RIGHT));
        rightAlignmentMenuItem.setSelected(textLabel.getHorizontalAlignment() == SwingUtilities.RIGHT);
        group.add(rightAlignmentMenuItem);
        editAlignmentMenu.add(rightAlignmentMenuItem);
        addMenuItemToPopupMenu(editAlignmentMenu);
        textLabel.setComponentPopupMenu(getMenu());
    }

    private void setAlignment(int alignment) {
        textLabel.setHorizontalAlignment(alignment);
        addSetting(signal.getName(), "Alignment", String.valueOf(alignment));
    }

    @Override
    public void loadSettings() {
        if (signal != null && settings.containsKey(signal.getName())) {
            thresholdAndLabelMap.clear();
            Map<String, String> signalSettings = settings.get(signal.getName());
            for (int i = 0; i < signalSettings.size(); i++) {
                if (signalSettings.containsKey("threshold" + i)) {
                    Double threshold = Double.parseDouble(signalSettings.get("threshold" + i));

                    JLabel label = new JLabel();
                    label.setText(signalSettings.get("text" + i));
                    label.setForeground(Color.decode(signalSettings.get("color" + i)));
                    label.setFont(Font.decode(signalSettings.get("font" + i)));

                    thresholdAndLabelMap.put(threshold, label);
                }
            }

            if (signalSettings.containsKey("Alignment")) {
                setAlignment(Integer.parseInt(signalSettings.get("Alignment")));
            }
        }
        refreshLabel();
    }

    @Override
    public void valueUpdate(double value, long timestamp) {
        lastValue = value;
        JLabel label = getLabelForValue(value);
        setLabel(label);
    }

    @Override
    public void stateUpdate() {

    }

    public void setLabel(JLabel label) {
        if (label != null) {
            String text = parseText(label.getText());
            textLabel.setText(text);
            textLabel.setForeground(label.getForeground());
            textLabel.setFont(label.getFont());
            repaint();
        }
    }

    private String parseText(String text) {
        String parsedText = text;
        try {
            if (!signal.hasDecimals() && lastValue <= Integer.MAX_VALUE) {
                parsedText = String.format(text, Util.getFormatArgumentArray((int) lastValue, text));
            } else {
                parsedText = String.format(text, Util.getFormatArgumentArray(lastValue, text));
            }
        } catch (IllegalFormatException exception) {
            //Tests user input, no need to log
        }
        return parsedText;
    }

    public void refreshLabel() {
        JLabel label = getLabelForValue(lastValue);
        setLabel(label != null ? label : createDefaultLabel());
        repaint();
    }

    private JLabel getLabelForValue(double value) {
        for (Map.Entry<Double, JLabel> entry : thresholdAndLabelMap.entrySet()) {
            if (value <= entry.getKey()) {
                return entry.getValue();
            }
        }
        return createDefaultLabel();
    }

    public double getLastValue() {
        return lastValue;
    }
}
