/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.viewers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.Map;
import javax.swing.ButtonGroup;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingUtilities;

import ErrorLogger.AppendToLogfile;
import HANtune.HANtune;
import components.JFontChooser;
import datahandling.CurrentConfig;
import datahandling.Signal;
import datahandling.SignalListener;
import HANtune.ReferenceWindow;
import nl.han.hantune.config.ApplicationProperties;
import util.MessagePane;
import util.Util;

import static nl.han.hantune.config.ConfigProperties.BOOLEANVIEWER_LABEL_POSITION;

/**
 * Displays the value of an asap2Data signal using a LED display
 *
 * @author Aart-Jan, Michiel Klifman
 */
@SuppressWarnings("serial")
public class BooleanViewer extends HANtuneViewer<BooleanViewer> implements SignalListener {

    private static final Dimension DEFAULT_SIZE = new Dimension(100, 100);
    private Signal signal;
    private Led led;
    private JLabel textLabel;

    /**
     * The constructor of the MultiLedViewer class
     */
    public BooleanViewer() {
        super(false);
        setTitle("HANtune - Signal");
        defaultMinimumSize = new Dimension(70, 80);
        getContentPane().setPreferredSize(DEFAULT_SIZE);
        setDefaultLabelPosition(ApplicationProperties.getTopBottomLabelPos(BOOLEANVIEWER_LABEL_POSITION).getLabelPositionString());

    }

    @Override
    protected BooleanViewer getListenerForSignal(Signal signal) {
        return createBooleanViewer(signal);
    }

    /**
     * @param signal
     * @return
     */
    private BooleanViewer createBooleanViewer(Signal signal) {
        this.signal = signal;
        GridBagConstraints gridBagConstraints;

        textLabel = new JLabel();
        textLabel.setHorizontalAlignment(SwingUtilities.CENTER);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.weighty = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        getContentPane().add(textLabel, gridBagConstraints);
        led = new Led();
        color = Color.RED;
        setLedColor();
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.weighty = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        getContentPane().add(led, gridBagConstraints);


        return this;
    }

    /**
     * This method load the HANtuneWindow specific settings for each asap2
     * reference. This method should be called only if settings have been
     * changed. The method automatically triggers a repaint after the settings
     * are loaded.
     *
     * The method loops through all asap2 references and will load the settings
     * for each asap2 reference.
     *
     * NOTE: Although the method loops through all asap2 references, this is not
     * yet supported. This HANtuneWindow allows for just a single asap2 reference
     * to be present. The developer will informed by a AssertException otherwise
     */
    @Override
    public void loadSettings() {
        if (signal != null && settings.containsKey(signal.getName())) {
            Map<String, String> signalSettings = settings.get(signal.getName());
            if (signalSettings.containsKey("color")) {
                //try and catch for backwards compatibility
                try {
                    color = Color.decode(signalSettings.get("color"));
                } catch (NumberFormatException e) {
                    AppendToLogfile.appendMessage("try and catch for backwards compatibility");
                    AppendToLogfile.appendError(Thread.currentThread(), e);
                    color = Util.getColor(signalSettings.get("color"));
                }
                led.setColor(color);
            }
            if (signalSettings.containsKey("limit")) {
                limit = Double.parseDouble(signalSettings.get("limit"));
            }
            if (signalSettings.containsKey("method")) {
                trigger = signalSettings.get("method");
            }
            if (signalSettings.containsKey("Text")) {
                textLabel.setText(signalSettings.get("Text"));
            }
            if (signalSettings.containsKey("TextColor")) {
                textLabel.setForeground(Color.decode(signalSettings.get("TextColor")));
            }
            if (signalSettings.containsKey("Font")) {
                textLabel.setFont(Font.decode(signalSettings.get("Font")));
            }
        }
        initializePopupMenu();

        if (CurrentConfig.getInstance().isServiceToolMode()) {
            setComponentPopupMenu(null);
        }
        else {
            addPopupMenuItems();
        }


        repaint();
    }

    private void addPopupMenuItems() {
        JMenuItem editColorMenuItem = new JMenuItem("Edit color");
        editColorMenuItem.addActionListener(e -> setColor());
        addMenuItemToPopupMenu(editColorMenuItem);

        JMenuItem modLimitMenuItem = new JMenuItem("Edit limit");
        modLimitMenuItem.addActionListener(e -> setLimit());
        addMenuItemToPopupMenu(modLimitMenuItem);

        JMenu modTriggerMenu = new JMenu("Modify trigger method");
        ButtonGroup modTriggerButtonGroup = new ButtonGroup();
        JRadioButtonMenuItem[] triggerRadioButtonMenuItem = new JRadioButtonMenuItem[2];

        triggerRadioButtonMenuItem[0] = new JRadioButtonMenuItem("Turn on above limit");
        triggerRadioButtonMenuItem[0].addActionListener(e -> setTrigger(ABOVE_TRIGGER));
        triggerRadioButtonMenuItem[0].setSelected(trigger.equals(ABOVE_TRIGGER));
        modTriggerButtonGroup.add(triggerRadioButtonMenuItem[0]);
        modTriggerMenu.add(triggerRadioButtonMenuItem[0]);

        triggerRadioButtonMenuItem[1] = new JRadioButtonMenuItem("Turn on below limit");
        triggerRadioButtonMenuItem[1].addActionListener(e -> setTrigger(BELOW_TRIGGER));
        triggerRadioButtonMenuItem[1].setSelected(trigger.equals(BELOW_TRIGGER));
        modTriggerButtonGroup.add(triggerRadioButtonMenuItem[1]);
        modTriggerMenu.add(triggerRadioButtonMenuItem[1]);
        addMenuItemToPopupMenu(modTriggerMenu);

        JMenuItem resetSizeMenuItem = new JMenuItem("Reset to default size");
        resetSizeMenuItem.addActionListener((ActionEvent e) -> {
            setSize(DEFAULT_SIZE);
            repaint();
        });
        addMenuItemToWindowMenu(resetSizeMenuItem);

        JMenu editTextMenu = new JMenu("Edit Text");

        JMenuItem editTextMenuItem = new JMenuItem("Edit Text");
        editTextMenuItem.addActionListener(e -> setText());
        editTextMenu.add(editTextMenuItem);

        JMenuItem editTextColorMenuItem = new JMenuItem("Edit Color");
        editTextColorMenuItem.addActionListener(e -> setTextColor());
        editTextMenu.add(editTextColorMenuItem);

        JMenuItem editFontMenuItem = new JMenuItem("Edit Font");
        editFontMenuItem.addActionListener(e -> setFont());
        editTextMenu.add(editFontMenuItem);

        addMenuItemToPopupMenu(editTextMenu);
    }

    /**
     * Performs the actions for the color menuitem
     */
    private void setColor() {
        Color newColor = JColorChooser.showDialog(this, "Choose color", led.getColor());
        if (!(newColor == null)) {
            color = newColor;
            led.setColor(color);
            addSetting(signal.getName(), "color", Integer.toString(newColor.hashCode()));
        }
        super.repaint();
    }

    /**
     * Performs the actions for the limit menuitem
     */
    private void setLimit() {
        double defaultLimit = 0;
        double currentLimit = defaultLimit;
        if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("limit")) {
            currentLimit = Double.parseDouble(settings.get(signal.getName()).get("limit"));
        }
        String limitString = HANtune.showQuestion("Modify limit", "Turn on " + trigger + " limit value (default=0):", "" + currentLimit);
        if (limitString != null) {
            try {
                limit = Double.parseDouble(limitString);
                if (limit != defaultLimit) {
                    addSetting(signal.getName(), "limit", "" + limit);
                } else {
                    removeSetting(signal.getName(), "limit");
                }
                updateLed(lastValue);
            } catch (Exception ex) {
                AppendToLogfile.appendMessage("Input was not a valid number");
                AppendToLogfile.appendError(Thread.currentThread(), ex);
                MessagePane.showError("Input was not a valid number");
            }
        }
    }

    /**
     * Performs the actions for the trigger method menuitem
     */
    private void setTrigger(String trigger) {
        addSetting(signal.getName(), "method", trigger);
        this.trigger = trigger;
        updateLed(lastValue);
    }

    @Override
    public void valueUpdate(double value, long timeStamp) {
        updateLed(value);
        lastValue = value;

    }

    @Override
    public void stateUpdate() {
        setLedColor();
    }

    private void setLedColor() {
        if (!signal.isActive() || inactiveReferences.contains(signal)) {
            led.setColor(ReferenceWindow.INACTIVE_COLOR);
        } else {
            led.setColor(color);
        }
        repaint();
    }


        private static final String ABOVE_TRIGGER = "above";
        private static final String BELOW_TRIGGER = "below";
        private double limit = 0;
        private String trigger = ABOVE_TRIGGER;
        private double lastValue = 0;
        private Color color;


        private void updateLed(double value) {
            led.setOn(shouldValueTrigger(value));
            repaint();
        }

    private boolean shouldValueTrigger(double value) {
        if (trigger.equals(ABOVE_TRIGGER)) {
            return value > limit;
        } else {
            return value < limit;
        }
    }


    private void setText() {
        String text = textLabel.getText().isEmpty() ? signal.getName() : Util.htmlToAscii(textLabel.getText());
        text = MessagePane.showTextAreaDialog("BooleanViewer - Edit text", text, textLabel.getFont());
        if (text != null) {
            text = Util.asciiToHtml(text);
            textLabel.setText(text);
            addSetting(signal.getName(), "Text", text);
        }
    }

    private void setTextColor() {
        Color newColor = JColorChooser.showDialog(this, "BooleanViewer - Choose color", textLabel.getForeground());
        if (newColor != null) {
            textLabel.setForeground(newColor);
            addSetting(signal.getName(), "TextColor", Integer.toString(newColor.hashCode()));
        }
    }

    private void setFont() {
        JFontChooser fontChooser = new JFontChooser();
        if (fontChooser.showDialog(null, Util.htmlToAscii(textLabel.getText()), textLabel.getFont()) == JFontChooser.OK_OPTION) {
            Font font = fontChooser.getSelectedFont();
            textLabel.setFont(font);
            String name = font.getName();
            String style = Util.getFontStyle(font.getStyle());
            String size = Integer.toString(font.getSize());
            addSetting(signal.getName(), "Font", name + " " + style + " " + size);
        }
    }
}
