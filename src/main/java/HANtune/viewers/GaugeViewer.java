/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.viewers;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.ListIterator;
import javax.swing.JMenuItem;

import ErrorLogger.AppendToLogfile;
import HANtune.DisplayrangeDialog;
import HANtune.SubrangeDialog;
import datahandling.CurrentConfig;
import datahandling.Signal;
import datahandling.SignalListener;
import nl.han.hantune.config.ApplicationProperties;
import util.Util;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.dial.DialBackground;
import org.jfree.chart.plot.dial.DialCap;
import org.jfree.chart.plot.dial.DialPlot;
import org.jfree.chart.plot.dial.DialPointer;
import org.jfree.chart.plot.dial.DialValueIndicator;
import org.jfree.chart.plot.dial.StandardDialFrame;
import org.jfree.chart.plot.dial.StandardDialRange;
import org.jfree.chart.plot.dial.StandardDialScale;
import org.jfree.data.general.DefaultValueDataset;
import util.MessagePane;

import static nl.han.hantune.config.ConfigProperties.GAUGEVIEWER_LABEL_POSITION;

/**
 * Displays the value of an asap2Data signal using a gauge display
 *
 * @author Aart-Jan, Michiel Klifman
 */
public class GaugeViewer extends HANtuneViewer<GaugeViewer> implements SignalListener {

    private static final long serialVersionUID = 7526471155622776179L;

    public static final int MINIMUMRANGES = 0;
    public static final int MAXIMUMRANGES = 4;
    private final List<StandardDialRange> subRange = new java.util.ArrayList<>();
    private NumberFormat format = NumberFormat.getInstance();
    public double default_lower = 0;
    public double default_upper = 100;
    public static final Color DEFAULTCOLOR = Color.red;
    private static final Dimension DEFAULT_SIZE = new Dimension(250, 250);
    private Signal signal;
    private DefaultValueDataset dataSet;
    private DefaultValueDataset displayDataSet;
    private ChartPanel chartPanel;
    private DialValueIndicator dialvalueindicator;
    private DialPlot plot;
    private int digitCount = 3; //0.0

    /**
     * The constructor of the MultiLedViewer class
     */
    public GaugeViewer() {
        super(false);
        defaultMinimumSize = new Dimension(150, 150);
        getContentPane().setPreferredSize(DEFAULT_SIZE);
        setTitle("HANtune - Signal");
        setDefaultLabelPosition(ApplicationProperties.getTopBottomLabelPos(GAUGEVIEWER_LABEL_POSITION).getLabelPositionString());
    }

    @Override
    protected GaugeViewer getListenerForSignal(Signal signal) {
        return createGaugeViewer(signal);
    }

    /**
     * @param signal
     * @return
     */
    private GaugeViewer createGaugeViewer(Signal signal) {
        this.signal = signal;
        dataSet = new DefaultValueDataset(signal.getValue());
        displayDataSet = new DefaultValueDataset(signal.getValue());

        // dial
        plot = new DialPlot();
        plot.setDataset(0, dataSet);
        plot.setDataset(1, displayDataSet);
        final JFreeChart chart = new JFreeChart(null, JFreeChart.DEFAULT_TITLE_FONT, plot, false);
        chart.setBackgroundPaint(new Color(0,0,0,0));
        plot.setDialFrame(new StandardDialFrame());
        plot.setBackground(new DialBackground());

        // indicator settings
        dialvalueindicator = new DialValueIndicator(0);
        dialvalueindicator.setTemplateValue(0);
        dialvalueindicator.setOutlinePaint(plot.getBackgroundPaint());
        dialvalueindicator.setRadius(0.65D);

        format = dialvalueindicator.getNumberFormat();
        format.setMinimumFractionDigits(signal.getDecimalCount());
        format.setMaximumFractionDigits(signal.getDecimalCount());
        plot.addLayer(dialvalueindicator);

        // value pointer
        plot.addPointer(new DialPointer.Pointer(1));
        ((DialPointer.Pointer) plot.getPointerForDataset(1)).setFillPaint(DEFAULTCOLOR);

        // cap of dial pointer
        DialCap dialcap = new DialCap();
        plot.setCap(dialcap);

        if (signal.getMinimum() >= Integer.MIN_VALUE && signal.getMaximum() <= Integer.MAX_VALUE) {
            default_lower = signal.getMinimum();
            default_upper = signal.getMaximum();
        } else {
            default_lower = Integer.MIN_VALUE;
            default_upper = Integer.MAX_VALUE;
        }

        StandardDialScale standarddialscale = new StandardDialScale();
        standarddialscale.setLowerBound(default_lower);
        standarddialscale.setUpperBound(default_upper);
        standarddialscale.setStartAngle(-140D);
        standarddialscale.setExtent(-260D);
        standarddialscale.setMajorTickIncrement((standarddialscale.getUpperBound()-standarddialscale.getLowerBound())/10);
        standarddialscale.setMinorTickCount(5);
        standarddialscale.setTickRadius(0.88D);
        standarddialscale.setTickLabelOffset(0.15D);
        standarddialscale.setTickLabelFont(new Font("Tahoma", 0, 11));
        standarddialscale.setTickLabelPaint(Color.black);
        standarddialscale.setTickLabelFormatter(new DecimalFormat("0"));
        plot.addScale(0, standarddialscale);

        // add dial to window
        chartPanel = new ChartPanel(chart);
        chartPanel.setMinimumDrawWidth(0);
        chartPanel.setMinimumDrawHeight(0);
        chartPanel.setMaximumDrawWidth(Integer.MAX_VALUE);
        chartPanel.setMaximumDrawHeight(Integer.MAX_VALUE);
        chartPanel.setOpaque(false);
        chartPanel.setBackground(new Color(0,0,0,0));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.weightx = 1;
        gridBagConstraints.weighty = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        add(chartPanel, gridBagConstraints);

        if (CurrentConfig.getInstance().isServiceToolMode()) {
            chartPanel.setPopupMenu(null);
        }
        else {
            addPopupMenuItems(signal);
            chartPanel.setPopupMenu(getMenu());
        }

        return this;
    }

    private void addPopupMenuItems(Signal signal) {
        //Menu item that calls DisplayrangeDialog, copy and edit this for viewer that use the dialog
        JMenuItem modDisplayrangeMenuItem = new JMenuItem("Modify display range");
        modDisplayrangeMenuItem.addActionListener(e -> new DisplayrangeDialog(this, signal));
        addMenuItemToPopupMenu(modDisplayrangeMenuItem);

        // Menu item that calls SubrangeDialog, copy and edit this for viewers that use the dialog
        JMenuItem modSubrangesMenuItem = new JMenuItem("Modify subranges");
        modSubrangesMenuItem.addActionListener(e -> {
            SubrangeDialog dialog = new SubrangeDialog(this, signal, MINIMUMRANGES, MAXIMUMRANGES, false);
            dialog.setVisible(true);
        });
        addMenuItemToPopupMenu(modSubrangesMenuItem);

        // Menu item that calls SubrangeDialog, copy and edit this for viewers that use the dialog
        JMenuItem modDecimalsMenuItem = new JMenuItem("Modify decimals");
        modDecimalsMenuItem.addActionListener((ActionEvent e) -> {
            actionDecimals(signal.getName());
            loadSettings();
        });
        addMenuItemToPopupMenu(modDecimalsMenuItem);


        JMenuItem resetSizeMenuItem = new JMenuItem("Reset to default size");
        resetSizeMenuItem.addActionListener((ActionEvent e) -> {
            Dimension defaultWindowSize = (new Dimension(((getSize().width - getContentPane().getSize().width) + DEFAULT_SIZE.width + 9), ((getSize().height - getContentPane().getSize().height) + DEFAULT_SIZE.height + 9)));
            setSize(defaultWindowSize);
            storeSize();
            repaint();
        });
        addMenuItemToWindowMenu(resetSizeMenuItem);
    }


    /**
     * This method load the HANtuneWindow specific settings for each asap2
     * reference. This method should be called only if settings have been
     * changed. The method automatically triggers a repaint after the settings
     * are loaded.
     *
     * The method loops through all asap2 references and will load the settings
     * for each asap2 reference.
     *
     * NOTE: Although the method loops through all asap2 references, this is not
     * yet supported. This HANtuneWindow allows for just a single asap2
     * reference to be present. The developer will informed by a AssertException
     * otherwise
     */
    @Override
    public void loadSettings() {
        if (signal != null) {


            // loadSettings for decimals
            if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("decimals")) {
                format.setMinimumFractionDigits(Integer.parseInt(settings.get(signal.getName()).get("decimals")));
                format.setMaximumFractionDigits(format.getMinimumFractionDigits());
            }

            // loadSettings for displayRange, copy and edit this for viewers that use DisplayrangeDialog
            if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("limit_lower")) {
                ((StandardDialScale) plot.getScale(0)).setLowerBound(Double.parseDouble(settings.get(signal.getName()).get("limit_lower")));
            }
            if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("limit_upper")) {
                ((StandardDialScale) plot.getScale(0)).setUpperBound(Double.parseDouble(settings.get(signal.getName()).get("limit_upper")));
            }
            if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("color")) {
                //try and catch for backwards compatibility
                try {
                    ((DialPointer.Pointer) plot.getPointerForDataset(1)).setFillPaint(Color.decode(settings.get(signal.getName()).get("color")));
                } catch (NumberFormatException e) {
                    AppendToLogfile.appendError(Thread.currentThread(), e);
                    ((DialPointer.Pointer) plot.getPointerForDataset(0)).setFillPaint(Util.getColor(settings.get(signal.getName()).get("color")));
                }
            }
            try {
                double upperBound = ((StandardDialScale) plot.getScale(0)).getUpperBound();
                double lowerBound = ((StandardDialScale) plot.getScale(0)).getLowerBound();
                ((StandardDialScale) plot.getScale(0)).setMajorTickIncrement((upperBound - lowerBound) / 10);
            } catch (Exception e) {
                AppendToLogfile.appendMessage("Upper limit must be higher than lower limit");
                AppendToLogfile.appendError(Thread.currentThread(), e);
                MessagePane.showError("Upper limit must be higher than lower limit.");
                DisplayrangeDialog displayrangeDialog = new DisplayrangeDialog(GaugeViewer.this, signal);
                displayrangeDialog.setVisible(true);
            }
            // read settings for subranges, copy and edit this into viewers that use the SubrangeDialog
            java.util.List<Color> colors = new java.util.ArrayList<>();
            java.util.List<Double> limits = new java.util.ArrayList<>();
            for (int loopcount = 0; loopcount <= MAXIMUMRANGES; loopcount++) {
                // get colors settings
                if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("color" + loopcount)) {
                    colors.add(loopcount, Color.decode(settings.get(signal.getName()).get("color" + loopcount)));
                }
                // if limit0 is not present in settings, get limits setting for limit0
                if (!(settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("limit0"))) {
                    if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("limit")) {
                        limits.add(loopcount, Double.parseDouble(settings.get(signal.getName()).get("limit")));
                    }

                }
                // get limits settings
                if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("limit" + loopcount) && loopcount < MAXIMUMRANGES) {
                    limits.add(loopcount, Double.parseDouble(settings.get(signal.getName()).get("limit" + loopcount)));
                } // break if no more limits or colors are found
                else {
                    break;
                }
            }
            // END

            // update subranges

            // remove old subranges
            for (StandardDialRange s : subRange) {
                if (plot.getLayerIndex(s) != -1) {
                    plot.removeLayer(s);
                }
            }

            double dialLowerBound = ((StandardDialScale) plot.getScale(0)).getLowerBound();
            double dialUpperBound = ((StandardDialScale) plot.getScale(0)).getUpperBound();

            try {

                subRange.clear();
                //Add subranges for each color
                if (limits.size() > 0) {
                    for (int loopcount = 0; loopcount < colors.size(); loopcount++) {
                        subRange.add(loopcount, new StandardDialRange());
                        subRange.get(loopcount).setPaint(colors.get(loopcount));
                        if (loopcount == 0) {
                            subRange.get(loopcount).setBounds(-1E308, limits.get(loopcount));
                        } else if (loopcount == (colors.size() - 1)) {
                            subRange.get(loopcount).setBounds(limits.get((loopcount - 1)), 1E308);
                        } else {
                            subRange.get(loopcount).setBounds(limits.get((loopcount - 1)), limits.get(loopcount));
                        }
                        subRange.get(loopcount).setInnerRadius(0.52D);
                        subRange.get(loopcount).setOuterRadius(0.55D);
                    }

                   }
                    //add layers for each subRange
                    for (ListIterator<StandardDialRange> iter = subRange.listIterator(); iter.hasNext();) {
                        StandardDialRange s = iter.next();

                        if (s.getLowerBound() > dialLowerBound && s.getUpperBound() < dialUpperBound) {
                            plot.addLayer(s);
                        }else if(s.getLowerBound() < dialLowerBound && s.getUpperBound() > dialUpperBound){
                            s.setBounds(dialLowerBound, dialUpperBound);
                            plot.addLayer(s);
                        }else if(s.getLowerBound() <= dialLowerBound && (s.getUpperBound() > dialLowerBound && s.getUpperBound() < dialUpperBound)){
                            s.setLowerBound(dialLowerBound);
                            plot.addLayer(s);
                        }else if((s.getLowerBound() >= dialLowerBound && s.getLowerBound() < dialUpperBound) && s.getUpperBound() >= dialUpperBound){
                            s.setUpperBound(dialUpperBound);
                            plot.addLayer(s);
                        }else {
                            iter.remove();
                        }
                    }
            } catch (Exception e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
                System.out.println(e + " in GaugeViewer loadsettings() 2");
            }

            // below is for backwards compatibility with projects made in versions below 0.7,
            //  this is only used when no subrange settings have been found

            if (colors.isEmpty() && limits.isEmpty()) {
                boolean oldRangesUsed = false;
                if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("range_normal")) {
                    String[] parts = settings.get(signal.getName()).get("range_normal").split(";");
                    if (parts.length == 3) {
                        settings.get(signal.getName()).put("limit0", parts[1]);
                        settings.get(signal.getName()).put("color0", Integer.toString(Util.getColor(parts[2]).hashCode()));
                        oldRangesUsed = true;
                    }
                }

                if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("range_warning")) {
                    String[] parts = settings.get(signal.getName()).get("range_warning").split(";");
                    if (parts.length == 3) {
                        settings.get(signal.getName()).put("limit1", parts[1]);
                        settings.get(signal.getName()).put("color1", Integer.toString(Util.getColor(parts[2]).hashCode()));
                        oldRangesUsed = true;
                    }
                }

                if (settings.containsKey(signal.getName()) && settings.get(signal.getName()).containsKey("range_critical")) {
                    String[] parts = settings.get(signal.getName()).get("range_critical").split(";");
                    if (parts.length == 3) {
                        settings.get(signal.getName()).put("color2", Integer.toString(Util.getColor(parts[2]).hashCode()));
                        oldRangesUsed = true;
                    }
                }
                if (oldRangesUsed) {
                    loadSettings();
                }
            }

        }
        repaint();
    }

    /**
     * method to scale the gaugeviewer
     */
    private void scaleGaugeViewer() {
        // set label size
        if (signal != null) {
            // first determine the size of the chart rendering area...
            Dimension size = chartPanel.getSize();
            Insets insets = chartPanel.getInsets();
            Rectangle2D available = new Rectangle2D.Double(insets.left, insets.top,
                    size.getWidth() - insets.left - insets.right,
                    size.getHeight() - insets.top - insets.bottom);

            // work out real width
            double drawWidth = available.getWidth();
            double drawHeight = available.getHeight();

            // apply view scaling so that the gauge will stay circular
            double horizontalScale = 1;
            double vertialScale = 1;

            if (drawWidth > drawHeight) {
                horizontalScale = drawWidth / drawHeight;
            } else {
                vertialScale = drawHeight / drawWidth;
            }
            plot.setView((1 - horizontalScale) / 2, (1 - vertialScale) / 2, horizontalScale, vertialScale);
        }
    }

    @Override
    public void validate() {
        super.validate();
        scaleGaugeViewer();
    }

    @Override
    public void valueUpdate(double value, long timeStamp) {
        if ((double) dataSet.getValue() != value) {
            if (digitCountChanged(value)) {
                dialvalueindicator.setTemplateValue(value);
            }
            dataSet.setValue(value);
            double dialLowerBound = ((StandardDialScale) plot.getScale(0)).getLowerBound();
            double dialUpperBound = ((StandardDialScale) plot.getScale(0)).getUpperBound();
            if (value < dialLowerBound) {
                dialvalueindicator.setPaint(Color.RED);
                displayDataSet.setValue(dialLowerBound);
            } else if (value > dialUpperBound) {
                dialvalueindicator.setPaint(Color.RED);
                displayDataSet.setValue(dialUpperBound);
            } else {
                dialvalueindicator.setPaint(Color.BLACK);
                displayDataSet.setValue(value);
            }
        }
    }

    private boolean digitCountChanged(double value) {
        int newDigitCount = String.valueOf(value).length();
        if (newDigitCount != digitCount) {
            digitCount = newDigitCount;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void stateUpdate() {

    }
}
