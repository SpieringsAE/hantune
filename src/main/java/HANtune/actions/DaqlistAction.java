/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.actions;

import javax.swing.tree.DefaultMutableTreeNode;

import HANtune.HANtune;
import HANtune.ProjectInfo;
import util.MessagePane;

public class DaqlistAction {

    public static final String UNLOAD_DAQ_LIST = "Unload DAQ list";
    public static final String LOAD_DAQ_LIST = "Load DAQ list";

    private HANtune hanTune;

    public DaqlistAction(HANtune hantune) {
        this.hanTune = hantune;
    }

    public void daqlistToggleLoad(DefaultMutableTreeNode node) {
        if (node == null || isNoAsap2FileLoaded()) {
            return;
        }

        int id = ((ProjectInfo) node).getId();

        // toggle: load or UN load
        if (hanTune.daqLists.get(id).isActive()) {
            hanTune.currentConfig.getHANtuneManager().unloadDaqlist(id, true, true);
        } else {
            hanTune.currentConfig.getHANtuneManager().loadDaqlist(id, true);
        }
        hanTune.updateDaqListListeners();
        hanTune.updateProjectTree();
    }

    public void daqlistRename(DefaultMutableTreeNode node) {
        if (node == null || isNoAsap2FileLoaded()) {
            return;
        }

        // the default DAQ list?
        int daqId = ((ProjectInfo) node).getId();
        if (daqId == 0) {
            MessagePane.showError("Cannot rename the Default DAQ List");
            return;
        }

        String oldName = hanTune.currentConfig.getHANtuneDocument().getHANtune().getDaqlistArray(daqId).getTitle();
        String newName;

        boolean loop = true;
        while (loop) {
            if ((newName = HANtune.showQuestion("Rename DAQ list", "Name:", oldName)) == null) {
                loop = false; // cancel key pressed
            } else {
                loop = isLoop(daqId, oldName, newName);
            }
        }
    }

    private boolean isLoop(int daqId, String oldName, String newName) {
        boolean rtn = true;
        if (newName.equals("")) {
            MessagePane.showError("Please enter a valid name");
        } else if (oldName.equals(newName)) {
            rtn = false; // same name
        } else {
            boolean foundDuplicate = false;
            for (DAQList.DAQList DAQList : hanTune.daqLists) {
                if (DAQList.getName().equals(newName)) {
                    foundDuplicate = true;
                    break;
                }
            }
            if (foundDuplicate) {
                MessagePane.showError("Another DAQlist with name '" + newName + "' already exists.\n"
                        + "Please enter another name");
            } else {
                // perform rename action
                hanTune.currentConfig.getHANtuneManager().renameDaqlist(daqId, newName);
                hanTune.updateDaqListListeners();
                hanTune.updateProjectTree();

                rtn = false; // exit while loop
            }
        }
        return rtn;
    }

    /**
     * Creates a new DAQ list to be shown in the project tree
     */
    public void daqlistCreate() {
        if (isNoAsap2FileLoaded()) {
            return;
        }

        // show popup to enter application
        String name = "";
        boolean validName = false;
        while (!validName) {
            name = HANtune.showQuestion("New DAQ list - Enter name", "Enter the name of the DAQ list:", "");
            if (name == null) {
                break;
            }
            validName = true;
            if (name.equals("")) {
                MessagePane.showError("Please enter a valid name");
                validName = false;
            }
            for (DAQList.DAQList DAQList : hanTune.daqLists) {
                if (DAQList.getName().equals(name)) {
                    MessagePane
                            .showError("A DAQlist with this name already exists.\nPlease enter another name");
                    validName = false;
                }
            }
        }

        // perform add action
        if (name != null) {
            hanTune.currentConfig.getHANtuneManager().newDaqlist(name, true);
        }
    }

    public void daqlistCopy(DefaultMutableTreeNode node) {
        if (node == null || isNoAsap2FileLoaded()) {
            return;
        }
        // confirmation dialog
        if (HANtune.showConfirm("Are you sure you want to copy \"" + ((ProjectInfo) node).getName()
                + "\"") != 0) {
            return;
        }

        // perform copy action
        hanTune.currentConfig.getHANtuneManager().copyDaqlist(((ProjectInfo) node).getId());

        hanTune.updateProjectTree();
    }

    public void daqlistRemove(DefaultMutableTreeNode node) {
        if (node == null || isNoAsap2FileLoaded()) {
            return;
        }
        // the default DAQ list?
        if (((ProjectInfo) node).getId() == 0) {
            MessagePane.showError("Cannot remove the Default DAQ List");
            return;
        }

        // confirmation dialog
        if (HANtune.showConfirm("Are you sure you want to remove \"" + ((ProjectInfo) node).getName()
                + "\" from this project?") != 0) {
            return;
        }

        // perform remove action
        hanTune.currentConfig.getHANtuneManager().removeDaqlist(((ProjectInfo) node).getId());
    }

    public void daqlistModify(DefaultMutableTreeNode node) {
        if (node == null || isNoAsap2FileLoaded()) {
            return;
        }
        // perform modify action
        hanTune.currentConfig.getHANtuneManager().modifyDaqlist(((ProjectInfo) node).getId());
        hanTune.updateDaqListListeners();
    }

    private boolean isNoAsap2FileLoaded() {
        if (hanTune.getActiveAsap2Id() == -1) {
            MessagePane.showInfo("Please open an ASAP2 file first!");
            return true;
        }
        return false;
    }
}
