/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.actions;

import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import HANtune.ProjectInfo;
import nl.han.hantune.scripting.Script;

/**
 * Renders the project tree
 *
 * @author Unkown, Michiel Klifman
 */
public class ProjectTreeRenderer extends DefaultTreeCellRenderer {

    private static final long serialVersionUID = 1L;

    /**
     * Renders the component
     * @param tree
     * @param value
     * @param sel
     * @param expanded
     * @param row
     * @param leaf
     * @param hasFocus
     * @return
     */
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

        // create correct node object
        if(!(value instanceof DefaultMutableTreeNode)){
            return this;
        }
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;

        // create correct project object of node
        if(!(node instanceof ProjectInfo)){
            return this;
        }
        ProjectInfo.Type type = ((ProjectInfo) node).getType();

        // default icon
        ImageIcon icon_folder = createImageIcon("/images/tree/FOLDER.png");
        if (icon_folder != null) {
            setIcon(icon_folder);
            setFont(new java.awt.Font("Tahoma", 0, 11));
        }

        // Info specific details
        if (type.equals(ProjectInfo.Type.Info)) {
            setIcon(null);
            setForeground(new Color(160, 160, 160));
        }

        // Root specific details
        if (type.equals(ProjectInfo.Type.Root)) {
            ImageIcon icon_project = createImageIcon("/images/tree/PROJECT.png");
            if (icon_project != null) {
                setIcon(icon_project);
            }
        }

        // Daqlist specific details
        if (type.equals(ProjectInfo.Type.Daqlist)) {
            ImageIcon icon_daq = createImageIcon("/images/tree/DAQLIST.png");
            if (icon_daq != null) {
                setIcon(icon_daq);
            }
            setFont(new java.awt.Font("Tahoma", 0, 11));
        }

        // Layout specific details
        if (type.equals(ProjectInfo.Type.Layout)) {
            ImageIcon icon_layout = createImageIcon("/images/tree/LAYOUT.png");
            if (icon_layout != null) {
                setIcon(icon_layout);
            }
            setFont(new java.awt.Font("Tahoma", 0, 11));
        }

        // Calibration specific details
        if (type.equals(ProjectInfo.Type.Calibration)) {
            ImageIcon icon_calibration = createImageIcon("/images/tree/CALIBRATION.png");
            if (icon_calibration != null) {
                setIcon(icon_calibration);
            }
            setFont(new java.awt.Font("Tahoma", 0, 11));
        }

        // ASAP2 file specific details
        if (type.equals(ProjectInfo.Type.Asap2File)) {
            ImageIcon icon_asap2 = createImageIcon("/images/tree/ASAP2.png");
            if (icon_asap2 != null) {
                setIcon(icon_asap2);
            }
            setFont(new java.awt.Font("Tahoma", 0, 11));
        }

        if (type.equals(ProjectInfo.Type.DbcFile)) {
            ImageIcon dbcIcon = createImageIcon("/images/sidepanel/dbc.png");
            if (dbcIcon != null) {
                setIcon(dbcIcon);
            }
            setFont(new java.awt.Font("Tahoma", 0, 11));
        }

        if (type.equals(ProjectInfo.Type.Script)) {
            Script script = (Script) node.getUserObject();
            ImageIcon scriptIcon = script.isRunning()
                    ? createImageIcon("/images/tree/SCRIPTv4Runningv5.png")
                    : createImageIcon("/images/tree/SCRIPTv4.png");
            if (scriptIcon != null) {
                setIcon(scriptIcon);
            }
            setFont(new java.awt.Font("Tahoma", 0, 11));
        }

        // active item?
        if (((ProjectInfo) node).isActive()) {
            setFont(new java.awt.Font("Tahoma", 1, 11));
        }

        // return default node component
        return this;
    }

    /**
     * Creates icon from specified file
     * @param path
     * @return
     */
    protected ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            return null;
        }
    }
}
