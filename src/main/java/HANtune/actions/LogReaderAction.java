/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.actions;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import ASAP2.ASAP2Data;
import ASAP2.ASAP2Measurement;
import HANtune.CustomFileChooser;
import HANtune.HANtune;
import can.CanDataProcessor;
import can.DbcManager;
import datahandling.CurrentConfig;
import nl.han.hantune.datahandling.DataLogger.LogDataReaderCsv;
import nl.han.hantune.datahandling.DataLogger.LogDataReaderCsv.ReaderResult;
import nl.han.hantune.datahandling.DataLogger.LogDataReaderCsv.ResultItem;
import nl.han.hantune.datahandling.DataLogger.LogDataWriterCsv;
import nl.han.hantune.gui.dialogs.LogPlayerDialog;
import util.CursorWait;
import util.MessagePane;

import static java.awt.event.InputEvent.SHIFT_DOWN_MASK;

@SuppressWarnings("serial")
public class LogReaderAction extends LogPlayerDialog {
    private static final long MILLIS_IN_SEC = 1000L;
    private static final long MIN_DELAY_MILLIS = 4L; // limit max read speed per line at 250 Hz
    private static final String LOG_TIME_FORMAT = "#0.000";
    private static final int DEFAULT_ITEMS_PER_PAGE = 100;
    private static final int MIN_FEED_ITEMS = 3;
    private static final int DEFAULT_SPEED_FACTOR = 1;
    private static final int INIT_FAST_SPEED_FACTOR = 5;
    private static final int SEC_TO_MICROS = 1000000;
    private static final double REFRESH_DURATION_SEC = 1.0d;
    private static final DecimalFormat df = new DecimalFormat(LOG_TIME_FORMAT);

    private static final LogReaderAction instance = new LogReaderAction();
    private boolean onScreenActive = false;  // dialog is on screen
    private boolean readingActive = false; // true: dialog is currently reading data from file.
    // False: any actions to read from file will not be executed.
    private boolean readWhileWriteMode = false;
    private CurrentConfig currentConfig;

    private File logFile;
    private LogDataReaderCsv reader;
    private boolean isInitComplete;

    private Timer timer = new Timer(true);

    private static final boolean PLAYING_ON = true;
    private static final boolean PLAYING_OFF = false;
    private boolean isPlaying = PLAYING_ON;

    private static final boolean PLAY_DIRECTION_FORWARD = true;
    private static final boolean PLAY_DIRECTION_BACKWARD = false;
    private boolean isPlayingForward = PLAY_DIRECTION_FORWARD;

    private static final boolean PLAYING_LIVE_ON = true;
    private static final boolean PLAYING_LIVE_OFF = false;
    private boolean isPlayingLive = PLAYING_LIVE_ON;

    private int speedFactor = DEFAULT_SPEED_FACTOR;
    private int pageSize = DEFAULT_ITEMS_PER_PAGE;
    private double refreshTime = 0.0d;
    private Point screenLocation = null;


    private LogReaderAction() {
        super();

        this.setModal(false);
        this.setAlwaysOnTop(false);

        currentConfig = CurrentConfig.getInstance();
        df.setRoundingMode(RoundingMode.HALF_DOWN);
    }


    public static LogReaderAction getInstance() {
        return instance;
    }


    /**
     * Request for filename and execute logfile player
     */
    public void openReaderActionDialog() {
        if ((logFile = chooseReadFile()) == null) {
            return;
        }

        // check if opening same file as currently writing
        if (LogDataWriterCsv.getInstance().isWritingActive() &&
            LogDataWriterCsv.getInstance().getFullFileName().equalsIgnoreCase(logFile.getPath())) {
            openReadWhileWriteActionDialog();
            return;
        }

        startOpenActionDialog();

        if (readWhileWriteMode) {
            dispose();
            readWhileWriteMode = false;
        }

        completeOpenActionDialog();
    }


    public void openReadWhileWriteActionDialog() {
        startOpenActionDialog();

        if (!reader.initReadWhileWrite()) {
            handleReaderException();
            return;
        }
        logFile = reader.getFile();

        readWhileWriteMode = true;

        completeOpenActionDialog();
    }


    private void startOpenActionDialog() {
        onScreenActive = false;
        isInitComplete = false;
        readingActive = false;
        refreshTime = 0.0d;
        reader = LogDataReaderCsv.getInstance();
    }


    private void completeOpenActionDialog() {
        if (!isVisible()) {
            if (screenLocation == null) {
                setLocationRelativeTo(HANtune.getInstance());
            } else {
                setLocation(screenLocation);
            }
        }


        updateDialogAllComponents();
        CursorWait.startWaitCursor(HANtune.getInstance().getRootPane());

        if (!isVisible()) {
            setVisible(true);
            if (readWhileWriteMode) {
                handleInitComplete(); // necessary to set focus to dialog when starting from 'Connect'
            }
        } else {
            requestFocus();
        }
    }


    private void setDialogTitle(String name) {
        if (!onScreenActive) {
            setTitle("Initializing " + name + ". Please wait...");
        } else {
            if (readWhileWriteMode) {
                setTitle("Read-while-write log: " + name);
            } else {
                setTitle("Read log: " + name);
            }
        }
    }

    private File chooseReadFile() {
        CustomFileChooser.FileType fType = CustomFileChooser.FileType.LOG_FILE;
        CustomFileChooser fileChooser = new CustomFileChooser("Open log file for reading", fType);
        return fileChooser.chooseOpenDialog(HANtune.getInstance(), "Select");
    }


    @Override
    public boolean isActive() {
        return onScreenActive;
    }

    public boolean isReadWhileWriteMode() {
        return readWhileWriteMode;
    }

    public void disableReadWhileWrite() {
        if (readWhileWriteMode) {
            readWhileWriteMode = false;
            stopPlaying();
            updateDialogAllComponents();
        }
    }


    /**
     * Disable all reading from csv file, but keep dialog on screen, readWhileWrite unchanged
     */
    public void disableRead() {
        stopPlaying();
        readingActive = false;
    }


    public boolean reEnableReadWrite() {
        openReadWhileWriteActionDialog();
        return true;
    }

    @Override
    public void dispose() {
        if (reader != null) {
            reader.stopReader(!readWhileWriteMode);
        }
        if (readWhileWriteMode) {
            readWhileWriteMode = false;
            LogDataWriterCsv.getInstance().setReadWhileWriteStandby(false);
        }
        onScreenActive = false;
        readingActive = false;
        stopPlaying();
        screenLocation = getLocation();

        super.dispose();

        //todo can this be safely removed, outside this class?
        HANtune.getInstance().updateDatalogGuiComponents();
    }


    @Override
    protected boolean handleInitComplete() {
        if (isInitComplete) {
            return true;
        }
        isInitComplete = true;

        CursorWait.stopWaitCursor(HANtune.getInstance().getRootPane());

        if (!readWhileWriteMode) {
            reader = LogDataReaderCsv.getInstance();
            if (!reader.initRead(logFile)) {
                handleReaderException();
                return false;
            }

            if (!isContinueLogIdsOk(logFile.getName())) {
                dispose();
                return false;
            }
        }

        onScreenActive = true;
        readingActive = true;

        initDialogAllComponents();
        if (readWhileWriteMode) {
            startPlaying(DEFAULT_SPEED_FACTOR, PLAY_DIRECTION_FORWARD, PLAYING_LIVE_ON);
            activateEndBtn();
        } else {
            handleGotoStartLogging();
        }
        activateSliderComponent();

        return true;
    }

    private void updateDialogAllComponents() {
        updateDialogTotals();
        updateDialogSlider(true, true);
    }


    private void initDialogAllComponents() {
        setDialogTitle(logFile.getName());
        setDialogEndButtonIcon(readWhileWriteMode);
        initDialogTotals();
        updateDialogSlider(true, true);
    }


    public void updateDialogReadWhileWriteComponents() {
        if (!onScreenActive || !readWhileWriteMode) {
            return;
        }
        if (isPlaying && isPlayingLive) {
            handleReadingLiveItem();
            updateDialogSlider(false, true);
        }

        // always update totals, but only 1x per REFRESH_DURATION_SEC. Slider: only update position, not the text
        double curTime = reader.getMaximumLogTimeSec();
        if (curTime > refreshTime) {
            updateDialogTotals();
            updateDialogSlider(true, false);
            refreshTime = curTime + REFRESH_DURATION_SEC;
        }
    }


    private void updateDialogTotals() {
        if (!onScreenActive) {
            return;
        }
        updateTotalTimeLabel("Total time: " + df.format(reader.getMaximumLogTimeSec()));
        updateTotalLinesLabel("Total lines: " + reader.getTotalDataLines());
    }


    private void initDialogTotals() {
        if (!onScreenActive) {
            return;
        }
        String hdrTxt = LogDataWriterCsv.getInstance().createHeaderInfoTextHtml(reader.getLogHeaderInfo());
        initTotalTimeLabel("Total time: " + df.format(reader.getMaximumLogTimeSec()), hdrTxt);
        initTotalLinesLabel("Total lines: " + reader.getTotalDataLines(), hdrTxt);
    }


    private void updateDialogSlider(boolean moveSlider, boolean updateText) {
        if (!readingActive) {
            return;
        }
        if (moveSlider) {
            int totalLines = reader.getTotalDataLines();
            if (totalLines > 0) {
                setSliderPosition((int) (((long) (reader.getCurrentDataLineNum() + 1) * MAX_SLIDER_VALUE) /
                    (long) totalLines));
            } else {
                setSliderPosition(0);
            }
        }
        if (updateText) {
            updateSliderSlaveComponents(df.format(reader.getCurrentLogTimeSec()));
            if (isPlayingLive) {
                updateLiveRotator();
            }
        } else {
            updateSliderSlaveComponents(null);
        }
    }


    private boolean isContinueLogIdsOk(String fName) {
        String err = "";
        if (reader.getLogHeaderInfo().getEcuId() != null) {
            if (currentConfig.getASAP2Data() == null || currentConfig.getASAP2Data().getEpromID() == null) {
                err += " - No ASAP2 file active. Cannot display ASAP2 signals.\n";
            } else if (!reader.getLogHeaderInfo().getEcuId().equals(currentConfig.getASAP2Data().getEpromID())) {
                err += " - Current ASAP2 application ID does not match the application ID from this log file:\n"
                    + "        ASAP2 ID: " + currentConfig.getASAP2Data().getEpromID() + "\n"
                    + "        Log ID:   " + reader.getLogHeaderInfo().getEcuId() + "\n"
                    + "    Not all values might be displayed properly while reading this log.\n";
            }
        }
        if (!reader.getLogHeaderInfo().getDbcInfos().isEmpty()) {
            if (DbcManager.getDbcList().isEmpty()) {
                err += " - No DBC file active. Cannot display DBC messages.\n";
            } else {
                for (String info : reader.getLogHeaderInfo().getDbcInfos()) {
                    err = addWarning(err, info);
                }
            }
        }
        if (!err.isEmpty()) {
            return MessagePane.showConfirmDialog(HANtune.getInstance(), "Warning: \n" + err + "\nContinue anyway?",
                "Continue Read Log " + fName + "?") == JOptionPane.OK_OPTION;
        }
        return true;
    }


    private String addWarning(String err, String info) {
        if (DbcManager.getDbcByName(info) == null) {
            err += " - DBC from this log file does not match any active DBC file.\n"
                + "   Not all CAN values might be displayed properly while reading this log.\n"
                + "   Log DBC name: " + info + "\n";
        }
        return err;
    }


    /**
     * @param val contains new position of slider (0..MAX_SLIDER_VALUE)
     */
    @Override
    protected void handleSliderMoved(int val) {
        if (!readingActive) {
            return;
        }
        stopPlaying();

        // goto a position 1 page BEFORE val
        int lines = reader.gotoRelativeElementWithOffset(((double) val) / MAX_SLIDER_VALUE, pageSize, false);

        // ... and wind all available lines into view
        updateLayoutData(readLogItemsAtCurrentReadingLocation(lines, true));

        updateDialogSlider(false, true);

    }


    /**
     * @param givenTimeStr string holding new position in seconds ("xx.yyy")
     */
    @Override
    protected void handlePositionLogging(String givenTimeStr) {
        stopPlaying();
        if (!readingActive) {
            return;
        }

        // goto a page BEFORE newPos
        int lines = reader.gotoRefElementWithOffset(Double.parseDouble(givenTimeStr), pageSize, false);
        if (lines <= 0) {
            activateSliderComponent();
            return;
        }
        // ... and wind all lines into view
        updateLayoutData(readLogItemsAtCurrentReadingLocation(lines, true));

        updateDialogSlider(true, true);
        activateSliderComponent();

    }


    @Override
    protected void handleGotoStartLogging() {
        if (!readingActive) {
            return;
        }

        stopPlaying();
        try {
            List<LogDataReaderCsv.ReaderResult> readerResults = new ArrayList<>();

            readerResults.add(reader.getFirstElement()); // move current reading location to start of file
            for (int count = 1; count < MIN_FEED_ITEMS; count++) {
                readerResults.add(reader.getNextElement());
            }

            updateLayoutData(readerResults);
            updateDialogSlider(true, true);
        } catch (IOException e) {
            handleReaderException();
        }
    }


    private void handleReadingLiveItem() {
        if (!readingActive) {
            return;
        }
        try {
            List<LogDataReaderCsv.ReaderResult> readerResults = new ArrayList<>();
            readerResults.add(reader.getLastElement());
            updateLayoutData(readerResults);
        } catch (IOException e) {
            handleReaderException();
        }
    }


    @Override
    protected void handleGotoEndLogging() {
        if (!readingActive) {
            return;
        }
        stopPlaying();
        try {
            reader.getLastElement(); // move current reading location to end of file
            int moveSize = reader.moveCurrentReadingLocation(pageSize, false); // move 1 page backward

            List<LogDataReaderCsv.ReaderResult> readerResults = new ArrayList<>();
            while (moveSize-- > 0) {
                readerResults.add(reader.getNextElement());
            }

            updateLayoutData(readerResults);
            updateDialogSlider(true, true);
            if (readWhileWriteMode) {
                // switch to playing 'live' data
                startPlaying(DEFAULT_SPEED_FACTOR, PLAY_DIRECTION_FORWARD, PLAYING_LIVE_ON);
            }
        } catch (IOException e) {
            handleReaderException();
        }
    }


    @Override
    protected void handleStepBackward() {
        if (!readingActive) {
            return;
        }
        stopPlaying();
        updateLayoutData(readLogItemsAtCurrentReadingLocation(1, false));
        updateDialogSlider(true, true);

    }


    @Override
    protected void handleStepForward() {
        if (!readingActive) {
            return;
        }
        stopPlaying();
        updateLayoutData(readLogItemsAtCurrentReadingLocation(1, true));
        updateDialogSlider(true, true);

    }

    @Override
    protected void handlePageDownLogging() {
        if (!readingActive) {
            return;
        }
        stopPlaying();
        updateLayoutData(readLogItemsAtCurrentReadingLocation(pageSize, false));
        updateDialogSlider(true, true);

    }

    @Override
    protected void handlePageUpLogging() {
        if (!readingActive) {
            return;
        }
        stopPlaying();
        updateLayoutData(readLogItemsAtCurrentReadingLocation(pageSize, true));
        updateDialogSlider(true, true);

    }


    // read numItems from logging data. Start reading at current reading location
    private List<ReaderResult> readLogItemsAtCurrentReadingLocation(int numItems, boolean forwardDirection) {
        List<ReaderResult> rdrRslts = new ArrayList<>();
        ReaderResult item;
        try {
            if (forwardDirection) {
                for (int iCnt = 0; iCnt < numItems; iCnt++) {
                    if ((item = reader.getNextElement()) != null) {
                        rdrRslts.add(item);
                    }
                }
            } else {
                // Always show data from left to right in scope windows. Therefore move readingLocation
                // 1 page + numItems to the 'left'. Followed by displaying pageSize elements to the right
                int feedSize = reader.moveCurrentReadingLocation(numItems + pageSize, false) - numItems;
                rdrRslts.addAll(reader.getNextElements(feedSize));
                reader.moveCurrentReadingLocation(rdrRslts.size(), true);
            }
        } catch (IOException e) {
            handleReaderException();
            rdrRslts.clear();
        }
        return rdrRslts;
    }


    @Override
    protected void handleFastForwardBtn() {
        if (!readingActive) {
            return;
        }

        if (isPlaying && isPlayingForward) {
            startPlaying(incrementSpeedFactor(speedFactor), PLAY_DIRECTION_FORWARD, PLAYING_LIVE_OFF);
        } else {
            startPlaying(INIT_FAST_SPEED_FACTOR, PLAY_DIRECTION_FORWARD, PLAYING_LIVE_OFF);
        }

    }


    @Override
    protected void handleFastRewindBtn() {
        if (!readingActive) {
            return;
        }

        if (isPlaying && !isPlayingForward) {
            startPlaying(incrementSpeedFactor(speedFactor), PLAY_DIRECTION_BACKWARD, PLAYING_LIVE_OFF);
        } else {
            startPlaying(INIT_FAST_SPEED_FACTOR, PLAY_DIRECTION_BACKWARD, PLAYING_LIVE_OFF);
        }
    }


    @Override
    protected void logPlayerKeyPressed(java.awt.event.KeyEvent evt) {
        if (!readingActive) {
            return;
        }
        switch (evt.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                if ((evt.getModifiersEx() & SHIFT_DOWN_MASK) != 0) {
                    handlePageDownLogging();
                } else {
                    handleStepBackward();
                }
                activateButtonPrev();
                break;

            case KeyEvent.VK_RIGHT:
                if ((evt.getModifiersEx() & SHIFT_DOWN_MASK) != 0) {
                    handlePageUpLogging();
                } else {
                    handleStepForward();
                }
                activateButtonNext();
                break;

            case KeyEvent.VK_PAGE_DOWN:
                handlePageDownLogging();
                activateButtonPrev();
                break;

            case KeyEvent.VK_PAGE_UP:
                handlePageUpLogging();
                activateButtonNext();
                break;

            case KeyEvent.VK_P:
                handlePlayPauseBtn();
                activatePlayPauseBtn();
                break;

            case KeyEvent.VK_F:
                handleFastForwardBtn();
                activateFastForwardBtn();
                break;

            case KeyEvent.VK_G:
                activateTxtCurrentTime();
                break;

            case KeyEvent.VK_R:
                handleFastRewindBtn();
                activateRewindBtn();
                break;

            case KeyEvent.VK_HOME:
            case KeyEvent.VK_B:
                handleGotoStartLogging();
                activateBeginningBtn();
                break;

            case KeyEvent.VK_END:
            case KeyEvent.VK_E:
                handleGotoEndLogging();
                activateEndBtn();
                break;

            case KeyEvent.VK_SPACE:
                if (evt.getComponent().getName().equals(SLIDER_TIME_POS)) {
                    stopPlaying();
                }
                break;

            default:
                break;
        }
    }


    @Override
    protected void activateTxtCurrentTime() {
        stopPlaying();
        super.activateTxtCurrentTime();
    }


    private int incrementSpeedFactor(int spd) {
        switch (spd) {
            case 0:
            case 1:
                return 5;

            case 5:
                return 10;

            case 10:
                return 20;

            default:
                return 1;
        }
    }


    private static TimerTask timerWrap(Runnable r) {
        return new TimerTask() {
            @Override
            public void run() {
                r.run();
            }
        };
    }


    private void stopPlaying() {

        timer.cancel();
        isPlaying = PLAYING_OFF;
        isPlayingForward = PLAY_DIRECTION_FORWARD;
        isPlayingLive = PLAYING_LIVE_OFF;
        speedFactor = DEFAULT_SPEED_FACTOR;

        setPlayingState(isPlaying, speedFactor, isPlayingForward, isPlayingLive);
    }


    private void startPlaying(int newSpeed, boolean newForward, boolean newLive) {
        if (!readingActive) {
            return;
        }

        timer.cancel();

        isPlaying = PLAYING_ON;
        speedFactor = newSpeed;
        isPlayingForward = newForward;
        isPlayingLive = newLive;

        setPlayingState(PLAYING_ON, speedFactor, isPlayingForward, isPlayingLive);

        if (!isPlayingLive) {
            timer = new Timer(true);
            executeRunTask();
        }
    }


    private void executeRunTask() {
        if (!readingActive || isPlayingLive) {
            return;
        }
        if (isPlaying) {
            if (!updateLayoutData(readLogItemsAtCurrentReadingLocation(1, isPlayingForward))) {
                if (isPlayingForward && readWhileWriteMode && !isPlayingLive) {
                    // Catching up with end of file, switch to playing 'live' data
                    startPlaying(DEFAULT_SPEED_FACTOR, PLAY_DIRECTION_FORWARD, PLAYING_LIVE_ON);
                    activateEndBtn();
                } else {
                    stopPlaying();
                }
                return;
            }
            updateDialogSlider(true, true);

            timer.schedule(timerWrap(this::executeRunTask), calculateNewDelay());
        }
    }


    private long calculateNewDelay() {
        long delay = (long) (reader.getDelayToAdjacentStep(isPlayingForward) * MILLIS_IN_SEC) / speedFactor;
        return Math.max(delay, MIN_DELAY_MILLIS);
    }


    @Override
    protected void handlePlayPauseBtn() {
        if (!readingActive) {
            return;
        }
        if (isPlaying) {
            stopPlaying();
        } else {
            startPlaying(DEFAULT_SPEED_FACTOR, PLAY_DIRECTION_FORWARD, PLAYING_LIVE_OFF);
        }
    }


    private boolean updateLayoutData(List<ReaderResult> rdrResults) {
        if (!readingActive || rdrResults.isEmpty()) {
            return false;
        }

        // update all elements of rdrResults to layout (necessary for Scope panels)
        SwingUtilities.invokeLater(() -> rdrResults.forEach(this::dataUpdateLayout));

        return true;
    }


    private void handleReaderException() {
        CursorWait.stopWaitCursor(this.getRootPane());

        LogDataWriterCsv.getInstance().enableDatalogging(false, false, false);
        HANtune.getInstance().updateDatalogGuiComponents();

        MessagePane.showError("Error reading logfile. Please select another file.\n" + reader.getError());
    }


    private void dataUpdateLayout(ReaderResult rdrRslt) {
        if (rdrRslt == null) {
            return;
        }

        long timeVal = (long) (rdrRslt.getTimeStampSecs() * SEC_TO_MICROS);
        ASAP2Data asap2Data = currentConfig.getASAP2Data();

        for (ResultItem item : rdrRslt.getResultItems()) {
            ASAP2Measurement mmt;
            if (item.getProtocolName().equals("XCP")) {
                if (asap2Data != null && (mmt = asap2Data.getMeasurementByName(item.getItemName())) != null) {
                    mmt.setValueAndTime(item.getItemValue(), timeVal);
                }
            } else if (item.getProtocolName().equals("CAN")) {
                CanDataProcessor.processCanMessage(item.getItemName(), item.getItemValue(), timeVal);
            }
        }
    }
}
