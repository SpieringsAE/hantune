/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.actions;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import ASAP2.ASAP2Data;
import haNtuneHML.Calibration;
import datahandling.CalibrationImportReader;

public class ExportFlashActionReport {

    private static final String HTML_PRE_START = "<pre>";
    private static final String HTML_PRE_END = "</pre>";

    private static final String FONT_COLOR_RED = "<font color=\"red\">";
    private static final String FONT_END_RED = "</font>";
    private final ASAP2Data asap;
    private final Calibration calib;
    private String reportString = null;
    private String finalReportString = null;

    ExportFlashActionReport(ASAP2Data asap, Calibration calib) {
        this.calib = calib;
        this.asap = asap;
    }

    void buildMergeReport(File baseFile, String reportBody, CalibrationImportReader.ImportReaderResult result) {
        String partialStr = createPartialString(asap, calib);
        String headerStr = createHeaderString(baseFile, calib, partialStr);
        String roundupStr = createReportRoundupString(baseFile, calib, partialStr, result);

        reportString = headerStr + reportBody + roundupStr;
    }

    String getMergeReport() {
        // add html stuff
        return HTML_PRE_START + reportString + HTML_PRE_END;
    }

    private String createHeaderString(File baseFile, Calibration calib, String partial) {
        return "Calibration data: <b>" + calib.getTitle() + "</b> " + partial + "\n"
                + "Merge into calibration data from base file: <b>" + baseFile.getName() + "</b>\n";
    }

    private String createPartialString(ASAP2Data asap, Calibration calib) {
        int totalAsapItems = asap.getASAP2Characteristics().keySet().size();
        String partialString;
        if (totalAsapItems > calib.sizeOfASAP2Array()) {
            partialString
                    = String.format("(partial: %d out of %d parameters)", calib.sizeOfASAP2Array(), totalAsapItems);
        } else {
            partialString = "";
        }
        return partialString;
    }

    private String createReportRoundupString(File baseFile, Calibration calib, String partialString,
            CalibrationImportReader.ImportReaderResult result) {
        String colorStrtStr;
        String warnErrStr;
        String baseFileStr;
        String calibNameStr;
        String colorEndStr;

        if (result == CalibrationImportReader.ImportReaderResult.READER_RESULT_OK) {
            warnErrStr = "";
            colorStrtStr = "";
            colorEndStr = "";
        } else {
            colorStrtStr = FONT_COLOR_RED;
            colorEndStr = FONT_END_RED;
            if (result == CalibrationImportReader.ImportReaderResult.READER_RESULT_WARNING) {
                warnErrStr = " (with warning)";
            } else {
                warnErrStr = " with error";
            }
        }
        baseFileStr = baseFile.getName();
        calibNameStr = calib.getTitle();

        return String.format("\n%sFinished merging%s:\n" + "   Base file: <b>%s</b>\n"
                + "   Calibration data: <b>%s</b> %s %s\n", colorStrtStr, warnErrStr, baseFileStr, calibNameStr,
                partialString, colorEndStr);
    }

    void buildFinalReport(String msg, File exportFile) {
        finalReportString = msg + "\nExport file: " + exportFile.getName() + "\n";
    }

    String getFinalMessage() {
        return finalReportString;
    }

    String getFinalReport() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String dateString = dateFormat.format(cal.getTime());

        return HTML_PRE_START + reportString + "\n" + dateString + " " + finalReportString + HTML_PRE_END;
    }
}
