/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.actions;

import java.io.File;
import java.io.IOException;

import javax.swing.tree.DefaultMutableTreeNode;

import ASAP2.ASAP2Data;
import ASAP2.ASAP2ModPar;
import HANtune.HANtune;
import HANtune.ProjectInfo;
import datahandling.CalibrationImportReader;
import datahandling.CalibrationMerger;
import datahandling.CalibrationMergerSRec;
import datahandling.CurrentConfig;
import HANtune.CustomFileChooser;
import util.MessagePane;
import util.Util;

@SuppressWarnings("java:S110") // suppress: Inheritance tree of classes should not be too deep
public class ExportFlashSRecAction extends ExportFlashDialogAction {

    public ExportFlashSRecAction(HANtune hantune) {
        super(hantune);
    }


    public void handleExportFlashDialog(DefaultMutableTreeNode node) {
        if (node == null) {
            return;
        }

        int calibrationId = ((ProjectInfo) node).getId();
        String calibrationName = ((ProjectInfo) node).getName();

        if (!initExportFlashDialogAction(calibrationId, calibrationName,
                CurrentConfig.getInstance().getFlashSRecBaseFileName(), "SREC", CustomFileChooser.FileType.SREC_FILE)) {
            return;
        }

        handleExportFlashDialog();

        if (isSuccesfullExport()) {
            CurrentConfig.getInstance().setFlashSRecBaseFileName(getBaseFileAbsolutePath());
        }
    }

    @Override
    protected boolean isExportFlashPreconditionsOk(ASAP2Data a2d) {
        boolean rtn = true;
        String msg = "";
        ASAP2ModPar mp = a2d.getModPar();
        if (mp == null) {
            msg += "MOD_PAR section not found in ASAP2 currently loaded.";
            rtn = false;
        } else {
            if (!mp.isRamFlashOffsetPresent()) {
                msg += " - SYSTEM_CONSTANT 'RAM-FlashOffset' not found in ASAP2 file" + Util.LF;
                rtn = false;
            }
            if (!mp.isAddrEpkPresent()) {
                msg += " - ADDR_EPK: Address EPROM ID not found in ASAP2 file" + Util.LF;
                rtn = false;
            }
        }

        if (!rtn) {
            MessagePane.showWarning("Cannot export/flash using current ASAP2 file: " + a2d.getName() + Util.LF + msg);
        }
        return rtn;
    }

    protected CalibrationMerger getCalibrationMerger(ASAP2Data asap) {
        return new CalibrationMergerSRec(calib, asap);
    }

    protected boolean handleExportAction(CalibrationMerger merger, File exportFile) throws IOException {
        // do actual export here...
        merger.writeToFile(exportFile);
        return true;
    }

    protected boolean handleExportFlashAction(CalibrationMerger merger, File exportFile) throws IOException {
        // do actual export here...
        merger.writeToFile(exportFile);
        if (merger.getMergeResult() == CalibrationImportReader.ImportReaderResult.READER_RESULT_ERROR) {
            return false;
        }

        FlashCalibration flashCalib = new FlashCalibration((HANtune) super.getParent());
        return flashCalib.doActionFlashSRec(exportFile.getAbsolutePath());
    }

    protected String getSuccessfullString(boolean doOptionalFlash) {
        return "Successfully exported" + (doOptionalFlash ? " and flashed " : " ") + "calibration data.";
    }

}
