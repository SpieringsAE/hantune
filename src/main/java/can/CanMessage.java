/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package can;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains all description data of a CAN Message and its signals from an imported CAN Database file
 * @author Michiel Klifman
 */
public class CanMessage {
    private static final int BASIC_DLC = 8;
    private final String name;
    private boolean extendedID = false;
    private final long id;
    private String idType;
    private final int dlc; // Data Length Code, number of bytes in CAN_Message
    private String sendingNode;
    private String comment;
    private List<CanSignal> signals = new ArrayList<>();
    private DbcObject parent;

    /**
     * @param id the id of this object
     * @param name the name of this object
     * @param dlc the data length code of this object, i.e. the number of bytes
     */
    public CanMessage(long id, String name, int dlc) {
        this.id = id;
        this.name = name;
        this.dlc = dlc;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Messages can have either an 11 bit (standard) or 29 bit (extended)
     * identifier. The id must be unique within the DBC file and is an
     * unsigned integer. Since integers in Java are always signed, some extended
     * id's might not fit in an integer.
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @return the idType
     */
    public String getIdType() {
        return id < 2048 ? "normal" : "extended";
    }

    /**
     * @return the DLC
     */
    public int getDLC() {
        return dlc;
    }

    /**
     *
     * @return true if DLC is longer than BASIC_DLC bytes length
     */
    public boolean isExtendedDLC() {
        return dlc > BASIC_DLC;
    }

    /**
     * @return the sendingNode, empty String if the sending node is the default Vector__XXX
     */
    public String getSendingNode() {
        return sendingNode;
    }

    /**
     * @param sendingNode the sendingNode to set
     */
    public void setSendingNode(String sendingNode) {
        this.sendingNode = sendingNode;
    }

    /**
     * @return the Comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the Comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the signals
     */
    public List<CanSignal> getSignals() {
        return signals;
    }

    /**
     * @param signals the signals to set
     */
    public void setSignals(List<CanSignal> signals) {
        this.signals = signals;
    }

    /**
     * @param name the name of the signal to get
     * @return the signal
     */
    public CanSignal getSignalByShortName(String name) {
        for(CanSignal signal : signals) {
            if(signal.getShortName().equals(name)) {
                return signal;
            }
        }
        return null;
    }

    /**
     * @param signal the signal to add
     */
    public void addSignal(CanSignal signal) {
        this.signals.add(signal);
    }

    /**
     * This overrides the toString method of Object so that an
     * object of this class can be used as a node in a JTree.
     * @return the name of this object
     */
    @Override
    public String toString() {
        return getName();
    }

    /**
     * @return the parent of this object
     */
    public DbcObject getParent() {
        return parent;
    }

    /**
     * @param parent the parent of this object
     */
    public void setParent(DbcObject parent) {
        this.parent = parent;
    }

    /**
     * @return a list of all the children of the parent of this object
     */
    public List<CanMessage> getSiblings() {
        return parent.getMessages();
    }

    public boolean isExtendedID() {
        return extendedID;
    }

    public void setExtendedID(boolean extendedID) {
        this.extendedID = extendedID;
    }
}
