/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package can;

import java.util.ArrayList;
import java.util.List;

import datahandling.Parameter;
import datahandling.ParameterListener;
import datahandling.ReferenceStateListener;
import datahandling.Signal;
import datahandling.SignalListener;

/**
 * Contains all description data of a CAN signal from an imported CAN Database file
 * @author Michiel Klifman
 */
public class CanSignal implements Signal, Parameter {
    public static final String CAN_SIGNAL_SEPARATOR = ".";

    //"Static" information about a CAN signal from a DBC file.
    private final String signalName;
    private final String type;
    private final int format;
    private final String multiplexer;
    private final int startBit;
    private final int length;
    private final double factor;
    private final double offset;
    private final double minimum;
    private final double maximum;
    private final String unit;
    private String comment;
    private CanMessage parent;

    //"Dynamic" information about a CAN signal.
    private double value;
    private long timestamp = 0;
    private boolean active = true;
    private boolean valueSent = true;
    private final List<SignalListener> signalListeners = new ArrayList<>();
    private final List<ParameterListener> parameterListeners = new ArrayList<>();
    private final List<ReferenceStateListener> referenceListeners = new ArrayList<>();

    /**
     * @param name
     * @param multiplexer whether this is a standard(null), multiplexer(M) or multiplexed(e.g. m0) signal
     * @param startBit the index at which this signal starts in the data frame of a message
     * @param length  the length in bits of this signal
     * @param format 0 for motorola(big endian), 1 for intel(little endian)
     * @param type "-" for signed, "+" for unsigned
     * @param factor the factor with which the raw value of the signal must be multiplied to get the real value
     * @param offset
     * @param minimum
     * @param maximum
     * @param unit
     */
    public CanSignal(CanMessage parent, String name, String multiplexer, int startBit, int length,
                     int format, String type, double factor, double offset, double minimum, double maximum,
                     String unit) {
        this.parent = parent;
        this.signalName = name;
        this.multiplexer = multiplexer;
        this.startBit = startBit;
        this.length = length;
        this.format = format;
        this.type = type;
        this.factor = factor;
        this.offset = offset;
        this.minimum = minimum;
        this.maximum = maximum;
        this.unit = unit;
        this.value = minimum > 0 || maximum < 0 ? minimum : 0;
    }

    /**
     * @return the name
     */
    @Override
    public String getName() {
        // unique identifying name based on message name + signal name
        return parent.getName() + CAN_SIGNAL_SEPARATOR + signalName;
    }

    public String getShortName() {
        return signalName; // only return signal part of name
    }

    /**
     * @return the type
     */
    public String getType() {
        return type.equals("+") ? "unsigned" : "signed";
    }

    /**
     * @return the format
     */
    public String getFormat() {
        return format == 0 ? "big endian" : "little endian";
    }

    /**
     * @return the multiplexer, returns "standard" if there was no multiplexer
     * defined in the dbc file.
     */
    public String getMultiplexer() {
        return multiplexer == null ? "standard" : multiplexer;
    }

    /**
     * @return the startBit
     */
    public int getStartBit() {
        return startBit;
    }

    /**
     * @return the length
     */
    public int getLength() {
        return length;
    }

    /**
     * @return the factor
     */
    @Override
    public double getFactor() {
        return factor;
    }

    /**
     * @return the offset
     */
    public double getOffset() {
        return offset;
    }

    /**
     * @return the minimum
     */
    @Override
    public double getMinimum() {
        return minimum;
    }

    /**
     * @return the maximum 'real' value as defined in the dbc file,
     * if the maximum is zero, return the maximum possible value this signal can have
     */
    @Override
    public double getMaximum() {
        return maximum != 0 ? maximum : (Math.pow(2, length)-1) * factor + offset;
    }

    /**
     * @return the unit
     */
    @Override
    public String getUnit() {
        return unit;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return the value of this signal
     */
    @Override
    public double getValue() {
        return value;
    }

    /**
     * This overrides the toString method of Object so that an
     * object of this class can be used as a node in a JTree.
     * @return the name of this object
     */
    @Override
    public String toString() {
        return getShortName();
    }

    /**
     * @return the parent of this object
     */
    public CanMessage getParent() {
        return parent;
    }

    /**
     * @return a list of all the children of the parent of this object
     */
    public List<CanSignal> getSiblings() {
        return parent.getSignals();
    }

    public boolean isConnectedToViewer() {
        return !signalListeners.isEmpty();
    }

    public boolean isConnectedToEditor() {
        return !parameterListeners.isEmpty();
    }

    @Override
    public void addListener(SignalListener listener) {
        if (!signalListeners.contains(listener)) {
            signalListeners.add(listener);
            DbcManager.broadcastSignalUpdate();
        }
    }

    @Override
    public void removeListener(SignalListener listener) {
        signalListeners.remove(listener);
        DbcManager.broadcastSignalUpdate();
    }

    @Override
    public String getSource() {
        return parent.getParent().getName();
    }

    @Override
    public String getProtocolName() {
        return "CAN";
    }

    @Override
    public void addListener(ParameterListener listener) {
        if (!parameterListeners.contains(listener)) {
                parameterListeners.add(listener);

            DbcManager.broadcastSignalUpdate();
        }
    }

    @Override
    public void removeListener(ParameterListener listener) {
        parameterListeners.remove(listener);
        DbcManager.broadcastSignalUpdate();
    }

    @Override
    public void send(double value) {
        updateParameterListeners(value);

        boolean sent = CanDataProcessor.sendSignal(this, getRawValue(value));
        if (sent) {
            this.value = value;
            this.timestamp = System.currentTimeMillis();
            updateSignalListeners();
        }

        if (sent != valueSent) {
            valueSent = sent;
            updateStateListeners();
        }
    }

    @Override
    public double getRawValue() {
        return getRawValue(value);
    }

    @Override
    public double getRawValue(double value) {
        return (value - offset) / factor;
    }

    /**
     * Converts a raw value to a physical value.
     *
     * @param rawValue - the raw value to convert
     * @return a physical value for the provided raw value
     */
    public double getPhysicalValue(long rawValue) {
        long bitmask = -1;
        bitmask &= ~(long) (Math.pow(2, length) - 1);
        if (type.equalsIgnoreCase("-") && (rawValue & (1 << length - 1)) != 0) {
            rawValue = ~rawValue;   // The number is being parsed as an unsigned number, to be stored in a signed long by Java
            rawValue &= ~bitmask;   // A correction is needed for negative numbers in order to retreive the correct value
            rawValue += 1;
            rawValue = -rawValue;
        }
        return rawValue * factor + offset;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public void setValueAndTime(double value, long timestamp) {
        this.value = value;
        this.timestamp = timestamp;
        updateSignalListeners();
    }

    public void updateSignalListeners() {
        for (SignalListener listener : signalListeners) {
            listener.valueUpdate(value, timestamp);
        }
    }

    public void updateParameterListeners(double value) {
        for (ParameterListener listener : parameterListeners) {
            listener.valueUpdate(value);
        }
    }

    @Override
    public int getDecimalCount() {
        return getDecimalCount(factor);
    }

    /**
     * Counts the number of digits behind a decimal point. Return 0 if the
     * value is effectively an integer (i.e. 1.0 or 2.00)
     * @param value - the value of which the decimal digits should be counted
     * @return the number of digits behind a decimal point
     */
    private int getDecimalCount(Double value) {
        if (value % 1 == 0) {
            return 0;
        } else {
            String[] splitter = value.toString().split("\\.");
            return splitter[1].length();
        }
    }

    @Override
    public boolean hasDecimals() {
        return factor % 1 != 0;
    }

    @Override
    public double validateValue(double value) {
        return value;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public double getLastSentValue() {
        return value;
    }

    @Override
    public boolean isInSync() {
        return valueSent;
    }

    @Override
    public void addStateListener(ReferenceStateListener listener) {
        if (!referenceListeners.contains(listener)) {
            referenceListeners.add(listener);
        }
    }

    @Override
    public void removeStateListener(ReferenceStateListener listener) {
        referenceListeners.remove(listener);
    }

    private void updateStateListeners() {
        for (ReferenceStateListener listener : referenceListeners) {
            listener.stateUpdate();
        }
    }

    @Override
    public String getBitString(double value) {
        return Long.toBinaryString((long) value);
    }

    @Override
    public String getHexString(double value) {
        return Long.toHexString((long) value);
    }
}
