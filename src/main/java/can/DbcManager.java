/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package can;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import ErrorMonitoring.ErrorObject;
import datahandling.CurrentConfig;


/**
 *
 * @author Michiel
 */
public class DbcManager {
    private static final int MAX_ITEMS_APPEND = 5; // Maximum number of items displayed in warning/error txt

    private static final DbcManager dbcFileManager = new DbcManager();
    private static final List<DbcObject> dbcObjects = new ArrayList<>();
    private static final List<DbcManagerListener> listeners = new ArrayList<>();


    private DbcManager() {
    }


    private static DbcManager getInstance() {
        return dbcFileManager;
    }


    public static void addDbc(DbcObject dbc) {
        dbcObjects.add(dbc);
        broadcastDbcUpdate();
    }


    public static void removeDbc(String dbcName) {
        Iterator<DbcObject> iter = dbcObjects.iterator();

        while (iter.hasNext()) {
            DbcObject dbc = iter.next();

            if (dbc.getName().equals(dbcName)) {
                iter.remove();
            }
        }
        broadcastDbcUpdate();
    }


    public static void removeAllDbcs() {
        dbcObjects.clear();
        broadcastDbcUpdate();
    }


    public static DbcObject getDbcByName(String dbcName) {
        for (DbcObject dbc : dbcObjects) {
            if (dbcName.equals(dbc.getName())) {
                return dbc;
            }
        }
        return null;
    }


    public static List<String> getAllDbcNames(){
        List<String> lst = new ArrayList<>();
        dbcObjects.stream().forEach(d -> lst.add(d.getName()));
        return lst;
    }


    private static void broadcastDbcUpdate() {
        for (DbcManagerListener listener : listeners) {
            listener.dbcUpdate();
        }
    }


    public static void broadcastSignalUpdate() {
        for (DbcManagerListener listener : listeners) {
            listener.signalUpdate();
        }
    }


    public static void addDbcContainerListener(DbcManagerListener listener) {
        listeners.add(listener);
    }


    public static List<DbcObject> getDbcList() {
        return dbcObjects;
    }


    public static boolean containsMessageByName(String name) {
        for (DbcObject dbc : dbcObjects) {
            if (dbc.getMessageByName(name) != null) {
                return true;
            }
        }
        return false;
    }


    public static CanMessage getMessageByName(String name) {
        for (DbcObject dbc : dbcObjects) {
            CanMessage message = dbc.getMessageByName(name);
            if (message != null)
                return message;
        }
        return null;
    }


    public static CanMessage getMessageById(int id) {
        for (DbcObject dbc : dbcObjects) {
            CanMessage message = dbc.getMessageById(id);
            if (message != null)
                return message;
        }
        return null;
    }


    public static boolean containsSignalByName(String name) {
        for (DbcObject dbc : dbcObjects) {
            for (CanMessage message : dbc.getMessages()) {
                if (message.getSignalByShortName(name) != null) {
                    return true;
                }
            }
        }
        return false;
    }


    public static CanSignal getSignalByName(String name) {
        return getAllSignals().stream().filter(elem->elem.getName().equals(name)).findFirst().orElse(null);
    }


    public static DbcObject getDbcOfMessage(CanMessage message) {
        for (DbcObject dbc : dbcObjects) {
            if (dbc.getMessages().contains(message)) {
                return dbc;
            }
        }
        return null;
    }


    private static List<CanMessage> getAllMessages() {
        return dbcObjects.stream().flatMap(elem -> elem.getMessages().stream()).collect(Collectors.toList());
    }


    private static List<CanSignal> getAllSignals() {
        return dbcObjects.stream().flatMap(elem -> elem.getSignals().stream()).collect(Collectors.toList());
    }


    public static Set<String> findExistingDuplicates(DbcObject additionalDbc) {
        List<String> additionalNames =
            additionalDbc.getMessages().stream().map(CanMessage::getName).collect(Collectors.toList());
        List<String> existingNames =
            getAllMessages().stream().map(CanMessage::getName).collect(Collectors.toList());

        return existingNames.stream().filter(item -> Collections.frequency(additionalNames, item) > 0)
            .collect(Collectors.toSet());
    }


    /**
     * check if dbc data can be loaded into HANtune and create error/warning when necessary
     *
     * @param dbc Data to be loaded
     * @param errObj holds error/warning string when necessary. Independent of return value
     * @return true, data may be loaded into HANtune. False, data contains some anomalies that infer HANtune
     *         from correct behavior. errObj.info contains error/warning string to be displayed in dialog
     */
    public static boolean isDbcLoadingOk(DbcObject dbc, ErrorObject errObj) {
        errObj.setInfo("");
        if (dbc == null) {
            return false;
        }

        if (dbc.getMessages().isEmpty()) {
            errObj.setInfo("<html><b>No messages found in file. Cannot load " + dbc.getName()
                + "</b></html>\n\n" + "File contains no messages.");
            return false;
        }

        if (dbc.getSignals().isEmpty()) {
            errObj.setInfo("<html><b>No signals found in file. Cannot load " + dbc.getName()
                + "</b></html>\n\n" + "File contains no signals.");
            return false;
        }

        if (containsInternalDuplicates(dbc, errObj)) {
            return false;
        }

        if (containsExistingDuplicates(dbc, errObj)) {
            return false;
        }

        if (containsDbcXcpId(dbc, errObj)) {
            return false;
        }

        containsExtendedDLC(dbc, errObj);

        return true;
    }

    /*
    * This method checks for duplicate CAN messages in dbc.
    * The dialog box contains up to five names of duplicates. When more than five duplicates are found only
    * the first five are shown with a number of how many more duplicates have been found.
    * IMPORTANT: The J1939 dbc file also contains duplicate dbc signals (not messages), which indicates
    * that the usage of duplicate signals is allowed by the DBC convention.
    * Duplicate signals can now be handled by HANtune. However, duplicate messages cannot and should be
    * handled as an error.
    * SEE #728 FOR FOLLOW-UP ACTIONS!!!
    * @param dbcObject: the dbcObject to be checked for duplicate messages
    * @param errObj upon completion: info field holds error string if duplicates found
    * @return true: at least 1 duplicate message found. false: no duplicate messages found.
    */
    private static boolean containsInternalDuplicates(DbcObject dbc, ErrorObject errObj) {
        boolean rtn = false;

        // Separate the unique messages from the duplicate ones
        Set<String> uniqueNames = new HashSet<>();
        Set<String> duplicateNames = new HashSet<>();
        for (CanMessage msg : dbc.getMessages()) {
            if (!uniqueNames.add(msg.getName())) {
                duplicateNames.add(msg.getName());
            }
        }

        if (!duplicateNames.isEmpty()) {
            String str =
                "<html><b>Duplicates found in file. Cannot load " + dbc.getName() + "</b></html>\n\n"
                 + "File contains duplicate DBC message entries.\n"
                + "Duplicate messages in a DBC file cause unexpected behaviour, therefore this file cannot be loaded.\n"
                + "The following duplicate DBC messages have been found:";

            errObj.setInfo(appendItemsToMessageTxt(str, new ArrayList<String>(duplicateNames)));

            rtn = true;
        }
        return rtn;
    }


    private static boolean containsExistingDuplicates(DbcObject dbc, ErrorObject errObj) {
        Set<String> duplicates = DbcManager.findExistingDuplicates(dbc);
        if (!duplicates.isEmpty()) {
            String str =
                "<html><b>Duplicates already in use. Cannot load " + dbc.getName() + "</b></html>\n\n"
                    + "File contains duplicates of DBC messages already present in memory.\n"
                    + "The following messages have been found as duplicates in memory:";

            errObj.setInfo(appendItemsToMessageTxt(str, new ArrayList<String>(duplicates)));

            return true;
        }
        return false;
    }


    private static boolean containsDbcXcpId(DbcObject dbc, ErrorObject errObj) {
        int xcpCanRxId = CurrentConfig.getInstance().getXcpsettings().getCANIDRx();
        int xcpCanTxId = CurrentConfig.getInstance().getXcpsettings().getCANIDTx();

        if (dbc.getMessages().stream().anyMatch(m -> m.getId() == xcpCanRxId || m.getId() == xcpCanTxId)) {
            errObj.setInfo("<html><b>CAN ID's in use. Cannot load " + dbc.getName() + "</b></html>\n\n"
                + "File contains messages with ID's that are currently in use for CAN on XCP data reception (<html><b>"
                + Integer.toHexString(xcpCanRxId) + "h</b></html>)\n" + "and/or transmission(<html><b>"
                + Integer.toHexString(xcpCanTxId) + "h</b></html>)\n"
                + "This may cause unexpected behaviour, therefore file cannot be loaded");
            return true;
        }
        return false;
    }


    /**
     * create warning if DbcObject contains items with extended length (DLC is more than 8 bytes in length)
     * HANtune cannot handle dbc message with extended length
     *
     * @param dbc
     * @param errObj upon completion: info field holds warning string.
     * @return true if messages with extended DLC have been found.
     */
    private static boolean containsExtendedDLC(DbcObject dbc, ErrorObject errObj) {
        List<String> extendedLengthItems = dbc.getMessages().stream().filter(CanMessage::isExtendedDLC)
            .map(CanMessage::getName).collect(Collectors.toList());

        if (!extendedLengthItems.isEmpty()) {
            String str =
                "<html><b>HANtune cannot handle DBC messages where message length exceeds 8 bytes.</b></html>\n\n"
                    + "The following " + extendedLengthItems.size() + " message(s) in " + dbc.getName()
                    + " contain(s) more than 8 data bytes and will be disabled:";
            errObj.setInfo(appendItemsToMessageTxt(str, extendedLengthItems));
            return true;
        }
        return false;
    }


    private static String appendItemsToMessageTxt(String msgTxt, List<String> extendedMsgs) {
        StringBuilder txt = new StringBuilder(msgTxt);

        int displayMsgCount =
            extendedMsgs.size() > MAX_ITEMS_APPEND ? MAX_ITEMS_APPEND : extendedMsgs.size();
        int hiddenMsgCount = extendedMsgs.size() - MAX_ITEMS_APPEND;

        for (int iCnt = 0; iCnt < displayMsgCount; iCnt++) {
            txt.append("\n  -  " + extendedMsgs.get(iCnt));
        }

        if (hiddenMsgCount > 0) {
            txt.append("\n... and " + hiddenMsgCount + " more...\n");
        }
        return txt.toString();
    }

}
