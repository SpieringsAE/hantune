/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package can;

import java.util.ArrayList;
import java.util.List;

import dbcXmlData.DbcXmlData;

public class DbcXmlImport {

    /**
     * Reads CAN Database data from XML document data, parses it and returns a DbcObject
     *
     * @param dbcXml
     * @return a CAN Database object
     */
    public static DbcObject importXmlDbc(DbcXmlData dbcXml) {
        DbcObject dbc = new DbcObject(dbcXml.getName(), dbcXml.getName());

        for (dbcXmlData.CanMessage xmlCan : dbcXml.getMessagesList()) {

            CanMessage can = new CanMessage(xmlCan.getId(), xmlCan.getName(), xmlCan.getDlc());
            can.setExtendedID(xmlCan.getExtendedID());
            can.setSendingNode(xmlCan.getSendingNode());

            can.setParent(dbc);
            can.setComment(xmlCan.getComment());

            can.setSignals(importXmlSignals(xmlCan, can));

            dbc.addMessage(can);
        }
        return dbc;
    }


    /**
     * @param xmlCan
     * @param msg
     */
    private static List<CanSignal> importXmlSignals(dbcXmlData.CanMessage xmlCan, CanMessage msg) {
        List<CanSignal> signals = new ArrayList<>();
        for (dbcXmlData.CanSignal xmlSg : xmlCan.getSignalsList()) {

            CanSignal signal = new CanSignal(msg, xmlSg.getName(), xmlSg.getMultiplexer(), xmlSg.getStartBit(),
                xmlSg.getLength(), xmlSg.getFormat(), xmlSg.getType(), xmlSg.getFactor(), xmlSg.getOffset(),
                xmlSg.getMinimum(), xmlSg.getMaximum(), xmlSg.getUnit());

            signal.setComment(xmlSg.getComment());

            signals.add(signal);
        }
        return signals;
    }

}
