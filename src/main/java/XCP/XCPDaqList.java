/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

/**
 * Contains all information about a XCP DAQ list configuration.
 * Objects created from this class are used to transfer DAQ list information
 * between application layer and XCP protocol layer.
 *
 * @author Aart-Jan
 */
public class XCPDaqList {

    /* Parameters return by GET_DAQ_LIST_INFO command */

    /**
     * Indicates whether this DAQ list is predefined by the slave device.
     */
    private boolean predefined = false;
    /**
     * Indicates whether the event channel of this DAQ list is fixed.
     */
    private boolean event_fixed = false;
    /**
     * Indicates whether STIM direction is allowed on this DAQ list.
     */
    private boolean stim_allowed = true;
    /**
     * Indicates whether DAQ direction is allowed on this DAQ list.
     */
    private boolean daq_allowed = true;
    /**
     * Indicated the maximum number of ODT's in this DAQ list.
     */
    private char max_odt = 0;
    /**
     * Indicates the maximum number of ODT entries in this DAQ list's ODT's.
     */
    private char max_odt_entries = 0;
    /**
     * Indicates the number of entries in this DAQ list.
     */
    private int entryCnt = 0;
    /**
     * Number of the fixed event channel used for this DAQ list.
     */
    private char fixed_event = 0;

    /* Parameters returned by SET_DAQ_LIST_MODE and GET_DAQ_LIST_MODE command */

    /**
     * Indicates whether this DAQ list is selected.
     */
    private boolean selected = false;
    /**
     * Indicates whether this DAQ list is running.
     */
    private boolean running = false;
    /**
     * Indicates whether this DAQ is part of a configuration used in RESUME mode.
     */
    private boolean resume = false;
    /**
     * Indicates the direction of this DAQ list. True = STIM direction, false = DAQ direction.
     */
    private boolean direction = false;
    /**
     * Indicates whether the timestamp is enabled for this DAQ list.
     */
    private boolean timestamp = false;
    /**
     * Indicates whether the Identification field is disabled for this DAQ list.
     */
    private boolean pid_off = false;
    /**
     * The event channel number.
     */
    private char event_no = 0;
    /**
     * The prescaler to the event channel sample period.
     */
    private int prescaler = 0;
    /**
     * DAQ list priority. 0xFF = highest.
     */
    private char priority = 0;
    /**
     * name to identify the DAQList
     */
    private String name = "";
    /**
     * Indicates the number of entries in this DAQ list.
     */
    private int dataLen = 0;

    /**
     * Object Descriptor Table array inside this DAQ list
     */
    public XCPOdt[] odt = new XCPOdt[0];

    /**
     * Initializes a new array of empty ODT's
     *
     * @param cnt the number of ODT's to be initialized
     */
    public void init(char cnt){
        odt = new XCPOdt[cnt];
        for(char i=0; i<cnt; i++){
            odt[i] = new XCPOdt();
        }
    }

    /**
     * Clears all present ODT's
     */
    public void clear(){
        for(char i=0; i<odt.length; i++){
            odt[i].clear();
        }
    }

    /**
     * Empties the ODT array
     */
    public void reset(){
        odt = new XCPOdt[0];
    }

    /**
     * Sets whether this DAQ list is predefined
     *
     * @param predefined Indicates whether this DAQ list is predefined
     */
    public void setPredefined(boolean predefined){
        this.predefined = predefined;
    }

    /**
     * Returns whether this DAQ list is predefined
     *
     * @return whether this DAQ list is predefined
     */
    public boolean getPredefined(){
        return predefined;
    }

    /**
     * Sets whether this DAQ list has a fixed event channel
     *
     * @param event_fixed Indicates whether this DAQ list has a fixed event channel
     */
    public void setEventFixed(boolean event_fixed){
        this.event_fixed = event_fixed;
    }

    /**
     * Returns whether this DAQ list has a fixed event channel
     *
     * @return whether this DAQ list has a fixed event channel
     */
    public boolean getEventFixed(){
        return event_fixed;
    }

    /**
     * Sets whether STIM direction is allowed for this DAQ list
     *
     * @param stim_allowed Indicates whether STIM direction is allowed for this DAQ list
     */
    public void setStimAllowed(boolean stim_allowed){
        this.stim_allowed = stim_allowed;
    }

    /**
     * Returns whether STIM direction is allowed for this DAQ list
     *
     * @return whether STIM direction is allowed for this DAQ list
     */
    public boolean getStimAllowed(){
        return stim_allowed;
    }

    /**
     * Sets whether DAQ direction is allowed for this DAQ list
     *
     * @param daq_allowed Indicates whether DAQ direction is allowed for this DAQ list
     */
    public void setDaqAllowed(boolean daq_allowed){
        this.daq_allowed = daq_allowed;
    }

    /**
     * Returns whether DAQ direction is allowed for this DAQ list
     *
     * @return whether DAQ direction is allowed for this DAQ list
     */
    public boolean getDaqAllowed(){
        return daq_allowed;
    }

    /**
     * Sets the number of the fixed event channel for this DAQ list
     *
     * @param fixed_event the number of the fixed event channel
     */
    public void setFixedEvent(char fixed_event){
        this.fixed_event = fixed_event;
    }

    /**
     * Returns the number of the fixed event channel of this DAQ list
     *
     * @return the number of the fixed event channel of this DAQ list
     */
    public char getFixedEvent(){
        return fixed_event;
    }

    /**
     * Returns the maximum number of ODT's in this DAQ list
     *
     * @return maximum number of ODT's
     */
    public char getMaxOdt(){
        return max_odt;
    }

    /**
     * Sets the maximum number of ODT entries for the ODT's in this DAQ list
     *
     * @param max_odt_entries maximum number of ODT entries
     * @return true if successful, false when an error has occured
     */
    public boolean setMaxOdtEntries(char max_odt_entries){
        if(max_odt_entries > 0xFF) return false;
        this.max_odt_entries = max_odt_entries;
        System.out.println("maximum odt entries is: " + this.max_odt_entries);
        return true;
    }

    /**
     * Returns the maximum number ODT entries in ODT's in this DAQ list
     *
     * @return maximum number of ODT entries in ODT's in this DAQ list
     */
    public char getMaxOdtEntries(){
        return max_odt_entries;
    }

    /**
     * Sets the maximum number of ODT's in this DAQ list
     *
     * @param max_odt maximum number of ODT's
     * @return true is successfull, false when an error has occured
     */
    public boolean setMaxOdt(char max_odt){
        if(max_odt > 0xFF) return false;
        this.max_odt = max_odt;
        return true;
    }

    /**
     * Sets whether this DAQ list is selected
     *
     * @param selected Indicates whether this DAQ list is selected
     */
    public void setSelected(boolean selected){
        this.selected = selected;
    }

    /**
     * Returns whether this DAQ list is selected
     *
     * @return whether this DAQ list is selected
     */
    public boolean getSelected(){
        return selected;
    }

    /**
     * Sets whether this DAQ list is running
     *
     * @param running Indicates whether this DAQ list is active
     */
    public void setRunning(boolean running){
        this.running = running;
    }

    /**
     * Returns whether this DAQ list is running
     *
     * @return whether this DAQ list is running
     */
    public boolean getRunning(){
        return running;
    }

    /**
     * Sets whether this DAQ list is part of a RESUME mode configuration
     *
     * @param resume Indicates whether this DAQ list is part of a RESUME mode configuration
     */
    public void setResume(boolean resume){
        this.resume = resume;
    }

    /**
     * Return whether this DAQ list is part of a RESUME mode configuration
     *
     * @return whether this DAQ list is part of a RESUME mode configuration
     */
    public boolean getResume(){
        return resume;
    }

    /**
     * Sets the used direction for this DAQ list
     *
     * @param direction Indicates the directions; true if STIM, false if DAQ
     */
    public void setDirection(boolean direction){
        this.direction = direction;
    }

    /**
     * Returns the used direction for this DAQ list.
     * true when using STIM direction, false when using DAQ direction
     *
     * @return direction of this DAQ list; true = STIM direction, false = DAQ direction
     */
    public boolean getDirection(){
        return direction;
    }

    /**
     * Sets whether the timestamp is enabled for this DAQ list
     *
     * @param timestamp Indicates whether timestamp is enabled
     */
    public void setTimestamp(boolean timestamp){
        this.timestamp = timestamp;
    }

    /**
     * Returns whether the timestamp is enabled for this DAQ list
     *
     * @return whether the timestamp is enabled for this DAQ list
     */
    public boolean getTimestamp(){
        return timestamp;
    }

    /**
     * Sets the amount of databits used by this DAQ list per sample-event
     *
     * @param dataLen the amount of data in no. of bits
     */
    public void setDataLen(int dataLen){
        this.dataLen = dataLen;
    }

    /**
     * Returns the amount of databits used by this DAQ list per sample-event
     */
    public int getDataLen(){
        return dataLen;
    }

    /**
     * Sets whether the Identification Field is disabled for this DAQ list
     *
     * @param pid_off Indicates whether the Identification Field is disabled for this DAQ list
     */
    public void setPidOff(boolean pid_off){
        this.pid_off = pid_off;
    }

    /**
     * Return whether the Identification Field is disabled for this DAQ list
     *
     * @return whether the Identification Field is disabled for this DAQ list
     */
    public boolean getPidOff(){
        return pid_off;
    }

    /**
     * Sets the event channel number used by this DAQ list
     *
     * @param event_no event channel number
     */
    public void setEventNo(char event_no){
        this.event_no = event_no;
    }

    /**
     * Returns the event channel number use by this DAQ list
     *
     * @return event channel number used by this DAQ list
     */
    public char getEventNo(){
        return event_no;
    }

    /**
     * Sets the value of the prescaler used for prescaling the rate of the used event channel
     *
     * @param prescalerChar
     * @return true if successful, false when an error has occured
     */
    public boolean setPrescaler(char prescalerChar){
        int Prescaler = (int) prescalerChar;
        this.prescaler = Prescaler;
        return true;
    }

    /**
     * Returns the value of the prescaler used for prescaling the rate of the used event channel
     *
     * @return prescaler used by this DAQ list
     */
    public int getPrescaler(){
        return prescaler;
    }

    /**
     * Sets the priority of this DAQ list
     *
     * @param priority Indicates the priority of this DAQ list. Value of 0 to 256.
     * @return true if successfull, false when an error has occured
     */
    public boolean setPriority(char priority){
        if(priority > 0xFF) return false;
        this.priority = priority;
        return true;
    }

    /**
     * Returns the priority of this DAQ list
     *
     * @return priority of this DAQ list
     */
    public char getPriority(){
        return priority;
    }

    /**
     * Creates a specified number of ODT's in this DAQ list
     *
     * @param cnt number of ODT's
     * @return true if successful, false when an error has occured
     */
    public boolean setOdtCnt(char cnt){
        if(cnt > 0xFF) return false;
        odt = new XCPOdt[cnt];
        return true;
    }

    /**
     * Returns the number of ODT's
     *
     * @return the number of ODT's
     */
    public char getOdtCnt(){
        return (char) odt.length;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the name
     */
    public int getEntryCnt() {
        return entryCnt;
    }

    /**
     *  Sets the number of entries in the DAQ list
     */
    public void setEntryCnt(int cnt) {
        entryCnt = cnt;
    }

    /**
     * Object Description Table. Contains ODT entries.
     */
    public class XCPOdt {

        /**
         * ODT entry array inside this ODT
         */
        public XCPOdtEntry[] odt_entry = new XCPOdtEntry[0];

        /**
         * Initializes a specified number of empty ODT entries in this ODT
         *
         * @param cnt number of ODT entries
         */
        public void init(char cnt){
            odt_entry = new XCPOdtEntry[cnt];
            for(char i=0; i<cnt; i++){
                odt_entry[i] = new XCPOdtEntry();
            }
        }

        /**
         * Clears all ODT entries in this ODT
         */
        public void clear(){
            for(char i=0; i<odt_entry.length; i++){
                odt_entry[i].clear();
            }
        }

        /**
         * Empties the ODT entry array in this ODT
         */
        public void reset(){
            odt_entry = new XCPOdtEntry[0];
        }

        /**
         * Creates a specified number of ODT entries in this ODT
         *
         * @param cnt number of ODT entries
         * @return true if successful, false when an error has occured
         */
        public boolean setOdtEntryCnt(char cnt){
            if(cnt > 0xFFFF) return false;
            odt_entry = new XCPOdtEntry[cnt];
            return true;
        }

        /**
         * Returns the number of ODT entries in this ODT
         *
         * @return the number of ODT entries in this ODT
         */
        public int getOdtEntryCnt(){
            return odt_entry.length;
        }

        /**
         * Object Description Table entry. Contains information about an ASAP2
         * element like address and size.
         *
         */
        public class XCPOdtEntry {

            /**
             * Bit offset of this ODT entry
             */
            private char bit_offset = 0xFF;
            /**
             * Size of this ODT entry
             */
            private char size = 1;
            /**
             * Address of this ODT entry
             */
            private long address = 0;
            /**
             * Address extension of this ODT entry
             */
            private char address_ext = 0;
            /**
             * Name of this ODT entry
             */
            private String name = "";
            /**
             * Relative ID in the full list of DAQ list entries
             */
            private char relativeId = 0;

            /**
             * Sets all properties of this entry to default values
             */
            public void clear(){
                bit_offset = 0xFF;
                size = 1;
                address = 0;
                address_ext = 0;
                name = "";
            }

            /**
             * Sets the bit offset for this ODT entry
             *
             * @param bit_offset bit offset
             * @return true if successful, false when an error has occured
             */
            public boolean setBitOffset(char bit_offset){
                if(bit_offset > 0xFF) return false;
                this.bit_offset = bit_offset;
                return true;
            }

            /**
             * Returns the bit offset for this ODT entry
             *
             * @return bit offset
             */
            public char getBitOffset(){
                return bit_offset;
            }

            /**
             * Sets the size of the element referenced by this ODT entry
             *
             * @param size size of the referenced element
             * @return true if successful, false when an error has occured
             */
            public boolean setSize(char size){
                if(size > 0xFF) return false;
                this.size = size;
                return true;
            }

            /**
             * Returns the size of the element referenced by this ODT entry
             *
             * @return size of the refered element
             */
            public char getSize(){
                return size;
            }

            /**
             * Sets the address of the element referenced by this ODT entry
             *
             * @param address address of the referenced element
             */
            public void setAddress(long address){
                this.address = address;
            }

            /**
             * Returns the address of the element referenced by this ODT entry
             *
             * @return address of the refered element
             */
            public long getAddress(){
                return address;
            }

            /**
             * Sets the address extension of the element referenced by this ODT entry
             *
             * @param address_ext address extension of the referenced element
             * @return true if successful, false when an error has occured
             */
            public boolean setAddressExt(char address_ext){
                if(address_ext > 0xFF) return false;
                this.address_ext = address_ext;
                return true;
            }

            /**
             * Returns the address extension of the element referenced by this ODT entry
             *
             * @return address extension of the refered element
             */
            public char getAddressExt(){
                return address_ext;
            }

            /**
             * Sets the name of the element referenced by this ODT entry
             *
             * @param name name of the referenced element
             */
            public void setName(String name){
                this.name = name;
            }

            /**
             * Returns the name of the element referenced by this ODT entry
             *
             * @return name of the refered element
             */
            public String getName(){
                return name;
            }

            /**
             * Sets the relative ID of the entry, in the full list of DAQ list entries
             *
             * @param relativeId
             */
            public void setRelativeId(char relativeId){
                this.relativeId = relativeId;
            }

            /**
             * Returns the relative ID of the entry, in the full list of DAQ list entries
             *
             * @return relativeId
             */
            public char getRelativeId(){
                return relativeId;
            }

        }

    }

}
