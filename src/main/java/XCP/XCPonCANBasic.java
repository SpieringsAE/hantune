/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

import javax.swing.SwingUtilities;

import ErrorLogger.AppendToLogfile;
import can.CanDataProcessor;
import datahandling.CurrentConfig;
import peak.can.basic.*;
import util.Util;

/**
 * XCP Transport Layer. This class extends the XCP Protocol Layer class by adding a transport layer specific
 * interface for connecting a XCP slave device and receiving and transmitting XCP data over a CAN network.
 *
 * @author Aart-Jan
 */
public class XCPonCANBasic extends XCP implements IRcvEventProcessor {
    private static CurrentConfig currentConfig = CurrentConfig.getInstance();
    /**
     * Instance of PCAN USB module driver
     */
    private PCANBasic pcan = null;
    /**
     * Status of CAN device
     */
    private TPCANStatus status = null;
    /**
     * PEAK hardware used for this CAN connection
     */
    private TPCANHandle CAN_hardware = TPCANHandle.PCAN_USBBUS1;
    /**
     * Message type used for this CAN connection
     */
    private byte CAN_message_type = TPCANMessageType.PCAN_MESSAGE_STANDARD.getValue();

    /**
     * The error message coming from pcan.initializeAPI
     */
    private String initializeAPIException = "";


    /**
     * Constructor of XCPonCAN, makes reference to main object
     *
     * @param HT
     */
    public XCPonCANBasic() {
        super();
        xcpTimeOutTimer.startTimer();
        useSlaveTimestamp = false; // The slave does generate a timestamp for each sample
    }


    /**
     * Makes a connection to a XCP slave over a CAN network.
     *
     * @return whether this method has been executed successfully
     */
    @Override
    protected boolean connect() {
        currentConfig.setTimeOffsetMicrosEmpty();
        if (pcan == null) {

            try {
                // initialize API
                pcan = new PCANBasic();
            } catch (Exception e) {
                AppendToLogfile.appendError(currentThread(), e);
                System.out.println(e.getMessage());
            }


            if (!initializeAPI()) {
                this.setError("Could not initialize PCAN API \n " + initializeAPIException, true);
                pcan = null;
                return false;
            }

            // initialize connection
            status = pcan.Initialize(CAN_hardware, currentConfig.getXcpsettings().getCANBaudrate(),
                TPCANType.PCAN_TYPE_NONE, 0, (short)0);
            if (status != TPCANStatus.PCAN_ERROR_OK) {
                StringBuffer buff = new StringBuffer("");
                pcan.GetErrorText(status, (short)0x09, buff);
                pcan.Uninitialize(CAN_hardware);
                pcan = null;
                this.setError(
                    "Could not initialize PCAN driver.\n Please close all other applications using the PCAN hardware and try again. \n"
                        + buff,
                    true);
                return false;
            }

            // set reception filter
            // status = pcan.FilterMessages(CAN_hardware, currentConfig.getXcpsettings().getCANIDRx(),
            // currentConfig.getXcpsettings().getCANIDRx(), TPCANMode.PCAN_MODE_STANDARD);
            status =
                pcan.FilterMessages(CAN_hardware, 0x00000000, 0x1FFFFFFF, TPCANMode.PCAN_MODE_EXTENDED);
            if (status != TPCANStatus.PCAN_ERROR_OK) {
                StringBuffer buff = new StringBuffer("");
                pcan.GetErrorText(status, (short)0x09, buff);
                pcan.Uninitialize(CAN_hardware);
                pcan = null;
                this.setError("Could not initialize CAN filter.\n" + buff, true);
                return false;
            }

            // initialize event mechanism
            RcvEventDispatcher.setListener(this);
            status = pcan.SetRcvEvent(CAN_hardware); // ticket refs#281
            if (status != TPCANStatus.PCAN_ERROR_OK) {
                StringBuffer buff = new StringBuffer("");
                pcan.GetErrorText(status, (short)0x09, buff);
                pcan.Uninitialize(CAN_hardware);
                pcan = null;
                this.setError("Could not initialize CAN interrupt handler.\n" + buff, true);
                return false;
            }

            // start xcp communication
            connected = true;
        }
        return true;
    }


    private boolean initializeAPI() {
        boolean isInitialized = false;
        try {
            isInitialized = pcan.initializeAPI();
        } catch (UnsatisfiedLinkError e) {
            AppendToLogfile.appendError(currentThread(), e);
            this.setError(e.getMessage(), true);
            isInitialized = false;
        }
        return isInitialized;
    }


    /**
     * Closes a connection to a XCP slave over a CAN network.
     *
     * @return whether this method has been executed successfully
     */
    @Override
    protected boolean disconnect() {
        // stop xcp communication
        stopXCP();
        if (currentConfig.getCan() != null) {
            currentConfig.getCan().stopSession();
        }

        if (pcan != null) {
            // stop event listener mechanism //When this method is run after unsuccesful connection attempt
            // (e.g. wrong XCP transmit/receive ID's) it causes a JVM crash.
            status = pcan.ResetRcvEvent(CAN_hardware);
            // stop pcan driver
            pcan.Uninitialize(CAN_hardware);
            pcan = null;
            connected = false;
        }
        return true;
    }


    /**
     * Sends an array of databytes to the XCP slave device over a CAN network.
     *
     * @param data message data array
     * @return whether the data has been send successfully
     */
    @Override
    protected boolean sendData(char[] data) {
        // check connection
        if (!connected) {
            return false;
        }

        // make TPCANMsg object of given data
        TPCANMsg msg_send = new TPCANMsg(currentConfig.getXcpsettings().getCANIDTx(), CAN_message_type,
            (byte)data.length, Util.charA2byteA(data));

        // print debug information to command line?
        if (DEBUG_PRINT) {
            System.out.print(" ID:0x65 " + data.length + " databyte(s) (");
            for (char i = 0; i < data.length; i++) {
                System.out.print(" 0x" + Integer.toHexString(data[i] & 0xFF));
            }
            System.out.print(" )\n");
        }

        // send message and return status
        status = pcan.Write(CAN_hardware, msg_send);
        XCP_waiting = true;
        xcpTimeOutTimer.setTimeInMiliseconds(0);
        cl.incrementCurrentID();

        if (status != TPCANStatus.PCAN_ERROR_OK) {
            StringBuffer buff = new StringBuffer("");
            pcan.GetErrorText(status, (short)0x09, buff);
            this.setError("PCAN-USB: " + buff, true);
            return false;
        }

        return true;
    }


    public boolean sendCanMessage(int id, byte[] data, boolean extId) {
        // check connection
        if (!connected) {
            return false;
        }

        // make TPCANMsg object of given data
        TPCANMsg msg_send = new TPCANMsg(id, CAN_message_type, (byte) data.length, data);

        // XCP message?
        if (currentConfig.getXcpsettings().getCANIDRx() == id) {

            // send message and return status
            status = pcan.Write(CAN_hardware, msg_send);
            XCP_waiting = true;
            xcpTimeOutTimer.setTimeInMiliseconds(0);
            cl.incrementCurrentID();
        }
        // non-XCP message
        else {
            // send message and return status
            status = pcan.Write(CAN_hardware, msg_send);
        }
        if (status != TPCANStatus.PCAN_ERROR_OK) {
            StringBuffer buff = new StringBuffer("");
            pcan.GetErrorText(status, (short) 0x09, buff);
            this.setError("PCAN-USB: " + buff, true);
            return false;
        }
        return true;
    }


    /**
     * Receive event listener for receiving data from PEAK CAN module. For receiving new CAN messages and
     * passing them to the Protocol Layer.
     *
     * @see XCP.XCP#receiveData(char[])
     */
    @Override
    public void processRcvEvent(TPCANHandle channel) {
//        TPCANTimestampAdapter rcvTime = new TPCANTimestampAdapter();
//        TPCANTimestamp rcvTime = new TPCANTimestamp();
        while (true) {
            TPCANMsg tpMsg = new TPCANMsg();

            // read new message
            TPCANStatus rslt = pcan.Read(CAN_hardware, tpMsg, null);
//            TPCANStatus rslt = pcan.Read(CAN_hardware, tpMsg, rcvTime);
            if (rslt == TPCANStatus.PCAN_ERROR_OK) {
                long timeStamp = calculateTimeStampMicros();
//                long timeStamp = rcvTime.getMicros();

                // print debug information
                if (DEBUG_PRINT) {
                    // filter CAN msg according to CanId, remove this filter to show ALL CAN data
                    if ((char)(tpMsg.getID() & 0xFFFF) == currentConfig.getXcpsettings().getCANIDRx()) {
                        System.out.print(
                            "XCPOnCanBasic>>> " + timeStamp + " - ID:0x" + Integer.toHexString(tpMsg.getID()) + " "
                                + tpMsg.getLength() + " databyte(s) (");
                        for (char i = 0; i < tpMsg.getLength(); i++) {
                            System.out.print(" 0x" + Integer.toHexString(tpMsg.getData()[i] & 0xFF));
                        }
                        System.out.print(" )\n");
                    }
                }

                // check whether CAN message belongs to XCP traffic
                if ((char)(tpMsg.getID() & 0xFFFF) == currentConfig.getXcpsettings().getCANIDRx()) {
                    processRcvXcpEvent(tpMsg, timeStamp);
                } else if (shouldProcessCanData()) {
                    // No XCP traffic? Handle as generic CAN data
                    SwingUtilities.invokeLater(() -> CanDataProcessor.processCanMessage(tpMsg, timeStamp));
                }
            } else {
                if (DEBUG_PRINT) {
                    System.out.println("pcanRead result: " + rslt);
                }


                break; // finished
            }
        }
    }


    /**
     * @param rcvTime
     */
    public static long calculateTimeStampMicros(TPCANTimestampAdapter canTime) {
        long canTimeLong = canTime.getTotalMicros();

        setTimeOffset(canTime.getTotalMicros());
        return canTimeLong + currentConfig.getTimeOffsetMicros();
    }


    public static long calculateTimeStampMicros() {
        // If time cannot be derived from connected hardware (function above), use system time
        long timeMicros = System.nanoTime() / 1000; // UP-time of hardware, converted to microseconds

        setTimeOffset(timeMicros);
        return timeMicros + currentConfig.getTimeOffsetMicros();
    }


    private static void setTimeOffset(long timeMicros) {
        // determine time offset for correcting to absolute timestamps
        if (currentConfig.isTimeOffsetEmpty()) {
            currentConfig.setTimeOffsetMicros(-timeMicros);
        }
    }


    /**
     * @param tpMsg
     * @param rcv
     */
    private void processRcvXcpEvent(TPCANMsg tpMsg, long timestamp) {
        try {
            receiveData(Util.byteA2charA(tpMsg.getData(), tpMsg.getLength()), timestamp);
        } catch (Exception e) {
            AppendToLogfile.appendMessage("Error while processing CAN message");
            AppendToLogfile.appendError(currentThread(), e);
            // set error and exit command list sequence
            e.printStackTrace();
            System.err.println(e);
            setError("Error while processing CAN message.\n" + e.getMessage(), true);
            XCP_isProcessing = false;
            XCP_waiting = false;
            cl.exit();
        }
    }

}
