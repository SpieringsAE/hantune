/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XCP;

/**
 * Object containing information about a XCP command.
 *
 * @author Aart-Jan
 */
public class XCPCmd {

    /**
     * Textlabel for this command
     */
    public String cmd = "";
    /**
     * Indicates the timeout length allowed for this command
     */
    public int timeout = 0;
    /**
     * Indicates the number of bytes used when sending this command
     */
    public char CRO_len = 0;
    /**
     * Indicates the number of bytes used by the responses to this command
     */
    public char CRM_len = 0;
    /**
     * Indicates the resource related to this command.
     */
    public char resource = 0x00;

    /**
     * Default constructor method for initialization of an empty command
     */
    public XCPCmd(){

    }

    /**
     * Constructor method for initialization of a command
     *
     * @param cmd Textlabel for this command
     * @param timeout Indicates the timeout length allowed for this command
     * @param CRO_len Indicates the number of bytes used when sending this command
     * @param CRM_len Indicates the number of bytes used by the responses to this command
     * @param resource Indicates the resource related to this command
     */
    public XCPCmd(String cmd, int timeout, char CRO_len, char CRM_len, char resource){
        this.cmd = cmd;
        this.timeout = timeout;
        this.CRO_len = CRO_len;
        this.CRM_len = CRM_len;
        this.resource = resource;
    }

}
