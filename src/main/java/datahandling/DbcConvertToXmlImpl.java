/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.util.HashSet;
import java.util.Map;

import can.CanMessage;
import can.CanSignal;
import can.DbcObject;
import dbcXmlData.DbcXmlData;
import haNtuneHML.Layout;
import haNtuneHML.Layout.Tab;
import haNtuneHML.Layout.Tab.Window;
import haNtuneHML.Layout.Tab.Window.ASAP2;
import haNtuneSHML.ShmlDocument.Shml;

/*
 * Class to convert Dbc data to Shml xml data, filtering elements found in Layout
 */
public class DbcConvertToXmlImpl implements ConvertToXml<DbcObject, Shml, Layout> {


    public DbcConvertToXmlImpl() {
    }


    @Override
    public Boolean filterRelevantDataIntoXml(DbcObject obj, Shml sh, Layout lt) {
        if (obj == null || sh == null || lt == null) {
            return false;
        }

        Map<String, CanSignal> objSignalsMap = obj.getSignalsMap();
        CanSignal sgn;

        // holds names of canmgs already added, to prevent duplicate can messages
        HashSet<String> canMsgNames = new HashSet<>();

        // loop all visible elements in layout
        for (Tab tElem : lt.getTabList()) {
            for (Window wElem : tElem.getWindowList()) {
                for (ASAP2 aElem : wElem.getASAP2List()) {

                    // Check for dbc source name and find aElem name in signalsMap
                    if (aElem.getSource() != null && aElem.getSource().equals(obj.getName())
                        && (sgn = objSignalsMap.get(aElem.getName())) != null) {
                        CanMessage parent = sgn.getParent();
                        if (!canMsgNames.contains(parent.getName())) {
                            canMsgNames.add(parent.getName());
                            addXmlCanMessage(parent, getDataListElementByName(sh, obj.getName()));
                        }
                    }
                }
            }
        }

        return true;
    }


    private void addXmlCanMessage(CanMessage canMsg, DbcXmlData xmlObj) {
        dbcXmlData.CanMessage xmlCanMsg = xmlObj.addNewMessages();

        xmlCanMsg.setId(canMsg.getId());
        xmlCanMsg.setName(canMsg.getName());
        xmlCanMsg.setDlc(canMsg.getDLC());

        xmlCanMsg.setExtendedID(canMsg.isExtendedID());
        xmlCanMsg.setSendingNode(canMsg.getSendingNode());
        // xmlCan.setParent(xmlObj);
        if (canMsg.getComment() != null) {
            xmlCanMsg.setComment(canMsg.getComment());
        }

        addXmlCanSignals(canMsg, xmlCanMsg);
    }


    /**
     * @param can
     * @param xmlCan
     */
    private void addXmlCanSignals(CanMessage can, dbcXmlData.CanMessage xmlCan) {

        dbcXmlData.CanSignal xmlSignal;
        for (CanSignal signal : can.getSignals()) {
            xmlSignal = xmlCan.addNewSignals();

            xmlSignal.setName(signal.getShortName());
            xmlSignal.setMultiplexer(signal.getMultiplexer());
            xmlSignal.setStartBit(signal.getStartBit());
            xmlSignal.setLength(signal.getLength());
            xmlSignal.setFormat(signal.getFormat().equals("big endian") ? 0 : 1);
            xmlSignal.setType(signal.getType());
            xmlSignal.setFactor(signal.getFactor());
            xmlSignal.setOffset(signal.getOffset());
            xmlSignal.setMinimum(signal.getMinimum());
            xmlSignal.setMaximum(signal.getMaximum());
            xmlSignal.setUnit(signal.getUnit());
            if (signal.getComment() != null) {
                xmlSignal.setComment(signal.getComment());
            }

        }
    }


    private DbcXmlData getDataListElementByName(Shml sh, String name){
        for (DbcXmlData xmlData : sh.getDbcDataList()) {
            if (xmlData.getName().equals(name)) {
                return xmlData;
            }
        }

        // if name not found in list, then add new element to list
        DbcXmlData newData = sh.addNewDbcData();
        newData.setName(name);

        return newData;
    }

}
