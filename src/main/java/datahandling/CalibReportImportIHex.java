/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.File;

public class CalibReportImportIHex extends CalibrationReport<CalibReportImportIHex.ReportLineTypeIHex> {

    enum ReportLineTypeIHex {
        IHEX_CALIB_IMPORT_OK("HEX item succesfully imported"),
        IHEX_CALIB_NOT_FOUND(
                "Calibration item address: %08X, size %d not found in HEX data at address %08X. Not imported"),
        IHEX_CALIB_NAME_NOT_FOUND("Calibration name not found in ASAP2 data. Not imported");

        // enum constructor
        ReportLineTypeIHex(String line) {
            this.description = line;
        }

        private String description;

        protected String getDescription() {
            return description;
        }

        protected String getDescription(long address, int valB) {
            return String.format(description, address, valB);
        }

        protected String getDescription(long address, int valB, long address2) {
            return String.format(description, address, valB, address2);
        }
    }

    CalibReportImportIHex(File file) {
        super(file);
    }

    @Override
    public int compareReportElements(CalibrationReport<ReportLineTypeIHex>.ReportLine item1,
            CalibrationReport<ReportLineTypeIHex>.ReportLine item2) {
        return item1.lineType.compareTo(item2.lineType);
    }

    @Override
    public boolean isOkElement(ReportLineTypeIHex item) {
        return (item.compareTo(ReportLineTypeIHex.IHEX_CALIB_IMPORT_OK) == 0) ? true : false;
    }

    @Override
    public String getReportHeaderLine() {
        String header = String.format("%d of %d calibration items successfully imported from file: %s\n",
                super.getOkLineCount(), super.getTotalLineCount(), super.getReportFile().getName());
        return header;
    }

    @Override
    public String getReportRoundup() {
        return "";
    }

}
