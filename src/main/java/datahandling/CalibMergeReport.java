/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.File;

public class CalibMergeReport extends CalibrationReport<CalibMergeReport.ReportLineType> {

    private static final String FONT_COLOR_RED = "<font color=\"red\">";
    private static final String FONT_COLOR_END = "</font>";
    private String reportHeader = "";
    private String mergeTypeText = "";
    private String calibrationName = "";
    private String epromId = "";

    enum ReportLineType {
        CALIB_MERGE_OK("Item successfully copied."),
        ITEM_ADDR_NOT_FOUND(FONT_COLOR_RED
                + "Item address: 0x%08X, size %d not found in base file at address 0x%08X. Data not copied."
                + FONT_COLOR_END),
        ITEM_NAME_NOT_FOUND(FONT_COLOR_RED + "Item name not found in ASAP2 data. Data not copied." + FONT_COLOR_END),
        GAP_IN_BASEFILE(
                FONT_COLOR_RED + "No adjacent address 0x%08X found in consecutive lines base file. Data not copied."
                + FONT_COLOR_END),
        RANGE_NOT_FOUND(FONT_COLOR_RED
                + "Item start address (0x%08X) OK, End address (0x%08X) not found in data. Data not copied."
                + FONT_COLOR_END),
        ITEM_INCORRECT_DATA_SIZE(
                FONT_COLOR_RED + "Item data size (%d) does not match ASAP2 data size. Data not copied."
                + FONT_COLOR_END),;

        // enum constructor
        ReportLineType(String line) {
            this.description = line;
        }

        private String description;

        public String formatDescription() {
            return description;
        }

        public String formatDescription(long valA) {
            return String.format(description, valA);
        }

        public String formatDescription(long valA, int valB) {
            return String.format(description, valA, valB);
        }

        public String formatDescription(long valA, long valB) {
            return String.format(description, valA, valB);
        }

        public String formatDescription(long valA, int valB, long address2) {
            return String.format(description, valA, valB, address2);
        }
    }

    public CalibMergeReport(File file, String mergeTypeText, String calibName, String epromId) {
        super(file);
        this.mergeTypeText = mergeTypeText;
        this.calibrationName = calibName;
        this.epromId = epromId;
    }

    @Override
    public int compareReportElements(ReportLine item1, ReportLine item2) {
        int iRtn = item1.lineType.compareTo(item2.lineType);
        if (iRtn == 0) {
            iRtn = item1.name.compareTo(item2.name);
        }
        return iRtn;
    }

    @Override
    public boolean isOkElement(ReportLineType item) {
        return item.compareTo(ReportLineType.CALIB_MERGE_OK) == 0;
    }

    public void setReportHeaderLine(String hdr) {
        reportHeader = hdr;
    }

    @Override
    public String getReportHeaderLine() {
        return reportHeader;
    }

    @Override
    public String getReportRoundup() {
        if (super.getOkLineCount() == super.getTotalLineCount()) {
            return String.format("%d of %d items successfully merged with application basefile: %s\n",
                    super.getOkLineCount(), super.getTotalLineCount(), super.getReportFile().getName());

        } else {
            return String.format(FONT_COLOR_RED + "Warning merged data: %d of %d items have not been merged"
                    + FONT_COLOR_END + "\n", super.getTotalLineCount() - super.getOkLineCount(),
                    super.getTotalLineCount());
        }
    }

}
