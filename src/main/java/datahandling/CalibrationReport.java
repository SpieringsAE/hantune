/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class CalibrationReport<E> {

    class ReportLine {

        E lineType;
        String name;
        String line;

        ReportLine(E type, String name, String line) {
            this.lineType = type;
            this.name = name;
            this.line = line;
            if (isOkElement(lineType)) {
                OkLineCount++;
            }

        }
    }

    private List<ReportLine> reportLines;
    private int OkLineCount;
    private File reportFile;

    public File getReportFile() {
        return reportFile;
    }

    CalibrationReport(File file) {
        reportLines = new ArrayList<ReportLine>();
        OkLineCount = 0;
        reportFile = file;
    }

    public void addReportLine(E rlt, String itemName, String reportLine) {
        ReportLine obj = new ReportLine(rlt, itemName, reportLine);
        reportLines.add(obj);
    }

    public List<ReportLine> getSortedReportItems() {
        Collections.sort(reportLines, new Comparator<ReportLine>() {
            @Override
            public int compare(ReportLine item1, ReportLine item2) {
                return compareReportElements(item1, item2);
            }
        });

        return reportLines;
    }

    public int getOkLineCount() {
        return OkLineCount;
    }

    public int getTotalLineCount() {
        return reportLines.size();
    }

    public String buildImportReport() {
        return buildImportReport("\n");
    }

    public String buildImportReport(String lineSeparator) {
        List<ReportLine> items = getSortedReportItems();

        StringBuilder lines = new StringBuilder();

        for (ReportLine item : items) {
            lines.append(item.name);
            lines.append(": ");
            lines.append(item.line);
            lines.append(lineSeparator);
        }

        return getReportHeaderLine() + lineSeparator + lines.toString() + getReportRoundup();
    }

    public abstract int compareReportElements(ReportLine item1, ReportLine item2);

    public abstract String getReportHeaderLine();

    public abstract String getReportRoundup();

    public abstract boolean isOkElement(E item1);
}
