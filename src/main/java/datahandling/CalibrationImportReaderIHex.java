/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.File;
import java.util.TreeMap;

import ASAP2.ASAP2Characteristic;
import HANtune.CustomFileChooser.FileType;
import haNtuneHML.Calibration;
import haNtuneHML.Calibration.ASAP2;
import util.Util;

public class CalibrationImportReaderIHex extends CalibrationImportLineReader {

    private static final int IHEX_RADIX = 16;

    private CalibReportImportIHex report;

    private int currentParseBaseAddr = 0;
    private boolean swapDataBytes = false; // Only swap data bytes if input is 'INTEL' order: little-endian

    /**
     * Reader implementation for HEX file
     *
     * @param EpromId holds eprom ID to be compared with eprom ID in file
     * @param addressEpk holds address where eprom ID of file is located
     * @param ramFlashOffset ofsset from RAM variable to flash location
     * @param swapDataBytes holds true if bytes need to be swapped for correct
     * endianness
     *
     */
    public CalibrationImportReaderIHex(String EpromId, long addressEpk, long ramFlashOffset,
            boolean swapDataBytes) {
        super(EpromId, addressEpk, ramFlashOffset, FileType.IHEX_FILE);
        this.swapDataBytes = swapDataBytes;
    }

    public ImportReaderResult transferDataIntoCalibration(Calibration calib,
            TreeMap<String, ASAP2Characteristic> characteristics) {

        // Iterate all memory addresses of each calibration element
        for (int i = 0; i < calib.sizeOfASAP2Array(); i++) {
            ASAP2 calibItem = calib.getASAP2Array(i);
            if (calibItem != null) {
                String calibrationParameterName = calibItem.getName();
                ASAP2Characteristic charact = characteristics.get(calibrationParameterName);

                if (charact != null) {
                    // Search all iHex for contiguous data of size and starting at address
                    long calibAddress = Integer.toUnsignedLong(charact.getAddress());
                    long calibAddressIhex = calibAddress + getRamFlashOffset();

                    int calibSize = calibItem.getByteArrayValue().length;

                    byte[] calibBytes = getLineDataContainer().getData(calibAddressIhex, calibSize);

                    // if found, then place into calibration
                    if (calibBytes != null) {
                        if (swapDataBytes) {
                            calibBytes = Util.swapEndianness(calibBytes, charact.getDatatypeSize());
                        }

                        calibItem.setByteArrayValue(calibBytes);
                        report.addReportLine(CalibReportImportIHex.ReportLineTypeIHex.IHEX_CALIB_IMPORT_OK,
                                calibrationParameterName,
                                CalibReportImportIHex.ReportLineTypeIHex.IHEX_CALIB_IMPORT_OK.getDescription());
                    } else {
                        report.addReportLine(CalibReportImportIHex.ReportLineTypeIHex.IHEX_CALIB_NOT_FOUND,
                                calibrationParameterName, CalibReportImportIHex.ReportLineTypeIHex.IHEX_CALIB_NOT_FOUND
                                        .getDescription(calibAddress, calibSize, calibAddressIhex));
                    }
                } else {
                    report.addReportLine(CalibReportImportIHex.ReportLineTypeIHex.IHEX_CALIB_NAME_NOT_FOUND,
                            calibrationParameterName,
                            CalibReportImportIHex.ReportLineTypeIHex.IHEX_CALIB_NAME_NOT_FOUND.getDescription());
                }

            }
        }

        return ImportReaderResult.READER_RESULT_OK;

    }

    protected LineDataObject parseSingleLine(String line) {
        if (line.startsWith(":")) {
            return extractIHexObject(line);
        }

        return null;
    }

    protected void createImportReport(File file) {
        report = new CalibReportImportIHex(file);
    }

    private IHexObject extractIHexObject(String line) {
        int recordType = Integer.parseUnsignedInt(line.substring(7, 9));

        if (recordType == IHexObject.RecordType.IHEX_DATA.getValue()) {
            return parseIHexData(line);
        } else if (recordType == IHexObject.RecordType.IHEX_EXTENDED_LINEAR_ADDRESS.getValue()) {
            return parseIHexExtLinAddress(line);
        }

        return null;
    }

    private IHexObject parseIHexExtLinAddress(String line) {
        // S BC Addr RT 02DataBytes Chk
        // : 02 0000 04 8002 78

        // check Checksum at end of line
        if (!isChecksumOK(line)) {
            return null;
        }

        // check line length (fixed)
        // Line length: 1 pos ':', 2 pos ByteCount, 4 pos address, 2 pos RecType, 4 pos databytes, 2 pos chk
        // => 15 pos
        if (line.length() != 15) {
            return null;
        }

        // get Extended Linear Address base. Global value, so it can be used during parsing Data
        currentParseBaseAddr = Integer.parseUnsignedInt(line.substring(9, 13), IHexObject.IHEX_RADIX);

        return new IHexObject(currentParseBaseAddr);
    }

    private IHexObject parseIHexData(String line) {
        // S BC Addr RT -------0x10 DataBytes----------- Chk
        // : 10 0000 00 00000000A000000023C29146005F0580 B0

        // check Checksum at end of line
        if (!isChecksumOK(line)) {
            return null;
        }

        int dataByteCount = Integer.parseUnsignedInt(line.substring(1, 3), IHexObject.IHEX_RADIX);

        // check line length
        // Line length: 1 pos ':', 2 pos ByteCount, 4 pos address, 2 pos RecType, 2 pos checksum => 11 pos
        // + 2 * dataByteCount
        if (line.length() != (dataByteCount * 2) + 11) {
            return null;
        }

        // get offset address value
        int offsetAddress = Integer.parseUnsignedInt(line.substring(3, 7), IHexObject.IHEX_RADIX);

        // fill data bytes
        byte[] data = new byte[dataByteCount];
        int offset = 9;
        for (int iCnt = 0; iCnt < dataByteCount; iCnt++) {
            data[iCnt] = (byte) Short.parseShort(line.substring(offset, offset + 2), IHexObject.IHEX_RADIX);
            offset += 2;
        }

        return new IHexObject(currentParseBaseAddr, offsetAddress, data);
    }

    boolean isChecksumOK(String line) {
        int numPairs = ((line.length() - 1) / 2); // Skip ':' at start
        int offset = 1;
        int checkSum = 0;
        for (int iCnt = 0; iCnt < numPairs; iCnt++) {
            checkSum += Short.parseShort(line.substring(offset, offset + 2), IHEX_RADIX);
            offset += 2;
        }

        return (byte) checkSum == 0x00;
    }

    @Override
    public String getImportReport() {
        return report.buildImportReport();
    }

}
