/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import static HANtune.HANtuneWindowFactory.HANtuneEditorType;
import static HANtune.HANtuneWindowFactory.HANtuneTableEditorType;

import java.util.HashSet;

import ASAP2.ASAP2AxisPts;
import ASAP2.ASAP2Characteristic;
import ASAP2.ASAP2CompuMethod;
import ASAP2.ASAP2Data;
import ASAP2.ASAP2Measurement;
import ASAP2.ASAP2ModPar;
import ASAP2.ASAP2RecordLayout;
import HANtune.HANtuneWindowFactory.HANtuneWindowType;
import asap2XmlData.Asap2Datatype;
import asap2XmlData.Asap2ModPar;
import asap2XmlData.Asap2ObjType;
import asap2XmlData.Asap2XmlData;
import asap2XmlData.Type;
import haNtuneHML.Daqlist;
import haNtuneHML.Daqlist.Signal;
import haNtuneHML.Layout;
import haNtuneHML.Layout.Tab;
import haNtuneHML.Layout.Tab.Window;
import haNtuneHML.Layout.Tab.Window.ASAP2;
import haNtuneSHML.ShmlDocument.Shml;

/*
 * Class to convert ASAP2 data to Shml xml data, filtering elements found in Layout
 */
public class ASAP2ConvertToXmlImpl implements ConvertToXml<ASAP2Data, Shml, Layout> {
    private HashSet<String> charactNameSet; // collects names of the relevant characteristics to be placed
                                            // in asapXml. Relevant: displayed in layout
    private HashSet<String> measrmntNameSet; // collects names of relevant measurements. Relevant:
                                             // name found in DAQ lists

    // ASAP2 items related to relevant characteristics/measurements
    private HashSet<String> compuMethodNameSet; // holds names of relevant compu methods.
    private HashSet<String> recordLayoutNameSet; // holds names of relevant record layouts
    private HashSet<String> axisPtsNameSet; // holds names of relevant axis pts


    public ASAP2ConvertToXmlImpl() {
        // keep names unique, use a set
        charactNameSet = new HashSet<String>();
        measrmntNameSet = new HashSet<String>();
        compuMethodNameSet = new HashSet<String>();
        recordLayoutNameSet = new HashSet<String>();
        axisPtsNameSet = new HashSet<String>();
    }


    /**
     * Filter relevant ASAP2 data (from a2l) referenced in Shml layout into Asap2XmlData
     *
     * @param as2 contains all ASAP2Data
     * @param sh contains Asap2XmlData except for relevant asap2 data
     * @return true: relevant ASAP2 data has been added to Shml. Data is relevant to filter, when layout in
     *         Shml references ASAP2 data
     */
    public Boolean filterRelevantDataIntoXml(ASAP2Data as2, Shml sh, Layout lt) {
        if (as2 == null || sh == null || lt == null) {
            return false;
        }

        Asap2XmlData asapXml = Asap2XmlData.Factory.newInstance();

        // Search calibration items in layout and place them in charactNameSet
        HANtuneWindowType htWtype;
        for (Tab tElem : lt.getTabList()) {
            for (Window wElem : tElem.getWindowList()) {
                htWtype = HANtuneWindowType.valueOf(wElem.getType());

                if (HANtuneEditorType.contains(htWtype) || HANtuneTableEditorType.contains(htWtype)) {
                    for (ASAP2 aElem : wElem.getASAP2List()) {
                        charactNameSet.add(aElem.getName());
                    }
                }

            }
        }

        // Search all daqlist items and place them also in measrmntNameSet
        for (Daqlist dqlist : sh.getDaqlistList()) {
            for (Signal sgnl : dqlist.getSignalList()) {
                measrmntNameSet.add(sgnl.getName());
            }
        }

        createRelevantCharacteristics(as2, asapXml, charactNameSet);
        createRelevantMeasurements(as2, asapXml, measrmntNameSet);
        createRelevantAxisPts(as2, asapXml, axisPtsNameSet);
        createRelevantRecordLayouts(as2, asapXml, recordLayoutNameSet);
        createRelevantCompuMethods(as2, asapXml, compuMethodNameSet);
        createModPar(as2, asapXml);
        sh.setASAP2Data(asapXml);

        return true;
    }


    private void createRelevantCharacteristics(ASAP2Data as2, Asap2XmlData asapXml,
                                               HashSet<String> nameSet) {
        for (String str : nameSet) {
            ASAP2Characteristic a2Char = as2.getASAP2Characteristics().get(str);
            if (a2Char != null) {
                asap2ToAsap2XmlCharacteristic(asapXml.addNewCharacteristics(), a2Char);

                this.compuMethodNameSet.add(a2Char.getConversion()); // Collect relevant compuMethods
                for (String axStr : a2Char.getAxisDescr()) {
                    axisPtsNameSet.add(axStr);// Collect relevant axisPoints
                }

            }
        }
    }


    private void createRelevantMeasurements(ASAP2Data as2, Asap2XmlData asapXml, HashSet<String> nameSet) {
        for (String str : nameSet) {
            ASAP2Measurement msrmnt = as2.getMeasurementByName(str);
            if (msrmnt != null) {
                asap2ToAsap2XmlMeasurement(asapXml.addNewMeasurements(), msrmnt);

                compuMethodNameSet.add(msrmnt.getConversion()); // Collect relevant compuMethods
            }
        }
    }


    private void createRelevantAxisPts(ASAP2Data as2, Asap2XmlData asapXml, HashSet<String> nameSet) {
        for (String str : nameSet) {
            ASAP2AxisPts axis = as2.getASAP2AxisPts().get(str);
            if (axis != null) {
                asap2ToAsap2XmlAxisPts(asapXml.addNewAxisPts(), axis);

                compuMethodNameSet.add(axis.getConversion()); // Collect relevant compuMethods
            }
        }
    }


    private void createRelevantCompuMethods(ASAP2Data as2, Asap2XmlData asapXml, HashSet<String> nameSet) {
        for (String str : nameSet) {
            ASAP2CompuMethod mtd = as2.getASAP2CompuMethods().get(str);
            if (mtd != null) {
                asap2ToAsap2XmlCompuMethod(asapXml.addNewCompuMethod(), mtd);
            }
        }
    }


    private void createRelevantRecordLayouts(ASAP2Data as2, Asap2XmlData asapXml, HashSet<String> nameSet) {
        for (String str : nameSet) {
            ASAP2RecordLayout mtd = as2.getASAP2RecordLayouts().get(str);
            if (mtd != null) {
                asap2ToAsap2XmlRecordLayout(asapXml.addNewRecordLayout(), mtd);
            }
        }
    }


    private void asap2ToAsap2XmlCharacteristic(asap2XmlData.Asap2Characteristic aXml,
                                               ASAP2Characteristic a2) {
        aXml.setType(Type.Enum.forString(a2.getType().toString()));
        aXml.setName(a2.getName());
        aXml.setDescription(a2.getDescription());
        aXml.setDatatype(Asap2Datatype.Enum.forString(a2.getDatatype().toString()));
        aXml.setAddress(a2.getAddress());

        aXml.setDeposit(a2.getDeposit());
        recordLayoutNameSet.add(a2.getDeposit());

        aXml.setMaxDiff(a2.getMaxDiff());
        aXml.setConversion(a2.getConversion());
        aXml.setMinimum(a2.getMinimum());
        aXml.setMaximum(a2.getMaximum());
        aXml.setLowerLimitEnabled(a2.isLowerLimitEnabled());
        aXml.setUpperLimitEnabled(a2.isUpperLimitEnabled());
        aXml.setColumnCount(a2.getColumnCount());
        aXml.setRowCount(a2.getRowCount());

        aXml.setUnit(a2.getUnit());
        aXml.setSource(a2.getSource());
        aXml.setFactor(a2.getFactor());
        aXml.setDecFormat(a2.getDecFormat());
        if (a2.getAxisColumnDescr() != null)
            aXml.setAxisColumnDescr(a2.getAxisColumnDescr());
        if (a2.getAxisRowDescr() != null)
            aXml.setAxisRowDescr(a2.getAxisRowDescr());
    }


    private void asap2ToAsap2XmlMeasurement(asap2XmlData.Asap2Measurement mXml, ASAP2Measurement a2) {
        mXml.setASAP2ObjType(Asap2ObjType.Enum.forString(a2.getASAP2ObjType().toString()));
        mXml.setName(a2.getName());
        mXml.setDescription(a2.getDescription());
        mXml.setDatatype(Asap2Datatype.Enum.forString(a2.getDatatype().toString()));
        mXml.setConversion(a2.getConversion());
        mXml.setResolution(a2.getResolution());
        mXml.setAccuracy(a2.getAccuracy());
        mXml.setMinimum(a2.getMinimum());
        mXml.setMaximum(a2.getMaximum());
        mXml.setLowerLimitEnabled(a2.isLowerLimitEnabled());
        mXml.setUpperLimitEnabled(a2.isUpperLimitEnabled());
        mXml.setAddress(a2.getAddress());
    }


    private void asap2ToAsap2XmlAxisPts(asap2XmlData.Asap2AxisPts ptXml, ASAP2AxisPts a2) {
        ptXml.setType(Type.AXIS_PTS);

        ptXml.setName(a2.getName());
        ptXml.setDescription(a2.getDescription());
        ptXml.setAddress(a2.getAddress());
        ptXml.setInputQuantity(a2.getInputQuantity());
        ptXml.setDeposit(a2.getDeposit());
        recordLayoutNameSet.add(a2.getDeposit());

        ptXml.setMaxDiff(a2.getMaxDiff());
        ptXml.setConversion(a2.getConversion());
        ptXml.setMaxAxisPoints(a2.getMaxAxisPoints());
        ptXml.setColumnCount(a2.getColumnCount());
        ptXml.setMinimum(a2.getMinimum());
        ptXml.setLowerLimitEnabled(a2.isLowerLimitEnabled());
        ptXml.setMaximum(a2.getMaximum());
        ptXml.setUpperLimitEnabled(a2.isUpperLimitEnabled());
    }


    private void asap2ToAsap2XmlCompuMethod(asap2XmlData.Asap2CompuMethod cmXml, ASAP2CompuMethod a2) {
        cmXml.setASAP2ObjType(Asap2ObjType.COMPU_METHOD);

        cmXml.setName(a2.getName());
        cmXml.setDescription(a2.getDescription());
        cmXml.setConversionType(a2.getConversionType());
        cmXml.setDecimals(a2.getDecimalCount());
        cmXml.setConversion(a2.getConversion());
        cmXml.setUnit(a2.getUnit());
        cmXml.setCoeffsArray(a2.getCoeffs());
    }


    private void asap2ToAsap2XmlRecordLayout(asap2XmlData.Asap2RecordLayout rlXml, ASAP2RecordLayout a2) {

        rlXml.setName(a2.getName());
        rlXml.setPosition(a2.getPosition());
        rlXml.setDatatype(Asap2Datatype.Enum.forString(a2.getDatatype().toString()));
        rlXml.setIndexMode(a2.getIndexMode());
        rlXml.setAddresstype(a2.getAddresstype());

        rlXml.setIndexIncr(a2.getIndexIncr());
        rlXml.setAddressing(a2.getAddressing());
    }


    private void createModPar(ASAP2Data as2, Asap2XmlData asapXml) {
        Asap2ModPar xmlMp = asapXml.addNewModPar();
        ASAP2ModPar as2Mp = as2.getModPar();

        if (as2Mp.isAddrEpkPresent()) {
            xmlMp.setAddrEpk(as2Mp.getAddrEpk());
        }
        if (as2Mp.getCpuType() != null) {
            xmlMp.setCpuType(as2Mp.getCpuType());
        }
        if (as2Mp.getEcu() != null) {
            xmlMp.setEcu(as2Mp.getEcu());
        }
        if (as2Mp.isRamFlashOffsetPresent()) {
            xmlMp.setRamFlashOffset(as2Mp.getRamFlashOffset());
        }
        xmlMp.setEpk(as2.getEpromID()); // special
    }

}
