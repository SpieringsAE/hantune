/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Scanner;

import ErrorLogger.AppendToLogfile;
import HANtune.CustomFileChooser.FileType;

public abstract class CalibrationImportLineReader implements CalibrationImportReader {

    private long ramFlashOffset;
    private long addressEpk;
    private String epromId;
    private FileType fType;
    private LineDataObjectContainer lineDataContainer;
    private String resultMessage = "";
    private String resultText = "";

    protected abstract LineDataObject parseSingleLine(String line);

    protected abstract void createImportReport(File file);

    LineDataObjectContainer getLineDataContainer() {
        return lineDataContainer;
    }

    CalibrationImportLineReader(String epromId, long addressEpk, long ramFlashOffset, FileType fType) {
        this.epromId = epromId;
        this.addressEpk = addressEpk;
        this.ramFlashOffset = ramFlashOffset;
        this.fType = fType;
    }

    // read line Objects from file
    public ImportReaderResult readCalibration(File file) throws IOException {

        // first: read all data lines from file and put in lineData
        lineDataContainer = readAllDataLines(file);
        return checkTargetDataEpromId(file);
    }

    private ImportReaderResult checkTargetDataEpromId(File file) {
        if (lineDataContainer.containerElementsIsEmpty()) {
            resultMessage
                    = "No items found in file: " + file.getName() + "\n\n" + "Report cannot be created";
            resultText = resultMessage;
            return ImportReaderResult.READER_RESULT_ERROR;
        }

        String epromIdSRec = getEpromIdStrFromObj(addressEpk, epromId.length());
        if (epromIdSRec == null) {
            resultMessage
                    = "MOD_PAR Eprom Id not found in basefile at address: 0x" + Long.toHexString(addressEpk)
                    + "\n" + "Basefile name: " + file.getName() + "\n\n" + "Report cannot be created";
            resultText = resultMessage;
            return ImportReaderResult.READER_RESULT_ERROR;
        }

        if (!epromId.equals(epromIdSRec)) {
            resultText = "Warning: Current ASAP2 application ID does not match the application ID "
                    + "from the " + "basefile just opened.\n" + " - Basefile name: " + file.getName() + "\n"
                    + " - Basefile Application ID: " + epromIdSRec + "\n" + " - ASAP2 Application ID:   "
                    + epromId;
            resultMessage = resultText + "\n\n" + "Do you wish to continue?";

            return ImportReaderResult.READER_RESULT_WARNING;
        }
        resultText = "Application ID: " + epromIdSRec;
        return ImportReaderResult.READER_RESULT_OK;
    }

    public FileType getFileType() {
        return fType;
    }

    public long getRamFlashOffset() {
        return ramFlashOffset;
    }

    public void setRamFlashOffset(long ramFlashOffset) {
        this.ramFlashOffset = ramFlashOffset;
    }

    public String getReadResultMessage() {
        return resultMessage;
    }

    public String getReadResultText() {
        return resultText + "\n";
    }

    protected LineDataObjectContainer readAllDataLines(File file) throws FileNotFoundException {
        createImportReport(file);
        LineDataObjectContainer dl = new LineDataObjectContainer();

        try (Scanner importFile = new Scanner(file)) {
            importFile.forEachRemaining(l -> {
                String line = l.trim();
                LineDataObject obj = parseSingleLine(line);
                if (obj != null) {
                    dl.addDataObject(obj);
                }
            });
        }

        return dl;
    }

    protected String getEpromIdStrFromObj(long address, int size) {
        byte[] bytes = lineDataContainer.getData(address, size);
        if (bytes == null || bytes.length == 0) {
            return null;
        }
        // look for 0 in the bytes
        int strLen = bytes.length;
        for (int idx = 0; idx < bytes.length; idx++) {
            if (bytes[idx] == '\0') {
                strLen = idx;
                break;
            }
        }
        byte[] strBytes = new byte[strLen];
        System.arraycopy(bytes, 0, strBytes, 0, strLen);

        try {
            return new String(strBytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            return null;
        }
    }

}
