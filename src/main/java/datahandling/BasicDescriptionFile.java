/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.util.ArrayList;
import java.util.List;

/**
 * A basic implementation of DescriptionFile. Can only be extended. A subclass
 * must implement getReferences() to provide this class with a generic list of
 * references.
 *
 * @author Michiel Klifman
 */
public abstract class BasicDescriptionFile implements DescriptionFile {

    private String name;
    private String path;

    public BasicDescriptionFile() {
    }

    public BasicDescriptionFile(String name, String path) {
        this.name = name;
        this.path = path;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public abstract List<? extends Reference> getReferences();

    @Override
    public <T extends Reference> List<T> getReferencesOfType(Class<T> type) {
        List<T> references = new ArrayList<>();
        for (Reference reference : getReferences()) {
            if (type.isInstance(reference)) {
                references.add(type.cast(reference));
            }
        }
        return references;
    }

    @Override
    public <T extends Reference> T getReferenceOfType(Reference reference, Class<T> type) {
        for (T t : getReferencesOfType(type)) {
            if (compareReferences(reference, t)) {
                return t;
            }
        }
        return null;
    }

    @Override
    public <T extends Reference> boolean hasReferenceOfType(Reference reference, Class<T> type) {
        for (T t : getReferencesOfType(type)) {
            if (compareReferences(reference, t)) {
                return true;
            }
        }
        return false;
    }

    public static boolean compareReferences(Reference reference1, Reference reference2) {
        if (reference1 == null || reference1.getName() == null) {
            return false;
        } else {
            return reference1.getName().equals(reference2.getName());
        }
    }
}
