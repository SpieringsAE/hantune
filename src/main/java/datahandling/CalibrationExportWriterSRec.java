/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import haNtuneHML.Calibration;

public class CalibrationExportWriterSRec implements CalibrationExportWriter {

    static final int MAX_SREC_HEADER_DATASIZE = 251;
    static final int MAX_DATABYTES_PER_SRECLINE = 32;

    @Override
    public void writeCalibration(Calibration calibration, File file) throws IOException {
        // No export of calibration ONLY possible. Always needs to be merged with target application
        // therefore not implemented
    }

    public void writeToFile(String headerString, File outFile, List<LineDataObject> srecObjs)
            throws IOException {
        if (!srecObjs.isEmpty()) {
            BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));

            writeHeader(outFile.getName(), headerString, bw);
            writeItems(srecObjs, bw);
            writeFooter(bw);

            bw.close();
        }
    }

    public String getWarningLines(){
        // not used, empty
        return "";
    }

    private void writeHeader(String fileLocation, String calibName, BufferedWriter w) throws IOException {
        String headerTxt = fileLocation + " " + calibName;
        String outStr = createS0String(headerTxt);
        w.write(outStr);

    }

    private void writeItems(List<LineDataObject> srecObjs, BufferedWriter w) throws IOException {
        for (LineDataObject sRecObject : srecObjs) {
            w.write(createS3String((SRecObject) sRecObject));
        }

    }

    private void writeFooter(BufferedWriter w) throws IOException {
        String outStr = createS7String();
        w.write(outStr);

    }

    private String createS0String(String headerText) {
        StringBuilder lines = new StringBuilder();
        int size = headerText.length();
        if (size > MAX_SREC_HEADER_DATASIZE) {
            size = MAX_SREC_HEADER_DATASIZE;
        }
        String headerStr = headerText.substring(0, size);

        lines.append(String.format("S0%02X0000", size + 3)); // 3: 2 addr bytes, 1 checksum byte

        byte[] strBytes = headerStr.getBytes();

        for (int iCnt = 0; iCnt < size; iCnt++) {
            lines.append(String.format("%02X", strBytes[iCnt]));
        }

        lines.append(String.format("%02X", SRecObject.calculateHeaderChecksum(strBytes)));
        lines.append(LF);

        return lines.toString();
    }

    private String createS7String() {
        return "S705000000FA" + LF;  // Assume only calibration, no starting point
    }

    private String createS3String(SRecObject obj) {
        StringBuilder lines = new StringBuilder();
        byte[] data = obj.getData();

        // conform to max line length. Create multiple SREC lines if necessary
        for (int strtIdx = 0; strtIdx < data.length; strtIdx += MAX_DATABYTES_PER_SRECLINE) {
            int size = data.length - strtIdx;

            if (size > MAX_DATABYTES_PER_SRECLINE) {
                size = MAX_DATABYTES_PER_SRECLINE;
            }

            // build the output line,
            lines.append(String.format("S3%02X%08X", size + SRecObject.EXTRA_SIZE_S3_ADDRESS_CHECKSUM,
                    (int) obj.getStartAddress() + strtIdx));

            for (int iCnt = 0; iCnt < size; iCnt++) {
                lines.append(String.format("%02X", data[iCnt + strtIdx]));
            }

            lines.append(String.format("%02X", obj.calculateS3Checksum(strtIdx, size)));
            lines.append(LF);
        }

        return lines.toString();
    }

}
