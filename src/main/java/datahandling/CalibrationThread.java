/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package datahandling;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import ASAP2.ASAP2Characteristic;
import ErrorLogger.defaultExceptionHandler;
import haNtuneHML.Calibration.ASAP2;
import util.MessagePane;
import util.ProgressDialog;
import util.Util;

/**
 * This class extends Thread to calibrate to the ECU in a seperate thread.
 *
 * @author Mike
 */
public class CalibrationThread extends Thread {
    private static final int PROGRESSINCREMENT = 1;
    private static final int INITIALPROGRESSVALUE = 0;
    private static final String LOADING_DIALOG_TITLE = "Loading a Calibration.";

    private String progressDialogTitle = LOADING_DIALOG_TITLE;
    private CurrentConfig currentConfig = CurrentConfig.getInstance();
    private ProgressDialog progressDialog;
    private Timer timer;
    private int progress;
    private int id;
    private List<String> warningLines = new ArrayList<>();


    /**
     * Constructor sets the given id and progressDialog.
     *
     * @param aID             :The id of the calibration.
     * @param aProgressDialog :The progressDialog needed to update the progress.
     */
    public CalibrationThread(final int aID, final ProgressDialog aProgressDialog) {
        this.id = aID;
        this.progressDialog = aProgressDialog;
        timer = new Timer(true);
        startDynamicTextTimer();

    }

    @Override
    public void run() {
        Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());

        progress = INITIALPROGRESSVALUE;

        // loop through asap2 elements
        for (ASAP2 asap2 : currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id).getASAP2List()) {
            updateProgress();

            if (progressDialog.isCalibrationCancelled()) {
                break;
            }
            ASAP2Characteristic asap2Characteristic = CurrentConfig.getInstance().getASAP2Data()
                .getASAP2Characteristics().get(asap2.getName());
            if (asap2Characteristic != null) {
                handleAsap2Data(asap2.getByteArrayValue(), asap2Characteristic);
            } else {
                warningLines.add(asap2.getName() + ": ASAP2 characteristic not found");
            }
        }
        finishCalibration();
    }

    /**
     * This method updates the progressBar by adding the PROGRESSINCREMENT to the progressBar value.
     */
    private void updateProgress() {
        progressDialog.updateProgress(progress);
        progress += PROGRESSINCREMENT;
    }

    /**
     * This method finishes the calibration.
     * It will dispose the progressDialog, enable HANtune and update the last modified date.
     */
    private void finishCalibration() {
        progressDialog.deconstruct();
        timer.cancel();

        // update last modified date
        currentConfig.getHANtuneDocument().getHANtune().getCalibrationArray(id).setDateMod(Calendar.getInstance());

        if (!warningLines.isEmpty()) {
            MessagePane.showWarning(getWarningLines());
        }

    }

    private String getWarningLines() {
        if (warningLines.isEmpty()) {
            return "";
        } else {
            StringBuilder warn = new StringBuilder(
                "<html><b>Warning: " + warningLines.size() +
                    " calibration item(s) could not be loaded into Working Parameter Set:</b></html>\n");
            warningLines.forEach(e -> warn.append(" - " + e + "\n"));
            return warn.toString();
        }
    }


    /**
     * This method handles data according to characteristic
     *
     * @param dataBytes:      actual data (in array of chars)
     * @param characteristic: The asap characteristic for the actual data
     */
    private void handleAsap2Data(final byte[] dataBytes, final ASAP2Characteristic characteristic) {
        if (dataBytes.length <= 0 || !characteristic.checkDataSize(dataBytes.length)) {
            warningLines.add(characteristic.getName() + ": ASAP2 data length doesn't match");
            return;
        }

        char[] dataChars = Util.byteA2charA(dataBytes, dataBytes.length);
        switch (characteristic.getType()) {
            case VALUE:
                handleObjectValue(dataChars, characteristic);
                break;

            case AXIS_PTS:
            case CURVE:
                handleObjectCurve(dataChars, characteristic);
                break;

            case MAP:
                handleObjectMap(dataChars, characteristic);
                break;

            default:
                warningLines.add(characteristic.getName() + ": Unsupported type " + characteristic.getType());
                break;
        }
    }

    /**
     * This method converts the data to a value object and sets the parameter value.
     *
     * @param dataChars:      actual data (in array of chars)
     * @param characteristic: The asap characteristic for the actual data
     */
    private void handleObjectValue(char[] dataChars, ASAP2Characteristic characteristic) {
        Object value = characteristic.convertValue(dataChars);
        currentConfig.getHANtuneManager().getSendParameterDataHandler().setParameter(characteristic.getName(),
            value, null, true);
    }

    /**
     * Converts the AXIS_PTS or CURVE data to a value object and sets the parameter values.
     *
     * @param dataChars:      actual data (in array of chars)
     * @param characteristic: The asap characteristic for the actual data
     */
    private void handleObjectCurve(char[] dataChars, ASAP2Characteristic characteristic) {
        for (int j = 0; j < characteristic.getColumnCount(); j++) {
            int[] position = {j};
            char[] value = new char[characteristic.getDatatypeSize()];
            System.arraycopy(dataChars, j * value.length, value, 0, value.length);
            Object val = characteristic.convertValue(value);
            currentConfig.getHANtuneManager().getSendParameterDataHandler().setParameter(characteristic.getName(), val,
                position, true);
        }
    }

    /**
     * Converts the MAP data to a value object and sets the parameter values.
     *
     * @param dataChars:      actual data (in array of chars)
     * @param characteristic: The asap characteristic for the actual data
     */
    private void handleObjectMap(char[] dataChars, ASAP2Characteristic characteristic) {

        for (int j = 0; j < characteristic.getColumnCount(); j++) {
            for (int k = 0; k < characteristic.getRowCount(); k++) {
                int[] position = {j, k};
                char[] value =
                    new char[characteristic.getDatatypeSize()];
                System.arraycopy(dataChars, (j * characteristic.getRowCount() + k) * value.length, value, 0,
                    value.length);
                Object val = characteristic.convertValue(value);
                currentConfig.getHANtuneManager().getSendParameterDataHandler().setParameter(characteristic.getName(),
                    val, position, true);
            }
        }
    }

    /**
     * This method starts a timerTask to update the title.
     * When started the timer will keep updating at a frequency of 1 second.
     */
    private void startDynamicTextTimer() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
                progressDialog.setTitle(progressDialogTitle);

                if (progressDialogTitle.contains("...")) {
                    progressDialogTitle = LOADING_DIALOG_TITLE;
                } else {
                    progressDialogTitle = progressDialogTitle + ".";
                }
            }
        }, 0, 1000);
    }
}
