package peak.can.basic;

/**
 * Defines the point of time at which a CAN message was received.
 *
 * @version 1.0
 * @LastChange $Date: 2017-07-05 10:31:00 +0200 (Wed, 05 Jul 2017) $
 * @author Jonathan Urban/Uwe Wilhelm/Fabrice Vergnaud
 *
 * @Copyright (C) 1999-2014  PEAK-System Technik GmbH, Darmstadt
 * more Info at http://www.peak-system.com
 */
public class TPCANTimestampFD
{
    private long _timestamp;

    /**
     * Default constructor
     */
    public TPCANTimestampFD()
    {
    }

    /**
     * @return the timestamp
     */
    public long getValue() {
        return _timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setValue(long timestamp) {
        this._timestamp = timestamp;
    }
}
