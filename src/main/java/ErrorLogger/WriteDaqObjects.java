/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ErrorLogger;

import DAQList.DAQList;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Laptop
 */
public class WriteDaqObjects {


    public void WriteDaqLists(DAQList daqList){
            try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("DAQlists.txt", true)))) {
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss - dd-MM-YYYY");
            Date date = new Date();
            out.println("****** " + dateFormat.format(date) + " ******");


            //Print DAQlist info
            out.println(daqList.getName());
            out.println(daqList.getRelativeId());
            out.println(daqList.getPrescaler());
            out.println(daqList.isActive());


            //Print DAQlist content
            int daqListSize = daqList.getASAP2Measurements().size();
            out.println("DAQlistSize: " + daqListSize);
            //For every DAQlist measurement, print the ASAP2 Measurement info.
            for(int i = 0; i < daqListSize ; i++){
               out.println("Name: "                 + daqList.getASAP2Measurements().get(i).getName());
               out.println("Description: "          + daqList.getASAP2Measurements().get(i).getDescription());
               out.println("Address: "              + daqList.getASAP2Measurements().get(i).getAddress());
               out.println("Conversion: "           + daqList.getASAP2Measurements().get(i).getConversion());
               out.println("DataClass: "            + daqList.getASAP2Measurements().get(i).getDataclass());
               out.println("DataType: "             + daqList.getASAP2Measurements().get(i).getDatatype());
               out.println("DataTypeSize: "         + daqList.getASAP2Measurements().get(i).getDatatypeSize());
               out.println("DecFormat: "            + daqList.getASAP2Measurements().get(i).getDecFormat());
               out.println("Decimals: "             + daqList.getASAP2Measurements().get(i).getDecimalCount());
               out.println("Factor: "               + daqList.getASAP2Measurements().get(i).getFactor());
               out.println("Type: "                 + daqList.getASAP2Measurements().get(i).getASAP2ObjType());
               out.println("Upper Limit: "          + daqList.getASAP2Measurements().get(i).getMaximum());
               out.println("Lower Limit: "          + daqList.getASAP2Measurements().get(i).getMinimum());
               out.println("Limit Enabled: "        + daqList.getASAP2Measurements().get(i).isLimitEnabled());
               out.println("LowerLimitEnabled: "    + daqList.getASAP2Measurements().get(i).isLowerLimitEnabled());
               out.println("UpperLimitEnabled: "    + daqList.getASAP2Measurements().get(i).isUpperLimitEnabled());
               out.println("Resolution: "           + daqList.getASAP2Measurements().get(i).getResolution());
            }



            out.flush();
            //out.close();
        } catch (IOException e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            System.out.println("Couldn't write to DAQlists.txt, " + e.getMessage() + "\n" + e);
        }
    }

}
