/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package util;

import java.awt.Container;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import javax.swing.SwingUtilities;

/**
 * A mouse listener that dispatches all mouse events of a component to the specified
 * target. This can be used if you want a component to only consume specific events
 * and have the rest of the mouse events go "through" the component to one of its parents.
 * @author Michiel Klifman
 */
public class MouseEventDispatcher extends MouseAdapter {

    private final Container target;

    public MouseEventDispatcher(Container target) {
        this.target = target;
    }

    @Override
    public void mouseClicked(MouseEvent event) {
        dispatchEventToTarget(event);
    }

    @Override
    public void mousePressed(MouseEvent event) {
        dispatchEventToTarget(event);
    }

    @Override
    public void mouseReleased(MouseEvent event) {
        dispatchEventToTarget(event);
    }

    @Override
    public void mouseEntered(MouseEvent event) {
        dispatchEventToTarget(event);
    }

    @Override
    public void mouseExited(MouseEvent event) {
        dispatchEventToTarget(event);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent event) {
        dispatchEventToTarget(event);
    }

    @Override
    public void mouseDragged(MouseEvent event) {
        dispatchEventToTarget(event);
    }

    @Override
    public void mouseMoved(MouseEvent event) {
        dispatchEventToTarget(event);
    }

    public void dispatchEventToTarget(MouseEvent event) {
        if (target != null) {
            target.dispatchEvent(SwingUtilities.convertMouseEvent(event.getComponent(), event, target));
        }
    }
}
