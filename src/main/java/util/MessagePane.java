/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package util;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import ErrorLogger.AppendToLogfile;
import ErrorLogger.defaultExceptionHandler;
import HANtune.HANtune;

/**
* Class used to create a thread safe JOptionPane.
* @author Mike
*/
public class MessagePane {
    /**
     * Constructor.
     */
    private MessagePane(){
    }

    /**
    * Shows error dialog
    * @param msg Error message to be shown
    */
    public static void showError(String msg) {
        showPane(HANtune.getInstance(), msg, HANtune.applicationName, JOptionPane.ERROR_MESSAGE, false);
    }

    /**
     * Shows error dialog
     * @param parentComponent hold parent
     * @param msg Error message to be shown
     */
    public static void showError(Component parentComponent, String msg) {
        showPane(parentComponent, msg, HANtune.applicationName, JOptionPane.ERROR_MESSAGE, false);
    }

    /**
    * Shows information dialog
    * @param msg Information message to be shown
    */
    public static void showInfo(String msg) {
        showPane(HANtune.getInstance(), msg, HANtune.applicationName, JOptionPane.INFORMATION_MESSAGE, false);
    }

    /**
    * Shows information dialog
    * @param parentComponent hold parent
    * @param msg Information message to be shown
    */
    public static void showInfo(Component parentComponent, String msg) {
        showPane(parentComponent, msg, HANtune.applicationName, JOptionPane.INFORMATION_MESSAGE, false);
    }

    /**
    * Shows warning dialog
    * @param msg warning message to be shown
    */
    public static void showWarning(String msg) {
        showPane(null, msg, HANtune.applicationName, JOptionPane.WARNING_MESSAGE, false);
    }

     /**
     * Creates a thread safe JOptionPane with the invokeLater or invokeAndWait command.
     * @param parentComponent Error message to be shown
     * @param message Message to be shown
     * @param title Title for the pane.
     * @param messageType MessageType of the pane. example: JOptionPane.INFORMATION_MESSAGE
     * @param wait Boolean to determine if the invokeLater or invokeAndWait needs to be used.
     */
     private static void showPane(final Component parentComponent, final Object message, final String title, final int messageType, final boolean wait) {
         if(!wait) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());

                    JOptionPane.showMessageDialog(parentComponent, message, title, messageType);
                }
            });
        } else {
            try {
                javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        Thread.currentThread().setUncaughtExceptionHandler(new defaultExceptionHandler());
                        JOptionPane.showMessageDialog(parentComponent, message, title, messageType);
                    }
                });
            } catch(InterruptedException | InvocationTargetException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
                Logger.getLogger(MessagePane.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

    /**
     * This method shows a confirmDialog with the given message.
     * @param message :The message of the confirmDialog.
     * @return 0 for yes option, 1 for no option.
     */
    public static int showConfirmDialog(String message, String title) {
        return JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
    }

    /**
     * This method shows a confirmDialog with the given message.
     * @param parent used as parent of this dialog. May be null
     * @param message :The message of the confirmDialog.
     * @return 0 for yes option, 1 for no option.
     */
    public static int showConfirmDialog(Component parent, String message, String title) {
        return JOptionPane.showConfirmDialog(parent, message, title, JOptionPane.YES_NO_OPTION);
    }

    public static int showOptionsDialog(final Component parentComponent, final String message, final String title,
                                        String[] options) {
        return JOptionPane.showOptionDialog(parentComponent, message, title, JOptionPane.DEFAULT_OPTION,
            JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
    }

   /**
    * Shows licence dialog with the option to Get Licence via e-mail
     * @return int n with value 0 if OK chosen, -1 if the window is closed
    */
    public static int showLicenceWarning() {
        String licenceText = "We hope you enjoyed HANtune - Free version \n" + "HANtune is free for non-commercial use. \n" + "Thank you for respecting the rules";
        String getLicenceText = "To acquire a licence please contact the HAN via Jan.Benders@han.nl";
        Object[] options = {"OK", "Get Licence"};
        int n = JOptionPane.showOptionDialog(null, licenceText, "Licence agreement", JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
        if (n == 1) {
            Object[] options1 = {"OK"};
            int m = JOptionPane.showOptionDialog(null, getLicenceText, "Get Licence", JOptionPane.PLAIN_MESSAGE, JOptionPane.INFORMATION_MESSAGE, null, options1, options1[0]);
            return m;
        }
        return n;
    }

    public static boolean showOptionDialog(String title, String dialogMessage, String checkBoxMessage) {
        JCheckBox checkbox = new JCheckBox(checkBoxMessage);
        Object[] params = {dialogMessage, checkbox};
        JOptionPane.showMessageDialog(null, params, title, JOptionPane.INFORMATION_MESSAGE);
        return checkbox.isSelected();
    }

    public static String showTextAreaDialog(String title, String defaultText, Font font) {
        JTextArea textArea = new JTextArea(defaultText);
        textArea.setTabSize(3);
        textArea.setLineWrap(true);
        textArea.setFont(font);
        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setPreferredSize(new Dimension(200, 100));
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        int option = JOptionPane.showConfirmDialog(null, scrollPane, title, JOptionPane.YES_NO_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            return textArea.getText();
        } else {
            return null;
        }
    }


    /**
     * Show a dialog with a simple text area, a large scrollable text area and 1 OK button
     * @param title
     * @param topText First line at top dialog box
     * @param textAreaText Large scrollable text area
     */
    public static void showTextAreaOKDialog(String title, String topText, String textAreaText) {
        JTextArea textArea = new JTextArea(textAreaText);

        textArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setPreferredSize(new Dimension(400, 250));
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        textArea.addHierarchyListener(new HierarchyListener() {
            // use this 'trick' to reach dialog object and make it resizable
            @Override
            public void hierarchyChanged(HierarchyEvent e) {
                Window window = SwingUtilities.getWindowAncestor(textArea);
                if (window instanceof Dialog) {
                    Dialog dialog = (Dialog)window;
                    if (!dialog.isResizable()) {
                        dialog.setResizable(true);
                    }
                }
            }
        });

        Object[] params = {" ", topText, " ",  scrollPane };
        JOptionPane.showMessageDialog(null, params, title, JOptionPane.INFORMATION_MESSAGE);
    }

}
