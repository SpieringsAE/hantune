/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package util;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class EncryptDecrypt {
    static Cipher cipher = null;
    byte[] IV = { 2, 1, 4, 5, 2, 8, 11, 4, 6, 77, 4, 22, 4, 5, 16, 77 }; // As for now, use a fixed
    // Now using a fixed Initialization Vector for all encryption/decryption. See also:
    // http://crypto.stackexchange.com/questions/2594/initialization-vector-length-insufficient-in-aes

    public EncryptDecrypt() throws NoSuchAlgorithmException, NoSuchPaddingException {
        cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
    }


    public byte[] encryptData(byte[] plainBytes, String passPhrase)
        throws NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException,
        IllegalBlockSizeException, BadPaddingException {

        Key key = generateKey(passPhrase);
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV));

        // return encrypted bytes
        return cipher.doFinal(plainBytes);
    }


    public byte[] decryptData(byte[] rawBytes, String passPhrase)
        throws NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException,
        IllegalBlockSizeException, BadPaddingException {
        // Generate the key first
        Key key = generateKey(passPhrase);

        // always use same IV for encryption-decryption (for now use a fixed IV)
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV));

        // return decrypted bytes
        return cipher.doFinal(rawBytes);
    }


    /**
     * This method generates a 16-bit key from an input string
     *
     * @param passphrase string to be turned into a valid key
     * @return key the generated key
     * @throws NoSuchAlgorithmException
     */
    // Converts Passphrase to a Key using AES and SHA
    static public Key generateKey(String passphrase) throws NoSuchAlgorithmException {
         MessageDigest password = MessageDigest.getInstance("SHA");
         password.update(passphrase.getBytes());
         Key key = new SecretKeySpec(password.digest(), 0, 16, "AES");

        return key;
    }


}
