/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package util;

import HANtune.HANtune;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

/**
 *
 * @author Mike
 */
public class ProgressDialog extends JDialog {
    private static final long serialVersionUID = 7526471155622776190L;

    private static final String CONFIRM_MESSAGE = "Warning: parameter value may already have been set!." + "\nAre you sure you want to cancel the calibration?";
    private static final String OPTIONPANE_MESSAGE = "Please wait while HANtune is calibrating!"+"\nThis may take a few minutes.\n\n";
    private static final String DIALOG_IMAGE_ICON = "/images/HANlogo_small.png";
    private boolean calibrationCancelled = false;
    private int progressBarMaximum = 100;
    private int progressBarMinimum = 0;
    private JProgressBar progressBar;

    /**
     * Constructor of ProgressDialog.
     * @param title :Title of the ProgressDialog.
     */
    public ProgressDialog() {
        progressBar = new JProgressBar();

        setIconImage(createImage(DIALOG_IMAGE_ICON));
    }

    /**
     * @param progressBar_Maximum the progressBar_Maximum to set
     */
    public void setProgressBarMaximum(int progressBar_Maximum) {
        this.progressBarMaximum = progressBar_Maximum;
    }

    /**
     * @return the calibrationCancelled
     */
    public boolean isCalibrationCancelled() {
        return calibrationCancelled;
    }

    /**
     * @param isCalibrationCancelled the calibrationCancelled to set
     */
    public void setCalibrationCancelled(boolean isCalibrationCancelled) {
        this.calibrationCancelled = isCalibrationCancelled;
    }

    /**
     * This method updates the value of the progressBar.
     * @param newValue :The new value of the progressBar.
     */
    public void updateProgress(int newValue) {
        progressBar.setValue(newValue);
    }

    /**
     * This method creates a JOptionPane, sets the limits of the progressBar and adds the bar to the dialog.
     */
    public void showDialog() {
        JOptionPane optionPane = new JOptionPane(OPTIONPANE_MESSAGE, JOptionPane.WARNING_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);

        progressBar.setMinimum(progressBarMinimum);
        progressBar.setMaximum(progressBarMaximum);

        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        addDialogWindowEventListener();
        setContentPane(optionPane);
        setAlwaysOnTop(true);
        add(progressBar);
        pack();

        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * This method deconstructs the progressDialog, enables hantune and sets the calibrationCancelled to false.
     */
    public void deconstruct() {
        HANtune hantune = HANtune.getInstance();
        hantune.setEnabled(true);

        setCalibrationCancelled(isCalibrationCancelled());
        this.dispose();
    }

    /**
    * This method creates an Image with the given imageFile.
    * @param imageFile :The File of the image to create.
    * @return Image.
    */
    private Image createImage(String imageFile) {
        return Toolkit.getDefaultToolkit().getImage(getClass().getResource(imageFile));
    }

    /**
     * This method adds an eventListener to the progressDialog.
     */
    private void addDialogWindowEventListener() {
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent evt) {
                setAlwaysOnTop(false);
                int confirm = MessagePane.showConfirmDialog(CONFIRM_MESSAGE,"Cancel Calibrating");

                if(confirm == 0) {
                    setCalibrationCancelled(true);
                    deconstruct();
                } else {
                    setAlwaysOnTop(true);
                }
            }
        });
    }
}
