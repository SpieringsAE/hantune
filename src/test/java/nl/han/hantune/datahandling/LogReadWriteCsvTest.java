package nl.han.hantune.datahandling;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Ignore;
import org.junit.Test;
import util.OptimizedRandomAccessFile;

import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;

//import java.nio.file.AccessMode;

public class LogReadWriteCsvTest {

    // not a real test (no assert in this method), just to see if things are working
    @Test
    public void testReadWriteFile() throws IOException {

        Path file = Files.createTempFile("HAN_TestChannelLogRdWrkoe", null);

        String saveName = file.toString();
        RandomAccessFile raf = new RandomAccessFile(saveName, "rw");
        ByteBuffer buffer = null;

        try (FileChannel fc = FileChannel.open(file, READ, WRITE)) {

            System.out.println(fc.isOpen());

            long startMillis = System.currentTimeMillis();
            for (int count = 0; count < 100; count++) {
                buffer = ByteBuffer.wrap(("testString " + count + "\r\n").getBytes());
                fc.write(buffer);
            }
            System.out.println("write Duration:" + (System.currentTimeMillis() - startMillis));


            buffer = ByteBuffer.allocate(150);

            fc.position(0);
            BufferedReader br = new BufferedReader(Channels.newReader(fc, "US-ASCII")); // UTF-8 ???
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println("String read: " + line);
            }
            System.out.println("read Duration:" + (System.currentTimeMillis() - startMillis));


            // random position
            fc.position(100);
            line = br.readLine();
            System.out.println("RandomRead1: " + line);
            System.out.println("position: " + fc.position());
            fc.position(1236);

            for (int count = 101; count < 2000; count++) {
                buffer = ByteBuffer.wrap(("testString " + count + "\r\n").getBytes());
                fc.write(buffer);
            }

            System.out.println("position: " + fc.position());
            line = br.readLine();
            System.out.println("RandomRead1a: " + line);
            fc.position(126);
            line = br.readLine();
            System.out.println("RandomRead2: " + line);
            fc.position(43211);
            line = br.readLine();
            System.out.println("RandomRead3: " + line);

            raf.seek(100);
            line = raf.readLine();
            System.out.println("raf1: " + line);
            System.out.println("position: " + raf.getFilePointer());

            raf.seek(1236);
            System.out.println("position: " + raf.getFilePointer());
            System.out.println("raf2: " + raf.readLine());

            raf.seek(36);
            System.out.println("position: " + raf.getFilePointer());
            System.out.println("raf3: " + raf.readLine());

            raf.seek(34326);
            System.out.println("position: " + raf.getFilePointer());
            System.out.println("raf4: " + raf.readLine());
            fc.force(true);
            fc.close();
            raf.close();

            fc.position(0L);
            int readSize;
            do {

                readSize = fc.read(buffer);
                byte[] bytes = buffer.array();
                String hlpStr = new String(bytes);
                buffer.rewind();
            } while (readSize > 0);


        } catch (IOException ex) {
            System.err.println("I/O Error: " + ex);
        }
    }



    String writeString;
    OptimizedRandomAccessFile raf = null;

    // not a real test (no assert in this method), just to see if things are working
    @Test
    @Ignore
    public void testReadWriteRandomAccesFile() throws IOException, InterruptedException {
        //Todo make testing independent of available paths !!
        Path file = Paths.get("l://MusicMan_TMP//testFile"); // SSD


        String fullName = file.toAbsolutePath().toString();

        raf = new OptimizedRandomAccessFile(fullName, "rw");

        String dataStr = "";
        long startMillis = System.currentTimeMillis();
        for (int count = 0; count < 1_000_000; count++) {
            dataStr = "testString" + count;
            raf.writeBytes((dataStr + "\r\n"));
        }
        long endMillis = System.currentTimeMillis();
        System.out.println("write Duration:" + (endMillis - startMillis));

        if (!dataStr.isEmpty()) { // keep compiler happy
            return;
        }

        startMillis = endMillis;
        // read data currently written into file
        raf.seek(0);
        int count = 0;
        while (true) {
            String str = raf.readLine();
            if (str == null) {
                break;
            }
        }
        endMillis = System.currentTimeMillis();
        System.out.println("read Duration:" + (endMillis - startMillis));

        startMillis = endMillis;
        // now read some random data from current file, while adding new data at end of file
        int posRead;
        for (count = 0; count < 1_000_000; count++) {
            posRead = 5 * count;
            raf.seek(posRead);

            raf.seek(raf.length());
            raf.write(("testStringRandom " + count + "\r\n").getBytes());
        }
        endMillis = System.currentTimeMillis();
        System.out.println("randomReadWrite Duration:" + (endMillis - startMillis));

        raf.close();
    }

    @Test
    public void testReadWriteRandomAccesFileThread() throws IOException, InterruptedException {
        Path file = Files.createTempFile("HAN_ReadWriteRaf", null);

        String fullName = file.toAbsolutePath().toString();

        raf = new OptimizedRandomAccessFile(fullName, "rw");
        writeString = "nowReading";
        Thread thread = new Thread(new WriteToLogThread());
        thread.start();
        thread.join();

        Thread.sleep(2000);
        long startMillis = System.currentTimeMillis();

        // read data currently written to file
        raf.seek(0);
        int count = 0;
        while (true) {
            String str = raf.readLine();
            if (raf.getFilePointer() >= raf.length() - 10) {
                break;
            }
            if (count++ == 10) {
                writeString = "10ReadCount";
            }
        }
        System.out.println("read Duration:" + (System.currentTimeMillis() - startMillis));
        Thread.sleep(1000);

        OptimizedRandomAccessFile tmp = raf;
        raf = null;
        tmp.close();
    }

    class WriteToLogThread extends Thread {

        WriteToLogThread() {
        }

        @Override
        public void run() {
            try {
                writeToTempLog();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        void writeToTempLog() throws IOException {


            long startMillis = System.currentTimeMillis();

            // write some data for a while
            for (int count = 0; count < 100_000; count++) {
                if (raf == null) {
                    break;
                }
                raf.seek(raf.length());
                raf.writeBytes(writeString + count + "\r\n");

            }
            System.out.println("writeToTempLog Duration:" + (System.currentTimeMillis() - startMillis));
        }

    }
}


