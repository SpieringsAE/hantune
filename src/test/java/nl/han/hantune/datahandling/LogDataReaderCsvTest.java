/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.datahandling;

import java.io.File;
import java.io.IOException;

import nl.han.hantune.datahandling.DataLogger.CsvLogInfo;
import nl.han.hantune.datahandling.DataLogger.LogDataReaderCsv;
import nl.han.hantune.datahandling.DataLogger.LogDataReaderCsv.ReaderResult;
import nl.han.hantune.datahandling.DataLogger.LogDataReaderCsv.ResultItem;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LogDataReaderCsvTest {

    @Test
    public void testReadHeader() throws IOException {
        LogDataReaderCsv testReader = LogDataReaderCsv.getInstance();
        // testDataLog.csv contains lines with comma as well as dot as decimal separator
        assertTrue(testReader.initRead(new File("../src/test/resources/nl/han/hantune/datahandling/testDataLog.csv")));
        CsvLogInfo csvInfo = testReader.getLogHeaderInfo();
        assertEquals("XCP_longDAQ.hml", csvInfo.getProjectName());
        assertEquals("HANcoder_Olimexino_Testmodel2014a_softwareID_v1_190418_134237", csvInfo.getEcuId());
        assertEquals("Fri Jul 05 22:55:57 CEST 2019", csvInfo.getStartDate());
        assertEquals("20Hz", csvInfo.getDaqListInfos().get(0).getFreq());
        assertEquals("TestDBC_728.dbc", csvInfo.getDbcInfos().get(0));
        assertEquals("DM4.EngPercentLoadAtCurrentSpeed", csvInfo.getProtocolSignalNames().get(23).getSignal());
        assertEquals("Message_131h.Signal_3", csvInfo.getProtocolSignalNames().get(33).getSignal());
        assertEquals("DTC_DoubleSignal", csvInfo.getProtocolSignalNames().get(34).getSignal());
        assertEquals("CAN", csvInfo.getProtocolSignalNames().get(33).getProtocol());
        assertEquals("XCP", csvInfo.getProtocolSignalNames().get(34).getProtocol());
        assertTrue(csvInfo.isHeaderComplete());

        // print some values
        testReader.getRefElement(1.05).getResultItems().forEach((v) -> System.out.println(
            v.getProtocolName() + " " + v.getItemName() + " " + v.getItemValue()));

        ReaderResult rslt = testReader.getRefElement(1.05);
        assertEquals(1.05256, rslt.getTimeStampSecs(), 0.0001d);
        assertEquals(27, rslt.getResultItems().size());

        ResultItem item = rslt.getResultItems().get(25);
        assertEquals("XCP-SI_FreeHeap", item.getProtocolName() + "-" + item.getItemName());
        assertEquals(260.0, item.getItemValue(), 0.0001d);

        // testDataLog.csv contains lines with comma as well as dot as decimal separator
        // Timestamp of first element should be "0.0000" (dot as decimal)
        assertEquals(0.00, testReader.getFirstElement().getTimeStampSecs(), 0.0001d);

        // Timestamp of second element should be "0,05009" (comma as decimal), but should still be handled correctly
        assertEquals(0.05009, testReader.getNextElement().getTimeStampSecs(), 0.0001d);
    }
}
