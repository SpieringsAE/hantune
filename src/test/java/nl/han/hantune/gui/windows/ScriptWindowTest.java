/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.windows;

import java.awt.Component;
import javax.swing.JButton;

import nl.han.hantune.gui.windows.ScriptWindow;
import nl.han.hantune.scripting.Script;
import nl.han.hantune.scripting.jython.JythonScript;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Michiel Klifman
 */
public class ScriptWindowTest {

    @Test
    public void testStateUpdate() {
        ScriptWindow window = new ScriptWindow();
        Script script = new JythonScript("test", "test.py");
        window.addReference(script);
        JButton button = null;
        for (Component c : window.getContentPane().getComponents()) {
            if (c instanceof JButton) {
                button = (JButton) c;
            }
        }
        if (button == null) {
            Assert.fail();
        } else {
            script.setRunning(true);
            String expectedResult = "Stop test";
            String result = button.getText();
            Assert.assertEquals(result, expectedResult);
        }
    }
}
