/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.gui.sidepanel.tree;

import java.awt.datatransfer.DataFlavor;
import java.io.Serializable;
import nl.han.hantune.gui.sidepanel.tree.TransferableTreeNode;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Michiel Klifman
 */
public class TransferableTreeNodeTest {

    //Todo: test fails. Should be improved
    @Test
    @Ignore
    public void testAddDataFlavors() {
        DataFlavor charSequenceFlavor = new DataFlavor(CharSequence.class, CharSequence.class.getSimpleName());
        DataFlavor serializableFlavor = new DataFlavor(Serializable.class, Serializable.class.getSimpleName());
        DataFlavor comparableFlavor = new DataFlavor(Comparable.class, Comparable.class.getSimpleName());
        DataFlavor stringFlavor = new DataFlavor(String.class, String.class.getSimpleName());
        DataFlavor[] testFlavors = new DataFlavor[]{charSequenceFlavor, serializableFlavor, comparableFlavor, stringFlavor};

        TransferableTreeNode node = new TransferableTreeNode("testString");
        assertArrayEquals(node.getDataFlavors(), testFlavors);
    }

    @Test
    public void testHasDataFlavor() {
        TransferableTreeNode node = new TransferableTreeNode("testString");
        DataFlavor stringFlavor = new DataFlavor(String.class, String.class.getSimpleName());
        DataFlavor integerFlavor = new DataFlavor(Integer.class, Integer.class.getSimpleName());
        assertTrue(node.hasFlavor(stringFlavor));
        assertFalse(node.hasFlavor(integerFlavor));
    }

}
