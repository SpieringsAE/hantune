/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public class ASAP2ImportTest extends ASAP2Import {

    String getPartContentString(ArrayList<String>[] prt, ArrayList<Integer>[] prt_prnt) {
        String rtnStr = "";
        int lvlIdx;
        for (lvlIdx = 0; lvlIdx < prt.length; lvlIdx++) {
            rtnStr = rtnStr.concat("\n*****************Part Level: " + lvlIdx + "**************\n");
            int count = 0;
            for (String partStr : prt[lvlIdx]) {
                rtnStr = rtnStr.concat(
                    "-----part level:" + lvlIdx + " index: " + count + "-----\n" + partStr + "\n\n");
                count++;
            }
        }
        for (lvlIdx = 0; lvlIdx < prt_prnt.length; lvlIdx++) {
            rtnStr = rtnStr.concat("\n*************part_parent Level: " + lvlIdx + "*************\n");
            int count = 0;
            for (Integer partVal : prt_prnt[lvlIdx]) {
                rtnStr = rtnStr.concat("part_parent level:" + lvlIdx + " index: " + count + " value: "
                    + partVal + "\n");
                count++;
            }
        }
        return rtnStr;
    }


    @Test
    public void testImportAsap() {
        long lTimeStart = System.currentTimeMillis();
        File asap2File = new File("../src/test/resources/ASAP2/TestImport.a2l");

        ASAP2ImportTest asap2Imp = new ASAP2ImportTest();
        ASAP2Data asap2Data = asap2Imp.importASAP2(asap2File);

        long lTimeEndImport = System.currentTimeMillis();
        System.out.println("testImportAsapNew total loading time: " + (lTimeEndImport - lTimeStart) + " ms");

        // some Characteristic check
        assertEquals(50, asap2Data.getASAP2Characteristics().size());
        ASAP2Characteristic charact = asap2Data.getASAP2Characteristics().get("DTC_Lookup2DTableData_int8");
        assertEquals(ASAP2Characteristic.Type.MAP, charact.getType());
        assertEquals(ASAP2Datatype.SBYTE, charact.getDatatype ());
        assertEquals(0x20000004, charact.getAddress());

        // some measurement check
        assertEquals(26, asap2Data.getASAP2Measurements().size());
        ASAP2Measurement measureMnt = asap2Data.getASAP2Measurements().get("DTC_Fixdt1sig16bit8fracSignal");
        assertEquals(ASAP2Datatype.SWORD, measureMnt.getDatatype());
        assertEquals(0x200039ba, measureMnt.getAddress());
        assertEquals("RAT_FUNC", asap2Data.getASAP2CompuMethods().get(measureMnt.getConversion()).getConversionType());
        assertEquals(Arrays.toString(new float[] { 0.0f, 256.0f, 0.0f, 0.0f, 0.0f, 1.0f }),
        Arrays.toString(asap2Data.getASAP2CompuMethods().get(measureMnt.getConversion()).getCoeffs()));
    }


    @Test(timeout=4000)
    /*
     * Test time it takes to load a big a2l file (9 MB). Should be less than 3500 ms on an average PC.
     */
    public void testImportAsapBigLoadingTime() {
        long lTimeStart = System.currentTimeMillis();
            File asap2File = new File("../src/test/resources/ASAP2/TestImportBig.a2l");


        ASAP2ImportTest asap2Imp = new ASAP2ImportTest();
        ASAP2Data asap2Data = asap2Imp.importASAP2(asap2File);

        long lTimeEndImport = System.currentTimeMillis();
        System.out.println("testImportAsapBigLoadingTime total loading time: " + (lTimeEndImport - lTimeStart) + " ms");
        assertTrue((lTimeEndImport - lTimeStart) < 3500); // Loading should take less then 3500 ms.

        // check a random characteristic
        // some Characteristic check
        assertEquals(1882, asap2Data.getASAP2Characteristics().size());
        ASAP2Characteristic charact = asap2Data.getASAP2Characteristics().get("prm1_s.Aux_Prm_s.Prop4_s.Profile_s.Y3_u16");
        assertEquals(ASAP2Characteristic.Type.VALUE, charact.getType());
        assertEquals(ASAP2Datatype.UWORD, charact.getDatatype());
        assertEquals(0x8200faa6, charact.getAddress());

        // check a random measurement
        // some measurement check
        assertEquals(43723, asap2Data.getASAP2Measurements().size());
        ASAP2Measurement measureMnt =
            asap2Data.getASAP2Measurements().get("Aux_s.CAN_s.Data.RegData_BAM_au8_2_.numBytes_u8");
        assertEquals(ASAP2Datatype.UBYTE, measureMnt.getDatatype());
        assertEquals(0x8200a9f4, measureMnt.getAddress());
        assertEquals("IDENTICAL", asap2Data.getASAP2CompuMethods().get(measureMnt.getConversion()).getConversionType());
        assertEquals(1, measureMnt.getResolution());
        assertEquals(100.0, measureMnt.getAccuracy(), 0.0001);
        assertEquals(0.0, measureMnt.getMinimum(), 0.000001);
        assertEquals(255.0, measureMnt.getMaximum(), 0.000001);
    }


    @Test
    public void testImportAsapParts() {
         File asap2File = new File("../src/test/resources/ASAP2/TestImport.a2l");

        ASAP2ImportPart asap2impPart;
        long lTimeStart = 10;
        long lTimeEndImport = 20;
        try {
            lTimeStart = System.nanoTime();

            asap2impPart = new ASAP2ImportPart(asap2File);
            ArrayList<String>[] testPart = asap2impPart.parseDelimitedDataChunks();
            ArrayList<Integer>[] testPartParent = asap2impPart.getParsedPartParent();

            lTimeEndImport = System.nanoTime();

            String partContentStr = getPartContentString(testPart, testPartParent);
            // System.out.println("PartContent: " + partContentStr);

            // File contains partContent from old (very slow) version of ASAP2import,
            // should be equal to partContentStr
            File compareFile = new File("../src/test/resources/ASAP2/TestImport_PartCompare.txt");
            FileInputStream fis;
            fis = new FileInputStream(compareFile);
            byte[] dataBytes = new byte[(int)compareFile.length()];
            //noinspection ResultOfMethodCallIgnored
            fis.read(dataBytes);
            String compareStr = new String(dataBytes);
            fis.close();

            // ignore possible '\r' (related to autocrlf in git)
            assertEquals(compareStr.replace("\r", ""), partContentStr.replace("\r", ""));
        } catch (IOException e) {
            e.printStackTrace();
            fail(); // error when test comes here
        }
        double d = (lTimeEndImport - lTimeStart) / 1000000.0;
        DecimalFormat df = new DecimalFormat("#.000");
        System.out.println("testImportAsapParts loading time: " + df.format(d) + " ms");
    }
}
