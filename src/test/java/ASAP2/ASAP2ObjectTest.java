/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package ASAP2;

import static org.junit.Assert.assertEquals;

import ASAP2.ASAP2Datatype;
import ASAP2.ASAP2Object;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ASAP2ObjectTest {

    ASAP2Object testAsap2;

    public ASAP2ObjectTest() {
    }


    @BeforeClass
    public static void setUpClass() throws Exception {
    }


    @AfterClass
    public static void tearDownClass() throws Exception {
    }


    @Before
    public void setUp() {
        testAsap2 = new ASAP2Object();
    }


    @After
    public void tearDown() throws Exception {
    }


    /**
     * Simple test to check if all DataTypes of ASAP2Object are correctly converted
     */
    @Test
    public void testAsap2DoubleToObjectConvert() {
        {
            short value = 1;
            testAsap2.setDatatype(ASAP2Datatype.BOOLEAN);
            Object Obj = testAsap2.convertValue((double)value);
            assertEquals(Obj.toString(), "1");
        }
        {
            short value = 255; // For UBYTE (unsigned byte) data type short (= signed) is used
            testAsap2.setDatatype(ASAP2Datatype.UBYTE);
            Object Obj = testAsap2.convertValue((double)value);
            assertEquals(Obj.toString(), "255");
        }
        {
            Byte value = 123;
            testAsap2.setDatatype(ASAP2Datatype.SBYTE);
            Object Obj = testAsap2.convertValue((double)value);
            assertEquals(Obj.toString(), "123");
        }
        {
            int value = 65535;
            testAsap2.setDatatype(ASAP2Datatype.UWORD);
            Object Obj = testAsap2.convertValue((double)value);
            assertEquals(Obj.toString(), "65535");
        }
        {
            short value = -32768;
            testAsap2.setDatatype(ASAP2Datatype.SWORD);
            Object Obj = testAsap2.convertValue((double)value);
            assertEquals(Obj.toString(), "-32768");
        }
        {
            long value = 4294967296L; // For ULONG (32 bit) data type long (= signed 64bit) is used.
            testAsap2.setDatatype(ASAP2Datatype.ULONG);
            Object Obj = testAsap2.convertValue((double)value);
            assertEquals(Obj.toString(), "4294967296");
        }
        {
            int value = -2147483648;
            testAsap2.setDatatype(ASAP2Datatype.SLONG);
            Object Obj = testAsap2.convertValue((double)value);
            assertEquals(Obj.toString(), "-2147483648");
        }
        {
            float value = 125678.23678f;
            testAsap2.setDatatype(ASAP2Datatype.FLOAT32_IEEE);
            Object Obj = testAsap2.convertValue((double)value);
            assertEquals(Obj.toString(), "125678.234");
        }
        {
            double value = 12345678.2345678;
            testAsap2.setDatatype(ASAP2Datatype.FLOAT64_IEEE);
            Object Obj = testAsap2.convertValue(value);
            assertEquals(Obj.toString(), "1.23456782345678E7");
        }
    }


    /**
     * Test of getType method, of class ASAP2Object.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        ASAP2Object instance = new ASAP2Object();
        ASAP2Object.ASAP2ObjType expResult = null;
        ASAP2Object.ASAP2ObjType result = instance.getASAP2ObjType();
        assertEquals(expResult, result);
    }


    /**
     * Test of setType method, of class ASAP2Object.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        ASAP2Object.ASAP2ObjType type = ASAP2Object.ASAP2ObjType.COMPU_METHOD;
        ASAP2Object instance = new ASAP2Object();
        instance.setASAP2ObjType(type);
        assertEquals(type, instance.getASAP2ObjType());
    }


    /**
     * Test of getConversion method, of class ASAP2Object.
     */
    @Test
    public void testGetConversion() {
        System.out.println("getConversion");
        ASAP2Object instance = new ASAP2Object();
        String expResult = "";
        String result = instance.getConversion();
        assertEquals(expResult, result);
    }


    /**
     * Test of setConversion method, of class ASAP2Object.
     */
    @Test
    public void testSetConversion() {
        System.out.println("setConversion");
        String conversion = "RAT_FUNC";
        ASAP2Object instance = new ASAP2Object();
        instance.setConversion(conversion);
        assertEquals("RAT_FUNC", instance.getConversion());
    }


    /**
     * Test of convertValue method, of class ASAP2Object.
     */
    @Test
    public void testConvertValue_double() {
        System.out.println("convertValue");
        double value = 1234.5;
        ASAP2Object instance = new ASAP2Object();
        instance.setDatatype(ASAP2Datatype.FLOAT64_IEEE);
        Object expResult = 1234.5;
        Object result = instance.convertValue(value);
        assertEquals(expResult, result);
    }


    /**
     * Test of formatDescription method, of class ASAP2Object.
     */
    @Test
    public void testGetDescription() {
        System.out.println("formatDescription");
        ASAP2Object instance = new ASAP2Object();
        String expResult = "";
        String result = instance.getDescription();
        assertEquals(expResult, result);
    }


    /**
     * Test of setDescription method, of class ASAP2Object.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "test";
        ASAP2Object instance = new ASAP2Object();
        instance.setDescription(description);
        assertEquals(description, instance.getDescription());
    }


    /**
     * Test of getAddress method, of class ASAP2Object.
     */
    @Test
    public void testGetAddress() {
        System.out.println("getAddress");
        ASAP2Object instance = new ASAP2Object();
        int expResult = 0;
        int result = instance.getAddress();
        assertEquals(expResult, result);
    }


    /**
     * Test of setAddress method, of class ASAP2Object.
     */
    @Test
    public void testSetAddress() {
        System.out.println("setAddress");
        int address = 12345678;
        ASAP2Object instance = new ASAP2Object();
        instance.setAddress(address);
        assertEquals(address, instance.getAddress());
    }


    /**
     * Test of getDatatype method, of class ASAP2Object.
     */
    @Test
    public void testGetDatatype() {
        System.out.println("getDatatype");
        ASAP2Object instance = new ASAP2Object();
        ASAP2Datatype expResult = ASAP2Datatype.UWORD;
        ASAP2Datatype result = instance.getDatatype();
        assertEquals(expResult, result);
    }


    /**
     * Test of setDatatype method, of class ASAP2Object.
     */
    @Test
    public void testSetDatatype() {
        System.out.println("setDatatype");
        ASAP2Datatype datatype = ASAP2Datatype.FLOAT32_IEEE;
        ASAP2Object instance = new ASAP2Object();
        instance.setDatatype(datatype);
        assertEquals(ASAP2Datatype.FLOAT32_IEEE, instance.getDatatype());
    }


    /**
     * Test of getDatatypeSize method, of class ASAP2Object.
     */
    @Test
    public void testGetDatatypeSize() {
        System.out.println("getDatatypeSize");
        ASAP2Object instance = new ASAP2Object();
        char result = instance.getDatatypeSize();
        assertEquals(2, result);
    }


    /**
     * Test of getDataclass method, of class ASAP2Object.
     */
    @Test
    public void testGetDataclass() {
        System.out.println("getDataclass");
        ASAP2Object instance = new ASAP2Object();
        Class<? extends Number> expResult = Integer.class;
        Class<? extends Number> result = instance.getDataclass();
        assertEquals(result, expResult);
    }


    /**
     * Test of getDecimalCount method, of class ASAP2Object.
     */
    @Test
    public void testGetDecimalCount() {
        System.out.println("getDecimalCount");
        ASAP2Object instance = new ASAP2Object();
        int expResult = 0;
        int result = instance.getDecimalCount();
        assertEquals(expResult, result);
    }


    /**
     * Test of setDecimals method, of class ASAP2Object.
     */
    @Test
    public void testSetDecimals() {
        System.out.println("setDecimals");
        int decimals = 2;
        ASAP2Object instance = new ASAP2Object();
        instance.setDecimals(decimals);
        assertEquals(2, instance.getDecimalCount());
    }


    /**
     * Test of getFactor method, of class ASAP2Object.
     */
    @Test
    public void testGetFactor() {
        System.out.println("getFactor");
        ASAP2Object instance = new ASAP2Object();
        double expResult = 1.0;
        double result = instance.getFactor();
        assertEquals(expResult, result, 0.0);
    }


    /**
     * Test of setFactor method, of class ASAP2Object.
     */
    @Test
    public void testSetFactor() {
        System.out.println("setFactor");
        double factor = 1.2;
        ASAP2Object instance = new ASAP2Object();
        instance.setFactor(factor);
        assertEquals(1.2, instance.getFactor(), 0.0);
    }


    /**
     * Test of getMinimum method, of class ASAP2Object.
     */
    @Test
    public void testGetMinimum() {
        System.out.println("getMinimum");
        ASAP2Object instance = new ASAP2Object();
        double expResult = 0.0;
        double result = instance.getMinimum();
        assertEquals(expResult, result, 0.0);
    }


    /**
     * Test of setMinimum method, of class ASAP2Object.
     */
    @Test
    public void testSetMinimum() {
        System.out.println("setMinimum");
        double lowerLimit = 0.4;
        ASAP2Object instance = new ASAP2Object();
        instance.setMinimum(lowerLimit);
        assertEquals(0.4, instance.getMinimum(), 0.0);
    }


    /**
     * Test of getMaximum method, of class ASAP2Object.
     */
    @Test
    public void testGetMaximum() {
        System.out.println("getMaximum");
        ASAP2Object instance = new ASAP2Object();
        double expResult = 0.0;
        double result = instance.getMaximum();
        assertEquals(expResult, result, 0.0);
    }


    /**
     * Test of setMaximum method, of class ASAP2Object.
     */
    @Test
    public void testSetMaximum() {
        System.out.println("setMaximum");
        double upperLimit = 999.9;
        ASAP2Object instance = new ASAP2Object();
        instance.setMaximum(upperLimit);
        assertEquals(999.9, instance.getMaximum(), 0.0);
    }


    /**
     * Test of isLowerLimitEnabled method, of class ASAP2Object.
     */
    @Test
    public void testIsLowerLimitEnabled() {
        System.out.println("isLowerLimitEnabled");
        ASAP2Object instance = new ASAP2Object();
        boolean expResult = false;
        boolean result = instance.isLowerLimitEnabled();
        assertEquals(expResult, result);
    }


    /**
     * Test of setLowerLimitEnabled method, of class ASAP2Object.
     */
    @Test
    public void testSetLowerLimitEnabled() {
        System.out.println("setLowerLimitEnabled");
        boolean lowerLimitEnabled = true;
        ASAP2Object instance = new ASAP2Object();
        instance.setLowerLimitEnabled(lowerLimitEnabled);
        instance.setMinimum(0.1);
        assertEquals(0.1, instance.getMinimum(), 0.0);
    }


    /**
     * Test of isUpperLimitEnabled method, of class ASAP2Object.
     */
    @Test
    public void testIsUpperLimitEnabled() {
        System.out.println("isUpperLimitEnabled");
        ASAP2Object instance = new ASAP2Object();
        boolean expResult = false;
        boolean result = instance.isUpperLimitEnabled();
        assertEquals(expResult, result);
    }


    /**
     * Test of setUpperLimitEnabled method, of class ASAP2Object.
     */
    @Test
    public void testSetUpperLimitEnabled() {
        System.out.println("setUpperLimitEnabled");
        boolean upperLimitEnabled = false;
        ASAP2Object instance = new ASAP2Object();
        instance.setUpperLimitEnabled(upperLimitEnabled);
        instance.setMaximum(999.9);
        assertEquals(999.9, instance.getMaximum(), 0.0);
    }


    /**
     * Test of isLimitEnabled method, of class ASAP2Object.
     */
    @Test
    public void testIsLimitEnabled() {
        System.out.println("isLimitEnabled");
        ASAP2Object instance = new ASAP2Object();
        boolean expResult = false;
        boolean result = instance.isLimitEnabled();
        assertEquals(expResult, result);
    }


    /**
     * Test of getDecFormat method, of class ASAP2Object.
     */
    @Test
    public void testGetDecFormat() {
        System.out.println("getDecFormat");
        ASAP2Object instance = new ASAP2Object();
        String expResult = "0";
        String result = instance.getDecFormat();
        assertEquals(expResult, result);
    }


    /**
     * Test of setDecFormat method, of class ASAP2Object.
     */
    @Test
    public void testSetDecFormat() {
        System.out.println("setDecFormat");
        String decFormat = "1.x";
        ASAP2Object instance = new ASAP2Object();
        instance.setDecFormat(decFormat);
        assertEquals("1.x", instance.getDecFormat());
    }


    /**
     * Test of hasDecimals method, of class ASAP2Object.
     */
    @Test
    public void testHasDecimals() {
        System.out.println("hasDecimals");
        ASAP2Object instance = new ASAP2Object();
        boolean expResult = false;
        boolean result = instance.hasDecimals();
        assertEquals(expResult, result);
    }


    /**
     * Test of getUnit method, of class ASAP2Object.
     */
    @Test
    public void testGetUnit() {
        System.out.println("getUnit");
        ASAP2Object instance = new ASAP2Object();
        String expResult = "";
        String result = instance.getUnit();
        assertEquals(expResult, result);
    }


    /**
     * Test of setUnit method, of class ASAP2Object.
     */
    @Test
    public void testSetUnit() {
        System.out.println("setUnit");
        String unit = "Sec";
        ASAP2Object instance = new ASAP2Object();
        instance.setUnit(unit);
        assertEquals("Sec", instance.getUnit());
    }


    /**
     * Test of getSource method, of class ASAP2Object.
     */
    @Test
    public void testGetSource() {
        System.out.println("getSource");
        ASAP2Object instance = new ASAP2Object();
        String expResult = ASAP2Object.DEFAULT_SOURCE;
        String result = instance.getSource();
        assertEquals(expResult, result);
    }


    /**
     * Test of getProtocolName method, of class ASAP2Object.
     */
    @Test
    public void testGetProtocolName() {
        System.out.println("getProtocolName");
        ASAP2Object instance = new ASAP2Object();
        String expResult = "XCP";
        String result = instance.getProtocolName();
        assertEquals(expResult, result);
    }


    /**
     * Test of getBitString method, of class ASAP2Object.
     */
    @Test
    public void testGetBitString() {
        System.out.println("getBitString");
        double value = 123.4;
        ASAP2Object instance = new ASAP2Object();
        instance.setDatatype(ASAP2Datatype.FLOAT64_IEEE);
        String expResult = " 0100 0000 0101 1110 1101 1001 1001 1001 1001 1001 1001 1001 1001 1001 1001 1010";
        String result = instance.getBitString(value);
        assertEquals(expResult, result);
    }


    /**
     * Test of getHexString method, of class ASAP2Object.
     */
    @Test
    public void testGetHexString() {
        System.out.println("getHexString");
        double value = 123.4;
        ASAP2Object instance = new ASAP2Object();
        instance.setDatatype(ASAP2Datatype.FLOAT64_IEEE);
        String expResult = " 40 5e d9 99 99 99 99 9a";
        String result = instance.getHexString(value);
        assertEquals(expResult, result);
    }


    /**
     * Test of toString method, of class ASAP2Object.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        ASAP2Object instance = new ASAP2Object();
        String expResult = "AsapTest";
        instance.setName(expResult);
        String result = instance.toString();
        assertEquals(expResult, result);
    }


}
