/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package util;

import java.awt.Component;
import java.awt.event.KeyEvent;
import javax.swing.JPanel;

import util.BitKeys;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mike
 */
public class BitKeysTest {
    BitKeys bitKeys;
    private Component comp = new JPanel();

    private KeyEvent keyEvent(int keyCode, int id) {
        return new KeyEvent(comp, id, -1L, 0, keyCode, ' ');
    }

    public BitKeysTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        bitKeys = new BitKeys();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void checkIfVariableIsSetWithKeyCodeOfTheShiftKey() {
        BitKeys.setKeyPressed(KeyEvent.VK_SHIFT);
        assertEquals(true, BitKeys.isKeyPressed(KeyEvent.VK_SHIFT));
    }

    @Test
    public void checkIfVariableIsCleared() {
        BitKeys.setKeyPressed(KeyEvent.VK_SHIFT);
        BitKeys.clearKeyPressed(KeyEvent.VK_SHIFT);
        assertEquals(false, BitKeys.isKeyPressed(KeyEvent.VK_SHIFT));
    }

    @Test
    public void checkIfVariableIsSetWhenKeyIsPressed() {
        bitKeys.keyPressed(keyEvent(KeyEvent.VK_SHIFT, KeyEvent.KEY_PRESSED));
        assertEquals(true, BitKeys.isKeyPressed(KeyEvent.VK_SHIFT));
    }

    @Test
    public void checkIfVariableIsClearedWhenKeyIsReleased() {
        bitKeys.keyPressed(keyEvent(KeyEvent.VK_SHIFT, KeyEvent.KEY_PRESSED));
        bitKeys.keyReleased(keyEvent(KeyEvent.VK_SHIFT, KeyEvent.KEY_RELEASED));
        assertEquals(false, BitKeys.isKeyPressed(KeyEvent.VK_SHIFT));
    }
}
