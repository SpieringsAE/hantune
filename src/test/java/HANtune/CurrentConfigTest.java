/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import HANtune.CustomFileChooser;
import datahandling.CurrentConfig;
import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mike
 */
public class CurrentConfigTest {
    public CurrentConfig currentConfig;
    public CurrentConfigTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        currentConfig = CurrentConfig.getInstance();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSetLogFile_PathDefaultDirectory() {
        currentConfig.setLogFile_Path(System.getProperty("user.dir") + "\\log\\");
        String defaultDirectory = currentConfig.getLogFile_Path();
        assertEquals(defaultDirectory, System.getProperty("user.dir") + "\\log\\");
    }

    @Test
    public void testSetPreviousPath_PathDefaultLogFileDirectory() {
        File file = new File("lastLogPath", System.getProperty("user.dir"));
        currentConfig.setPreviousPath(CustomFileChooser.FileType.LOG_FILE, file);
        File setDirectory = currentConfig.getPreviousPath(CustomFileChooser.FileType.LOG_FILE);
        assertEquals(setDirectory, file);
    }

    @Test
    public void testSingletonInstanceCurrentConfigIsSame() {
        CurrentConfig singletoneTest = CurrentConfig.getInstance();
        assertSame(singletoneTest, currentConfig);
    }
}
