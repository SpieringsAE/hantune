/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import HANtune.DocumentFile;
import HANtune.HANtuneDocumentFileImpl;
import HANtune.HANtuneManager;
import HANtune.HANtuneProject;
import org.junit.Test;

import datahandling.CurrentConfig;
import haNtuneHML.HANtuneDocument;


public class HANtuneProjectTest {

    @Test
    public void isHANtuneDocumentModified_ShouldBeFalse() {

        // Prepare objects for testing.
        // Use a mock for CurrentConfig
        CurrentConfig configMock = mock(CurrentConfig.class);
        File currentConfigProjectFile = new File("a:/b/c/testproj.hml");
        when(configMock.getProjectFile()).thenReturn(currentConfigProjectFile);

        // Prepare lastSaved document
        DocumentFile<HANtuneDocument> hmlDoc = new HANtuneDocumentFileImpl();
        HANtuneDocument testHtDoc =
            hmlDoc.loadProjectData(new File("../src/test/resources/HANtune/TestProjectFile.hml"));
        HANtuneManager mockMngr = mock(HANtuneManager.class);
        when(configMock.getHANtuneManager()).thenReturn(mockMngr);
        when((configMock.getHANtuneManager()).loadProjectDataFromHmlData(currentConfigProjectFile))
            .thenReturn(testHtDoc);

        // Prepare copy current document
        HANtuneDocument mockDoc = mock(HANtuneDocument.class);
        when(configMock.getHANtuneDocument()).thenReturn(mockDoc);
        HANtuneDocument mockDocCopy = mock(HANtuneDocument.class);
        when(configMock.getHANtuneDocument().copy()).thenReturn(mockDocCopy);

        try {
            // String TestProjectFileString_NotModified has been modified at some places, but should NOT
            // yield a modified result.
            String testCurrentCopyString = new String(Files
                .readAllBytes(Paths.get("../src/test/resources/HANtune/TestProjectFileString_NotModified.txt")));
            when(configMock.getHANtuneDocument().copy().toString()).thenReturn(testCurrentCopyString);
        } catch (IOException e) {
            e.printStackTrace();
        }

        HANtuneProject testProj = HANtuneProject.getInstance();
        boolean isModified = testProj.testIsDocumentModified(configMock);

        System.out.println("isHANtuneDocumentModified_ShouldBeFalse(): " + isModified);
        assertFalse(isModified);
    }


    @Test
    public void isHANtuneDocumentModified_ShouldBeTrue() {

        // Prepare objects for testing.
        // Use a mock for CurrentConfig
        CurrentConfig configMock = mock(CurrentConfig.class);
        File currentConfigProjectFile = new File("a:/b/c/testproj.hml");
        when(configMock.getProjectFile()).thenReturn(currentConfigProjectFile);

        DocumentFile<HANtuneDocument> hmlDoc = new HANtuneDocumentFileImpl();

        // Prepare lastSaved document
        HANtuneDocument testHtDoc =
            hmlDoc.loadProjectData(new File("../src/test/resources/HANtune/TestProjectFile.hml"));
        HANtuneManager mockMngr = mock(HANtuneManager.class);
        when(configMock.getHANtuneManager()).thenReturn(mockMngr);
        when((configMock.getHANtuneManager()).loadProjectDataFromHmlData(currentConfigProjectFile))
            .thenReturn(testHtDoc);

        // Prepare copy current document
        HANtuneDocument mockDoc = mock(HANtuneDocument.class);
        when(configMock.getHANtuneDocument()).thenReturn(mockDoc);

        HANtuneDocument mockDocCopy = mock(HANtuneDocument.class);
        when(configMock.getHANtuneDocument().copy()).thenReturn(mockDocCopy);

        try {
            // String TestProjectFileString_Modified has been modified at 1 place, and should therefore
            // yield a modified result. (difference line 55: "ButtonEditor"x="756" -> "656")
            String testCurrentCopyString = new String(
                Files.readAllBytes(Paths.get("../src/test/resources/HANtune/TestProjectFileString_Modified.txt")));
            when(configMock.getHANtuneDocument().copy().toString()).thenReturn(testCurrentCopyString);
        } catch (IOException e) {
            e.printStackTrace();
        }

        HANtuneProject testProj = HANtuneProject.getInstance();
        boolean isModified = testProj.testIsDocumentModified(configMock);

        System.out.println("isHANtuneDocumentModified_ShouldBeTrue(): " + isModified);
        assertTrue(isModified);
    }

}
