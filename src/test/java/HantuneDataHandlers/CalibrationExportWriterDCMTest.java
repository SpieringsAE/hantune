/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HantuneDataHandlers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import ASAP2.ASAP2Characteristic;
import ASAP2.ASAP2Data;
import ASAP2.ASAP2Datatype;
import ASAP2.Asap2Parameter;
import datahandling.CalibrationExportWriter;
import datahandling.CalibrationExportWriterDCM;
import datahandling.CalibrationHandler;
import datahandling.CurrentConfig;
import haNtuneHML.Calibration;
import haNtuneHML.Calibration.ASAP2;
import haNtuneHML.HANtuneDocument;
import util.Util;

/**
 *
 * @author Michel
 */
public class CalibrationExportWriterDCMTest {

    public CalibrationHandler calibrationHandler;
    private CurrentConfig config;


    public CalibrationExportWriterDCMTest() {
    }

    public class MyClassTest {

        @Before
        public void setup() {
        }

    }


    @Before
    public void setUp() {
        calibrationHandler = CalibrationHandler.getInstance();
        config = CurrentConfig.getInstance();
    }


    @Test
    public void testSingletonInstanceCalibrationHandlerIsSame() {
        CalibrationHandler singletoneTest = CalibrationHandler.getInstance();
        assertSame(singletoneTest, calibrationHandler);
    }


    @Test
    public void testHandleExportCalibration() throws IOException {
        HANtuneDocument hantuneDocument = HANtuneDocument.Factory.newInstance();
        hantuneDocument.addNewHANtune();
        hantuneDocument.getHANtune().addNewCalibration();
        Calibration calib = hantuneDocument.getHANtune().getCalibrationArray(0);
        config.setASAP2Data(new ASAP2Data());

        // 13 Test VALUEs
        addCalibrationItemToTest(calib, config, "DcmTest1", ASAP2Datatype.FLOAT64_IEEE, 1234.5678);
        addCalibrationItemToTest(calib, config, "DcmTest2", ASAP2Datatype.FLOAT32_IEEE, 12.56789f);
        addCalibrationItemToTest(calib, config, "DcmTest3", ASAP2Datatype.BOOLEAN, 1);
        addCalibrationItemToTest(calib, config, "DcmTest4", ASAP2Datatype.UBYTE, 210);
        addCalibrationItemToTest(calib, config, "DcmTest5", ASAP2Datatype.SBYTE, -33);
        addCalibrationItemToTest(calib, config, "DcmTest6", ASAP2Datatype.SWORD, -30000);
        addCalibrationItemToTest(calib, config, "DcmTest7", ASAP2Datatype.UWORD, 62255);
        addCalibrationItemToTest(calib, config, "DcmTest8", ASAP2Datatype.ULONG, 2123456789);
        addCalibrationItemToTest(calib, config, "DcmTest9", ASAP2Datatype.SLONG, -2023456789);
        addCalibrationItemToTest(calib, config, "DcmTest10", ASAP2Datatype.A_INT64, -9223372036854775807L);
        addCalibrationItemToTest(calib, config, "DcmTest11", ASAP2Datatype.A_INT64, 9223372036854775807L);
        addCalibrationItemToTest(calib, config, "DcmTest12", ASAP2Datatype.A_UINT64, 0L);
        addCalibrationItemToTest(calib, config, "DcmTest13", ASAP2Datatype.A_UINT64, 0xfffffffffffffffeL);

        // 8 Test VALUEs with conversion factor
        addCalibrationItemToTest(calib, config, "DcmTest21", ASAP2Datatype.FLOAT64_IEEE, 1234.56789, 1.0001);
        addCalibrationItemToTest(calib, config, "DcmTest22", ASAP2Datatype.FLOAT32_IEEE, 12.56789f, 1.0001);
        addCalibrationItemToTest(calib, config, "DcmTest23", ASAP2Datatype.UBYTE, 210, 1.0001);
        addCalibrationItemToTest(calib, config, "DcmTest24", ASAP2Datatype.SLONG, 123456, -1.0001);
        addCalibrationItemToTest(calib, config, "DcmTest25", ASAP2Datatype.UWORD, 62255, 256);
        addCalibrationItemToTest(calib, config, "DcmTest26", ASAP2Datatype.SLONG, -2023456789, 1.0001);
        addCalibrationItemToTest(calib, config, "DcmTest27", ASAP2Datatype.A_INT64, 28972036854775804L, 2);
        // testing this datatype with values more than 17 digits will give inaccurate results.
        // although a theoretical test value of max 9223372036854775804L should be possible.
        addCalibrationItemToTest(calib, config, "DcmTest28", ASAP2Datatype.A_INT64, 9223372036854775804L, 2);

        // 3 Test CURVEs with conversion factor
        Object fltArr[] = { 1.234f, 2.3456f };
        addCalibrationItemToTest(calib, config, "DcmTest31", ASAP2Datatype.FLOAT32_IEEE, fltArr, 1.0001,
            ASAP2Characteristic.Type.CURVE);
        Object dblArr[] = { 1.2345678, 2.345612345 };
        addCalibrationItemToTest(calib, config, "DcmTest32", ASAP2Datatype.FLOAT64_IEEE, dblArr, 1.0001,
            ASAP2Characteristic.Type.CURVE);
        Object longArr[] = { -12345678L, 2345612345678L, 0L };
        addCalibrationItemToTest(calib, config, "DcmTest33", ASAP2Datatype.A_INT64, longArr, 1.0001,
            ASAP2Characteristic.Type.CURVE);

        // 1 Test MAP on long with conversion factor. 3cols x 2rows
        Object longArrMatrix[] = { -12345678L, 0L, 1L, 2345612345678L, 0L, 2L };
        addCalibrationItemToTest(calib, config, "DcmTest41", ASAP2Datatype.A_INT64, longArrMatrix, 1.0001,
            ASAP2Characteristic.Type.MAP, 2);

        // 1 Test MAP on double with conversion factor. 3 x 3
        Object dblArrMatrix[] = { -12345678.456, 2345612345679999998.9, -0.0, 1.0, 1.001, 1.5E244, -1.001,
            -2E-34, -2.012345e-33 };
        addCalibrationItemToTest(calib, config, "DcmTest42", ASAP2Datatype.FLOAT64_IEEE, dblArrMatrix, 1.0001,
            ASAP2Characteristic.Type.MAP, 3);

        // 1 Test should NOT appear in output file
        // Add a characteristic with incorrect number of data items corresponding calibration data...
        byte[] data = { 1, 2 }; // test specifies SLONG data type which uses 4 bytes...
        addWrongCalibrationItemToTest(calib, config, "DcmTest51", ASAP2Characteristic.Type.VALUE, 1, 1,
            data);

//        // Add 1 test
//        // Add a non existing characteristic
//        // Should appear as error message in output file
//        byte[] dataNonExistingCharact = { 1, 2, 3, 4 };
//        addWrongCalibrationItemToTest(calib, config, "DcmTest52", ASAP2Characteristic.Type.FOLDER, 1, 1,
//            dataNonExistingCharact);

        // test exportWriter unit
        try {
            CalibrationExportWriter exportWriter = new CalibrationExportWriterDCM();
            File file = File.createTempFile("HAN", null);
            exportWriter.writeCalibration(calib, file);

            // check results, expect 27 - 1 = 26 results. 'DcmTest51' name should NOT appear.
            assertEquals(26, checkOutputFile(file)); // Finally, check number of values found in file

            file.delete(); // Delete file when finished. File will remain available when test fails
        } catch (IOException ex) {
            fail("Test failed, exception occurred");
        }
    }


    private void addCalibrationItemToTest(Calibration calib, CurrentConfig config, String naam,
        ASAP2Datatype dType, Object value) {

        addCalibrationItemToTest(calib, config, naam, dType, value, 1.0);
    }


    private void addCalibrationItemToTest(Calibration calib, CurrentConfig config, String name,
        ASAP2Datatype datType, Object value, double factor) {

        Object values[] = new Object[1];
        values[0] = value;
        addCalibrationItemToTest(calib, config, name, datType, values, factor,
            ASAP2Characteristic.Type.VALUE, 1);
    }


    private void addCalibrationItemToTest(Calibration calib, CurrentConfig config, String name,
        ASAP2Datatype datType, Object values[], double factor, ASAP2Characteristic.Type characterType) {

        addCalibrationItemToTest(calib, config, name, datType, values, factor, characterType, 1);
    }


    private void addCalibrationItemToTest(Calibration calib, CurrentConfig config, String naam,
        ASAP2Datatype datType, Object values[], double factor, ASAP2Characteristic.Type characterType,
        int numRows) {

        ASAP2Characteristic asapPar = new Asap2Parameter();
        asapPar.setName(naam);
        asapPar.setDatatype(datType);
        asapPar.setType(characterType);
        asapPar.setColumnCount(values.length / numRows);
        asapPar.setRowCount(numRows);
        asapPar.setFactor(factor);

        config.getASAP2Data().getASAP2Characteristics().put(naam, asapPar);

        ASAP2 asap2 = calib.addNewASAP2();
        asap2.setName(naam);

        ByteBuffer buf = ByteBuffer
            .allocate(asapPar.getDatatypeSize() * asapPar.getColumnCount() * asapPar.getRowCount());

        for (int i = 0; i < values.length; i++) {
            buf.put(Util.charA2byteA(asapPar.convertValue(values[i])));
        }
        asap2.setByteArrayValue(buf.array());
    }


    private void addWrongCalibrationItemToTest(Calibration calib, CurrentConfig config, String name,
        ASAP2Characteristic.Type characterType, int row, int col, byte data[]) {

        ASAP2Characteristic asapPar = new Asap2Parameter();
        asapPar.setName(name);
        asapPar.setDatatype(ASAP2Datatype.SLONG);
        asapPar.setType(characterType);
        asapPar.setRowCount(row);
        asapPar.setColumnCount(col);
        asapPar.setFactor(1.0);
        config.getASAP2Data().getASAP2Characteristics().put(name, asapPar);

        ASAP2 asap2 = calib.addNewASAP2();
        asap2.setName(name);

        asap2.setByteArrayValue(data);
    }


    private int checkOutputFile(File file) throws FileNotFoundException {
        int valueFound = 0;
        Scanner in;
        in = new Scanner(file);

        // Read each line until end of file is reached
        while (in.hasNextLine()) {
            String line = in.nextLine().trim(); // trim line to avoid nasty empty first string after split
            String words[] = line.split("\\s++"); // split on whitespace
            if (words.length == 2 && words[0].equals("FESTWERT")) {
                valueFound += checkParamValue("DcmTest1", in, words, "1234.5678") ? 1 : 0;
                valueFound += checkParamValue("DcmTest2", in, words, "12.56789") ? 1 : 0;
                valueFound += checkParamValue("DcmTest3", in, words, "1") ? 1 : 0;
                valueFound += checkParamValue("DcmTest4", in, words, "210") ? 1 : 0;
                valueFound += checkParamValue("DcmTest5", in, words, "-33") ? 1 : 0;
                valueFound += checkParamValue("DcmTest6", in, words, "-30000") ? 1 : 0;
                valueFound += checkParamValue("DcmTest7", in, words, "62255") ? 1 : 0;
                valueFound += checkParamValue("DcmTest8", in, words, "2123456789") ? 1 : 0;
                valueFound += checkParamValue("DcmTest9", in, words, "-2023456789") ? 1 : 0;
                valueFound += checkParamValue("DcmTest10", in, words, "-9223372036854775807") ? 1 : 0;
                valueFound += checkParamValue("DcmTest11", in, words, "9223372036854775807") ? 1 : 0;
                valueFound += checkParamValue("DcmTest12", in, words, "0") ? 1 : 0;
                valueFound += checkParamValue("DcmTest13", in, words, "18446744073709551614") ? 1 : 0;

                valueFound += checkParamValue("DcmTest21", in, words, "1234.56789") ? 1 : 0;
                valueFound += checkParamValue("DcmTest22", in, words, "12.56789") ? 1 : 0;
                valueFound += checkParamValue("DcmTest23", in, words, "209") ? 1 : 0;
                valueFound += checkParamValue("DcmTest24", in, words, "123455") ? 1 : 0;
                valueFound += checkParamValue("DcmTest25", in, words, "62208") ? 1 : 0;
                valueFound += checkParamValue("DcmTest26", in, words, "-2023456788") ? 1 : 0;
                valueFound += checkParamValue("DcmTest27", in, words, "28972036854775804") ? 1 : 0;

                // result is NOT accurate.
                valueFound += checkParamValue("DcmTest28", in, words, "9223372036854775807") ? 1 : 0;

                // should NOT be found!!
                checkNotFound("DcmTest51", in, words); // do NOT increment valueFound if not found.

            } else if (words.length == 3 && words[0].equals("FESTWERTEBLOCK")) {
                valueFound +=
                    checkArrayValues("DcmTest31", in, words, new String[] { "1.234", "2.3456001" }) ? 1
                        : 0;

                valueFound += checkArrayValues("DcmTest32", in, words,
                    new String[] { "1.2345678", "2.345612345" }) ? 1 : 0;

                valueFound += checkArrayValues("DcmTest33", in, words,
                    new String[] { "-12345677", "2345612345677", "0" }) ? 1 : 0;

            } else if (words.length == 5 && words[0].equals("FESTWERTEBLOCK") && words[3].equals("@")) {
                String expectString[] = { "-12345677",  "0", "0", "0", "2345612345677", "1" };
                valueFound += checkMatrixValues("DcmTest41", in, words, expectString, 2) ? 1 : 0;

                expectString = new String[] {   "-1.2345678456E7",   "1.0",     "-1.001",
                                                "2.34561234568E18",  "1.001",   "-2.0E-34",
                                                "-0.0",              "1.5E244", "-2.012345E-33" };
                valueFound += checkMatrixValues("DcmTest42", in, words, expectString, 3) ? 1 : 0;


//            } else if (words.length == 10 && words[0].equals("****")) {
//                if (words[1].equals("DcmTest52:") && words[9].equals("FOLDER"))
//                    valueFound += 1;
            }
        }
        in.close();

        return valueFound;
    }


    private boolean checkParamValue(String valueName, Scanner in, String[] words, String expectString) {
        if (words[1].equals(valueName)) {
            String line2 = in.nextLine().trim();
            String words2[] = line2.split("\\s++");
            if (words2[0].equals("WERT")) {
                assertEquals(expectString, words2[1]);
                return true;
            }
        }
        return false;
    }


    private boolean checkArrayValues(String valueName, Scanner in, String[] words, String expectString[]) {
        if (words[1].equals(valueName)) {
            assertEquals(words[2], Integer.toString(expectString.length));
            String line2 = in.nextLine().trim();
            String words2[] = line2.split("\\s++");
            if (words2[0].equals("WERT")) {
                for (int iCnt = 1; iCnt < words2.length; iCnt++) {
                    assertEquals(expectString[iCnt - 1], words2[iCnt]);
                }
                return true;
            }
        }
        return false;
    }


    private boolean checkMatrixValues(String valueName, Scanner in, String[] words, String expectString[],
        int numRows) {
        if (words[1].equals(valueName)) {
            int numRowsOk = 0;
            int numCols = expectString.length / numRows;
            assertEquals(words[2], Integer.toString(numCols));
            assertEquals(words[4], Integer.toString(numRows));

            for (int iRow = 0; iRow < numRows; iRow++) {
                String line2 = in.nextLine().trim();
                String words2[] = line2.split("\\s++");

                if (words2[0].equals("WERT")) {
                    for (int iCnt = 1; iCnt < words2.length; iCnt++) {
                        assertEquals(expectString[(iRow * numCols) + iCnt - 1], words2[iCnt]);
                    }
                    numRowsOk++;
                }
            }
            assertEquals(numRowsOk, numRows);
            return true;
        }
        return false;
    }


    private boolean checkNotFound(String valueName, Scanner in, String[] words) {
        // if value name appears in file then assertion error. Should NOT appear in file
        assertFalse(words[1].equals(valueName));

        return true;
    }



}
