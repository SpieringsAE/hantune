/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HantuneDataHandlers;

import datahandling.SendParameterDataHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.Timer;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Michel
 */
public class SendParameterDataHandlerTest {

    SendParameterDataHandler sendDataHandler;

    public SendParameterDataHandlerTest() {
    }

    @Before
    public void setUp() {
        sendDataHandler = new SendParameterDataHandler(100);
    }

    @Test
    public void testCreateDataQueueTimerIfTimerIsCreated() {
        Timer timer = sendDataHandler.getDataQueueTimer();
        assertNotNull(timer);
    }

    @Test
    public void testCreateDataQueueTimerTimerDelay() {
        Timer timer = sendDataHandler.getDataQueueTimer();
        assertEquals(100 , timer.getDelay());
    }

    @Test
    public void handleSendDataQueueDataAdded() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method handleSendDataQueue = SendParameterDataHandler.class.getDeclaredMethod("handleSendDataQueue",String.class,Object.class,int[].class);
        handleSendDataQueue.setAccessible(true);

        int[] position = {0,1};
        handleSendDataQueue.invoke(sendDataHandler, "engineSpeed", 10 , position);
        Object[] ExpectedArray = {"engineSpeed", 10, position};
        ArrayList<Object[]> sendDataQueue = sendDataHandler.getSendDataQueue();

        assertTrue(Arrays.equals(sendDataQueue.get(0), ExpectedArray));
    }

    @Test
    public void handleSendDataQueueDataValueReplaced() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method handleSendDataQueue = SendParameterDataHandler.class.getDeclaredMethod("handleSendDataQueue",String.class,Object.class,int[].class);
        handleSendDataQueue.setAccessible(true);

        int[] position = {0,1};
        handleSendDataQueue.invoke(sendDataHandler, "engineSpeed", 10 , position);
        handleSendDataQueue.invoke(sendDataHandler, "engineSpeed", 12 , position);
        Object[] ExpectedArray = {"engineSpeed", 12, position};
        ArrayList<Object[]> sendDataQueue = sendDataHandler.getSendDataQueue();

        assertTrue(Arrays.equals(sendDataQueue.get(0), ExpectedArray));
    }

    @Test
    public void handleSendDataQueueDataValueAddedDifferentPosition() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method handleSendDataQueue = SendParameterDataHandler.class.getDeclaredMethod("handleSendDataQueue",String.class,Object.class,int[].class);
        handleSendDataQueue.setAccessible(true);

        int[] position = {0,1};
        int[] positionTwo = {0,2};
        handleSendDataQueue.invoke(sendDataHandler, "engineSpeed", 10 , position);
        handleSendDataQueue.invoke(sendDataHandler, "engineSpeed", 10 , positionTwo);

        Object[] ExpectedArray = {"engineSpeed", 10, position};
        Object[] ExpectedArrayPositionTwo = {"engineSpeed", 10, positionTwo};
        ArrayList<Object[]> sendDataQueue = sendDataHandler.getSendDataQueue();

        assertTrue(Arrays.equals(sendDataQueue.get(0), ExpectedArray));
        assertTrue(Arrays.equals(sendDataQueue.get(1), ExpectedArrayPositionTwo));
    }

    @Test
    public void handleSendDataQueueDataValueAddedDifferentAsap2Ref() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method handleSendDataQueue = SendParameterDataHandler.class.getDeclaredMethod("handleSendDataQueue",String.class,Object.class,int[].class);
        handleSendDataQueue.setAccessible(true);

        int[] position = {0,1};
        handleSendDataQueue.invoke(sendDataHandler, "engineSpeed", 10 , position);
        handleSendDataQueue.invoke(sendDataHandler, "engineTemp", 10 , position);

        Object[] ExpectedArray = {"engineSpeed", 10, position};
        Object[] ExpectedArrayDifferentAsap2Ref = {"engineTemp", 10, position};
        ArrayList<Object[]> sendDataQueue = sendDataHandler.getSendDataQueue();

        assertTrue(Arrays.equals(sendDataQueue.get(0), ExpectedArray));
        assertTrue(Arrays.equals(sendDataQueue.get(1), ExpectedArrayDifferentAsap2Ref));
    }

    @Test
    public void removeAsap2refFromSendDataQueueTest() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method handleSendDataQueue = SendParameterDataHandler.class.getDeclaredMethod("handleSendDataQueue",String.class,Object.class,int[].class);
        handleSendDataQueue.setAccessible(true);
        Method removeAsap2refFromSendDataQueue = SendParameterDataHandler.class.getDeclaredMethod("removeAsap2refFromSendDataQueue",String.class,Object.class,int[].class);
        removeAsap2refFromSendDataQueue.setAccessible(true);

        int[] position = {0,1};
        handleSendDataQueue.invoke(sendDataHandler, "engineSpeed", 10 , position);
        Object[] ExpectedArray = {"engineSpeed", 10, position};
        ArrayList<Object[]> sendDataQueue = sendDataHandler.getSendDataQueue();
        assertTrue(Arrays.equals(sendDataQueue.get(0), ExpectedArray));

        removeAsap2refFromSendDataQueue.invoke(sendDataHandler, "engineSpeed", 10 , position);
        sendDataQueue = sendDataHandler.getSendDataQueue();
        assertEquals(0, sendDataQueue.size());
    }

    @Test
    public void removeAsap2refFromSendDataQueueWrongParameter() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method handleSendDataQueue = SendParameterDataHandler.class.getDeclaredMethod("handleSendDataQueue",String.class,Object.class,int[].class);
        handleSendDataQueue.setAccessible(true);
        Method removeAsap2refFromSendDataQueue = SendParameterDataHandler.class.getDeclaredMethod("removeAsap2refFromSendDataQueue",String.class,Object.class,int[].class);
        removeAsap2refFromSendDataQueue.setAccessible(true);

        int[] position = {0,1};
        handleSendDataQueue.invoke(sendDataHandler, "engineSpeed", 10 , position);
        Object[] ExpectedArray = {"engineSpeed", 10, position};
        ArrayList<Object[]> sendDataQueue = sendDataHandler.getSendDataQueue();
        assertTrue(Arrays.equals(sendDataQueue.get(0), ExpectedArray));

        removeAsap2refFromSendDataQueue.invoke(sendDataHandler, "engineSpeedWrong", 10 , position);
        sendDataQueue = sendDataHandler.getSendDataQueue();
        assertTrue(Arrays.equals(sendDataQueue.get(0), ExpectedArray));
    }

    @Test
    public void removeAsap2refFromSendDataQueueValueChanged() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method handleSendDataQueue = SendParameterDataHandler.class.getDeclaredMethod("handleSendDataQueue",String.class,Object.class,int[].class);
        handleSendDataQueue.setAccessible(true);
        Method removeAsap2refFromSendDataQueue = SendParameterDataHandler.class.getDeclaredMethod("removeAsap2refFromSendDataQueue",String.class,Object.class,int[].class);
        removeAsap2refFromSendDataQueue.setAccessible(true);

        int[] position = {0,1};
        handleSendDataQueue.invoke(sendDataHandler, "engineSpeed", 10 , position);
        handleSendDataQueue.invoke(sendDataHandler, "engineSpeed", 20 , position);

        ArrayList<Object[]> sendDataQueue = sendDataHandler.getSendDataQueue();
        Object[] ExpectedArray = {"engineSpeed", 20, position};

        removeAsap2refFromSendDataQueue.invoke(sendDataHandler, "engineSpeedWrong", 10 , position);
        sendDataQueue = sendDataHandler.getSendDataQueue();
        assertTrue(Arrays.equals(sendDataQueue.get(0), ExpectedArray));
    }

    @Test
    public void sendValueToEcuNoEcuConnected() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method sendValueToEcu = SendParameterDataHandler.class.getDeclaredMethod("sendValueToEcu",String.class,Object.class,int[].class);
        sendValueToEcu.setAccessible(true);

        int[] position = {0,1};
        boolean Expected = (boolean)sendValueToEcu.invoke(sendDataHandler, "engineSpeed", 10 , position);

        assertFalse(Expected);
    }

    //TODO: Improve test. Test fails with outOfBoundsException
    @Test
    @Ignore
    public void compareObjarrParametersEqualParamters() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method objArrayHasAsap2RefAndPosition = SendParameterDataHandler.class.getDeclaredMethod("objArrayHasAsap2RefAndPosition",Object[].class,String.class,int[].class);
        objArrayHasAsap2RefAndPosition.setAccessible(true);

        int[] position = {0,1};
        Object[] compareArray = {"engineSpeed", position};
        boolean Expected = (boolean)objArrayHasAsap2RefAndPosition.invoke(sendDataHandler,compareArray, "engineSpeed", position);

        assertTrue(Expected);
    }

    @Test
    public void compareObjarrParametersDifferentAsap2Ref() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method objArrayHasAsap2RefAndPosition = SendParameterDataHandler.class.getDeclaredMethod("objArrayHasAsap2RefAndPosition",Object[].class,String.class,int[].class);
        objArrayHasAsap2RefAndPosition.setAccessible(true);

        int[] position = {0,1};
        Object[] compareArray = {"engineSpeed", position};
        boolean Expected = (boolean)objArrayHasAsap2RefAndPosition.invoke(sendDataHandler,compareArray, "engineSpeedDifferentAsap2Ref", position);

        assertFalse(Expected);
    }

    //TODO: Improve test. Test fails with outOfBoundsException
    @Test
    @Ignore
    public void compareObjarrParametersDifferentPosition() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method objArrayHasAsap2RefAndPosition = SendParameterDataHandler.class.getDeclaredMethod("objArrayHasAsap2RefAndPosition",Object[].class,String.class,int[].class);
        objArrayHasAsap2RefAndPosition.setAccessible(true);

        int[] position = {0,1};
        int[] differentPosition = {0,2};
        Object[] compareArray = {"engineSpeed", position};
        boolean Expected = (boolean)objArrayHasAsap2RefAndPosition.invoke(sendDataHandler,compareArray, "engineSpeed", differentPosition);

        assertFalse(Expected);
    }

    @Test
    public void compareObjarrParametersDifferentPositionAndAsap2Ref() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method objArrayHasAsap2RefAndPosition = SendParameterDataHandler.class.getDeclaredMethod("objArrayHasAsap2RefAndPosition",Object[].class,String.class,int[].class);
        objArrayHasAsap2RefAndPosition.setAccessible(true);

        int[] position = {0,1};
        int[] differentPosition = {0,2};
        Object[] compareArray = {"engineSpeed", position};
        boolean Expected = (boolean)objArrayHasAsap2RefAndPosition.invoke(sendDataHandler,compareArray, "engineSpeedDiffernetAsap2Ref", differentPosition);

        assertFalse(Expected);
    }

    //TODO: Improve test. Test fails with outOfBoundsException
    @Test
    @Ignore
    public void compareObjarrParametersWithPositionNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method objArrayHasAsap2RefAndPosition = SendParameterDataHandler.class.getDeclaredMethod("objArrayHasAsap2RefAndPosition",Object[].class,String.class,int[].class);
        objArrayHasAsap2RefAndPosition.setAccessible(true);

        int[] position = null;
        Object[] compareArray = {"engineSpeed", position};
        boolean Expected = (boolean)objArrayHasAsap2RefAndPosition.invoke(sendDataHandler,compareArray, "engineSpeed",null);

        assertTrue(Expected);
    }

    // Todo: test fails. Should be improved
    @Test
    @Ignore
    public void compareObjarrParametersWithPositionNullNotEqual() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method objArrayHasAsap2RefAndPosition = SendParameterDataHandler.class.getDeclaredMethod("objArrayHasAsap2RefAndPosition",Object[].class,String.class,int[].class);
        objArrayHasAsap2RefAndPosition.setAccessible(true);

        int[] position = {1,2};
        Object[] compareArray = {"engineSpeed", position};
        boolean Expected = (boolean)objArrayHasAsap2RefAndPosition.invoke(sendDataHandler,compareArray, "engineSpeed",null);

        assertFalse(Expected);
    }

    @Test
    public void compareObjarrParametersWithValue() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method objectArrayHasValue = SendParameterDataHandler.class.getDeclaredMethod("objectArrayHasValue",Object[].class,Object.class);
        objectArrayHasValue.setAccessible(true);

        int[] position = {1,2};
        Object[] compareArray = {"engineSpeed",10, position};
        boolean Expected = (boolean)objectArrayHasValue.invoke(sendDataHandler,compareArray, 10);

        assertTrue(Expected);
    }

    @Test
    public void compareObjarrParametersWithValueDifferentValues() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Method objectArrayHasValue = SendParameterDataHandler.class.getDeclaredMethod("objectArrayHasValue",Object[].class,Object.class);
        objectArrayHasValue.setAccessible(true);

        int[] position = {1,2};
        Object[] compareArray = {"engineSpeed",12, position};
        boolean Expected = (boolean)objectArrayHasValue.invoke(sendDataHandler,compareArray, 10);

        assertFalse(Expected);
    }

}
