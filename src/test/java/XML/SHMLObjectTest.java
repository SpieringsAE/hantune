/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package XML;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.xmlbeans.XmlException;
import org.junit.Test;

import ASAP2.ASAP2Characteristic;
import ASAP2.ASAP2Data;
import ASAP2.ASAP2Import;
import ASAP2.ASAP2Measurement;
import ASAP2.ASAP2XmlImport;
import HANtune.DocumentFile;
import HANtune.HANtune;
import HANtune.HANtuneManager;
import HANtune.ServiceToolDocumentFileImpl;
import can.CanSignal;
import can.DbcObject;
import can.DbcParser;
import can.DbcXmlImport;
import datahandling.ASAP2ConvertToXmlImpl;
import datahandling.ConvertToXml;
import datahandling.CurrentConfig;
import datahandling.DbcConvertToXmlImpl;
import datahandling.ShmlHmlConverter;
import haNtuneHML.Daqlist;
import haNtuneHML.Layout;
import haNtuneHML.Layout.Tab;
import haNtuneHML.Layout.Tab.Window;
import haNtuneHML.Layout.Tab.Window.ASAP2;
import haNtuneSHML.ShmlDocument;
import haNtuneSHML.ShmlDocument.Shml;

public class SHMLObjectTest {


    public SHMLObjectTest() {
    }


    /**
     * Simple test to check if all HANtuneDocument items are correctly converted to SHML
     */
    @Test
    public void testOutputSHML() {
        long lTimeStart = System.currentTimeMillis();
        HANtuneManager manager = new HANtuneManager(HANtune.getInstance());
        CurrentConfig currentConfig = CurrentConfig.getInstance();
        currentConfig.setHANtuneManager(manager);

        File asap2File = new File("../src/test/resources/XML/Asap2Test.a2l");
        ASAP2Import ASAP2i = new ASAP2Import();
        ASAP2Data importASAP2 = ASAP2i.importASAP2(asap2File);

        long lTimeDiff = System.currentTimeMillis() - lTimeStart;
        System.out.println("Importing ASAP2 time: " + lTimeDiff + " mSec");
        lTimeStart = System.currentTimeMillis();

        File testHmlFile = new File("../src/test/resources/XML/TestHmlFile.xml");
        File dbcFile = new File("../src/test/resources/XML/TestDbcFile.dbc");

        try {
            haNtuneHML.HANtuneDocument htDoc;
            htDoc = haNtuneHML.HANtuneDocument.Factory.parse(testHmlFile);
            haNtuneHML.HANtuneDocument.HANtune htWrite = htDoc.getHANtune();

            DbcObject dbcObj = DbcParser.parseDBC(dbcFile);

            lTimeDiff = System.currentTimeMillis() - lTimeStart;
            System.out.println("parsing hml file: " + lTimeDiff + " mSec");
            lTimeStart = System.currentTimeMillis();

            ShmlDocument shmlDocWrite = ShmlHmlConverter.createShmlDocFromHANtuneDoc(htDoc);
            System.out.println("SHML Write version: " + shmlDocWrite.getShml().getSHMLVersion());

            // add ASAP2 data
            ConvertToXml<ASAP2Data, Shml, Layout> filter = new ASAP2ConvertToXmlImpl();
            filter.filterRelevantDataIntoXml(importASAP2, shmlDocWrite.getShml(),
                (Layout)shmlDocWrite.getShml().getLayout());
            // for (Asap2Measurement mm : shmlDoc.getShml().getASAP2Data().getMeasurementsList()) {
            // System.out
            // .println("ASAP2 measurement: " + mm + " " + mm.getDatatype() + " " + mm.getMinimum());
            // }
            // for (Asap2Characteristic ch : shmlDoc.getShml().getASAP2Data().getCharacteristicsList()) {
            // System.out
            // .println("ASAP2 characteristic: " + ch.getType() + " " + ch.getName());
            // }

            // add Dbc data
            ConvertToXml<DbcObject, Shml, Layout> filterDbc = new DbcConvertToXmlImpl();
            filterDbc.filterRelevantDataIntoXml(dbcObj, shmlDocWrite.getShml(),
                (Layout)shmlDocWrite.getShml().getLayout());

            // export file
            File file = File.createTempFile("HAN_SHML", null);
            DocumentFile<ShmlDocument> shmlDoc = new ServiceToolDocumentFileImpl();
            shmlDoc.saveProjectData(file, shmlDocWrite);
            /////////// end of first part. Temp file has been created.

            //////////////////////////////////////////////////////////
            // now read the same file again, load data in a new object
            ShmlDocument shmlDocRead = shmlDoc.loadProjectData(file);


            // validate xml and read possible errors
            boolean isValid = shmlDoc.isValidDocument();
            assertTrue(shmlDoc.getError(), isValid);


            Shml shmlRead = shmlDocRead.getShml();
            System.out.println("SHML Read Version: " + shmlRead.getSHMLVersion());

            haNtuneHML.HANtuneDocument.HANtune htRead =
                ShmlHmlConverter.createHANtuneDocFromShmlDoc(shmlDocRead).getHANtune();

            asap2XmlData.Asap2XmlData asap2XmlRead = shmlRead.getASAP2Data();
            ASAP2XmlImport asap2DataFromXml = new ASAP2XmlImport(asap2XmlRead);
            ASAP2Data asap2DataRead = asap2DataFromXml.convertXmlAsapToASAP2();

            dbcXmlData.DbcXmlData dbcXmlRead = shmlRead.getDbcDataArray(0);
            DbcObject dbcObjRead = DbcXmlImport.importXmlDbc(dbcXmlRead);

            lTimeDiff = System.currentTimeMillis() - lTimeStart;
            System.out.println("Test time: " + lTimeDiff + " mSec");


            /////////// Verify the value is passed on via the temp file

            // Check if an incorrect value will be detected by inserting an extra value
            // htWrite.getDaqlistArray(0).insertNewSignal(3);

            for (int iCnt = 0; iCnt < htRead.sizeOfDaqlistArray(); iCnt++) {
                Daqlist dlRead = htRead.getDaqlistArray(iCnt);
                System.out.println("DaqlistRead: " + dlRead.getTitle() + " " + dlRead.getDate());

                for (int iSgCnt = 0; iSgCnt < dlRead.sizeOfSignalArray(); iSgCnt++) {
                    System.out.println("SignalRead: " + dlRead.getSignalArray(iSgCnt).getName() + " "
                        + dlRead.getSignalArray(iSgCnt).getAddress());

                    assertEquals(htWrite.getDaqlistArray(iCnt).getSignalArray(iSgCnt).getName(),
                        dlRead.getSignalArray(iSgCnt).getName());
                    assertEquals(htWrite.getDaqlistArray(iCnt).getSignalArray(iSgCnt).getAddress(),
                        dlRead.getSignalArray(iSgCnt).getAddress());
                }
            }

            // Check if an incorrect value will be detected by introducing an incorrect DataType
            // ASAP2Measurement mmTest = importASAP2.getASAP2Measurements().get("DTC_Lookup2Dout_Uint8");
            // mmTest.setDatatype(ASAP2Datatype.FLOAT64_IEEE);

            for (ASAP2Measurement mmRead : asap2DataRead.getASAP2Measurements().values()) {
                System.out.println("ASAP2Read measurement: " + mmRead + " " + mmRead.getDatatype() + " "
                    + mmRead.getMinimum());
                ASAP2Measurement mmWrite = importASAP2.getASAP2Measurements().get(mmRead.getName());
                assertEquals(mmWrite.getName(), mmRead.getName());
                assertEquals(mmWrite.getDatatype(), mmRead.getDatatype());
            }
            for (ASAP2Characteristic chRead : asap2DataRead.getASAP2Characteristics().values()) {
                System.out
                    .println("ASAP2Read characteristic: " + chRead.getType() + " " + chRead.getName());
                ASAP2Characteristic chWrite = importASAP2.getASAP2Characteristics().get(chRead.getName());
                assertEquals(chWrite.getName(), chRead.getName());
                assertEquals(chWrite.getDatatype(), chRead.getDatatype());
            }

            // check if dbc signalnames from layout can be found in dbcObjRead
            Map<String, CanSignal> signalsMapRead = dbcObjRead.getSignalsMap();
            for (Tab tElem : ((Layout)shmlRead.getLayout()).getTabList()) {
                for (Window wElem : tElem.getWindowList()) {
                    for (ASAP2 aElem : wElem.getASAP2List()) {
                        // System.out.println("aElem name: " + aElem.getName() + " Dbcname: " +
                        // dbcRead.getName());
                        System.out.println("Layout sourcename: " + aElem.getSource());
                        if (aElem.getSource().equals(dbcObjRead.getName())) {
                            System.out.println("Layout signalName: " + aElem.getName());
                            assertTrue("Name: " + aElem.getName() + " not found in signalsMapRead",
                                signalsMapRead.containsKey(aElem.getName()));
                        }
                    }
                }
            }


        } catch (XmlException | IOException e1) {
            e1.printStackTrace();
            fail("Test failed, exception occurred");
        }

    }


}
